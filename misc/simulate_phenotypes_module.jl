#################################
###							  ###
###   Simulate_phenotypes	  ###
###   or load pre-existing	  ###
### genotype & phenotype data ###
###							  ###
################################# same module name and filename

module simulate_phenotypes_module

#####################
###				  ###
### load packages ###
###				  ###
#####################
using Distributions

#####################
###				  ###
### main function ###
###				  ###
#####################
function simulate_phenotype(X, n_QTL, MAF, heritability)
	### Inputs:
	### X 		= matrix of genotype data
	### n_QTL 	= number of QTL to simulate
	### MAF 	= minimum allele frequency of QTL
	### heritability

	#TESTS:
	# filename_X = "res/At_134Salk_SNP.rds"
	# n_QTL = 10
	# MAF = 0.1
	# heritability = 0.5

	### Determine dimensions of the genotype data
	n = size(X)[1]
	l = size(X)[2]

	### Sample from QTL effects from some distribution
	chisq_distn = Distributions.Chisq(5)
	QTL_effects = rand(chisq_distn, n_QTL)

	### Filter candidate alleles for QTL-ship
	allele_freq = zeros(l)
	[ allele_freq[i] = sum(X[:,i]) / n for i in 1:l ]
	allele_freq_idx_filtered = []
	for i in 1:l
		if allele_freq[i] > MAF && allele_freq[i] < (1-MAF)
			append!(allele_freq_idx_filtered, i)
		end
	end

	### Sample QTL positions from the filtered list above
	QTL_pos = sample(allele_freq_idx_filtered, n_QTL, replace=false)
	QTL_freq = allele_freq[QTL_pos[:]]

	### Associate QTL effects with their corresponding positions
	b = zeros(l)
	[ b[QTL_pos[i]]= QTL_effects[i] for i in 1:n_QTL ]

	### Sample residual effects based on heritability
	Vg = var(X * b)
	Vε = (Vg / heritability) - Vg
	normal_distn = Distributions.Normal(0, sqrt(Vε))
	ε = rand(normal_distn, n)

	### Calculate simulated phenotypes
	y = (X * b) + ε

	### Return QTL positions, QTL frequencies, QTL effects and the phenotypes
	return [QTL_pos, QTL_freq, QTL_effects, y]
end

end #end of simulate_phenotypes_module