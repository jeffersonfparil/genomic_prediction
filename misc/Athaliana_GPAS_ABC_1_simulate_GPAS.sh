#!/bin/bash

####################################################################
### SIMULATE QTL, PHENOTYPES, AND PERFORM GPAS:
###   (1) Across population samples with Indi-seq
###   (2) Across population samples with Pool-seq
###   (3) Within population samples with Indi-seq
###   (4) Within population samples with Pool-seq
####################################################################

### INPUTS
nQTL=$1               ### number of QTL to simulate
REP=$2                ### replication ID number
DIR=$3                ### directory where the k2029 data are located (i.e. root folder of the Athaliana GPAS ABC validations/simulations)
GEN_PRED_SRC_DIR=$4   ### directory where the genomic_prediction.git source codes are located
# nQTL=100
# REP=1
# DIR=/data/cephfs/punim0543/jparil/arabidopsis_GPAS_ABC
# GEN_PRED_SRC_DIR=/data/cephfs/punim0543/jparil/Softwares/genomic_prediction/src
### EXECUTE
echo -e "##################"
echo -e "Prepare directory"
echo -e "##################"
cd ${DIR}
mkdir /tmp/OUTPUT-REP${REP}-QTL${nQTL}
cp ROI_CLUSTER_??.csv /tmp/OUTPUT-REP${REP}-QTL${nQTL}
cp ROI_ALL.bim /tmp/OUTPUT-REP${REP}-QTL${nQTL}
cd /tmp/OUTPUT-REP${REP}-QTL${nQTL}
echo -e "##################"
echo -e "Simulate QTL"
echo -e "##################"
echo -e 'args = commandArgs(trailingOnly=TRUE)
  nQTL = as.numeric(args[1])
  bim = read.table("ROI_ALL.bim")
  l = nrow(bim)
  idx_QTL = sample(1:l, size=nQTL)
  eff_QTL = rchisq(n=nQTL, df=1)
  # OUT = data.frame(IDX=idx_QTL, CHROM=bim$V1[idx_QTL], POS=bim$V4[idx_QTL], ALLELE=bim$V5[idx_QTL], EFF=eff_QTL)
  OUT = data.frame(IDX=idx_QTL, CHROM=bim$V1[idx_QTL], POS=bim$V4[idx_QTL], ALLELE=rep("A", times=length(idx_QTL)), EFF=eff_QTL)
  write.table(OUT, file="ROI_QTL_INFO_for_JULIA.csv", sep=",", row.names=FALSE, col.names=TRUE, quote=FALSE)
' > simulate_QTL.r
Rscript simulate_QTL.r $nQTL
echo "CHROM,POS,ALLELE,EFFECT" > ROI_QTL_INFO.csv
cut -d, -f2- ROI_QTL_INFO_for_JULIA.csv | tail -n+2 >> ROI_QTL_INFO.csv
### OUTPUTS:
### (1) ROI_QTL_INFO_for_JULIA.csv (IDX, CHROM, POS, ALLELE, EFF)
### (2) ROI_QTL_INFO.csv (CHROM, POS, ALLELE, EFF)
echo -e "##################"
echo -e "Simulate Phenotypes"
echo -e "##################"
echo $'using DelimitedFiles
  using DataFrames
  using CSV
  using Statistics
  using Distributions
  fname_csv = ARGS[1]
  QTL_csv = ARGS[2]
  h2 = parse(Float64, ARGS[3])
  # fname_csv="ROI_CLUSTER_01.csv"; QTL_csv="ROI_QTL_INFO_for_JULIA.csv"; h2=0.5
  X = DelimitedFiles.readdlm(fname_csv, \',\')
  QTL = CSV.read(QTL_csv)
  b = zeros(size(X,2),1)
  b[QTL.IDX] = QTL.EFF
  y_initial = (X * b)[:,1]
  Vg = Statistics.var(y_initial)
  Ve = (Vg/h2)-Vg
  y_untr = y_initial + Statistics.rand(Distributions.Normal(0,sqrt(Ve)), length(y_initial))
  y = (y_untr .- minimum(y_untr)) ./ (maximum(y_untr)-minimum(y_untr))
  OUT = DataFrames.DataFrame(ID=1:size(X,1), y_untr=y_untr, y=y)
  fname_out = string(split(fname_csv, ".csv")[1], "_PHENOTYPES.csv")
  CSV.write(fname_out, OUT)
' > simulate_phenotypes.jl
echo -e '#!/bin/bash
  i=$1
  # i=ROI_CLUSTER_01.csv
  echo $i
  julia simulate_phenotypes.jl $i ROI_QTL_INFO_for_JULIA.csv 0.50
' > simulate_phenotypes.sh
chmod +x simulate_phenotypes.sh
# parallel -j5 ./simulate_phenotypes.sh {} ::: $(ls ROI_CLUSTER_??.csv)
time parallel ./simulate_phenotypes.sh {} ::: $(ls ROI_CLUSTER_??.csv)
### OUTPUTS:
### (1-14) ROI_CLUSTER_${i}_PHENOTYPES.csv (ID, y_untr, y; for i in for i in 01 02 03 ... 14)
echo -e "##################"
echo -e "Pooling per population"
echo -e "##################"
echo $'using DelimitedFiles
  using Statistics
  using StatsBase
  using ProgressMeter
  fname_geno = ARGS[1]
  fname_pheno = ARGS[2]
  npools = parse(Int64, ARGS[3])
  fname_loci = ARGS[4]
  # fname_geno="ROI_CLUSTER_01.csv";fname_pheno="ROI_CLUSTER_01_PHENOTYPES.csv"; npools=5; fname_loci="ROI_ALL.bim"
  # fname_geno="ROI_CLUSTER_04.csv";fname_pheno="ROI_CLUSTER_04_PHENOTYPES.csv"; npools=5; fname_loci="ROI_ALL.bim"
  GENO = DelimitedFiles.readdlm(fname_geno, \',\', Int8)
  PHENO = DelimitedFiles.readdlm(fname_pheno, \',\', Float64, skipstart=1)
  LOCI_SPEC = DelimitedFiles.readdlm(fname_loci)
  n = size(GENO,1)
  l = size(GENO,2)
  pool_size = Int(round(n/npools))
  if (n != size(PHENO,1))
      println("Oh no! The number of individuals do match between the genotype and phenotype data!")
      println("Exiting!")
      exit()
  end
  if (n < 100) & (npools != 1)
    exit() ### exiting because we will be setting the number of pools to 1 for whole population Pool-seq
  end
  ### group the individuals into npools groups, each equally sized: pool_size
  PERCENTILES = append!([0.0], Statistics.quantile(PHENO[:,3], [1.0/npools] .* collect(1:npools)))
  PHENO_POOLS = Array{Float64}(undef, npools, 1)
  GENO_POOLS = Array{Float64}(undef, npools, l)
  IDX_POOLING = Array{Int64}(undef, pool_size, npools)
  for i in 1:npools
      test = (PHENO[:,3] .>= PERCENTILES[i]) .& (PHENO[:,3] .<= PERCENTILES[i+1])
      idx = StatsBase.sample(collect(1:n)[test], pool_size, replace=true) #simulate random sampling during leaf sampling, DNA extraction, library preparation, and sequencing
      PHENO_POOLS[i] = Statistics.mean(PHENO[idx,3])
      GENO_POOLS[i,:] = Statistics.mean(GENO[idx,:], dims=1)
      IDX_POOLING[:, i] = idx #the index of individuals per pool
  end
  ### simulate a synchronized pileup file (*.sync) using these pool datasets
  DEPTH = 100 #just a fixed arbitrary simulted sequencing or read depth
  chr = convert(Array{Int64,1}, LOCI_SPEC[:,1])
  pos = convert(Array{Int64,1}, LOCI_SPEC[:,4])
  ref = convert(Array{String,1}, LOCI_SPEC[:,5])
  alt = convert(Array{String,1}, LOCI_SPEC[:,6])
  #ref = []; alleles = ["A", "T", "C", "G", "DEL"]
  sync = Array{String}(undef, l, npools)
  pb = ProgressMeter.Progress(l, dt=1, barlen=50, color=:yellow)
  for i in 1:l
      allele1 = Int.(round.(GENO_POOLS[:,i] .* DEPTH))
      allele2 = Int.(round.((1 .- GENO_POOLS[:,i]) .* DEPTH))
      for j in 1:npools
          sync[i, j] = string(allele1[j], ":", allele2[j], ":0:0:0:0") ### for ease just putting the alleles at A and T positions (real ref alleles are still encoded in the 3 column of the resulting sync file)
      end
      ProgressMeter.update!(pb, i)
  end
  SYNC = hcat(chr, pos, ref, sync)
  if (npools > 1)
    fname_out = string(split(fname_geno, ".csv")[1], ".sync")
    fname_out_py = string(split(fname_geno, ".csv")[1], "_POOLPHEN.py")
    fname_out_csv = string(split(fname_geno, ".csv")[1], "_POOLPHEN.csv")
  else
    fname_out = string(split(fname_geno, ".csv")[1], "_ONEPOOL.sync")
    fname_out_py = string(split(fname_geno, ".csv")[1], "_ONEPOOL_POOLPHEN.py")
    fname_out_csv = string(split(fname_geno, ".csv")[1], "_ONEPOOL_POOLPHEN.csv")
  end
  DelimitedFiles.writedlm(fname_out, SYNC)
  ### output pooled phenotype files in py format
  line1 = "Pheno_name=\'pheno\';"
  line2 = string("sig=", Statistics.std(PHENO[:,3]), ";")
  line3 = string("MIN=", minimum(PHENO[:,3]), ";")
  line4 = string("MAX=", maximum(PHENO[:,3]), ";")
  line5 = string("perc=[", join(cumsum(repeat([1/npools],(npools-1))), ","), "];")
  line6 = string("q=[", join(PERCENTILES[2:end-1], ","), "];")
  PY_PHEN = vcat(line1, line2, line3, line4, line5, line6)
  DelimitedFiles.writedlm(fname_out_py, PY_PHEN)
  ### output pooled phenotype files in csv format
  y_pool = zeros(npools)
  for i in 1:npools
    y_pool[i] = mean(PHENO[IDX_POOLING[:,i], 3])
  end
  CSV_PHEN = hcat(repeat([pool_size], npools), convert(Array{Any,1}, y_pool))
  DelimitedFiles.writedlm(fname_out_csv, CSV_PHEN, \',\')
' > pooling.jl
echo -e '#!/bin/bash
  GENO=$1
  PHENO=$2
  echo $GENO
  ### 5 pools per pop
  julia pooling.jl ${GENO} ${PHENO} 5 ROI_ALL.bim
  ### 1 pool per pop
  julia pooling.jl ${GENO} ${PHENO} 1 ROI_ALL.bim
' > pooling.sh
chmod +x pooling.sh
# parallel -j5 --link ./pooling.sh {1} {2} ::: $(ls ROI_CLUSTER_??.csv) ::: $(ls ROI_CLUSTER_??_PHENOTYPES.csv)
time parallel --link ./pooling.sh {1} {2} ::: $(ls ROI_CLUSTER_??.csv) ::: $(ls ROI_CLUSTER_??_PHENOTYPES.csv)
### OUTPUTS:
### (*) ROI_CLUSTER_${i}.sync (for i in for i in 01 02 03 ... 14)
### (*) ROI_CLUSTER_${i}_POOLPHEN.py (for i in for i in 01 02 03 ... 14)
### (*) ROI_CLUSTER_${i}_POOLPHEN.csv (POOLSIZES, mean_y; for i in for i in 01 02 03 ... 14)
### (1-14) ROI_CLUSTER_${i}_ONEPOOL.sync (for i in for i in 01 02 03 ... 14)
### (1-14) ROI_CLUSTER_${i}_ONEPOOL_POOLPHEN.py (for i in for i in 01 02 03 ... 14)
### (1-14) ROI_CLUSTER_${i}_ONEPOOL_POOLPHEN.csv (POOLSIZES, mean_y; for i in for i in 01 02 03 ... 14)
### where (*) is for the populations with at least 100 individuals
echo -e "##################"
echo -e "Across population samplings"
echo -e "##################"
ls ROI_CLUSTER_??.csv > ROI_CLUSTER_POPULATIONS_LIST.txt
echo $'using DelimitedFiles
  using StatsBase
  using Statistics
  ### load list of filenames of genotypes and phenotypes per cluster
  fname_list = ARGS[1]
  n_per_sample = parse(Int64, ARGS[2])  ### number of populations per across population sample
  TOTAL_nlib = parse(Int64, ARGS[3])
  # fname_list = "ROI_CLUSTER_POPULATIONS_LIST.txt"
  # n_per_sample = 4 ### number of populations per across population sample
  # TOTAL_nlib = 500
  ### counts
  GENO_FNAMES_LIST = convert(Array{String,1}, DelimitedFiles.readdlm(fname_list)[:,1])
  PHENO_FNAMES_LIST = string.(hcat(split.(GENO_FNAMES_LIST, ".csv")...)[1,:], "_PHENOTYPES.csv")
  GENO_FNAMES_LIST_ONEPOOL = string.(hcat(split.(GENO_FNAMES_LIST, ".csv")...)[1,:], "_ONEPOOL.sync")
  PHENO_FNAMES_LIST_ONEPOOL_CSV = string.(hcat(split.(GENO_FNAMES_LIST, ".csv")...)[1,:], "_ONEPOOL_POOLPHEN.csv")
  TOTAL_npop = length(GENO_FNAMES_LIST)
  n_samples = Int(ceil(TOTAL_npop / n_per_sample)) ### total number of populations samples
  l_per_sample = Int(ceil(TOTAL_nlib / n_per_sample)) ### maximum number of individual samples per population
  ### randomize and generate the grouping index for each across population samplings
  rand_pop_list = StatsBase.sample(collect(1:TOTAL_npop), TOTAL_npop, replace=false)
  grouping = hcat(repeat(collect(1:n_samples), inner=n_per_sample)[1:TOTAL_npop], rand_pop_list) ### GROUPING_ID, POPULATION_ID
  ### build pooled Indi-seq and Pool-seq files and their corresponding phenotype files
  ### Indi-seq: build each genotype csv file and the corresponding csv phenotype file
  function Indiseq_grouping(idx, GENO_FNAMES_LIST, PHENO_FNAMES_LIST, l_per_sample)
    sort!(idx)
    cluster_numbers = string.(idx)
    cluster_numbers[idx .< 10] = string.("0", cluster_numbers[idx .< 10])
    fname_out_geno = string("ROI_CLUSTERS_", join(cluster_numbers, "-"), ".csv")
    fname_out_pheno = string("ROI_CLUSTERS_", join(cluster_numbers, "-"), "_PHENOTYPES.csv")
    if isfile(fname_out_geno)
      rm(fname_out_geno)
    end
    if isfile(fname_out_pheno)
      rm(fname_out_pheno)
    end
    io_GENO = open(fname_out_geno, "a")
    io_PHENO = open(fname_out_pheno, "a"); DelimitedFiles.writedlm(io_PHENO, ["ID,y_untr,y"])
    for j in idx
      DelimitedFiles.writedlm(io_GENO, readlines(GENO_FNAMES_LIST[j])[1:minimum([l_per_sample,end]),:])
      DelimitedFiles.writedlm(io_PHENO, readlines(PHENO_FNAMES_LIST[j])[2:minimum([l_per_sample+1,end]),:])
    end
    close(io_GENO)
    close(io_PHENO)
    return(0)
  end
  ### Pool-seq: build each sync file and the corresponding csv and py phenotype files
  function Poolseq_grouping(idx, GENO_FNAMES_LIST_ONEPOOL, PHENO_FNAMES_LIST_ONEPOOL_CSV)
    ### copied from pooling.jl (we could therefore make a self-standing module but mehhh...)
    sort!(idx)
    cluster_numbers = string.(idx)
    cluster_numbers[idx .< 10] = string.("0", cluster_numbers[idx .< 10])
    npools = length(idx)
    cluster_numbers = string.(idx)
    cluster_numbers[idx .< 10] = string.("0", cluster_numbers[idx .< 10])
    fname_out_geno = string("ROI_CLUSTERS_", join(cluster_numbers, "-"), ".sync")
    fname_out_pheno_py = string("ROI_CLUSTERS_", join(cluster_numbers, "-"), "_POOLPHEN.py")
    fname_out_pheno_csv = string("ROI_CLUSTERS_", join(cluster_numbers, "-"), "_POOLPHEN.csv")

    SYNC = []
    PHENO = []
    for i in idx
      if (i == idx[1])
        push!(SYNC, DelimitedFiles.readdlm(GENO_FNAMES_LIST_ONEPOOL[i]))
      else
        push!(SYNC, DelimitedFiles.readdlm(GENO_FNAMES_LIST_ONEPOOL[i])[:,end])
      end
      push!(PHENO, DelimitedFiles.readdlm(PHENO_FNAMES_LIST_ONEPOOL_CSV[i], \',\'))
    end
    PHENO_OUT = vcat(PHENO...)
    SYNC_OUT = hcat(hcat(SYNC...)[:,1:3], hcat(SYNC...)[:, sortperm(PHENO_OUT[:,end]) .+ 3])
    DelimitedFiles.writedlm(fname_out_geno, hcat(SYNC...))
    DelimitedFiles.writedlm(fname_out_pheno_csv, vcat(PHENO...), \',\')
    ### output pooled phenotype files in py format
    line1 = "Pheno_name=\'pheno\';"
    line2 = string("sig=", Statistics.std(PHENO_OUT[:,end]), ";")
    line3 = string("MIN=", minimum(PHENO_OUT[:,end]), ";")
    line4 = string("MAX=", maximum(PHENO_OUT[:,end]), ";")
    line5 = string("perc=[", join(cumsum(PHENO_OUT[:,1] ./ sum(PHENO_OUT[:,1]))[1:size(PHENO_OUT,1)-1], ","), "];")
    line6 = string("q=[", join(PHENO_OUT[1:end-1,end], ","), "];")
    PY_PHEN = vcat(line1, line2, line3, line4, line5, line6)
    DelimitedFiles.writedlm(fname_out_pheno_py, PY_PHEN)
    return(0)
  end
  ### Iterate across samplings
  for i in 1:n_samples
    # i = 2
    idx = grouping[grouping[:,1] .== i, 2]
    ### Indi-seq grouping
    Indiseq_grouping(idx, GENO_FNAMES_LIST, PHENO_FNAMES_LIST, l_per_sample)
    ### Pool-seq grouping
    Poolseq_grouping(idx, GENO_FNAMES_LIST_ONEPOOL, PHENO_FNAMES_LIST_ONEPOOL_CSV)
  end
' > grouping.jl
echo -e '#!/bin/bash
  julia grouping.jl ROI_CLUSTER_POPULATIONS_LIST.txt $1 500
' > grouping.sh
chmod +x grouping.sh
NPOP_LIST=$(seq 2 7) ### list of the number for across population groupings (samplings)
NITER=5
time parallel ./grouping.sh {1} {2} ::: $NPOP_LIST ::: $(seq 1 $NITER)
### OUTPUTS:
### (len(NPOP_LIST)*NITER) ROI_CLUSTERS-{join(*), "-"}.csv
### (len(NPOP_LIST)*NITER) ROI_CLUSTERS-{join(*), "-"}_PHENOTYPES.csv
### (len(NPOP_LIST)*NITER) ROI_CLUSTERS-{join(*), "-"}.sync ### 1 population : 1 pool
### (len(NPOP_LIST)*NITER) ROI_CLUSTERS-{join(*), "-"}_POOLPHEN.py
### (len(NPOP_LIST)*NITER) ROI_CLUSTERS-{join(*), "-"}_POOLPHEN.csv
echo -e "##################"
echo -e "Calculate covariates"
echo -e "##################"
ls ROI_CLUSTER_??.csv > ROI_Within_Indiseq_fnames.txt
ls ROI_CLUSTER_??.sync > ROI_Within_Poolseq_fnames_sync.txt
ls ROI_CLUSTER_??_*POOLPHEN.csv | grep -v "ONEPOOL" > ROI_Within_Poolseq_fnames_csv.txt
ls -S ROI_CLUSTERS_*.csv | grep -v "POOLPHEN" | grep -v "PHENOTYPES" | grep -v "COVARIATE" > ROI_Across_Indiseq_fnames.txt
ls ROI_CLUSTERS_*.sync > ROI_Across_Poolseq_fnames_sync.txt
ls ROI_CLUSTERS_*_POOLPHEN.csv > ROI_Across_Poolseq_fnames_csv.txt
### Indi-seq (PC1 [and Kinship matrix--> too slow!])
echo $'using Statistics
  using StatsBase
  using LinearAlgebra
  using DelimitedFiles
  using ProgressMeter
  ### input
  fname = ARGS[1]
  MAF = parse(Float64, ARGS[2])
  # fname="ROI_CLUSTER_01.csv"; MAF=0.01
  ### load
  OUT = DelimitedFiles.readdlm(fname, \',\', Int8)
  n, l = size(OUT)
  ### principal components
  LOC = collect(1:l)[ ((mean(OUT, dims=1) ./ 2) .> MAF)[:] .& ((mean(OUT, dims=1) ./ 2) .< (1.0-MAF))[:] ]
  X = OUT[:, LOC]
  X_std = (X .- mean(X, dims=1)) ./ std(X, dims=1)
  X_SVD = LinearAlgebra.svd(X)
  PC1 = X_SVD.U * LinearAlgebra.Diagonal(X_SVD.S)[:,1]
  fname_out_PC1 = string(split(fname, ".csv")[1], "_COVARIATE_PC.csv")
  DelimitedFiles.writedlm(fname_out_PC1, PC1, ",")
  # ### kinship matrix (equation 1 of Goudet, Kay & Weir, 2018)
  # M = zeros(n, n)
  # pb = ProgressMeter.Progress(n, 1, "Calculating Kinship...", 50)
  # for i in 1:n
  #   for j in 1:n
  #     M[i,j] = sum( (1 .+ ( (X[i,:] .- 1) .* (X[j,:] .- 1) )) ./ 2 )
  #   end
  #   ProgressMeter.update!(pb, i)
  # end
  # M_nodiag = copy(M)
  # M_nodiag[diagind(M_nodiag)] .= 0
  # Ms = sum(M_nodiag) / (n*(n-1))
  # K = (M .- Ms) ./ (1 .- Ms)
  # ### output
  # fname_out_K = string(split(fname, ".csv")[1], "_COVARIATE_K.csv")
  # DelimitedFiles.writedlm(fname_out_K, K, ",")
' > covar_indiseq.jl
echo -e '#!/bin/bash
  fname=$1
  MAF=$(echo "scale=10; 1/" $(cat ${fname%.csv*}_PHENOTYPES.csv | wc -l) | bc)
  echo $fname
  julia covar_indiseq.jl ${fname} ${MAF}
' > covar_indiseq.sh
chmod +x covar_indiseq.sh
# time parallel -j3 ./covar_indiseq.sh {1} ::: $(cat ROI_Within_Indiseq_fnames.txt)
time parallel ./covar_indiseq.sh {1} ::: $(cat ROI_Within_Indiseq_fnames.txt)
# time parallel -j2 ./covar_indiseq.sh {1} ::: $(cat ROI_Across_Indiseq_fnames.txt)
time parallel -j5 ./covar_indiseq.sh {1} ::: $(cat ROI_Across_Indiseq_fnames.txt)
### Pool-seq (Fst matrix)
echo $'using Pkg
  using DelimitedFiles
  using GWAlpha
  fname_sync = ARGS[1]
  fname_csv = ARGS[2]
  window_size = parse(Int64, ARGS[3])
  pool_sizes = convert(Array{Int64,1}, DelimitedFiles.readdlm(fname_csv, \',\')[:,1])
  GWAlpha.poolFST_module.Fst_pairwise(sync_fname=fname_sync, window_size=window_size, pool_sizes=pool_sizes, METHOD="Hivert")
' > covar_poolseq.jl
echo -e '#!/bin/bash
  julia covar_poolseq.jl $1 $2 $3
' > covar_poolseq.sh
chmod +x covar_poolseq.sh
window_size=1000000 #1Mb
# time parallel --link ./covar_poolseq.sh {1} {2}  ${window_size} ::: $(cat ROI_Within_Poolseq_fnames_sync.txt) ::: $(cat ROI_Within_Poolseq_fnames_csv.txt)
# time parallel --link ./covar_poolseq.sh {1} {2}  ${window_size} ::: $(cat ROI_Across_Poolseq_fnames_sync.txt) ::: $(cat ROI_Across_Poolseq_fnames_csv.txt)
time parallel --link ./covar_poolseq.sh {1} {2}  ${window_size} ::: $(cat ROI_Within_Poolseq_fnames_sync.txt ROI_Across_Poolseq_fnames_sync.txt) ::: $(cat ROI_Within_Poolseq_fnames_csv.txt ROI_Across_Poolseq_fnames_csv.txt)
### OUTPUTS:
### (1) Indi-seq: *_COVARIATE_PC.csv (and *_COVARIATE_K.csv --> too slow)
### (1) Pool-seq: *_COVARIATE_FST.csv
echo -e "##################"
echo -e "Prepare list of filename inputs"
echo -e "AND"
echo -e "Tanspose and add chrom-pos-allele columns into Indi-seq data"
echo -e "##################"
PREP_LIST(){
  ### Prepare chrom_pos_allele_columns.temp
  cut -f1 ROI_ALL.bim > chrom.temp
  cut -f4 ROI_ALL.bim > pos.temp
  cut -f5 ROI_ALL.bim > allele.temp
  paste -d, chrom.temp pos.temp allele.temp > chrom_pos_allele_columns.temp
  ### Indi-seq genotype csv data transposition
  echo -e '#!/bin/bash
    f=$1
    echo $f
    cat $f | datamash transpose -t, > ${f}_transposed
    paste -d, chrom_pos_allele_columns.temp ${f}_transposed > ${f%.*}_GENOTYPES.csv
  ' > transpose_indiseq.sh
  chmod +x transpose_indiseq.sh
  parallel ./transpose_indiseq.sh {} ::: $(ls -S ROI_CLUSTER_??.csv | grep -v "GENOTYPES")
  ###### OUTPUTS: ROI_CLUSTER_??_GENOTYPES.csv
  parallel -j7 ./transpose_indiseq.sh {} ::: $(ls -S ROI_CLUSTERS_*.csv | grep -v "GENOTYPES" | grep -v "POOLPHEN" | grep -v "PHENOTYPES" | grep -v "COVARIATE")
  ###### OUTPUTS: ls -S ROI_CLUSTERS_*_GENOTYPES.csv | grep -v "POOLPHEN" | grep -v "PHENOTYPES" | grep -v "COVARIATE"
  ### Within populations Indi-seq
  ls ROI_CLUSTER_??_GENOTYPES.csv | sort -g > ROI_Within_Indiseq_fnames_geno.txt
  ls ROI_CLUSTER_??_PHENOTYPES.csv | sort -g > ROI_Within_Indiseq_fnames_pheno.txt
  ls ROI_CLUSTER_??_COVARIATE_PC.csv | sort -g > ROI_Within_Indiseq_fnames_covar.txt
  printf 'NONE\n%.0s' $(seq 1 $(cat ROI_Within_Indiseq_fnames_geno.txt | wc -l)) > ROI_Within_Indiseq_fnames_NULL.txt
  ### Within populations Pool-seq
  ls ROI_CLUSTER_??.sync | sort -g > ROI_Within_Poolseq_fnames_geno.txt
  ls ROI_CLUSTER_??_POOLPHEN.py | sort -g > ROI_Within_Poolseq_fnames_pheno_py.txt
  ls ROI_CLUSTER_??_POOLPHEN.csv | sort -g > ROI_Within_Poolseq_fnames_pheno_csv.txt
  ls ROI_CLUSTER_??_COVARIATE_FST.csv | sort -g > ROI_Within_Poolseq_fnames_covar.txt
  printf 'NONE\n%.0s' $(seq 1 $(cat ROI_Within_Poolseq_fnames_geno.txt | wc -l)) > ROI_Within_Poolseq_fnames_NULL.txt
  printf '_ALLELEFREQ.csv\n%.0s' $(seq 1 $(cat ROI_Within_Poolseq_fnames_geno.txt | wc -l)) > addcol_ALLELEFREQ.temp
  cut -d. -f1 ROI_Within_Poolseq_fnames_geno.txt > addcol_ALLELEFREQ2.temp
  paste addcol_ALLELEFREQ2.temp addcol_ALLELEFREQ.temp | sed 's/\t//g'  > ROI_Within_Poolseq_fnames_geno_csv.txt
  ### Across populations Indi-seq
  ls -S ROI_CLUSTERS_*_GENOTYPES.csv | grep -v "POOLPHEN" | grep -v "PHENOTYPES" | grep -v "COVARIATE" | grep -v "ALLELEFREQ" | sort -g > ROI_Across_Indiseq_fnames_geno.txt
  ls -S ROI_CLUSTERS_*.csv | grep -v "POOLPHEN" | grep "PHENOTYPES" | grep -v "COVARIATE" | sort -g > ROI_Across_Indiseq_fnames_pheno.txt
  ls -S ROI_CLUSTERS_*.csv | grep -v "POOLPHEN" | grep -v "PHENOTYPES" | grep "COVARIATE_PC" | sort -g > ROI_Across_Indiseq_fnames_covar.txt
  printf 'NONE\n%.0s' $(seq 1 $(cat ROI_Across_Indiseq_fnames_geno.txt | wc -l)) > ROI_Across_Indiseq_fnames_NULL.txt
  ### Across populations Pool-seq
  ls ROI_CLUSTERS_*.sync | sort -g > ROI_Across_Poolseq_fnames_geno.txt
  ls ROI_CLUSTERS_*_POOLPHEN.py | sort -g > ROI_Across_Poolseq_fnames_pheno_py.txt
  ls ROI_CLUSTERS_*_POOLPHEN.csv | sort -g > ROI_Across_Poolseq_fnames_pheno_csv.txt
  ls ROI_CLUSTERS_*_COVARIATE_FST.csv | sort -g > ROI_Across_Poolseq_fnames_covar.txt
  printf 'NONE\n%.0s' $(seq 1 $(cat ROI_Across_Poolseq_fnames_geno.txt | wc -l)) > ROI_Across_Poolseq_fnames_NULL.txt
  printf '_ALLELEFREQ.csv\n%.0s' $(seq 1 $(cat ROI_Across_Poolseq_fnames_geno.txt | wc -l)) > addcol_ALLELEFREQ.temp
  cut -d. -f1 ROI_Across_Poolseq_fnames_geno.txt > addcol_ALLELEFREQ2.temp
  paste addcol_ALLELEFREQ2.temp addcol_ALLELEFREQ.temp | sed 's/\t//g'  > ROI_Across_Poolseq_fnames_geno_csv.txt
  ################
  ### Indi-seq ###
  ################
  ###### Within
  cut -d_ -f3 ROI_Within_Indiseq_fnames_geno.txt | cut -d. -f1 > clustnames.temp
  printf 'CLUST\n%.0s' $(seq 1 $(cat clustnames.temp | wc -l)) > prefix_clustnames.temp
  paste -d- prefix_clustnames.temp clustnames.temp > POP_ID_Within_INDI.temp
  echo "POP,GENO,PHENO,COVARIATE_PC,COVARIATE_K" > ROI_Within_Indiseq_INPUT.csv
  paste -d, POP_ID_Within_INDI.temp ROI_Within_Indiseq_fnames_geno.txt ROI_Within_Indiseq_fnames_pheno.txt ROI_Within_Indiseq_fnames_covar.txt ROI_Within_Indiseq_fnames_NULL.txt \
    >> ROI_Within_Indiseq_INPUT.csv
  ###### Across
  cut -d_ -f3 ROI_Across_Indiseq_fnames_geno.txt > clustnames.temp
  printf 'CLUST\n%.0s' $(seq 1 $(cat clustnames.temp | wc -l)) > prefix_clustnames.temp
  paste -d- prefix_clustnames.temp clustnames.temp > POP_ID_Across_INDI.temp
  echo "POP,GENO,PHENO,COVARIATE_PC,COVARIATE_K" > ROI_Across_Indiseq_INPUT.csv
  paste -d, POP_ID_Across_INDI.temp ROI_Across_Indiseq_fnames_geno.txt ROI_Across_Indiseq_fnames_pheno.txt ROI_Across_Indiseq_fnames_covar.txt ROI_Across_Indiseq_fnames_NULL.txt \
    >> ROI_Across_Indiseq_INPUT.csv
  ###### Cleanup
  rm *.temp
  ################
  ### Pool-seq ###
  ################
  ###### Within
  cut -d_ -f3 ROI_Within_Poolseq_fnames_geno.txt | cut -d. -f1 > clustnames.temp
  printf 'CLUST\n%.0s' $(seq 1 $(cat clustnames.temp | wc -l)) > prefix_clustnames.temp
  paste -d- prefix_clustnames.temp clustnames.temp > POP_ID_Within_POOL.temp
  echo "POP,GENO_SYNC,GENO_CSV,PHENO_PY,PHENO_CSV,COVARIATE_NPSTAT,COVARIATE_FST" > ROI_Within_Poolseq_INPUT.csv
  paste -d, POP_ID_Within_POOL.temp ROI_Within_Poolseq_fnames_geno.txt ROI_Within_Poolseq_fnames_geno_csv.txt ROI_Within_Poolseq_fnames_pheno_py.txt ROI_Within_Poolseq_fnames_pheno_csv.txt ROI_Within_Poolseq_fnames_NULL.txt ROI_Within_Poolseq_fnames_covar.txt \
    >> ROI_Within_Poolseq_INPUT.csv
  ###### Across
  cut -d_ -f3 ROI_Across_Poolseq_fnames_geno.txt | cut -d. -f1 > clustnames.temp
  printf 'CLUST\n%.0s' $(seq 1 $(cat clustnames.temp | wc -l)) > prefix_clustnames.temp
  paste -d- prefix_clustnames.temp clustnames.temp > POP_ID_Across_POOL.temp
  echo "POP,GENO_SYNC,GENO_CSV,PHENO_PY,PHENO_CSV,COVARIATE_NPSTAT,COVARIATE_FST" > ROI_Across_Poolseq_INPUT.csv
  paste -d, POP_ID_Across_POOL.temp ROI_Across_Poolseq_fnames_geno.txt ROI_Across_Poolseq_fnames_geno_csv.txt ROI_Across_Poolseq_fnames_pheno_py.txt ROI_Across_Poolseq_fnames_pheno_csv.txt ROI_Across_Poolseq_fnames_NULL.txt ROI_Across_Poolseq_fnames_covar.txt \
    >> ROI_Across_Poolseq_INPUT.csv
  ###### Cleanup
  rm *.temp
  # ### sanity check
  # R
  # FNAMES = c("ROI_Within_Indiseq_INPUT.csv", "ROI_Across_Indiseq_INPUT.csv", "ROI_Within_Poolseq_INPUT.csv", "ROI_Across_Poolseq_INPUT.csv")
  # for (f in FNAMES){
  #   dat = read.csv(f)
  #   print(head(dat))
  #   print(tail(dat))
  #   str(dat)
  # }
  echo "Filenames of input data prepared!"
}
time PREP_LIST
### OUTPUTS:
### (1) ROI_Within_Indiseq_INPUT.csv
### (2) ROI_Across_Indiseq_INPUT.csv
### (3) ROI_Within_Poolseq_INPUT.csv
### (4) ROI_Across_Poolseq_INPUT.csv
echo -e "##################"
echo -e "GPAS Cross-validation"
echo -e "##################"

echo -e "#@@@@@@@@@@@@#"
echo -e "WITHIN-INDI"
echo -e "#@@@@@@@@@@@@#"
time \
julia ${GEN_PRED_SRC_DIR}/GPASim_04_GWAS_GP.jl \
  /temp/OUTPUT-REP${REP}-QTL${nQTL} \
  ROI_Within_Indiseq_INPUT.csv \
  INDI \
  ROI_QTL_INFO.csv \
  1 \
  Bonferroni \
  0.01
rm ROI_CLUSTER_??.csv ### clean-up a bit

echo -e "#@@@@@@@@@@@@#"
echo -e "WITHIN-POOL"
echo -e "#@@@@@@@@@@@@#"
time \
julia ${GEN_PRED_SRC_DIR}/GPASim_04_GWAS_GP.jl \
  /temp/OUTPUT-REP${REP}-QTL${nQTL} \
  ROI_Within_Poolseq_INPUT.csv \
  POOL \
  ROI_QTL_INFO.csv \
  1 \
  Bonferroni \
  0.01

echo -e "#@@@@@@@@@@@@#"
echo -e "ACROSS-INDI"
echo -e "#@@@@@@@@@@@@#"
time \
julia ${GEN_PRED_SRC_DIR}/GPASim_04_GWAS_GP.jl \
  /temp/OUTPUT-REP${REP}-QTL${nQTL} \
  ROI_Across_Indiseq_INPUT.csv \
  INDI \
  ROI_QTL_INFO.csv \
  1 \
  Bonferroni \
  0.01

echo -e "#@@@@@@@@@@@@#"
echo -e "ACROSS-POOL"
echo -e "#@@@@@@@@@@@@#"
time \
julia ${GEN_PRED_SRC_DIR}/GPASim_04_GWAS_GP.jl \
  /temp/OUTPUT-REP${REP}-QTL${nQTL} \
  ROI_Across_Poolseq_INPUT.csv \
  POOL \
  ROI_QTL_INFO.csv \
  1 \
  Bonferroni \
  0.01

### INPUTS:
### (1) directory where the julia scripts are located
### (2) csv file of lists of individual or pool data per population or across populations; HEADER: POP(population ID), GENO(full path + filename of the genotype *.csv file), PHENO(full path + filename of phenotype *.csv file)
###     HEADERS:
###     POP,GENO(nloci x (3+nind); where first 3 columns are chrom,pos,ref),PHENO,COVARIATE_PC,COVARIATE_K for: POP_FNAME_WITHIN_INDI_DF.csv and POP_FNAME_ACROSS_INDI_DF.csv
###     POP,GENO_SYNC,GENO_CSV,PHENO_PY,PHENO_CSV,COVARIATE_NPSTAT,COVARIATE_FST for: POP_FNAME_WITHIN_POOL_DF.csv or POP_FNAME_ACROSS_POOL_DF.csv
### (3) cross-validations to perform: {"WITHIN_INDI", "WITHIN_POOL", "ACROSS_INDI", "ACROSS_POOL"}
### (4) QTL identities (CHROM,POS,ALLELE,EFFECT)
### (5) size of linkage block in kilobases
### (6) p-value adjustment method: "Bonferroni", "BenjaminiHochberg" or "BenjaminiYekutieli"
### (7) significance level (decimal): prbability of incorrectly  rejecting the null hypothesis
### OUTPUTS:
### (1) CROSS_VALIDATION_OUTPUT_ROI_Within_Indiseq_INPUT.csv
### (2) CROSS_VALIDATION_OUTPUT_ROI_Across_Indiseq_INPUT.csv
### (3) CROSS_VALIDATION_OUTPUT_ROI_Within_Poolseq_INPUT.csv
### (4) CROSS_VALIDATION_OUTPUT_ROI_Across_Poolseq_INPUT.csv

echo -e "#@@@@@@@@@@@@#"
echo -e "CLEAN-UP"
echo -e "#@@@@@@@@@@@@#"
rm ROI_*
cd /tmp/
mv OUTPUT-REP${REP}-QTL${nQTL} ${DIR}
