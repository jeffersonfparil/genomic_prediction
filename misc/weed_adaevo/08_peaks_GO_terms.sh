#!/bin/bash

#############################################
###    Analysis of the Pool-GWAS output   ###
###       and GO term enrichment          ###
#############################################

### Inputs:
DIR_GWAS=$1     ### (1) directory of Pool-GWAS output
ANNOTATION=$2   ### (2) genome annotation file (*.gff3)
REFERENCE=$3    ### (3) reference genome
DIR_BIAS=$4     ### (4) output directory for the bias assessment
# DIR_GWAS=/data/Lolium/Quantitative_Genetics/06_GWAS
# ANNOTATION=/data/Lolium/Genomics/SEQUENCES/RNA/Lp_Annotation_V1.1.mp.gff3
# REFERENCE=/data/Lolium/Quantitative_Genetics/02_FASTQ/REFERENCE/Reference.fasta
# DIR_BIAS=/data/Lolium/Quantitative_Genetics/08_BIAS

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### Summary statistics
### Outputs:
### (1) ${herbi}_summary_counts.csv
### (2) ${herbi}_effects_size_distributions.svg
### (3) ${herbi}_common_counts.csv
echo '
  args = commandArgs(trailingOnly=TRUE)
  # args = c("/data/Lolium/Quantitative_Genetics/06_GWAS", "GLYPH", "0.01", "1")
  dirname = args[1]
  herbi = args[2]
  alpha = as.numeric(args[3])
  LD_kb = as.numeric(args[4])
  list_fnames = system(paste0("find ", dirname, "/*", herbi, "*-OUTPUT.csv | grep -v RANEF"), intern=TRUE)

  ### counts statistics per Pool-GPAS dataset
  VECTOR_total_n_alleles = c(); VECTOR_pass_n_alleles = c()
  VECTOR_total_n_loci =    c(); VECTOR_pass_n_loci =    c()
  VECTOR_total_n_scaff =   c(); VECTOR_pass_n_scaff =   c()
  for (f in list_fnames){
    # f = list_fnames[1]
    dat = read.csv(f)
    total_n_alleles = nrow(dat)
    total_n_loci = length(unique(paste0(dat$CHROM, "-", dat$POS)))
    total_n_scaff = length(unique(dat$CHROM))
    bonferroni_threshold = -log10(alpha / total_n_alleles)
    subdat = droplevels(dat[dat$LOD >= bonferroni_threshold, ])
    pass_n_alleles = nrow(subdat)
    pass_n_loci = length(unique(paste0(subdat$CHROM, "-", subdat$POS)))
    pass_n_scaff = length(unique(subdat$CHROM))
    ### extract counts statistics
    VECTOR_total_n_alleles = c(VECTOR_total_n_alleles, total_n_alleles)
    VECTOR_total_n_loci =    c(VECTOR_total_n_loci,    total_n_loci)
    VECTOR_total_n_scaff =   c(VECTOR_total_n_scaff,   total_n_scaff)
    VECTOR_pass_n_alleles =  c(VECTOR_pass_n_alleles,  pass_n_alleles)
    VECTOR_pass_n_loci =     c(VECTOR_pass_n_loci,     pass_n_loci)
    VECTOR_pass_n_scaff =    c(VECTOR_pass_n_scaff,    pass_n_scaff)
  }
  STATS = data.frame(DATASET=basename(list_fnames),
                    TOTAL_ALLELES=VECTOR_total_n_alleles,
                    TOTAL_LOCI=VECTOR_total_n_loci,
                    TOTAL_SCAFFOLDS=VECTOR_total_n_scaff,
                    CANDIDATE_ALLELES=VECTOR_pass_n_alleles,
                    CANDIDATE_LOCI=VECTOR_pass_n_loci,
                    CANDIDATE_SCAFFOLDS=VECTOR_pass_n_scaff)
  write.csv(STATS, file=paste0(dirname, "/", herbi, "_summary_counts.csv"))
  ### distribution of the absolute allelic effects that passed above the Bonferroni threshold
  list_colors = c("#BF7EBE", "#FFBF7F", "#8BA5F2", "#FF7F7E", "#78C679") ### clethodim, glyphosate, sulfometuron, terbuthylazine, and trifluralin
  if (herbi == "CLETH"){
    color = list_colors[1]
  } else if (herbi == "GLYPH"){
    color = list_colors[2]
  } else if (herbi == "SULFO"){
    color = list_colors[3]
  } else if (herbi == "TERBU"){
    color = list_colors[4]
  } else if (herbi == "TRIFL"){
    color = list_colors[5]
  }
  svg(paste0(dirname, "/", herbi, "_effects_size_distributions.svg"), width=12, height=8)
  layout_nrow = round(sqrt(length(list_fnames)))
  layout_ncol = round(length(list_fnames) / layout_nrow)
  if ((layout_nrow*layout_ncol) < length(list_fnames)){
    layout_ncol = layout_ncol + 1
  }
  par(mfrow=c(layout_nrow, layout_ncol))
  for (f in list_fnames){
    # f = list_fnames[1]
    dat = read.csv(f)
    total_n_alleles = nrow(dat)
    bonferroni_threshold = -log10(alpha / total_n_alleles)
    subdat = droplevels(dat[dat$LOD >= bonferroni_threshold, ])
    pass_n_alleles = nrow(subdat)
    pass_n_loci = length(unique(paste0(subdat$CHROM, "-", subdat$POS)))
    pass_n_scaff = length(unique(subdat$CHROM))
    pop = unlist(strsplit(basename(f), "_"))[3]
    hist(abs(subdat$ALPHA),
        main=paste0(herbi, "\n(", pop, ")"),
        xlab="| Effect size |",
        col=color, bord=FALSE)
    grid(lty=2, col="gray")
    ### variance explained by the top 25% of the significant QTL
    abs_qtl_eff = abs(subdat$ALPHA)
    abs_qtl_eff = abs_qtl_eff[order(abs_qtl_eff, decreasing=TRUE)]
    top10perc = abs_qtl_eff[1:round(length(abs_qtl_eff)*0.10)]
    perc_effect_among_QTL = round(sum(top10perc/sum(abs_qtl_eff))*100)
    ### add legend
    legend("topright", legend=c(paste0(pass_n_scaff, " scaffolds"),
                                paste0(pass_n_loci, " loci"),
                                paste0(pass_n_alleles, " alleles"),
                                paste0("Top 10% of the QTL accounts for ",  perc_effect_among_QTL, "%\nof the cummutative QTL effects")),
          bg="white")
  }
  dev.off()
  ### Commonalities in the loci above the Bonferroni threshold
  MAT_N_COMMON = matrix(0, nrow=length(list_fnames), ncol=length(list_fnames))
  colnames(MAT_N_COMMON) = list_fnames
  rownames(MAT_N_COMMON) = list_fnames
  for (i in 1:length(list_fnames)) {
    # i = 1
    f1 = list_fnames[i]
    pop1 = paste(unlist(strsplit(basename(f1), "_"))[2:3], collapse="_")
    df1 = read.csv(f1)
    # bonferroni_threshold1 = -log10(alpha/length(unique(paste0(df1$CHROM, "_", df1$POS))))
    bonferroni_threshold1 = -log10(alpha/nrow(df1))
    df1 = droplevels(df1[df1$LOD >= bonferroni_threshold1, ])
    for (j in 1:length(list_fnames)) {
      # j = 2
      f2 = list_fnames[j]
      pop2 = paste(unlist(strsplit(basename(f2), "_"))[2:3], collapse="_")
      df2 = read.csv(f2)
      # bonferroni_threshold2 = -log10(alpha/length(unique(paste0(df2$CHROM, "_", df2$POS))))
      bonferroni_threshold2 = -log10(alpha/nrow(df2))
      df2 = droplevels(df2[df2$LOD >= bonferroni_threshold2, ])
      ### merge
      MERGED = merge(df1, df2, by="CHROM", all=FALSE)
      if (nrow(MERGED)>0){
        MERGED = droplevels(MERGED[abs(MERGED$POS.x - MERGED$POS.y) <= LD_kb*1000, ])
        if (nrow(MERGED)>0){
          ### output counts of common peaks - use the minimum number of loci that are within LD_kb of one another
          MAT_N_COMMON[i, j] = min(c(length(unique(paste(MERGED$CHROM, MERGED$POS.x, sep="_"))),
                                    length(unique(paste(MERGED$CHROM, MERGED$POS.y, sep="_")))))
          rownames(MAT_N_COMMON)[i] = pop1
          colnames(MAT_N_COMMON)[j] = pop2
        }
      }
    }
  }
  write.csv(MAT_N_COMMON, file=paste0(dirname, "/", herbi, "_common_counts.csv"))
' > 08_summ_stats.r
time \
parallel \
Rscript 08_summ_stats.r ${DIR_GWAS} {} 0.01 1 ::: CLETH GLYPH SULFO TERBU

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### GO term enrichment analysis
### Outputs:
### (1) BLASTOUT-ORYZA-${herbi}*GFF3_PEAKS.txt
### (2) GO term enrich analysis results manually loaded and save from ShinyGO (http://bioinformatics.sdstate.edu/go/)
# extract neighboring gff3 names (+/-1kb from the peaks)
echo 'args = commandArgs(trailingOnly=TRUE)
      # args = c("/data/Lolium/Quantitative_Genetics/06_GWAS/CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv",
      #          "/data/Lolium/Genomics/SEQUENCES/RNA/Lp_Annotation_V1.1.mp.gff3",
      #          "1")
      f = args[1]
      fname_annotation = args[2]
      LD_kb = as.numeric(args[3])
      GFF3 = read.delim(fname_annotation, skip=1, header=FALSE)
      colnames(GFF3) = c("seqid", "source", "type", "start", "end", "score", "strand", "phase", "attributes")
      GFF3$seqid = as.character(GFF3$seqid)
      print(f)
      dat = read.csv(f)
      dat = dat[order(dat$LOD, decreasing=TRUE), ]
      dat$CHROM = as.character(dat$CHROM)
      bonferroni_threshold = -log10(alpha/nrow(dat))
      dat = droplevels(dat[dat$LOD >= bonferroni_threshold, ])
      for (i in 1:nrow(dat)){
          # i = 1
          idx = (as.character(dat$CHROM[i]) == as.character(GFF3$seqid)) & ((dat$POS[i]-GFF3$start)<=(1000*LD_kb)) & ((dat$POS[i]-GFF3$end)<=(1000*LD_kb))
          if (exists("PEAKS")==FALSE){
              PEAKS = GFF3[idx, ]
              PEAKS$LOD = rep(dat$LOD[i], times=nrow(PEAKS))
          } else {
              peaks = GFF3[idx, ]
              peaks$LOD = rep(dat$LOD[i], times=nrow(peaks))
              PEAKS = rbind(PEAKS, peaks)
          }
          ### peaks with no nearby gff3 predicted gene
          if (sum(idx)==0){
            if (exists("NO_GFF3")==FALSE){
                NO_GFF3 = dat[i, ]
            } else {
                NO_GFF3 = rbind(NO_GFF3, dat[i, ])
            }
          } else {
            if (exists("YES_GFF3")==FALSE){
                YES_GFF3 = dat[i, ]
            } else {
                YES_GFF3 = rbind(YES_GFF3, dat[i, ])
            }
          }
      }
      PEAKS = PEAKS[PEAKS$type == "gene", ]
      PEAKS = PEAKS[!duplicated(PEAKS[,c(1,4,5)]), ]
      fname_out = sub("-OUTPUT.csv", "-GFF3_PEAKS.csv", basename(f))
      write.table(PEAKS, file=fname_out, sep=",", row.names=FALSE, quote=FALSE)
      
      ### peaks with and without nearby gff3 predicted gene
      fname_out = sub("-OUTPUT.csv", "-PEAKS_LIST_YES_GFF3.csv", basename(f))
      write.table(YES_GFF3, file=fname_out, sep=",", row.names=FALSE, quote=FALSE)
      fname_out = sub("-OUTPUT.csv", "-PEAKS_LIST_NO_GFF3.csv", basename(f))
      write.table(NO_GFF3, file=fname_out, sep=",", row.names=FALSE, quote=FALSE)
' > 08_gff3_peaks.r
time \
parallel \
Rscript 08_gff3_peaks.r {} ${ANNOTATION} 1 ::: $(find ${DIR_GWAS}/*-OUTPUT.csv | grep -v "RANEF")
mv *-GFF3_PEAKS.csv ${DIR_GWAS}/


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 20200804
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### OR SINCE THERE IS BIAS IN Lolium perenne genome annotations (see the las sections of this code fopr the bias analysis)
### extract coordinates of the peaks instead of the gff3 annotations
### NOTE! USING 5kb LD blocks!!!!
echo 'args = commandArgs(trailingOnly=TRUE)
      # args=c("/data/Lolium/Quantitative_Genetics/06_GWAS/CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv",
      #        "1")
      f = args[1]
      LD_kb = as.numeric(args[2])
      print(f)
      dat = read.csv(f)
      dat = dat[order(dat$LOD, decreasing=TRUE), ]
      dat$CHROM = as.character(dat$CHROM)
      bonferroni_threshold = -log10(0.01/nrow(dat))
      dat = droplevels(dat[dat$LOD >= bonferroni_threshold, ])
      out = data.frame(CHROM=dat$CHROM, POS=dat$POS, ALLELE=dat$ALLELE, POS_INIT=dat$POS-(LD_kb*1000), POS_SLUT=dat$POS+(LD_kb*1000))
      out$POS_INIT[out$POS_INIT<1] = 1
      fname_out = sub("-OUTPUT.csv", "-SEQ_PEAKS.csv", basename(f))
      write.table(out, file=fname_out, sep=",", row.names=FALSE, quote=FALSE)
' > 08_coor_peaks.r
time \
parallel \
Rscript 08_coor_peaks.r {} 1 ::: $(find ${DIR_GWAS}/*-OUTPUT.csv | grep -v "RANEF")
mv *-SEQ_PEAKS.csv ${DIR_GWAS}/
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



# clean-up
mkdir ${DIR_GWAS}/LIST_YES_NO_GFF3/
mv *-PEAKS_LIST_*_GFF3.csv ${DIR_GWAS}/LIST_YES_NO_GFF3/
# Reformat the genome assembly (i.e. remove sequence wrapping)
if [ -f $(dirname $REFERENCE)/REFORMATED_GENOME.fasta ]
then
    echo $(dirname $REFERENCE)/REFORMATED_GENOME.fasta exists!
else
    grep "^>" ${REFERENCE} > SCAFFOLDS.temp
    csplit ${REFERENCE} /\>/ '{*}'
    ls xx* > SPLIT_FNAMES.temp
    rm $(head -n1 SPLIT_FNAMES.temp) ### remove the first file which contains nada!
    ls xx* > SPLIT_FNAMES.temp
    touch $(dirname $REFERENCE)/REFORMATED_GENOME.fasta
    time for i in $(seq 1 $(cat SPLIT_FNAMES.temp | wc -l))
    do
      echo $i
      seq=$(head -n${i} SPLIT_FNAMES.temp | tail -n1)
      head -n1 ${seq} > temp_name
      tail -n+2 ${seq} > temp_seq
      sed -zi 's/\n//g' temp_seq
      cat temp_name >> $(dirname $REFERENCE)/REFORMATED_GENOME.fasta
      cat temp_seq >> $(dirname $REFERENCE)/REFORMATED_GENOME.fasta
      echo -e "" >> $(dirname $REFERENCE)/REFORMATED_GENOME.fasta ### insert newline character that was removed by sed
    done
    rm *temp* xx*
fi
# blast gff3 peaks on rice genes instead of TAIR10 or the full ncbi nucleotide database
# download rice ensemble genome and gff3
cd ${DIR_GWAS}
for i in 1 2 3 4 5 6 7 8 9 10 11 12 Mt Pt
do
    wget ftp://ftp.ensemblgenomes.org/pub/release-47/plants/fasta/oryza_sativa/dna/Oryza_sativa.IRGSP-1.0.dna.chromosome.${i}.fa.gz
    wget ftp://ftp.ensemblgenomes.org/pub/release-47/plants/gff3/oryza_sativa/Oryza_sativa.IRGSP-1.0.47.chromosome.${i}.gff3.gz
done
gunzip -d Oryza_sativa*.gz
# remove sequence wrapping within chromosome
touch ORYZA.fasta
time for i in 1 2 3 4 5 6 7 8 9 10 11 12 Mt Pt
do
    # i=1
    echo $i
    seq=Oryza_sativa.IRGSP-1.0.dna.chromosome.${i}.fa
    head -n1 ${seq} > temp_name
    tail -n+2 ${seq} > temp_seq
    sed -zi 's/\n//g' temp_seq
    cat temp_name >> ORYZA.fasta
    cat temp_seq >> ORYZA.fasta
    echo -e "" >> ORYZA.fasta ### insert newline character that was removed by sed
done
rm *temp*
# extract the sequences for each gene and build the blast database
cat Oryza_sativa*.gff3 | sort | uniq | grep gene_id > ORYZA.gff3
rm ORYZA.genes.fasta || touch ORYZA.genes.fasta
touch ORYZA.genes.fasta
time \
while IFS= read -r i
do
    # i=$(head -n1 ORYZA.gff3)
    chrom=$(cut -f1 <<<$i)
    start=$(cut -f4 <<<$i)
    end=$(cut -f5 <<<$i)
    gene=$(cut -d";" -f1 <<<$(cut -f9 <<<$i) | sed 's/ID=//g' | sed 's/gene://g')
    grep -A 1 "^>${chrom}" ORYZA.fasta | tail -n1 > seq.temp
    echo -e "> ${gene}:${chrom}:${start}-${end}" >> ORYZA.genes.fasta
    cut -c${start}-${end} seq.temp >> ORYZA.genes.fasta
    rm seq.temp
done < ORYZA.gff3
# generate the blast database
time \
makeblastdb -in ORYZA.genes.fasta \
            -title "ORYZA.genes.blastdb" \
            -dbtype nucl
# # blasting (90% percent identity)
# blasting (75% percent identity)
echo '#!/bin/bash
    gff3_peaks_file=${1}
    DB=${2}
    REFORMATED_GENOME=${3}
    # ### TEST:
    # gff3_peaks_file=CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-GFF3_PEAKS.csv
    # gff3_peaks_file=CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-SEQ_PEAKS.csv
    # DB=ORYZA.genes.fasta
    # REFORMATED_GENOME=/data/Lolium/Quantitative_Genetics/02_FASTQ/REFERENCE/REFORMATED_GENOME.fasta
    ### BUILD THE QUERY SEQUENCES
    rm ${gff3_peaks_file}.query.fasta || touch ${gff3_peaks_file}.query.fasta
    for locus in $(cat ${gff3_peaks_file} | tail -n+2)
    do
      # locus=$(cat ${gff3_peaks_file} | tail -n+2 | head -n1)
      chrom=$(cut -d, -f1 <<<$locus)
      start=$(cut -d, -f4 <<<$locus)
      end=$(cut -d, -f5 <<<$locus)
      grep -A 1 ${chrom} ${REFORMATED_GENOME} | tail -n1 > ${gff3_peaks_file}.seq.temp
      if [ $end -gt $(tail -n1 ${gff3_peaks_file}.seq.temp | wc -c) ]
      then
        end=$(tail -n1 ${gff3_peaks_file}.seq.temp | wc -c)
      fi
      echo -e "> ${chrom}:${pos}:${start}-${end}" >> ${gff3_peaks_file}.query.fasta
      cut -c${start}-${end} ${gff3_peaks_file}.seq.temp >> ${gff3_peaks_file}.query.fasta
      rm ${gff3_peaks_file}.seq.temp
    done
    ### BLAST OFF!
    DIR=$(dirname $gff3_peaks_file)
    FNAME=$(basename $gff3_peaks_file)
    blastn -db ${DB} \
      -query ${gff3_peaks_file}.query.fasta \
      -perc_identity 75 \
      -outfmt "6 qseqid staxids pident evalue qcovhsp bitscore stitle" \
      -out ${DIR}/BLASTOUT-ORYZA-${FNAME%.csv*}.txt
    # ### extract top hit per query
    # cp ${DIR}/BLASTOUT-ORYZA-${FNAME%.csv*}.txt ${DIR}/BLASTOUT-ORYZA-${FNAME%.csv*}.txt.all
    # Rscript extract_top_hit_per_query.r ${DIR}/BLASTOUT-ORYZA-${FNAME%.csv*}.txt
' > blast_oryza_gff3_peaks.sh
chmod +x blast_oryza_gff3_peaks.sh
# echo '
#   args = commandArgs(trailingOnly=TRUE)
#   # args = c("BLASTOUT-ORYZA-CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-GFF3_PEAKS.txt")
#   # args = c("BLASTOUT-ORYZA-CLETH_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-GFF3_PEAKS.txt")
#   fname_input = args[1]
#   dat = read.delim(fname_input, header=FALSE)
#   colnames(dat) = c("qseqid", "staxids", "pident", "evalue", "qcovhsp", "bitscore", "stitle")
#   for (qseqid in levels(dat$qseqid)){
#     # qseqid = levels(dat$qseqid)[1]
#     subdat = droplevels(dat[dat$qseqid==qseqid, ])
#     subdat = subdat[order(subdat$bitscore, decreasing=TRUE), ][1, ]
#     if (exists("OUT")==FALSE){
#       OUT = subdat
#     } else {
#       OUT = rbind(OUT, subdat)
#     }
#   }
#   write.table(OUT, file=fname_input, sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)
# ' > extract_top_hit_per_query.r
time \
parallel ./blast_oryza_gff3_peaks.sh {} ORYZA.genes.fasta $(dirname $REFERENCE)/REFORMATED_GENOME.fasta ::: $(ls *-GFF3_PEAKS.csv)
time \
parallel ./blast_oryza_gff3_peaks.sh {} ORYZA.genes.fasta $(dirname $REFERENCE)/REFORMATED_GENOME.fasta ::: $(ls *-SEQ_PEAKS.csv)

# merge peaks from sequence based and gff3 based blast hits
for herbi in CLETH GLYPH SULFO TERBU TRIFL
do
    echo $herbi
    cat BLASTOUT-ORYZA-${herbi}*GFF3_PEAKS.txt | cut -f 7 | cut -d":" -f1 | sed 's/gene-//g' | sort | uniq > ${herbi}-GFF3_PEAKS.oryza.blastout
    cat BLASTOUT-ORYZA-${herbi}*GFF3_PEAKS.txt | sort | uniq > ${herbi}-GFF3_PEAKS.oryza.txt
    cat BLASTOUT-ORYZA-${herbi}*SEQ_PEAKS.txt | cut -f 7 | cut -d":" -f1 | sed 's/gene-//g' | sort | uniq > ${herbi}-SEQ_PEAKS.oryza.blastout
    cat BLASTOUT-ORYZA-${herbi}*SEQ_PEAKS.txt | sort | uniq > ${herbi}-SEQ_PEAKS.oryza.txt
    #cat ${herbi}-GFF3_PEAKS.oryza.blastout.temp ${herbi}-SEQ_PEAKS.oryza.blastout.temp | sort | uniq > ${herbi}.blastout.forgo
done
rm *.temp
# GO term enrichment analysis with ShinyGO (http://bioinformatics.sdstate.edu/go/)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### Commonalities across populations and across herbicides in terms of the Oryza sative genes detected as candidate loci
### Outputs:
### (1) VENN-${herbi}.svg
### (2) VENN-ALL.svg
cd -
echo '
  ### load GWAS ouput directory
  args = commandArgs(trailingOnly=TRUE)
  # args = c("/data/Lolium/Quantitative_Genetics/06_GWAS")
  dir = args[1]
  setwd(dir)
  ### load Venn diagram library
  library(VennDiagram)
  ### extract filenames of GWAS ORYZA BLAST peaks excluding Trifluralin
  list_fnames = system("ls BLASTOUT-ORYZA-*-GFF3_PEAKS.txt | grep -v TRIFL", intern=TRUE)
  ### set dataset ID names
  id = unlist(lapply(strsplit(list_fnames, "-"), FUN=function(x){paste(unlist(strsplit(x[3], "_"))[c(1,3)], collapse="_")}))
  ### assign the dataset ID names to each gene list
  for (i in 1:length(list_fnames)){
    # i = 1
    tryCatch(assign(id[i], unique(sort(as.character(read.table(list_fnames[i], header=FALSE)$V7)))),
            error=function(e){})
  }
  ### exclude gene lists without any blast hits
  id = id[id %in% ls()]
  ### generate the list of gene lists
  list_list = eval(parse(text=paste("list(", paste(paste(id, id, sep="="), collapse=", "), ")")))
  ### Venn diagrams and partitioning (i.e. save svg plots and csv partiton)
  ### Venn diagram per herbicide
  herbi = unlist(lapply(strsplit(id, "_"), FUN=function(x){x[1]}))
  pop = unlist(lapply(strsplit(id, "_"), FUN=function(x){x[2]}))
  for (i in 1:length(unique(herbi))){
    # i = 1
    h = unique(herbi)[i]
    names_pop = pop[herbi==h]
    names_pop[names_pop!="ALL"] = paste0(names_pop[names_pop!="ALL"], "-GWAlpha")
    names_pop[names_pop=="ALL"] = "ALL-MIXED-REML"
    ### plot ven diagram
    sub_list = list_list[herbi==h]
    svg(paste0("VENN-", h, "-genes.svg"), width=10, height=10)
    grid.newpage()
    grid.draw(venn.diagram(sub_list, filename=NULL, main=h, category.names=names_pop))
    dev.off()
    ### gene list partitioning
    per_herbicides = get.venn.partitions(sub_list)
    SET = per_herbicides$..set..
    GENES = unlist(lapply(per_herbicides$..values.., FUN=function(x){paste(x, collapse=";")}))
    write.csv(data.frame(SET, GENES), file=paste0("VENN-", h, ".csv"), row.names=FALSE, quote=FALSE)
    ### merge all gene names per herbicide
    assign(h, unique(sort(unlist(sub_list))))
  }
  ### Venn diagram across herbicides
  merged_list = eval(parse(text=paste0("list(", paste(paste(ls()[ls() %in% unique(herbi)], ls()[ls() %in% unique(herbi)], sep="="), collapse=", "), ")")))
  svg("VENN-ALL.svg", width=10, height=10)
  grid.newpage()
  grid.draw(venn.diagram(merged_list, filename=NULL))
  dev.off()
  ### gene list partitioning
  across_herbicides = get.venn.partitions(merged_list)
  SET = across_herbicides$..set..
  GENES = unlist(lapply(across_herbicides$..values.., FUN=function(x){paste(x, collapse=";")}))
  write.csv(data.frame(SET, GENES), file="VENN-ALL.csv", row.names=FALSE, quote=FALSE)
' > 08_common_genes.r
time \
Rscript 08_common_genes.r ${DIR_GWAS}


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### Scaffold Venn Diagrams
### Outputs:
### (1) VENN-${herbi}.svg
### (2) VENN-ALL.svg

echo '
  ### load GWAS ouput directory
  args = commandArgs(trailingOnly=TRUE)
  # args = c("/data/Lolium/Quantitative_Genetics/06_GWAS", "0.01", "1")
  dir = args[1]
  alpha = as.numeric(args[2])
  LD_kb = as.numeric(args[3])
  setwd(dir)
  ### load Venn diagram library
  library(VennDiagram)
  ### extract filenames of GWAS ORYZA BLAST peaks excluding Trifluralin
  list_fnames = system("ls *-OUTPUT.csv | grep -v RANEF | grep -v TRIFL", intern=TRUE)
  ### extract herbicide names
  list_herbi = unique(sort(unlist(lapply(strsplit(list_fnames, "_"), FUN=function(x){x[1]}))))
  ### Venn Diagrams per herbicide
  for (i in 1:length(list_herbi)){
    # i = 1
    herbi = list_herbi[i]
    list_fnames_herbi = list_fnames[grepl(herbi, list_fnames)]
    list_pop = unlist(lapply(strsplit(list_fnames_herbi, "_"), FUN=function(x){x[3]}))
    for (j in 1:length(list_fnames_herbi)){
      # j = 1
      fname = list_fnames_herbi[j]
      pop = unlist(strsplit(fname, "_"))[3]
      dat = read.csv(fname)
      total_n_alleles = nrow(dat)
      bonferroni_threshold = -log10(alpha / total_n_alleles)
      sub_dat = dat[dat$LOD >= bonferroni_threshold, ]
      assign(pop, unique(unique(as.character(sub_dat$CHROM))))
    }
    LIST_SCAFF = eval(parse(text=paste0("list(", paste(paste0(list_pop, "=", list_pop), collapse=", "), ")")))
    names(LIST_SCAFF)[names(LIST_SCAFF)!="ALL"] = paste0(names(LIST_SCAFF)[names(LIST_SCAFF)!="ALL"], "-GWAlpha")
    names(LIST_SCAFF)[names(LIST_SCAFF)=="ALL"] = paste0(names(LIST_SCAFF)[names(LIST_SCAFF)=="ALL"], "-MIXED-REML")
    svg(paste0("VENN-", herbi, "-scaffolds.svg"), width=10, height=10)
    grid.newpage()
    grid.draw(venn.diagram(LIST_SCAFF, filename=NULL))
    dev.off()
    ### prepare merged herbicide scaffolds list
    assign(herbi, unique(sort(unlist(LIST_SCAFF))))
    eval(parse(text=paste0("names(", herbi, ") = NULL")))
  }

  ### Venn Diagrams across herbicides
  MERGED_LIST_SCAFF = eval(parse(text=paste0("list(", paste(paste0(list_herbi, "=", list_herbi), collapse=", "), ")")))
  svg(paste0("VENN-ALL-scaffolds.svg"), width=10, height=10)
  grid.newpage()
  grid.draw(venn.diagram(MERGED_LIST_SCAFF, filename=NULL))
  dev.off()
' > 08_common_scaffolds.r
time \
Rscript 08_common_scaffolds.r ${DIR_GWAS} 0.01 1

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### Bias in the genes captured by Pool-ddRADseq?
### (1) List scaffold names capture by Pool-ddRADseq after all the filtering we've performed
cut -d, -f1 $(ls ${DIR_GWAS}/*-OUTPUT.csv | grep -v RANEF) | grep 'scaffold' | sort | uniq > POOL-DDRADSEQ-SCAFFOLDS.txt
for herbi in CLETH GLYPH SULFO TERBU
do
cut -d, -f1 $(ls ${DIR_GWAS}/${herbi}*-OUTPUT.csv | grep -v RANEF) | grep 'scaffold' | sort | uniq > ${herbi}-POOL-DDRADSEQ-SCAFFOLDS.txt
done
### (2) Extract all L.perenne annontations in all these captured scaffolds
grep -f POOL-DDRADSEQ-SCAFFOLDS.txt ${ANNOTATION} | sort | uniq | sed 's/\t/,/g' > POOL-DDRADSEQ-ANNOTATIONS.csv
for herbi in CLETH GLYPH SULFO TERBU
do
grep -f ${herbi}-POOL-DDRADSEQ-SCAFFOLDS.txt ${ANNOTATION} | sort | uniq | sed 's/\t/,/g' > ${herbi}-POOL-DDRADSEQ-ANNOTATIONS.csv
done
### (3) Extract sequences and BLAST into the O. sativa genome annotations
time \
./blast_oryza_gff3_peaks.sh POOL-DDRADSEQ-ANNOTATIONS.csv \
                            ${DIR_GWAS}/ORYZA.genes.fasta \
                            $(dirname $REFERENCE)/REFORMATED_GENOME.fasta
time \
parallel \
./blast_oryza_gff3_peaks.sh {} \
                            ${DIR_GWAS}/ORYZA.genes.fasta \
                            $(dirname $REFERENCE)/REFORMATED_GENOME.fasta \
                            ::: $(ls *-POOL-DDRADSEQ-ANNOTATIONS.csv)
### extract gene names
cut -d: -f1 <(cut -f7 BLASTOUT-ORYZA-POOL-DDRADSEQ-ANNOTATIONS.txt) | sed 's/gene-//g' | sort | uniq > GENE_NAMES-ALL.txt
for herbi in CLETH GLYPH SULFO TERBU
do
cut -d: -f1 <(cut -f7 BLASTOUT-ORYZA-${herbi}-POOL-DDRADSEQ-ANNOTATIONS.txt) | sed 's/gene-//g' | sort | uniq > GENE_NAMES-${herbi}.txt
done
### GO term enrichment analysis with ShinyGO (http://bioinformatics.sdstate.edu/go/)
### Bias towards stress-related genes found!

### Bias in Oryza sativa Japonica Group genome annotations?
cut -f9 ${DIR_GWAS}/ORYZA.gff3 | cut -d';' -f1 | cut -d: -f2 | sed 's/gene-//g' | sort | uniq > GENE_NAMES-ORYZA_ALL.txt
### GO term enrichment analysis with ShinyGO (http://bioinformatics.sdstate.edu/go/)
### No bias found!

### Bias in Lolium perenne genome annotations?
sort $ANNOTATION | uniq | sed 's/\t/,/g' > LPERENNE-ANNOTATIONS.csv
time \
./blast_oryza_gff3_peaks.sh LPERENNE-ANNOTATIONS.csv \
                            ${DIR_GWAS}/ORYZA.genes.fasta \
                            $(dirname $REFERENCE)/REFORMATED_GENOME.fasta
cut -d: -f1 <(cut -f7 BLASTOUT-ORYZA-LPERENNE-ANNOTATIONS.txt) | sort | uniq > GENE_NAMES-LPERENNE.txt
### GO term enrichment analysis with ShinyGO (http://bioinformatics.sdstate.edu/go/)
### Bias??!?!?! 2020-07-30

mv *ANNOTATIONS* ${DIR_BIAS}
mv *GENE_NAMES* ${DIR_BIAS}

################
### FINDINGS ###
################
### There are biases!
### Significant enrichment for stress-related response genes in the scaffolds captured by Pool-ddRADseq with MseI and NsiI on L. rigidum mapped on L. perenne reference genome !
### However, this does not nullify our results, since the biases may coome from:
###   1.) L. perenne gene annotations - most of our candidate loci were not within 1kb of predicted L. perenne genes
###   2.) L. perenne reference genome is higly fragmented
### These biases cannot be attributable to the O. sativa gene annotations - stress-related genes may be characterised better since they are very important in rice breeding


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### clean-up
rm 08_summ_stats.r \
   08_gff3_peaks.r \
   08_coor_peaks.r \
   08_common_genes.r \
   blast_oryza_gff3_peaks.sh \
  #  extract_top_hit_per_query.r

