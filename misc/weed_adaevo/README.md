These are the scripts and phenotype data for *Lolium rigidum* herbicide resistance PoolGPAS (genomic prediction and association analysis using Pools)

NOTE: res/ is where phenotype data *.py  (for GWAlpha) and ALL_PHENO.csv for description of all phenotype data are located.