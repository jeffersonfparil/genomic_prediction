#!/bin/bash

####################################
### Prepare the reference genome ###
####################################

### Input the directory where we want to save the reference genome
DIR_REF=$1
# DIR_REF=/data/Lolium/Quantitative_Genetics/02_FASTQ/REFERENCE/

### Download the reference genome
cd ${DIR_REF}/
# wget http://185.45.23.197:5080/ryegrassdata/GENOMIC_ASSEMBLY/lope_V1.0.fasta ### (Byrne et al, 2017)
cp /data/Lolium_rigidum_ASSEMBLY/GENOME_ASSEMBLY/APGP_CSIRO_Lrig_flye-racon-polca-allhic-juicebox_v0.1n_clean1.fasta .

# ### Fix the reference genome format
echo -e "
import os, sys
from Bio import SeqIO
INPUT_fasta = sys.argv[1]
OUTPUT_fasta = sys.argv[2]
SeqIO.convert(INPUT_fasta, 'fasta', OUTPUT_fasta, 'fasta')
" > 01_fix_ref.py
python3 01_fix_ref.py APGP_CSIRO_Lrig_flye-racon-polca-allhic-juicebox_v0.1n_clean1.fasta Reference.fasta

### Index the reference genome for bwa
bwa index -p Reference -a bwtsw Reference.fasta 

### Index the reference genome for samtools
samtools faidx Reference.fasta

# ### Clean-up
# rm 01_fix_ref.py
cd -