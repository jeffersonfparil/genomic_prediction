using GWAlpha
using DelimitedFiles
using RCall

# ARGS = ["/data/Lolium/Quantitative_Genetics/05_SYNC", "/data/Lolium/Quantitative_Genetics/01_PHENOTYPES", "CLETH"]
DIR_SYNC = ARGS[1]
DIR_PHEN = ARGS[2]
HERBI = ARGS[3]

cd(DIR_SYNC)

sync_fname = string(HERBI, "_MERGED_ALL_MAF0.001_DEPTH50.sync")
geno_fname = string(HERBI, "_MERGED_ALL_MAF0.001_DEPTH50_ALLELEFREQ.csv")
phen_fname = string(HERBI, "_MERGED_ALL_pheno.csv")
cova_fname = string(HERBI, "_MERGED_ALL_MAF0.001_DEPTH50_COVARIATE_FST.csv")

SYNC = DelimitedFiles.readdlm(sync_fname, '\t'); GWAlpha.sync_processing_module.sync_parse(sync_fname)
GENO = DelimitedFiles.readdlm(geno_fname, ',')
PHEN = DelimitedFiles.readdlm(string(DIR_PHEN, "/", phen_fname), ',')
COVA = DelimitedFiles.readdlm(cova_fname, ',')
n = size(PHEN,1)
p = size(GENO,1)

MODEL = []
Y_TRUE = []
Y_PRED = []
@time for i in 1:n
    ### divide into n-1 training datapoints and 1 validation datapoint
    idx = collect(1:n)[collect(1:n) .!= i]
    TRAIN_SYNC = hcat(SYNC[:, 1:3], SYNC[:, idx .+ 3])
    TRAIN_GENO = hcat(GENO[:, 1:3], GENO[:, idx .+ 3])
    TRAIN_PHEN = PHEN[idx, :]
    TRAIN_COVA = COVA[idx, idx]
    VALID_SYNC = hcat(SYNC[:, 1:3], SYNC[:, i + 3])
    VALID_GENO = hcat(GENO[:, 1:3], GENO[:, i + 3])
    VALID_PHEN = reshape(PHEN[i, :], 1, 2)
    ### filenames
    TRAIN_SYNC_fname = string("TRAIN_", sync_fname)
    TRAIN_GENO_fname = string("TRAIN_", geno_fname)
    TRAIN_PHEN_fname = string("TRAIN_", phen_fname)
    TRAIN_COVA_fname = string("TRAIN_", cova_fname)
    VALID_SYNC_fname = string("VALID_", sync_fname)
    VALID_GENO_fname = string("VALID_", geno_fname)
    VALID_PHEN_fname = string("VALID_", phen_fname)
    ### write-out
    DelimitedFiles.writedlm(TRAIN_SYNC_fname, TRAIN_SYNC)
    DelimitedFiles.writedlm(TRAIN_GENO_fname, TRAIN_GENO, ',')
    DelimitedFiles.writedlm(TRAIN_PHEN_fname, TRAIN_PHEN, ',')
    DelimitedFiles.writedlm(TRAIN_COVA_fname, TRAIN_COVA, ',')
    DelimitedFiles.writedlm(VALID_SYNC_fname, VALID_SYNC)
    DelimitedFiles.writedlm(VALID_GENO_fname, VALID_GENO, ',')
    DelimitedFiles.writedlm(VALID_PHEN_fname, VALID_PHEN, ',')
    ### MIXED-REML-WEIRCOCK
    @time GWAlpha.PoolGPAS(filename_sync=TRAIN_SYNC_fname, filename_phen=TRAIN_PHEN_fname, maf=0.001, depth=50, model="MIXED", filename_random_covariate=TRAIN_COVA_fname, varcomp_est="REML")
    ### RIDGE
    @time GWAlpha.PoolGPAS(filename_sync=TRAIN_SYNC_fname, filename_phen=TRAIN_PHEN_fname, maf=0.001, depth=50, model="GLMNET", glmnet_alpha=0.00)
    ### LASSO
    @time GWAlpha.PoolGPAS(filename_sync=TRAIN_SYNC_fname, filename_phen=TRAIN_PHEN_fname, maf=0.001, depth=50, model="GLMNET", glmnet_alpha=1.00)
    ### validation
    BETA_MIXED_fname = replace(TRAIN_SYNC_fname, ".sync" => "-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv")
    BETA_RIDGE_fname = replace(TRAIN_SYNC_fname, ".sync" => "-GLMNET_ALPHA0.0-OUTPUT.csv")
    BETA_LASSO_fname = replace(TRAIN_SYNC_fname, ".sync" => "-GLMNET_ALPHA1.0-OUTPUT.csv")
    for f in [BETA_MIXED_fname, BETA_RIDGE_fname, BETA_LASSO_fname]
        model = split(f, "-")[2]
        BETA = DelimitedFiles.readdlm(f, ',')
        chrom=BETA[2:end,1]; pos=BETA[2:end,2]; allele=BETA[2:end,3]; effect=BETA[2:end,5]; intercept=BETA[2,5];
        @rput chrom; @rput pos; @rput allele; @rput effect; @rput intercept; @rput VALID_GENO;
        R"BETA = data.frame(chrom=unlist(as.character(chrom)), pos=unlist(as.numeric(pos)), allele=unlist(as.character(allele)), effect=unlist(as.numeric(effect)))
          VALID_GENO = as.data.frame(VALID_GENO)
          colnames(VALID_GENO) = c('chrom', 'pos', 'allele', 'freq')
          VALID_GENO$chrom = as.character(VALID_GENO$chrom)
          VALID_GENO$pos = as.numeric(VALID_GENO$pos)
          VALID_GENO$allele = as.character(VALID_GENO$allele)
          VALID_GENO$freq = as.numeric(VALID_GENO$freq)
          MERGED = merge(BETA, VALID_GENO, by=c('chrom', 'pos', 'allele'))
          y_pred = intercept + sum(MERGED$freq * MERGED$effect)
        "
        @rget y_pred
        ### outputs
        push!(MODEL, model)
        push!(Y_TRUE, VALID_PHEN[2])
        push!(Y_PRED, y_pred)
    end
    ### clean-up
    rm(TRAIN_SYNC_fname)
    rm(TRAIN_GENO_fname)
    rm(TRAIN_PHEN_fname)
    rm(TRAIN_COVA_fname)
    rm(VALID_SYNC_fname)
    rm(VALID_GENO_fname)
    rm(VALID_PHEN_fname)
    rm(BETA_MIXED_fname); rm(string(replace(TRAIN_SYNC_fname, ".sync" => "-MIXEDREML_USER_DEFINED_COVARIATE-RANEF-OUTPUT.csv")))
    rm(BETA_RIDGE_fname)
    rm(BETA_LASSO_fname)
end

### OUTPUT
@rput MODEL; @rput Y_TRUE; @rput Y_PRED; @rput HERBI;
R"model = unlist(as.character(MODEL))
  y_true = unlist(as.numeric(Y_TRUE))
  y_pred = unlist(as.numeric(Y_PRED))
  DF = data.frame(MODEL=model, Y_TRUE=y_true, Y_PRED=y_pred)
  DF$MODEL = as.character(DF$MODEL)
  MODEL = c(); CORRELATION = c(); RMSE = c(); MEAN_ABS_DEV = c()

  png(paste0(HERBI, '_LOOCV.png'), width=800, height=800)
  plot(x=DF$Y_TRUE, y=DF$Y_PRED, type='n', main=HERBI, xlab='Observed Resistance', ylab='Predicted Resistance')
  colors_light=c('#66c2a5', '#fc8d62', '#8da0cb'); colors_dark=c('#1b9e77', '#d95f02', '#7570b3')
  for (i in 1:length(unique(DF$MODEL))){
    model = unique(DF$MODEL)[i]
    sub = droplevels(DF[DF$MODEL==model, ])
    MODEL = c(MODEL, model)
    CORRELATION = c(CORRELATION, cor(sub$Y_TRUE, sub$Y_PRED))
    RMSE = c(RMSE, sqrt(mean((sub$Y_TRUE-sub$Y_PRED)^2)))
    MEAN_ABS_DEV = c(MEAN_ABS_DEV, mean(abs(sub$Y_TRUE-sub$Y_PRED)))
    points(x=sub$Y_TRUE, y=sub$Y_PRED, col=colors_light[i], pch=20)
    abline(lm(Y_PRED ~ Y_TRUE, data=sub), col=colors_dark[i])
  }
  legend('topleft', legend=unique(DF$MODEL), pch=20, lty=1, col=colors_light)
  dev.off()
  OUTPUT = data.frame(MODEL, CORRELATION, RMSE, MEAN_ABS_DEV)
  write.table(OUTPUT, file=paste0(HERBI, '_LOOCV.csv'), sep=',', quote=FALSE, row.names=FALSE)
  write.table(DF, file=paste0(HERBI, '_LOOCV_RAW.csv'), sep=',', quote=FALSE, row.names=FALSE)
"
