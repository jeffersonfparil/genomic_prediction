#!/bin/bash

###################################################################################
### Pile-up the reads from ech bam alignment in preparation for variant calling ###
###################################################################################

### Inputs:
DIR_BAM=$1      ### (1) input directory where the bam alingments are located;
DIR_MPILEUP=$2  ### (2) output directory where we want to save the piled-up alignments (*.mpileup);
PHENO=$3        ### (3) phenotype file for grouping the bam alingments based on herbicide treatments (comma-separated; HEADER:YEAR,SAMPLING,POP_ID,POPULATION,POOL,POOL_SIZE,HERBICIDE,QUANT_RES,SURVIVAL_RATE)
REF=$4          ### (4) reference genome (fasta format, indexed with samtools); and
MAPQ=$5         ### (5) minimum PHRED mapping quality (PHRED = -10log_10(accuracy))
BASQ=$6         ### (6) minimuim base calling quality (PHRED = -10log_10(accuracy)) to filter the SNPs with
# DIR_BAM=/data/Lolium/Quantitative_Genetics/03_BAM
# DIR_MPILEUP=/data/Lolium/Quantitative_Genetics/04_MPILEUP
# PHENO=/data/Lolium/Quantitative_Genetics/01_PHENOTYPES/ALL_PHENO.csv
# REF=/data/Lolium/Quantitative_Genetics/02_FASTQ/REFERENCE/Reference.fasta
# MAPQ=20
# BASQ=20

### Generate bam lists per experiment

###@@@ ${PHENO} data structure (csv):
###@@@ YEAR,SAMPLING,POP_ID,POPULATION,POOL,POOL_SIZE,HERBICIDE,QUANT_RES,SURVIVAL_RATE

###@@@ For within population GWAlpha:
grep "WITHIN" ${PHENO} | cut -d, -f2,4,7 | sort | uniq > herbi_within_pop_list.temp
for i in $(cat herbi_within_pop_list.temp)
do
    # i=$(head -n1 herbi_within_pop_list.temp)
    echo $i
    sampling=$(cut -d, -f1 <(echo $i))
    population=$(cut -d, -f2 <(echo $i))
    herbicide=$(cut -d, -f3 <(echo $i))
    if [ $(grep "ACC" <(echo $population) | wc -l) -eq 1 ]
    then
        ### for ACC* population
        find ${DIR_BAM}/*.bam | grep ${population} | grep "Pool" | sort > ${herbicide:0:5}_${sampling}_${population}_bam.list
    else
        ### for Inverleigh and Urana populations
        if [ ${herbicide} == "TRIFLURALIN" ]
        then
            ### for trifluralin herbicide (F instead of T)
            find ${DIR_BAM}/*.bam | grep ${population:0:1}F | sort > ${herbicide:0:5}_${sampling}_${population:0:5}_bam.list
        else
            ### for glyphosate (G), sulfometurion (S), and terbuthylazine (T) herbicides
            find ${DIR_BAM}/*.bam | grep ${population:0:1}${herbicide:0:1} | sort > ${herbicide:0:5}_${sampling}_${population:0:5}_bam.list
        fi
    fi
done
rm herbi_within_pop_list.temp
###@@@ For all populations and pools for Pool-GPAS:
for herbicide in $(cut -d, -f7 ${PHENO} | tail -n+2 | sort | uniq)
do
    # herbicide=$(cut -d, -f7 ${PHENO} | tail -n+2 | sort | uniq | head -n1)
    echo $herbicide
    ### for ACC* populations
    grep ${herbicide} ${PHENO} | grep ACROSS | cut -d, -f4 | sort | uniq > herbi_merged_all_ACROSS_list.temp
    grep ${herbicide} ${PHENO} | grep WITHIN | cut -d, -f4 | sort | uniq > herbi_merged_all_WITHIN_list.temp
    find ${DIR_BAM}/*.bam | grep -f herbi_merged_all_ACROSS_list.temp | grep -v "Pool" >  ${herbicide:0:5}_MERGED_ALL_bam.list.temp
    find ${DIR_BAM}/*.bam | grep -f herbi_merged_all_WITHIN_list.temp | grep    "Pool" >> ${herbicide:0:5}_MERGED_ALL_bam.list.temp
    ### for Inverleigh and Urana populations
    if [ $(grep -v "ACC" herbi_merged_all_WITHIN_list.temp | wc -l) != 0 ]
    then
        if [ $herbicide == "TRIFLURALIN" ]
        then
            herbi_one_letter=F
        else      
            herbi_one_letter=$(echo ${herbicide:0:1})
        fi
        grep -v "ACC" herbi_merged_all_WITHIN_list.temp | cut -c1 | sed s/$/${herbi_one_letter}/g > herbi_merged_inv_ura_one_letter_list.temp
        find ${DIR_BAM}/*.bam | grep -f herbi_merged_inv_ura_one_letter_list.temp >> ${herbicide:0:5}_MERGED_ALL_bam.list.temp
    fi
    ### sort and remove duplicates
    sort ${herbicide:0:5}_MERGED_ALL_bam.list.temp | uniq > ${herbicide:0:5}_MERGED_ALL_bam.list
    rm *.temp
done

### Pile-up the alignments (bam to mpileup)
echo -e '#!/bin/bash
BAM_LIST=$1
FNAME_REF=$2
MAPQ=$3
BASQ=$4
echo $BAM_LIST
samtools mpileup \
    --min-MQ ${MAPQ} \
    --min-BQ ${BASQ} \
    --fasta-ref ${FNAME_REF} \
    --bam-list ${BAM_LIST} > ${BAM_LIST%_bam.list*}.mpileup
' > 03_bam_to_mpileup.sh
chmod +x 03_bam_to_mpileup.sh
time \
parallel ./03_bam_to_mpileup.sh {} ${REF} ${MAPQ} ${BASQ} ::: $(ls *_bam.list)


### Clean-up
mv *.mpileup ${DIR_MPILEUP}/
mv *_bam.list ${DIR_MPILEUP}/
# rm 03_bam_to_mpileup.sh


# ### Extracting depth across the genome
# MPILEUP=/data/Lolium/Quantitative_Genetics/04_MPILEUP/CLETH_MERGED_ALL.mpileup
# time \
# cut -f$(seq 4 3 $(head -n1 $MPILEUP | awk '{print NF}') | sed -z 's/\n/,/g' | sed "s/,$//g") \
#     ${MPILEUP} > DEPTHS.txt
