#!/bin/bash

###########################################
### Synchronize the piled up alignments ###
###########################################

### Inputs:
DIR_MPILEUP=$1  ### (1) input directory where the piled-up alignments are located (*.mpileup);
DIR_SYNC=$2     ### (2) output directory where we want to save the  synchronized mpileup files (*.sync);
DIR_POPOO=$3    ### (3) popoolation2 directory
BASQ=$4         ### (4) minimuim base calling quality (PHRED = -10log_10(accuracy)) to filter the SNPs with
NTHREADS=$5     ### (5) number of computing threads to use per mpileup to sync conversion (i.e. per job)
# DIR_MPILEUP=/data/Lolium/Quantitative_Genetics/04_MPILEUP
# DIR_SYNC=/data/Lolium/Quantitative_Genetics/05_SYNC
# DIR_POPOO=/data/Lolium/Softwares/popoolation2_1201
# BASQ=20
# NTHREADS=8

### Set the number of parallel jobs to run based on the number of cores available and the number of thtreads per job set,
### as well as the amount of memory per job given the total RAM available and the number parallel jobs.
NTOTAL_THREADS=$(nproc --all)
# TOTAL_MEMGB=$(free -h | head -n2 | tail -n1 | sed 's/ \+ /\t/g' | cut -f7 | sed 's/Gi//g')
# NJOBS=$(echo "$NTOTAL_THREADS / $NTHREADS" | bc)
# MEMGB=$(echo "$TOTAL_MEMGB / $NJOBS" | bc)

# ### Synchronize
# time \
# parallel --jobs ${NJOBS} --link \
# java -ea -Xmx${MEMGB}g -jar \
# ${DIR_POPOO}/mpileup2sync.jar \
#     --input {1} \
#     --output {2}.sync \
#     --fastq-type sanger \
#     --min-qual ${BASQ} \
#     --threads ${NTHREADS} \
# ::: $(find ${DIR_MPILEUP}/*.mpileup) \
# ::: $(find ${DIR_MPILEUP}/*.mpileup | sed 's/.mpileup//g')

time \
for f in $(find ${DIR_MPILEUP}/*.mpileup)
do
    echo $f
    java -ea -Xmx${MEMGB}g -jar \
    ${DIR_POPOO}/mpileup2sync.jar \
        --input ${f} \
        --output ${f%.mpileup*}.sync \
        --fastq-type sanger \
        --min-qual ${BASQ} \
        --threads ${NTOTAL_THREADS}
done



### Clean-up
mv ${DIR_MPILEUP}/*.sync ${DIR_SYNC}/



# ############################
# ### Test using grenedelf ###
# ############################
# DIR=/data/Lolium/Softwares
# cd $DIR
# git clone --recursive https://github.com/lczech/grenedalf.git
# cd grenedalf
# make

# GRENEDALF=${DIR}/grenedalf/bin/grenedalf

# DIR_INPUT=/data/Lolium/Quantitative_Genetics
# time \
# $GRENEDALF sync-file \
#     --pileup-file ${DIR_INPUT}/04_MPILEUP/CLETH_MERGED_ALL.mpileup \
#     --out-dir ${DIR_INPUT} \
#     --verbose \
#     --threads 30
