#!/bin/bash

#############################################
### Pool-GWAS using intra-population data ###
#############################################

### Inputs:
DIR_SYNC=$1     ### (1) input and output directory of synchronized pileup files files (*.sync) which have been
                ###     filtered by MAF and DEPTH;
DIR_PHEN=$2     ### (2) directory where the phenotype data are located (*.py)
DIR_GWAS=$3     ### (3) output directory for Pool-GWAS
# DIR_SYNC=/data/Lolium/Quantitative_Genetics/05_SYNC
# DIR_PHEN=/data/Lolium/Quantitative_Genetics/01_PHENOTYPES
# DIR_GWAS=/data/Lolium/Quantitative_Genetics/06_GWAS

### SNP-wise Pool-GWAS (i.e. GWAlpha)
### on INTRA-POPULATION DATASETS
echo 'using Distributed
      Distributed.addprocs(Int(length(Sys.cpu_info())/4))
      @everywhere using GWAlpha
      # ARGS = ["/data/Lolium/Quantitative_Genetics/05_SYNC/GLYPH_WITHIN_ACC62_MAF0.001_DEPTH50.sync",
      #         "/data/Lolium/Quantitative_Genetics/01_PHENOTYPES/GLYPH_WITHIN_ACC62_pheno.py"]
      fname_sync = ARGS[1]
      fname_phen = ARGS[2]
      if split(basename(fname_sync), "_")[1:3] != split(basename(fname_phen), "_")[1:3]
        println("Mismatched genotype and phenotype files:")
        println(string("\tGENOTYPE FILE: ", basename(fname_sync)))
        println(string("\tPHENOTYPE FILE: ", basename(fname_phen)))
        exit()
      end
      maf = parse(Float64, split(split(basename(fname_sync), "_")[4], "MAF")[2])
      depth = parse(Int64, split(split(split(basename(fname_sync), "_")[5], "DEPTH")[2], ".sync")[1])
      @time GWAlpha.PoolGPAS(filename_sync=fname_sync, filename_phen=fname_phen, maf=maf, depth=depth, plot=true)
' > 07_intra_poolgwas.jl
time \
parallel -j 4 --link \
julia 07_intra_poolgwas.jl {1} {2} ::: $(find ${DIR_SYNC}/*_WITHIN_*MAF0.001_DEPTH10.sync | sort) \
                                   ::: $(find ${DIR_PHEN}/*_WITHIN_*_pheno.py | sort)

### Pool-GWAS on all SNPs simultaneously (i.e. MIXED_REML)
### on INTER-POPULATION DATASETS
echo 'using GWAlpha
      # ARGS = ["/data/Lolium/Quantitative_Genetics/05_SYNC/TERBU_MERGED_ALL_MAF0.001_DEPTH50.sync",
      #         "/data/Lolium/Quantitative_Genetics/01_PHENOTYPES/TERBU_MERGED_ALL_pheno.csv",
      #         "/data/Lolium/Quantitative_Genetics/05_SYNC/TERBU_MERGED_ALL_MAF0.001_DEPTH50_COVARIATE_FST.csv"]
      fname_sync = ARGS[1]
      fname_phen = ARGS[2]
      fname_cova = ARGS[3]
      if split(basename(fname_sync), "_")[1:3] != split(basename(fname_phen), "_")[1:3] != split(basename(fname_cova), "_")[1:3]
        println("Mismatched genotype and phenotype files:")
        println(string("\tGENOTYPE FILE: ", basename(fname_sync)))
        println(string("\tPHENOTYPE FILE: ", basename(fname_phen)))
        exit()
      end
      maf = parse(Float64, split(split(basename(fname_sync), "_")[4], "MAF")[2])
      depth = parse(Int64, split(split(split(basename(fname_sync), "_")[5], "DEPTH")[2], ".sync")[1])
      @time GWAlpha.PoolGPAS(filename_sync=fname_sync,
                             filename_phen=fname_phen,
                             model="MIXED",
                             filename_random_covariate=fname_cova,
                             varcomp_est="REML",
                             maf=maf, depth=depth, plot=true)
' > 07_inter_poolgwas.jl
time \
parallel --link \
julia 07_inter_poolgwas.jl {1} {2} {3} ::: $(find ${DIR_SYNC}/*_MERGED_ALL_MAF0.001_DEPTH10.sync | sort) \
                                       ::: $(find ${DIR_PHEN}/*_MERGED_ALL_pheno.csv | sort) \
                                       ::: $(find ${DIR_SYNC}/*_MERGED_ALL_MAF0.001_DEPTH10_COVARIATE_FST.csv | sort)

### clean-up
mv ${DIR_SYNC}/*OUTPUT* ${DIR_GWAS}/
rm 07_intra_poolgwas.jl 07_inter_poolgwas.jl


# #### compute LMM R-squared-adjusted
# R
# for (herbi in c("CLETH", "GLYPH", "SULFO", "TERBU")){
#   # herbi="CLETH"
#   fname_vevu = paste0("/data/Lolium/Quantitative_Genetics/06_GWAS/", herbi, "_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-VARCOMP_Ve_Vu-OUTPUT.csv")
#   fname_vevu = paste0(herbi, "_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-VARCOMP_Ve_Vu-OUTPUT.csv")
#   fname_phen = paste0("/data/Lolium/Quantitative_Genetics/01_PHENOTYPES/", herbi, "_MERGED_ALL_pheno.csv")

#   VARCOMP = read.csv(fname_vevu, header=FALSE)
#   PHEN = read.csv(fname_phen, header=FALSE)

#   Ve = VARCOMP$V1[1]
#   Vu = VARCOMP$V1[2]
#   print(herbi)
#   print(1 - (Ve/Vy))
# }
# ### OUTPUT
# CLETH = 0.004233525
# GLYPH = 0.1648469
# SULFO = 0.4595772
# TERBU = 0.196746