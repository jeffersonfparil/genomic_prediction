#!/bin/bash

##############################################################
### Align the Illumina short-reads to the reference genome ###
##############################################################

### Inputs:
DIR_FASTQ=$1    ### (1) directory where the Illumina paired-ed short-read sequences (*.fastq.gz) are located (suffixes: _combined_R1.fastq.gz and _combined_R2.fastq.gz);
DIR_BAM=$2      ### (2) output directory where we want to save the alignments (*.bam);
REF=$3          ### (3) reference genome prefix-no extension name (indexed with bwa); and
MAPQ=$4         ### (4) minimum PHRED mapping quality (PHRED = -10log_10(accuracy))
# DIR_FASTQ=/data/Lolium/Quantitative_Genetics/02_FASTQ
# DIR_BAM=/data/Lolium/Quantitative_Genetics/03_BAM
# REF=/data/Lolium/Quantitative_Genetics/02_FASTQ/REFERENCE/Reference
# MAPQ=20

### Create the parallelisable alignment script
echo -e '#!/bin/bash
FNAME_READ1=$1
FNAME_READ2=$2
BASENAME_REF=$3
MAPQ=$4
FNAME_OUT=$(basename $FNAME_READ1 | sed s/_combined_R1.fastq.gz//g)
bwa mem ${BASENAME_REF} ${FNAME_READ1} ${FNAME_READ2} | \
    samtools view -b -q ${MAPQ} --reference ${BASENAME_REF}.fasta | \
    samtools sort > ${FNAME_OUT}.bam
' > 02_parallel_align.sh
chmod +x 02_parallel_align.sh

### Align the short-read sequences into the reference genome to generata bam files
### (Note: will probably need to tweak the number of parallel jons to make sure we have enough RAM to run them in parallel.)
time \
parallel -j 30 --link ./02_parallel_align.sh \
            {1} \
            {2} \
            ${REF} \
            ${MAPQ} \
            ::: $(find ${DIR_FASTQ}/*_combined_R1.fastq.gz | sort) \
            ::: $(find ${DIR_FASTQ}/*_combined_R2.fastq.gz | sort)

### Clean-up
mv *.bam ${DIR_BAM}/
# rm 02_parallel_align.sh


### FINDINGS (2022-02-16):
echo '(1) Using the reads as if they were unpaired does not work!'
echo '(2) The LOL-WGS reads are mapping more reads to the Lope genome than the Lori gene it was used to error correct on during assembly!'
echo '(3) Re-doing remapping, piling up, syncing, and SNP filtering'

# ###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ### aligning as if the reads were not paired-end
# REF=/data/Lolium/Quantitative_Genetics/02_FASTQ/REFERENCE/Reference
# DIR_FASTQ=/data/Lolium/Quantitative_Genetics/02_FASTQ
# FNAME_READ1=${DIR_FASTQ}/ACC09_combined_R1.fastq.gz
# FNAME_READ2=${DIR_FASTQ}/ACC09_combined_R2.fastq.gz

# echo -e '#!/bin/bash
# FNAME_READ1=$1
# FNAME_READ2=$2
# FNAME_REF=$3
# MAPQ=$4
# FNAME_OUT=$(basename $FNAME_READ1 | sed s/_combined_R1.fastq.gz//g)
# zcat ${FNAME_READ1} ${FNAME_READ2} > ${FNAME_OUT}.fastq
# bwa mem ${FNAME_REF} ${FNAME_OUT}.fastq | \
#     samtools view -q ${MAPQ} -b | \
#     samtools sort > ${FNAME_OUT}.bam
# ' > 02_parallel_align_NOT_PAIRED_END.sh
# chmod +x 02_parallel_align_NOT_PAIRED_END.sh
# time \
# ./02_parallel_align_NOT_PAIRED_END.sh \
#     ${FNAME_READ1} \
#     ${FNAME_READ2} \
#     ${REF} \
#     20

# mkdir UNPAIRED/
# FNAME_OUT=$(basename $FNAME_READ1 | sed s/_combined_R1.fastq.gz//g)
# mv ${FNAME_OUT}.fastq UNPAIRED/
# mv ${FNAME_OUT}.bam UNPAIRED/

# cd UNPAIRED/
# time samtools stats ${FNAME_OUT}.bam > ${FNAME_OUT}.stat
# plot-bamstats ${FNAME_OUT}.stat -p ${FNAME_OUT}
# samtools view -F 0x0004 ${FNAME_OUT}.bam | cut -f 1 | sort | uniq | wc -l ### 1,833,679
# samtools view -f 0x0400 ${FNAME_OUT}.bam | cut -f 1 | sort | uniq | wc -l ###         0
# samtools flagstat ${FNAME_OUT}.bam > ${FNAME_OUT}.flagstat
# zcat $FNAME_READ1 | grep "^@" - | wc -l                                   ### 6,727,181

# time samtools stats ${FNAME_OUT}.bam > ${FNAME_OUT}.stat
# plot-bamstats ${FNAME_OUT}.stat -p ${FNAME_OUT}
# samtools view -F 0x0004 ${FNAME_OUT}.bam | cut -f 1 | sort | uniq | wc -l ###     917,364
# samtools view -f 0x0400 ${FNAME_OUT}.bam | cut -f 1 | sort | uniq | wc -l ###           0
# samtools flagstat ${FNAME_OUT}.bam > ${FNAME_OUT}.flagstat
     


# ###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ### Looking at bam statistics
# DIR=/data/Lolium/Quantitative_Genetics
# cd $DIR
# BAM=ACC09.bam ### ACC09 mapped to our new Lolium rigidum genome assembly
# # BAM=BK/03_BAM/ACC09.bam ### ACC09 mapped to the old Lolium perenne assembly

# BAMBASE=$(echo ${BAM%.bam*})

# time samtools stats ${BAMBASE}.bam > ${BAMBASE}.stat
# plot-bamstats ${BAMBASE}.stat -p ${BAMBASE}

# samtools view -F 0x0004 ${BAMBASE}.bam | cut -f 1 | sort | uniq | wc -l     ###     4,047,565 reads mapped          (Lope REF aligned got 6,255,374 mapped reads!!!!)
# samtools view -f 0x0400 ${BAMBASE}.bam | cut -f 1 | sort | uniq | wc -l     ###             0 reads unmapped???!!!
# # samtools flagstat ${BAMBASE}.bam > ${BAMBASE}.flagstat
# ### ^-- does not give the unique reads mapped

# FASTQ=$(ls ${DIR}/02_FASTQ/$(basename $BAMBASE)_combined* | head -n1)
# zcat $FASTQ | rg "^@" - | wc -l                                             ###     8,904,498 reads


# samtools index ${BAM}
# MIN_COVERAGE_DEPTH=10
# samtools mpileup ${BAM} | awk -v X="${MIN_COVERAGE_DEPTH}" '$4>=X' | wc -l  ###     5,440,100 bp (Lope REF aligned got 5,081,147 bp coverage)
# ### Whole L. rigium assembly size =                                             2,438,467,165 bp

# rm ${BAMBASE}*.png ${BAMBASE}*.gp ${BAMBASE}*.html ${BAMBASE}*.*stat

# # From bwa:
# # Chr	Flag	Description
# # p	0x0001	the read is paired in sequencing
# # P	0x0002	the read is mapped in a proper pair
# # u	0x0004	the query sequence itself is unmapped
# # U	0x0008	the mate is unmapped
# # r	0x0010	strand of the query (1 for reverse)
# # R	0x0020	strand of the mate
# # 1	0x0040	the read is the first read in a pair
# # 2	0x0080	the read is the second read in a pair
# # s	0x0100	the alignment is not primary
# # f	0x0200	QC failure
# # d	0x0400	optical or PCR duplicat





# ### ALIGNING GENOME ASSEMBLY ILLUMINA READS
# DIR_ILLUMINA_ASSEMBLY_READS=/data/Lolium_rigidum_ASSEMBLY/assembly_annotation_pipeline_tests_20210104/FASTQ/ILLUMINA
# READ1=${DIR_ILLUMINA_ASSEMBLY_READS}/LOL-WGS-1.0_combined_R1.fastq.gz
# READ2=${DIR_ILLUMINA_ASSEMBLY_READS}/LOL-WGS-1.0_combined_R2.fastq.gz

# DIR=/data/Lolium/Quantitative_Genetics
# cd ${DIR}
# #########################################
# ### USING LORI ##########################
# #########################################
# REF=${DIR}/02_FASTQ/REFERENCE/Reference
# BAM=LOL-WGS-1.0_on_Lrigidum.bam
# time \
# bwa mem ${REF} ${READ1} ${READ2}| \
#     samtools view -q 20 -b | \
#     samtools sort > ${BAM}

# BAMBASE=$(echo ${BAM%.bam*})
# time samtools stats ${BAMBASE}.bam > ${BAMBASE}.stat
# plot-bamstats ${BAMBASE}.stat -p ${BAMBASE}
# samtools view -F 0x0004 ${BAMBASE}.bam | cut -f 1 | sort | uniq | wc -l ### 34,119,834
# samtools view -f 0x0400 ${BAMBASE}.bam | cut -f 1 | sort | uniq | wc -l ###          0
# samtools flagstat ${BAMBASE}.bam > ${BAMBASE}.flagstat
# FASTQ=$(ls ${DIR}/02_FASTQ/$(basename $BAMBASE)_combined* | head -n1)
# zcat $FASTQ | grep "^@" - | wc -l                                       ### 55,060,660
# #########################################
# ### USING LOPE ##########################
# #########################################
# REF=${DIR}/02_FASTQ/REFERENCE/BK_Lperenne/Reference
# BAM=LOL-WGS-1.0_on_Lperenne.bam
# time \
# bwa mem ${REF} ${READ1} ${READ2}| \
#     samtools view -q 20 -b | \
#     samtools sort > ${BAM}

# BAMBASE=$(echo ${BAM%.bam*})
# time samtools stats ${BAMBASE}.bam > ${BAMBASE}.stat
# plot-bamstats ${BAMBASE}.stat -p ${BAMBASE}
# samtools view -F 0x0004 ${BAMBASE}.bam | cut -f 1 | sort | uniq | wc -l ### 37,483,184
# samtools view -f 0x0400 ${BAMBASE}.bam | cut -f 1 | sort | uniq | wc -l ###          0
# samtools flagstat ${BAMBASE}.bam > ${BAMBASE}.flagstat



