#!/bin/bash

###############################################################################################
### Prepare the phenotype and genotype data as well as estimate Fst for the merged datasets ###
###############################################################################################

### Inputs:
DIR_SYNC=$1     ### (1) input and output directory of synchronized pileup files files (*.sync) which have been
                ###     filtered by MAF and DEPTH, and
                ###     for the merged datasets only;
DIR_MPILEUP=$2  ### (2) directory where the list of piled up bam files are located;
PHENO=$3        ### (3) phenotype file for grouping the bam alingments based on herbicide treatments (comma-separated; HEADER:YEAR,SAMPLING,POP_ID,POPULATION,POOL,POOL_SIZE,HERBICIDE,QUANT_RES,SURVIVAL_RATE)
# DIR_SYNC=/data/Lolium/Quantitative_Genetics/05_SYNC
# DIR_MPILEUP=/data/Lolium/Quantitative_Genetics/04_MPILEUP
# PHENO=/data/Lolium/Quantitative_Genetics/01_PHENOTYPES/ALL_PHENO.csv

### prepare the phenotype data for the merged datasets
echo 'args = commandArgs(trailingOnly=TRUE)
      # args = c("/data/Lolium/Quantitative_Genetics/04_MPILEUP/CLETH_MERGED_ALL_bam.list", "/data/Lolium/Quantitative_Genetics/01_PHENOTYPES/ALL_PHENO.csv")
      # args = c("/data/Lolium/Quantitative_Genetics/04_MPILEUP/GLYPH_MERGED_ALL_bam.list", "/data/Lolium/Quantitative_Genetics/01_PHENOTYPES/ALL_PHENO.csv")
      # args = c("/data/Lolium/Quantitative_Genetics/04_MPILEUP/TRIFL_MERGED_ALL_bam.list", "/data/Lolium/Quantitative_Genetics/01_PHENOTYPES/ALL_PHENO.csv")
      fname_bam_list = args[1]
      fname_pheno = args[2]
      herbi = unlist(strsplit(basename(fname_bam_list), "_"))[1]
      ### read and parse the population ID from the bam filenames
      bam_list = gsub(".bam", "", basename(as.character(read.delim(fname_bam_list, header=FALSE)$V1)))
      ### define the sampling type
      bam_sampling = rep("ACROSS", times=length(bam_list))
      bam_sampling[grepl("Pool", bam_list) | (nchar(bam_list)==3)] = "WITHIN"
      ### define the population names and pool ID
      bam_population = bam_list
      bam_pool = rep(1, times=length(bam_list))
          ### For the ACC* populations:
          if (length(strsplit(bam_population[grepl("-Pool", bam_population)], "-Pool"))>0){
            MAT_pop_pool = matrix(unlist(strsplit(bam_population[grepl("-Pool", bam_population)], "-Pool")), ncol=2, byrow=TRUE)
            bam_population[grepl("-Pool", bam_list)] = MAT_pop_pool[,1]
            bam_pool[grepl("-Pool", bam_list)] = MAT_pop_pool[,2]
          }
          ### For the Inverleigh and Urana populations:
          if (length(bam_population[nchar(bam_population)==3])>0){
            inv_ura_list = matrix(unlist(strsplit(bam_population[nchar(bam_population)==3], "")), ncol=3, byrow=TRUE)[,1]
            inv_ura_list[inv_ura_list=="I"] = "INVERLEIGH"
            inv_ura_list[inv_ura_list=="U"] = "URANA"
            bam_pool[nchar(bam_population)==3] = matrix(unlist(strsplit(bam_population[nchar(bam_population)==3], "")), ncol=3, byrow=TRUE)[,3]
            bam_population[nchar(bam_population)==3] = inv_ura_list
          }
      ### merge for subsetting the phenotype file
      BAM_ID = data.frame(FNAME=bam_list, SAMPLING=bam_sampling, POPULATION=bam_population, POOL=bam_pool)
      ### load the phenotype data and retain only the herbicide resistance trait of interest
      PHENO = read.csv(fname_pheno)
      PHENO = droplevels(PHENO[substr(PHENO$HERBICIDE, start=1, stop=5)==herbi, ])
      ### merge bam list ID with the phenotype data, sort according to the original bam list and prepare output data
      OUT_TEMP = merge(BAM_ID, PHENO, by=c("SAMPLING", "POPULATION", "POOL"))
      OUT_PHENO = OUT_TEMP
      for (i in 1:length(bam_list)){
          f = bam_list[i]
          OUT_PHENO[i, ] = OUT_TEMP[OUT_TEMP$FNAME==f, ]
      }
      OUT_PHENO = OUT_PHENO[, !grepl("FNAME", colnames(OUT_PHENO))]
      OUT_CSV = cbind(OUT_PHENO$POOL_SIZE, OUT_PHENO$SURVIVAL_RATE)
      ### write-out
      write.table(OUT_PHENO, file=paste0(dirname(fname_pheno), "/", herbi, "_MERGED_ALL.pheno"),       sep=",", row.names=FALSE, col.names=TRUE,  quote=FALSE)
      write.table(OUT_CSV,   file=paste0(dirname(fname_pheno), "/", herbi, "_MERGED_ALL_pheno.csv"),   sep=",", row.names=FALSE, col.names=FALSE, quote=FALSE)
' > 06_extract_pheno.r
time \
parallel \
Rscript 06_extract_pheno.r {} ${PHENO} ::: $(find ${DIR_MPILEUP}/*MERGED*_bam.list)
### outputs:
### (1) $(dirname $PHENO)/${herbi}_MERGED_ALL.pheno
### (2) $(dirname $PHENO)/${herbi}_MERGED_ALL_pheno.csv

### parse synchronised pileup file and compute Fst
echo "
    using DelimitedFiles
    using Distributed
    Distributed.addprocs(Int(length(Sys.cpu_info())/4))
    @everywhere using GWAlpha
    # ARGS = [\"/data/Lolium/Quantitative_Genetics/05_SYNC/GLYPH_MERGED_ALL_MAF0.001_DEPTH50.sync\", \"/data/Lolium/Quantitative_Genetics/01_PHENOTYPES/MERGED_GLYPH.csv\"]
    sync_fname = ARGS[1]
    phen_fname = ARGS[2]
    pool_sizes = convert(Array{Int64}, DelimitedFiles.readdlm(phen_fname, ',')[:,1])
    ### parse
    GWAlpha.sync_processing_module.sync_parse(sync_fname)
    ### estimate Fst
    # GWAlpha.relatedness_module.Fst_pairwise(filename_sync=sync_fname, window_size=100000, pool_sizes=pool_sizes, METHOD=\"Hivert\")
    GWAlpha.relatedness_module.Fst_pairwise(filename_sync=sync_fname, window_size=100000, pool_sizes=pool_sizes, METHOD=\"WeirCock\")
" > 06_parse_and_fst.jl
time \
parallel --jobs 4 --link \
julia 06_parse_and_fst.jl {1} {2} ::: $(find ${DIR_SYNC}/*_MERGED_*_MAF*DEPTH*.sync | sort) \
                                  ::: $(find $(dirname $PHENO)/*_MERGED_ALL_pheno.csv | sort)
### output:
### (1) ${DIR_SYNC}/*_MERGED_*_MAF*DEPTH*_COVARIATE_FST.csv

### clean-up
# rm 06_extract_pheno.r 06_parse_and_fst.jl
