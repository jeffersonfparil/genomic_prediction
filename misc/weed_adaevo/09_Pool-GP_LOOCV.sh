#!/bin/bash

##############################################
### Leave-One-Out Cross-Validation (LOOCV) ###
##############################################

### Inputs:
DIR_SYNC=$1     ### (1) directory containing the sync file and the corresponding parsed allele frequency files
DIR_PHEN=$2     ### (2) direciory containing the phenotype csv files
DIR_SRC=$3      ### (3) directory containing the 09_Pool-GP_LOOCV.jl script
DIR_GP=$4       ### (4) output directory for genomic prediction LOOCV
# DIR_SYNC=/data/Lolium/Quantitative_Genetics/05_SYNC
# DIR_PHEN=/data/Lolium/Quantitative_Genetics/01_PHENOTYPES
# DIR_SRC=/data/Lolium/Softwares/genomic_prediction/misc/weed_adaevo
# DIR_GP=/data/Lolium/Quantitative_Genetics/07_GP

time \
parallel \
julia ${DIR_SRC}/09_Pool-GP_LOOCV.jl ${DIR_SYNC} ${DIR_PHEN} {} ::: CLETH GLYPH SULFO TERBU TRIFL

### plot predicted values against observed values for each herbicide resistance trait
echo '
    args = commandArgs(trailingOnly=TRUE)
    # args = c("/data/Lolium/Quantitative_Genetics/05_SYNC", "CLETH")
    dir = args[1]
    herbi = args[2]
    setwd(dir)
    if (herbi=="CLETH"){
        herbi_name = "Clethodim"
    } else if (herbi=="GLYPH"){
        herbi_name = "Glyphosate"
    } else if (herbi=="SULFO"){
        herbi_name = "Sulfometuron"
    } else if (herbi=="TERBU"){
        herbi_name = "Terbuthylazine"
    }
    fname = paste0(herbi, "_LOOCV_RAW.csv")
    dat = read.csv(fname)
    levels(dat$MODEL) = c("RIDGE", "LASSO", "MIXED_FST")
    train_size = (nrow(dat)/3) -1
    ### restrict predicted values between 0 and 1
    dat$Y_PRED[dat$Y_PRED < 0] = 0
    dat$Y_PRED[dat$Y_PRED > 1] = 1
    ### colors
    colors_points = c("#7bccc4", "#fecc5c", "#bae4b3")
    colors_lines = c("#0868ac", "#fd8d3c", "#31a354")
    ### plot
    svg(paste0(herbi, "_LOOCV.svg"), width=7, height=7)
    plot(x=c(0,1), y=c(0,1), type="n", xlab=paste0("Observed ", herbi_name, " Resistance"), ylab=paste0("Predicted ", herbi_name, " Resistance"))
    grid(col="gray")
    R2=c(); CORR=c(); MAD=c(); RMSE=c()
    for (i in 1:nlevels(dat$MODEL)){
        # i = 1
        mod = levels(dat$MODEL)[i]
        sub = dat[dat$MODEL==mod, ]
        points(x=sub$Y_TRUE, y=sub$Y_PRED, pch=20, col=colors_points[i])
        reg_mod = lm(sub$Y_PRED ~ sub$Y_TRUE)
        abline(reg_mod, col=colors_lines[i], lwd=2)
        R2 = c(R2, summary(reg_mod)$adj.r.sq)
        CORR = c(CORR, cor(sub$Y_PRED, sub$Y_TRUE))
        MAD = c(MAD, mean(abs(sub$Y_PRED-sub$Y_TRUE)))
        RMSE = c(RMSE, mean(sqrt((sub$Y_PRED-sub$Y_TRUE)^2)))
    }
    legend("topleft", legend=c(paste0(levels(dat$MODEL), ": R2=", round(R2*100, 2), "%; RMSE=", round(RMSE,4)), paste0("Training Size=", train_size)), fill=c(colors_points, NA), bord=c(colors_lines, NA))
    dev.off()
' > 09_anal_loocv.r
time \
parallel \
Rscript 09_anal_loocv.r ${DIR_SYNC} {} ::: CLETH GLYPH SULFO TERBU

### Clean-up
rm ${DIR_SYNC}/TRAIN* ${DIR_SYNC}/VALID*
mv ${DIR_SYNC}/*LOOCV* ${DIR_GP}