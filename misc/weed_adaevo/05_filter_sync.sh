#!/bin/bash

########################################################################################################
### Filter synchronized piled up alignments by minimum allele frequency and minimum depth thresholds ###
########################################################################################################

### Inputs:
DIR_SYNC=$1     ### (1) input and output directory of the synchronized pileup files files (*.sync);
MAF=$2          ### (2) minimuim allele threshold
DEPTH=$3        ### (3) minimuim depth threshold
# DIR_SYNC=/data/Lolium/Quantitative_Genetics/05_SYNC
# MAF=0.001
# DEPTH=10

### prepare the julia script with GWAlpha.jl
echo "
using GWAlpha
GWAlpha.sync_processing_module.sync_filter(filename_sync=ARGS[1], MAF=parse(Float64, ARGS[2]), DEPTH=parse(Int64, ARGS[3]))
" > 05_filter_sync.jl

### execute in parallel
time \
parallel \
julia 05_filter_sync.jl {} ${MAF} ${DEPTH} ::: $(find ${DIR_SYNC}/*.sync)

### clean-up
# rm 05_filter_sync.jl
