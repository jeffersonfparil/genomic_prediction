# SOFTWARE INSTALLATION

## Install GNU-parallel

```bash
sudo apt install parallel
```

## Install bwa, samtools, bcftools, vcftools, and ncbi-blast+

```bash
sudo apt install bwa samtools bcftools vcftools ncbi-blast+
```

## Install Python3, pip3, and the following Python packages

```bash
sudo apt install python3-pip
pip3 install biopython --user
```

## Install java and popoolation2

```bash
sudo apt install default-jre
wget https://sourceforge.net/projects/popoolation2/files/popoolation2_1201.zip
unzip popoolation2_1201.zip -d popoolation2_1201
rm popoolation2_1201.zip
```

## Install R, and the following R packages

```r
install.packages("glmnet")
install.packages("RColorBrewer")
```

## Install Julia, and the following Julia packages

```julia
Pkg.add(["CSV",
          "Distributions",
          "DataFrames",
          "DelimitedFiles",
          "LinearAlgebra",
          "Optim",
          "UnicodePlots",
          "ColorBrewer",
          "ProgressMeter",
          "Statistics",
          "StatsBase",
          "Distributed",
          "SharedArrays",
          "GeneticVariation",
          "RCall"
        ])
Pkg.add(PackageSpec(url="https://github.com/jeffersonfparil/GWAlpha.jl.git", rev="master"))
```

## Intialise the project directories, and copy the sequence and phenotype data into their respective folders

```bash
mkdir 01_PHENOTYPES/
mkdir 02_FASTQ/; mkdir mkdir 02_FASTQ/REFERENCE/
mkdir 03_BAM/
mkdir 04_MPILEUP/
mkdir 05_SYNC/
mkdir 06_GWAS/
mkdir 07_GP/
mkdir 08_BIAS/
```