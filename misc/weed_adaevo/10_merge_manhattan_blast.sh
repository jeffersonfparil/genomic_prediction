### highlighting BLAST output in the Manhattan plots
DIR=/data/Lolium/Quantitative_Genetics/06_GWAS
### Outputs:
### (1) MERGED_ALL_GWAS_OUTPUT.csv
### (2) ${herbi}_${popname}_MANHATTAN_PLOTS.jpeg
### (3) ${herbi}_${popname}_MANHATTAN_PEAKS.svg

echo '
    args = commandArgs(trailingOnly=TRUE)
    # args = c("/data/Lolium/Quantitative_Genetics/06_GWAS")
    DIR = args[1]
    ### set the working directory
    setwd(DIR)
    ### load GWAS ouput files
    print("Merging the GWAS outputs:")
    fnames_list = system("find *-OUTPUT.csv | grep -v TRIFL | grep -v RANEF", intern=TRUE)
    # fnames_list = system("find GWAS_PER_HERBI_PER_POP/*-GWAlpha-OUTPUT.csv | grep -v TRIFL", intern=TRUE)
    herbi_list = c()
    popnames_list = c()
    for (f in fnames_list){
        herbi = unlist(strsplit(basename(f), "_"))[1]
        popname = unlist(strsplit(basename(f), "_"))[3]
        herbi_list = c(herbi_list, herbi)
        popnames_list = c(popnames_list, popname)
        print(paste0(herbi, "-", popname))
        dat = read.csv(f)
        colnames(dat)[4:ncol(dat)] = paste0(herbi, "_", popname, "_", colnames(dat)[4:ncol(dat)])
        if (!exists("GWAS")){
            GWAS = dat
        } else {
            GWAS = merge(GWAS, dat, by=c("CHROM", "POS", "ALLELE"), all=TRUE)
        }
    }
    GWAS = GWAS[order(GWAS$ALLELE), ]
    GWAS = GWAS[order(GWAS$POS), ]
    GWAS = GWAS[order(GWAS$CHROM), ]
    GWAS$LOCUS = paste(GWAS$CHROM, GWAS$POS, sep="-")
    GWAS$IDX = 1:nrow(GWAS)
    write.table(GWAS, file="MERGED_ALL_GWAS_OUTPUT.csv", sep=",", row.names=FALSE, quote=FALSE)

    ##################
    ### JUST PEAKS ###
    ##################
    ### plot manhattan plots per population  highlight the peaks and 
    ### zoom in per scaffold for the top 5% scaffolds with max(-log10(p-value)) above the Bonferroni threshold
    print("Plotting:")
    colors_points = c("#41b6c4", "#a1dab4")
    colors_lines = c("red", rgb(0.2, 0.2, 0.2, alpha=0.10), "#feb24c")
    for (i in 1:length(herbi_list)){
        # i = 1
        herbi = herbi_list[i]
        popname = popnames_list[i]
        print(paste0(herbi, "-", popname))
        LOD=eval(parse(text=paste0("GWAS$", herbi, "_", popname, "_LOD")))
        gwas = data.frame(CHROM=GWAS$CHROM, POS=GWAS$POS, IDX=GWAS$IDX, LOD=LOD)
        nloci = length(unique(paste(GWAS$LOCUS[!is.na(LOD)])))
        p = sum(!is.na(gwas$LOD)) ### total number of alleles across loci (total number of predictors)
        alpha = 0.01
        bon_thresh = -log10(alpha/p)
        agg = aggregate(LOD ~ CHROM, data=gwas, FUN=max)
        agg = agg[order(agg$LOD, decreasing=TRUE), ]
        idx = agg$LOD >= bon_thresh
        scaff_above_thresh_0.05 = agg$CHROM[idx][1:round(sum(idx)*0.05)]
        agg = agg[order(agg$CHROM), ]
        agg$IDX = 1:nrow(agg)
        ### load blasted GWAS peaks
        BLASTOUT = read.delim(paste0(herbi, "-GFF3_PEAKS.oryza.txt"), header=FALSE)
        colnames(BLASTOUT)[1] = "seqid"
        BLASTOUT$CHROM = matrix(unlist(strsplit(as.character(BLASTOUT$seqid), "::")), ncol=2, byrow=TRUE)[,1]
        BLASTOUT$POS_INIT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(BLASTOUT$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 1])
        BLASTOUT$POS_SLUT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(BLASTOUT$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 2])
        idx_scaff_with_peaks = gwas$CHROM %in% gwas$CHROM[(gwas$LOD >= bon_thresh) & !is.na(gwas$LOD)]
        GWAS_SCAFF_PEAKS = droplevels(gwas[idx_scaff_with_peaks, ])
        GWAS_SCAFF_PEAKS = aggregate(LOD ~ CHROM +  POS, data=GWAS_SCAFF_PEAKS, FUN=max)
        min_pos = aggregate(POS ~ CHROM, dat=data.frame(CHROM=c(as.character(GWAS_SCAFF_PEAKS$CHROM), rep(as.character(BLASTOUT$CHROM), times=2)), POS=c(GWAS_SCAFF_PEAKS$POS, BLASTOUT$POS_INIT, BLASTOUT$POS_SLUT) ), FUN=min)
            colnames(min_pos)[2] = "MIN_POS"
        max_pos = aggregate(POS ~ CHROM, dat=data.frame(CHROM=c(as.character(GWAS_SCAFF_PEAKS$CHROM), rep(as.character(BLASTOUT$CHROM), times=2)), POS=c(GWAS_SCAFF_PEAKS$POS, BLASTOUT$POS_INIT, BLASTOUT$POS_SLUT) ), FUN=max)
            colnames(max_pos)[2] = "MAX_POS"
        pos_df = merge(min_pos, max_pos, by="CHROM")
        pos_df = pos_df[order(pos_df$CHROM), ]
        # cumsum_delta = cumsum(pos_df$MAX_POS - pos_df$MIN_POS)
        cumsum_delta = cumsum(pos_df$MAX_POS)
        pos_df$ADD_POS = c(0, cumsum_delta[1:(length(cumsum_delta)-1)])
        GWAS_SCAFF_PEAKS = merge(GWAS_SCAFF_PEAKS, pos_df, by="CHROM")
        min_LOD = min(GWAS_SCAFF_PEAKS$LOD,na.rm=TRUE)
        max_LOD = max(GWAS_SCAFF_PEAKS$LOD,na.rm=TRUE)
        n_loci_above_thresh = sum((GWAS_SCAFF_PEAKS$LOD>=bon_thresh) & !is.na(GWAS_SCAFF_PEAKS$LOD))
        n_scaff_with_blast_hits =  sum(BLASTOUT$CHROM %in% GWAS_SCAFF_PEAKS$CHROM)


        jpeg(paste0(herbi, "_", popname, "_MANHATTAN_PLOTS.jpeg"), quality=100, width=1000, height=900)
        par(mfrow=c(3,1))
        ### plot LOD of each allele across loci across scaffolds
        plot(x=gwas$IDX, y=gwas$LOD, type="n", xlab="Locus", ylab="-log10(p-value)", main=paste0("Number of loci = ", nloci, "\nNumber of alleles x loci = ", p), sub=paste0("Bonferroni threshold (alpha=", alpha, ") = ", round(bon_thresh, 2)))
        grid(col="gray")
        for (j in 1:nlevels(gwas$CHROM)){
            # j = 1
            sub = droplevels(gwas[gwas$CHROM==levels(gwas$CHROM)[j], ])
            points(x=sub$IDX, y=sub$LOD, col=colors_points[(j%%2)+1], pch=20)
        }
        abline(h=bon_thresh, col=colors_lines[1], lty=2, lwd=2)
        ### plot max(LOD) per scaffold
        plot(x=agg$IDX, y=agg$LOD, type="n", xlab="Scaffold", ylab="max(-log10(p-value))", main=paste0("Number of scaffolds = ", nrow(agg)))
        grid(col="gray")
        abline(v=agg$IDX[agg$CHROM %in% scaff_above_thresh_0.05], lty=1, lwd=6, col=colors_lines[2])
        points(x=agg$IDX, y=agg$LOD, pch=20, col=colors_points)
        abline(h=bon_thresh, col=colors_lines[1], lty=2, lwd=2)
        ### plot only scaffolds with peaks and overlay the blast hits
        plot(x=c(0,max(cumsum_delta)), y=c(min_LOD, max_LOD), type="n", main=paste0("Number of loci above the Bonferroni threshold = ", n_loci_above_thresh, "\nNumber of scaffolds with loci peaks within 1kb of a blast hit = ", n_scaff_with_blast_hits), xlab="Loci", ylab="-log10(p-value)")
        grid(col="gray")
        for (l in 1:length(unique(GWAS_SCAFF_PEAKS$CHROM))){
            # l = 1
            scaff = unique(GWAS_SCAFF_PEAKS$CHROM)[l]
            sub = droplevels(GWAS_SCAFF_PEAKS[GWAS_SCAFF_PEAKS$CHROM==scaff, ])
            blast = droplevels(BLASTOUT[BLASTOUT$CHROM==scaff, ])
            if (nrow(blast)>0){
                for (m in 1:nrow(blast)){
                    xleft = (blast$POS_INIT[m] - sub$MIN_POS[m]) + sub$ADD_POS[m]
                    xright = (blast$POS_SLUT[m] - sub$MIN_POS[m]) + sub$ADD_POS[m]
                    rect(xleft=xleft, xright=xright, ybottom=min_LOD, ytop=max_LOD, density=NA, col=colors_lines[3])
                }
            }
            x = (sub$POS - sub$MIN_POS) + sub$ADD_POS
            y = sub$LOD
            points(x, y, col=colors_points[(l%%2)+1], pch=20)
        }
        abline(h=bon_thresh, col=colors_lines[1], lty=2, lwd=2)
        dev.off()
        ### plot top 5% peaks
        layout_width = ceiling(sqrt(length(scaff_above_thresh_0.05)))
        layout_height = ceiling(length(scaff_above_thresh_0.05)/layout_width)
        svg(paste0(herbi, "_", popname, "_MANHATTAN_PEAKS.svg"), width=layout_width*4, height=layout_height*2.5)
        par(mfrow=c(layout_height, layout_width))
        for (k in 1:length(scaff_above_thresh_0.05)){
            # k = 1
            scaff = scaff_above_thresh_0.05[k]
            sub = droplevels(gwas[gwas$CHROM==scaff, ])
            plot(x=c(min(sub$POS), max(sub$POS)), y=c(min(gwas$LOD, na.rm=TRUE), max(gwas$LOD, na.rm=TRUE)), type="n", main=scaff, xlab="Position (bp)", ylab="-log10(p-value)")
            grid(col="gray")
            points(x=sub$POS, y=sub$LOD, col=colors_points[(k%%2)+1], pch=20)
            abline(h=bon_thresh, col=colors_lines[1], lty=2, lwd=2)
        }
        dev.off()
    }

    # ###########################################
    # ### merge peaks per herbicide treatment ###
    # ###########################################
    # for (i in 1:length(unique(herbi_list))){
    #     # i = 1
    #     herbi = unique(herbi_list)[i]
    #     popnames = popnames_list[herbi_list==herbi]
    #     print(paste(c(herbi, popnames), collapse="-"))
    #     for (j in 1:length(popnames)){
    #         # j = 1
    #         popname = popnames[j]
    #         print(paste0(herbi, "-", popname))
    #         if (exists("LOD")==FALSE){
    #             LOD=eval(parse(text=paste0("GWAS$", herbi, "_", popname, "_LOD")))
    #         } else {
    #             LOD = cbind(LOD, eval(parse(text=paste0("GWAS$", herbi, "_", popname, "_LOD"))))
    #         }
    #     }
    #     gwas = cbind(data.frame(GWAS$CHROM, GWAS$POS), LOD)
    #     colnames(gwas) = c("CHROM", "POS", paste0("LOD_", popnames))
    #     idx_2d = LOD >= matrix(rep(-log10(0.01/colSums(!is.na(LOD))), times=nrow(gwas)), nrow=nrow(gwas), byrow=TRUE)
    #     idx_2d[is.na(idx_2d)] = FALSE
    #     idx = (rowSums(idx_2d) == ncol(LOD)) & !is.na(apply(LOD, MARGIN=1, FUN=sum))
    #     gwas = gwas[idx, ]
    #     write.table(gwas, file=paste0(herbi, "_MERGED_ALL_PEAKS.csv"), sep=",", row.names=FALSE, col.names=TRUE, quote=FALSE)
    # }

    # ########################
    # ### pairwise merging ###
    # ######################## and highlight blast hits per scaffold
    # for (i in 1:length(unique(herbi_list))){
    #     # i = 1
    #     herbi = unique(herbi_list)[i]
    #     popnames = popnames_list[herbi_list==herbi]
    #     blastout = read.delim(paste0("BLASTOUT_", herbi, ".txt"), header=TRUE)
    #     blastout$CHROM = matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,1]
    #     blastout$POS_INIT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 1])
    #     blastout$POS_SLUT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 2])
    #     for (j in 1:(length(popnames)-1)){
    #         # j = 1
    #         popname1 = popnames[j]
    #         for (k in (j+1):length(popnames)){
    #             # k = j+2
    #             popname2 = popnames[k]
    #             print(paste(c(herbi, popname1, popname2), collapse="-"))
    #             LOD = cbind(eval(parse(text=paste0("GWAS$", herbi, "_", popname1, "_LOD"))),
    #                         eval(parse(text=paste0("GWAS$", herbi, "_", popname2, "_LOD"))))
    #             gwas = cbind(data.frame(GWAS$CHROM, GWAS$POS), LOD)
    #             colnames(gwas) = c("CHROM", "POS", paste0("LOD_", c(popname1, popname2)))
    #             idx_2d = LOD >= matrix(rep(-log10(0.01/colSums(!is.na(LOD))), times=nrow(gwas)), nrow=nrow(gwas), byrow=TRUE)
    #             idx_2d[is.na(idx_2d)] = FALSE
    #             idx = (rowSums(idx_2d) == ncol(LOD)) & !is.na(apply(LOD, MARGIN=1, FUN=sum))
    #             gwas = gwas[idx, ]
    #             write.table(gwas, file=paste0(herbi, "_MERGED_PAIR_PEAKS-", popname1, "_", popname2, ".csv"), sep=",", row.names=FALSE, col.names=TRUE, quote=FALSE)

    #             ### plot manhattan plots per scaffold and highlight blast hits
    #             if (nrow(gwas)>0){
    #                 scaff_list = unique(gwas$CHROM)
    #                 df = data.frame(CHROM=GWAS$CHROM, POS=GWAS$POS, LOD_1=LOD[,1], LOD_2=LOD[,2])
    #                 layout_width = ceiling(sqrt(length(scaff_list)))
    #                 layout_height = ceiling(length(scaff_list)/layout_width)
    #                 svg(paste0(herbi, "_MANHATTAN_PAIR_MERGED_", popname1, "-", popname2, ".svg"), width=layout_width*4, height=layout_height*2.5)
    #                 par(mfrow=c(layout_height, layout_width))
    #                 for (l in 1:length(scaff_list)){
    #                     # l = 1
    #                     scaff = scaff_list[l]
    #                     sub = df[df$CHROM==scaff, ]
    #                     blastout_sub = blastout[blastout$CHROM==scaff, ]
    #                     plot(x=c(0,max(c(sub$POS, blastout_sub$POS_SLUT))), y=c(min(LOD,na.rm=TRUE), max(LOD,na.rm=TRUE)), type="n", main=scaff, xlab="Position (bp)", ylab="-log10(p-value)")
    #                     grid(col="gray")
    #                     points(x=sub$POS, y=sub$LOD_1, col=colors_points[1], pch=20)
    #                     points(x=sub$POS, y=sub$LOD_2, col=colors_points[2], pch=20)
    #                     if(nrow(blastout_sub)>0){
    #                         for (m in 1:nrow(blastout_sub)){
    #                             rect(xleft=blastout_sub$POS_INIT[m], xright=blastout_sub$POS_SLUT[m], ybottom=min(LOD,na.rm=TRUE), ytop=max(LOD,na.rm=TRUE), density=NA, col=colors_lines[3])
    #                         }
    #                     }
    #                 }
    #                 dev.off()
    #             }
    #         }
    #     }
    # }

    # ### plot scaffolds with blast hits (blast hist from peaks)
    # for (i in 1:length(unique(herbi_list))){
    #     # i = 1
    #     herbi = unique(herbi_list)[i]
    #     popnames = popnames_list[herbi_list==herbi]
    #     blastout = read.delim(paste0("BLASTOUT_", herbi, ".txt"), header=TRUE)
    #     blastout$CHROM = matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,1]
    #     blastout$POS_INIT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 1])
    #     blastout$POS_SLUT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 2])
    #     print(paste(c(herbi, popnames), collapse="-"))
    #     rm(LOD)
    #     for (j in 1:length(popnames)){
    #         # j = 1
    #         popname = popnames[j]
    #         print(paste0(herbi, "-", popname))
    #         if (exists("LOD")==FALSE){
    #             LOD=eval(parse(text=paste0("GWAS$", herbi, "_", popname, "_LOD")))
    #         } else {
    #             LOD = cbind(LOD, eval(parse(text=paste0("GWAS$", herbi, "_", popname, "_LOD"))))
    #         }
    #     }
    #     gwas = cbind(data.frame(GWAS$CHROM, GWAS$POS), LOD)
    #     colnames(gwas) = c("CHROM", "POS", paste0("LOD_", popnames))
    #     scaff_list = unique(blastout$CHROM)
    #     layout_width = ceiling(sqrt(length(scaff_list)))
    #     layout_height = ceiling(length(scaff_list)/layout_width)
    #     svg(paste0(herbi, "_", popname, "_MANHATTAN_BLAST_HITS.svg"), width=layout_width*4, height=layout_height*2.5)
    #     par(mfrow=c(layout_height, layout_width))
    #     for (l in 1:length(scaff_list)){
    #         # l = 1
    #         scaff = scaff_list[l]
    #         sub = gwas[df$CHROM==scaff, ]
    #         blastout_sub = blastout[blastout$CHROM==scaff, ]
    #         plot(x=c(0,max(c(sub$POS, blastout_sub$POS_SLUT))), y=c(min(LOD,na.rm=TRUE), max(LOD,na.rm=TRUE)), type="n", main=scaff, xlab="Position (bp)", ylab="-log10(p-value)")
    #         grid(col="gray")
    #         points(x=rep(sub$POS, each=(ncol(gwas)-2)), y=as.vector(t(sub[,3:ncol(sub)])), col=colors_points, pch=20)
    #         if(nrow(blastout_sub)>0){
    #             for (m in 1:nrow(blastout_sub)){
    #                 rect(xleft=blastout_sub$POS_INIT[m], xright=blastout_sub$POS_SLUT[m], ybottom=min(LOD,na.rm=TRUE), ytop=max(LOD,na.rm=TRUE), density=NA, col=colors_lines[3])
    #             }
    #         }
    #     }
    #     dev.off()
    # }
' > 10_manhattan_plots_with_blast_highlights.r
time \
Rscript 10_manhattan_plots_with_blast_highlights.r ${DIR}
