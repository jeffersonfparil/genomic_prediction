###################################################################
###																###
### SVM, Random Forest, Neural Networks, ... ###
###																###
###################################################################

module impute

#####################
###				  ###
### load packages ###
###				  ###
#####################
using RData

############################
###						 ###
### function definitions ###
###						 ###
############################

#load rds data
X = RData.load("At_134Salk_SNP.rds")

#divide into 5 pools of allele frequencies
p = 5 #number of pools
n = size(X)[1] #number of individuals
m = size(X)[2] #number of loci

# POOLS = zeros(m, p)
# for i in 1:p
#
# X[()]


#####################
###				  ###
### main function ###
###				  ###
#####################

end #end of impute module
