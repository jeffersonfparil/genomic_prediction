##########################################################
###                                                    ### @Weir1984
### Fst across all populations and all loci per window ### Estimation F-statistics for the analysis ofpopulation structure. 1984. Evolution. Vol 38. No 6)
###                                                    ### (Weir and Cockerham's method;
##########################################################
# sample execution
# julia ${GEN_PRED_SRC_DIR}/quantinemo2_geno_sim_03_POOLS_GENO_Fst.jl \
# QUANTI_g140_POOLS_GENO.sync \
# 1000000 \
# 20,20,20,20,20

##############
### inputs ###
##############
sync_fname = ARGS[1]
window_size = parse(Int64, ARGS[2])
pool_sizes = parse.(Int64, split(ARGS[3], ",")) #poolsizes delimited by comma e.g. 10,20,30,20,10
# # test
# sync_fname = "QUANTI_g140_ALLPOP_GENO.sync"
# window_size = parse(Int64, "1000000") #non-overlapping 10 Mb windows
# pool_sizes = parse.(Int64, split("20,20,20,20,20,20,20", ","))

######################
### load libraries ###
######################
using CSV
using DataFrames
using Statistics

###########################################
### load the sync allele frequency data ###
###########################################
sync = CSV.read(sync_fname, delim="\t", datarow=1)

########################
### input parameters ###
########################
nPools = DataFrames.ncol(sync)  - 3
nLoci = DataFrames.nrow(sync)
println(string("Synchronized pileup file: ", sync_fname))
println(string("Non-overlapping window size: ", window_size, " bp"))
println(string("Number of pools: ", nPools))
for i in 1:length(pool_sizes)
    println(string("Pool ", i, " size : ", pool_sizes[i]))
end
println(string("Number of loci: ", nLoci))

###############################################################################################################
### sanity check if the number of pools in the input sync file is equal to the number of pool sizes entered ###
###############################################################################################################
if (nPools != length(pool_sizes))
    println("The number of pools in the synchronized pileup file does not match the number of pool sizes entered!")
    exit()
end

####################################################
### calculate Fst per choromosome across windows ###
####################################################
println("Calculating the Fst!")
CHROM=[]; WINDOW=[]; FST=[]
for chrom in DataFrames.levels(sync.Column1)
    # test
    # chrom = 1
    subsync = sync[sync.Column1 .== chrom, :]
    nWindows = convert(Int, ceil(maximum(subsync.Column2) / window_size))
    for window in 1:nWindows
        # test ### NEED TO DEAL WITH ZERO AND ONE LOCUS windowsync DATAFRAMES!!!!! 20190620
        # window = 1
        _start_ = window_size*(window-1) + 1
        _end_ = window_size*(window)
        windowsync = subsync[(subsync.Column2 .>= _start_) .& (subsync.Column2 .<= _end_), :]
        l = DataFrames.nrow(windowsync)     #number of loci in this window
        a = 6                               #number of alleles which is 6 for the sync formal
        r = DataFrames.ncol(windowsync) - 3 #number of pools or subpopulations or populations in the sync file
        p_3d = Array{Float64}(undef, l, a, r)
        for pop in 1:r
            for locus in 1:l
                # #test:
                # pop=1; locus=1
                COUNTS = parse.(Int64, split(windowsync[locus, 3+pop], ":"))
                FREQS = COUNTS ./ sum(COUNTS)
                p_3d[locus, :, pop] = FREQS
            end
        end
        # # remove fixed loci
        # idx_fixed_loci = Statistics.var(Statistics.maximum(p_3d, dims=2), dims=3) .> 1e-5
        # p_3d = p_3d[idx_fixed_loci[:],:,:]
        # # redefine the number of loci
        # l = size(p_3d)[1]
        # start computations
        n_bar = sum(pool_sizes) / r
        n_c = ( r*n_bar - ((sum(pool_sizes.^2))/(r*n_bar)) ) / (r-1)
        n_3d = reshape(repeat(pool_sizes, inner=l*a), (l,a,r))
        p_bar_2d = sum(n_3d .* p_3d, dims=3) ./ (r*n_bar)
        s_sq_2d = sum(n_3d .* ((p_3d .- p_bar_2d) .^ 2), dims=3) ./ (n_bar*(r-1))
        # bulting the HWE-based P(heterozygotes)
        h_3d = 2 .* p_3d .* (1 .- p_3d)
        h_bar_2d = sum(n_3d .* h_3d, dims=3) ./ (r * n_bar)
        # calculte the components of Fst
        a_2d = (n_bar/n_c) * ( (s_sq_2d) - ((1/(n_bar-1)) .* ( (p_bar_2d .* (1 .- p_bar_2d)) .- (((r-1)/r) .* s_sq_2d) .- (h_bar_2d ./ 4) )) )
        b_2d = (n_bar/(n_bar-1)) .* ( (p_bar_2d.* (1 .- p_bar_2d)) .- (((r-1)/r) .* s_sq_2d) .- ((((2*n_bar)-1)/(4*n_bar)) .* h_bar_2d) )
        c_2d = h_bar_2d ./ 2
        # calculate Fst and append to output vectors
        if sum(a_2d) <= 0.0 #for Fst values less than or equal to zero are essentially zero, i.e. Fst = (H_between - H_within) / H_between; --> H_between </= H_within
            Fst = NaN
            append!(FST, Fst)
        else
            Fst = sum(a_2d) / sum( a_2d .+ b_2d .+ c_2d )
            append!(FST, Fst)
        end
        append!(CHROM, chrom)
        append!(WINDOW, window)
    end #window
end #chrom

#################################################################################################
### output the Fst across chromosomes and windows and the mean across chromosomes and windows ###
#################################################################################################
println("Writing the output into the files:")
Fst_data_out_fname = string(split(sync_fname, ".")[1], "_window_", window_size, "bp_Fst_data.csv")
Fst_sumstats_out_fname = string(split(sync_fname, ".")[1], "_window_", window_size, "bp_Fst_sumstats.csv")
println(string("(1/2) ", Fst_data_out_fname, " for the Fst per chromosome per locus."))
println(string("(2/2) ", Fst_sumstats_out_fname, " for the summary statistics of Fst acros chromosomes and loci (i.e. mean, min, max and var)."))

OUT_Fst = DataFrames.DataFrame(chr=convert(Array{Int32},CHROM), window=convert(Array{Int64},WINDOW), Fst=convert(Array{Float64},FST))
### performing a Shapiro-Wilk's test for normality shows that the Fst across windows is normally distributed!
SUMSTATS_Fst = DataFrames.DataFrame(mean_Fst=Statistics.mean(OUT_Fst.Fst[.!isnan.(OUT_Fst.Fst)]), min_Fst=Statistics.minimum(OUT_Fst.Fst[.!isnan.(OUT_Fst.Fst)]), max_Fst=Statistics.maximum(OUT_Fst.Fst[.!isnan.(OUT_Fst.Fst)]), var_Fst=Statistics.var(OUT_Fst.Fst[.!isnan.(OUT_Fst.Fst)]))
CSV.write(Fst_data_out_fname, OUT_Fst, delim=",")
CSV.write(Fst_sumstats_out_fname, SUMSTATS_Fst, delim=",")
