#!/usr/bin/env Rscript
###########################################
###										###
### Prepare phenotype files for GWAlpha	###
###										###
###########################################
args = commandArgs(trailing = TRUE)
dat = read.csv(args[1]) #input csv file with headers: LIBRARY, RESISTANCE, POOL
# dat = read.csv("prepare_pheno_py_data.csv")

lib_vec = levels(dat$LIBRARY)
for (i in lib_vec){
	# i = lib_vec[1]
	print(i)
	sub = droplevels(subset(dat, LIBRARY==i))
	NPOOL = nlevels(as.factor(sub$POOL))
	PHENO_NAME=i
	SIG = sqrt(var(sub$RESISTANCE))
	MIN = min(sub$RESISTANCE)
	MAX = max(sub$RESISTANCE)
	PERC = cumsum(as.vector(table(sub$POOL)/nrow(sub)))[1:(NPOOL-1)]
	perc_func = ecdf(sub$RESISTANCE)
	Q = quantile(perc_func, PERC)
	OUT_PY = data.frame(x=c(
		paste0("Pheno_name=\"", PHENO_NAME, "\";"),
		paste0("sig=", SIG, ";"),
		paste0("MIN=", MIN, ";"),
		paste0("MAX=", MAX, ";"),
		paste0("perc=[", paste(PERC, collapse=","), "];"),
		paste0("q=[", paste(Q, collapse=","), "];")
		))
	OUT_CSV =	aggregate(RESISTANCE ~ POOL, FUN=mean, data=sub)
	write.table(OUT_PY, file=paste0(i, "_pheno.py"), sep="\t", quote=FALSE, col.names=FALSE, row.names=FALSE)
	write.table(OUT_CSV, file=paste0(i, "_pheno.csv"), sep=",", quote=FALSE, col.names=FALSE, row.names=FALSE)
}
