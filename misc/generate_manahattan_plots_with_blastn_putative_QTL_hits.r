### GENERATE MAHATTAN PLOTS
### TOP (HIGHEST LOD OR -log10(p-value)) BLASTN HITS (NOTE: not all peaks have blastn hits)

### INPUT:
### OUTPUT:
# paste0("Manhattan_plot-", id, "-", model, ".svg")

### inputs:
args = commandArgs(trailing=TRUE)
fname_loci_consolidated = args[1]
fname_blastout_consolidated = args[2]
DIR = args[3]
# fname_loci_consolidated = "LOCI_CONSOLIDATED.csv"
# fname_blastout_consolidated = "BLAST_OUT_CONSOLIDATED.txt"
# DIR = "/data/Lolium/Quantitative_Genetics/GWAS_GP_2018_Inverleigh_Urana/GPAS/"

### consolidated loci, blastn hits across samples and models
LOCI_INFO = read.csv(fname_loci_consolidated)
LOCI_INFO = LOCI_INFO[with(LOCI_INFO, order(CHROM, POS)), ]
LOCI_INFO$LOCI_IDX = 1:nrow(LOCI_INFO)
BLAST_OUT = read.delim(fname_blastout_consolidated, sep="\t", header=TRUE)
# GFF3_PRED = read.delim("CONSOLIDATED-GFF3-HITS.txt", sep="\t", header=TRUE)
### set working directory where the *ALphas.csv files are located
setwd(DIR)
ALPHAS_LIST = system("ls *_Alphas.csv", intern=TRUE)
ALPHAS_MATRIX = matrix(unlist(strsplit(ALPHAS_LIST, "-")), ncol=2, byrow=TRUE)
ALPHAS_MATRIX[,2] = gsub("_Alphas.csv", "", ALPHAS_MATRIX[,2])
for (counter in 1:nrow(ALPHAS_MATRIX)){
  ### sample- and model-specific alphas file (GPAS allelic effects file)
# counter = 2
  id = ALPHAS_MATRIX[counter, 1]
  model = ALPHAS_MATRIX[counter, 2]
  FNAME_ALPHAS = paste0(id, "-", model, "_Alphas.csv")
  dat = read.csv(FNAME_ALPHAS)
  X = aggregate(LOD ~ CHROM + POS, data=dat, FUN=max)
  X = X[with(X, order(CHROM, POS)), ]
  sub_blast = subset(subset(BLAST_OUT, ID==id), MODEL==model)
  if (!is.na(var(X$LOD, na.rm=TRUE)) & (var(X$LOD, na.rm=TRUE) > 0.0) & (length(sub_blast$PUTATIVE_QTL) > 0)){
    print(FNAME_ALPHAS)
    MERGED = merge(LOCI_INFO, X, by=c("CHROM", "POS"), all=TRUE)
    ### plot tests
    # svg(paste0("Manhattan_plot-", id, "-", model, ".svg"), width=10, height=3)
    png(paste0("Manhattan_plot-", id, "-", model, ".png"), width=2400, height=1400)
    # plot(x=MERGED$LOCI_IDX, y=MERGED$LOD, xlab="LOCI", ylab="-log10(p-value)", main=paste0(id, "-", model), type="p", pch=20, col=rgb(0.33, 0.58, 0.92, alpha=0.5))
    ### testing 2-alternating colors per scaffold
    bicolors = c(rgb(0.31,0.70,0.83,alpha=0.75), rgb(0.66,0.87,0.71,alpha=0.75))
    layout(matrix(c(1,1,2), nrow=3, ncol=1))
    par(cex=3, mar=c(4,4.5,3,1))
    plot(x=MERGED$LOCI_IDX, y=MERGED$LOD, xlab="LOCI", ylab=expression(-log[10]("p-value")), main=paste0(id, "-", model), type="n")

    pb = txtProgressBar(min=0, max=nlevels(MERGED$CHROM), style=3, width=30)
    for (i in 1:nlevels(MERGED$CHROM)){
      # i = 1
      sub_by_chrom = subset(MERGED, CHROM==as.character(levels(MERGED$CHROM)[i]))
      points(x=sub_by_chrom$LOCI_IDX, y=sub_by_chrom$LOD, pch=20, col=bicolors[(i %% 2)+1])
      setTxtProgressBar(pb, i)
    }
    close(pb)

    ### adding threshold line
    bonferroni_threshold = -log10(1e-6 / sum(!is.na(MERGED$LOD))) ### significance at 99.9999% or FPR = 0.000001 = 1e-6
    abline(h=bonferroni_threshold, lty=2, lwd=4, col=rgb(0.25,0.25,0.25,alpha=0.75))
    ### appending top blast hits
    sub_blast_chrom_pos_range = matrix(unlist(strsplit(as.character(sub_blast$PUTATIVE_QTL), ":")), ncol=3, byrow=TRUE)
    sub_blast$CHROM = sub_blast_chrom_pos_range[,1]
    sub_blast$POS = as.numeric(sub_blast_chrom_pos_range[,2])
    sub_blast = merge(LOCI_INFO, sub_blast, by=c("CHROM", "POS"), all=FALSE)
    sub_blast = droplevels(sub_blast)
    chrom_blast = c()
    pos_blast = c()
    idx_blast = c()
    lod_blast = c()
    labs_blast = c()
    for (i in levels(sub_blast$PUTATIVE_QTL)){
      # i = levels(sub_blast$PUTATIVE_QTL)[1]
      # print(i)
      sub_sub = subset(sub_blast, PUTATIVE_QTL==i)
      sub_sub = sub_sub[with(sub_sub, order(BITSCORE, decreasing=TRUE)), ]
      chrom_blast = c(chrom_blast, as.character(sub_sub$CHROM[1]))
      pos_blast = c(pos_blast, sub_sub$POS[1])
      idx_blast = c(idx_blast, sub_sub$LOCI_IDX[1])
      lod_blast = c(lod_blast, X$LOD[(as.character(X$CHROM)==tail(chrom_blast,1)) & (X$POS==tail(pos_blast,1))])
      labs_blast = c(labs_blast, as.character(sub_sub$HIT_INFO[1]))
    }
    ### top 5 peaks with BLAST hits in terms of LOD sorry -log10(p-value) scores (NOTE: some of the peaks may not have blast hits)
    blast_hits_points = data.frame(CHROM=chrom_blast, POS=pos_blast, LOCI_IDX=idx_blast, LOD=lod_blast, HIT_INFO=labs_blast)
    hits_table = table(blast_hits_points$HIT_INFO)
    idx_peaks_blast = c()
    lod_peaks_blast = c()
    lab_peaks_blast = c()
    for (i in names(hits_table[order(hits_table, decreasing=TRUE)])[1:5]){
      sub_sub = subset(blast_hits_points, HIT_INFO==i)
      sub_sub = sub_sub[with(sub_sub, order(LOD, decreasing=TRUE)),][1,]
      idx_peaks_blast = c(idx_peaks_blast, sub_sub$LOCI_IDX)
      lod_peaks_blast = c(lod_peaks_blast, sub_sub$LOD)
      lab_peaks_blast = c(lab_peaks_blast, as.character(sub_sub$HIT_INFO))
    }
    top5_peaks = data.frame(LOCI_IDX=idx_peaks_blast, LOD=lod_peaks_blast, HIT_INFO=lab_peaks_blast)
    top5_peaks = top5_peaks[order(top5_peaks$LOCI_IDX, decreasing=FALSE), ]
    ### label the points with BLAST hits
    points(x=top5_peaks$LOCI_IDX, y=top5_peaks$LOD, cex=2.5, pch=1, col=rgb(0.03,0.41,0.67,alpha=0.9))
    text(x=top5_peaks$LOCI_IDX, y=top5_peaks$LOD, label=as.character(1:nrow(top5_peaks)), pos=4, col=rgb(0.25,0.25,0.25,alpha=0.9))
    # text(x=top5_peaks$LOCI_IDX, y=top5_peaks$LOD, label=top5_peaks$HIT_INFO, pos=4)
    ### add legend to list the BLAST hits as a new plot hahaha
    par(mar=c(1,4,1,1), cex=1.5)
    plot(x=seq(from=0, to=1, length=nrow(top5_peaks)+1), y=0:nrow(top5_peaks), type="n", xlab="", ylab="", main="", xaxt="n", yaxt="n", bty="n")
    text(x=0, y=1:nrow(top5_peaks), lab=rev(paste(c(1:nrow(top5_peaks)), top5_peaks$HIT_INFO, sep=": ")), pos=4)
    dev.off()
  }
}
