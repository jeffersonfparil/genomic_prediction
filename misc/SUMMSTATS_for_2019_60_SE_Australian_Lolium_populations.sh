#!/bin/bash
###########################################################################
### Estimate Population Genetics Summary Statistics for each Population ###
###   and Identify Genomic Evidence of Selective Sweeps Via Tajima's D	###
###########################################################################

#################################
### SETUP WORKING DIRECTORIES ###
#################################
SRC_DIR=/data/Lolium/Softwares/genomic_prediction/misc
NPSTAT_DIR=/data/Lolium/Softwares/npstat
PILEUP_FNAME=/data/Lolium/Population_Genetics/2018November_Ryegrass_Collection/VCF/Lolium2019_10X_filtered.mpileup ### 10X AWK FILTERING from fastq to bam to mpileup
DIR=/data/Lolium/Population_Genetics/2018November_Ryegrass_Collection/SUMMSTATS
mkdir ${DIR}
cd ${DIR}

cut -f-3 ${PILEUP_FNAME} > SCAF_POS_REF.temp
NCOL=$(head -n1 ${PILEUP_FNAME} | awk '{print NF}' -)
seq 4 3 ${NCOL} > COL_IDX.temp
sed 's/SAM\///g' /data/Lolium/Population_Genetics/2018November_Ryegrass_Collection/bam.list | sed 's/\.bam//g'> POP_NAMES.temp

for i in $(seq 1 $(cat POP_NAMES.temp | wc -l))
do
# i=1
idx_start=$(head -n${i} COL_IDX.temp | tail -n1)
idx_end=$(echo "${idx_start} + 2" | bc)
cut -f${idx_start}-${idx_end} ${PILEUP_FNAME} > SEQ_DATA.temp
FNAME=$(head -n${i} POP_NAMES.temp | tail -n1)
paste SCAF_POS_REF.temp SEQ_DATA.temp > ${FNAME}.pileup
echo $FNAME
done


nhaplo=42
mincov=10
maxcov=$(echo ${mincov%.*} "* 100" | bc)
window_size=10000 #10KB

time \
for i in $(ls ACC*.pileup)
do
# i=ACC01.pileup
echo $i
for j in $(cut -f1 SCAF_POS_REF.temp | uniq)
do
# j=$(cut -f1 SCAF_POS_REF.temp | uniq | head -n1)
grep ${j} ${i} > ${i%.pileup*}_${j}.pileup
done
# ${SRC_DIR}/npstat_parallel.sh ${NPSTAT_DIR} ${mincov} ${maxcov} ${window_size} ${i%.pileup*}_${j}.pileup ${j}
parallel --link ${SRC_DIR}/npstat_parallel.sh ${NPSTAT_DIR} ${nhaplo} ${mincov} ${maxcov} ${window_size} {1} {2} ::: $(ls ${i%.pileup*}_*.pileup) ::: $(cut -f1 SCAF_POS_REF.temp | uniq)
cat *_NPSTAT.stats > ${i%.pileup*}.NPSTAT.stats
rm *_NPSTAT.stats
rm ${i%.pileup*}_*.pileup
done


### plotting
NPSTAT_FNAMES=$(ls ACC*.NPSTAT.stats)
### In R: (test_plot_TajimasD.r)
# args = commandArgs(trailing=TRUE)
# fname = args[1]
# # fname = "ACC01.NPSTAT.stats"
# dat = read.table(fname, header=FALSE)
# colnames(dat) = c("SCAFFOLD", "WINDOW", "COVERAGE", "COVERAGE_OUTGROUP", "DEPTH", "Segregating_sites", "Watterson_estimator", "Tajimas_Pi", "Tajimas_D", "Fay_Wu_H_unorm", "Fay_Wu_H_norm", "Var_watterson", "Divergence", "Nonsyn_polymorph", "Syn_polymorph", "Nonsyn_diver", "Syn_diver", "Frac_subs_positive_sel")
# svg(paste0(fname, ".svg"), width=10, height=5)
# plot(dat$Tajimas_D[dat$DEPTH >=100], type="p", col="blue", main=fname, xlab="WINDOWS", ylab="Tajima's D")
# dev.off()
# # barplot(dat$Tajimas_D, col="blue")
# # ### Normalizing Tajima's D
# # D = (dat$Tajimas_D - mean(dat$Tajimas_D, na.rm=TRUE)) / sd(dat$Tajimas_D, na.rm=TRUE)
# # plot(D, type="p")
for i in ${NPSTAT_FNAMES}
do
  echo $i
  Rscript test_plot_TajimasD.r ${i}
done
