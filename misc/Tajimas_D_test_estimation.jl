### testing Tajima's D estimation using sync/allele freq data

using DataFrames
using CSV
using DelimitedFiles
using StatsBase
using Statistics
using LinearAlgebra

fname = "test_1kloci_g1000_p01_POOLS_GENO_ALLELEFREQ.csv"
n = 100

allele_freqs_df = DelimitedFiles.readdlm(fname, ',')
n_alleles = 6
n_loci = convert(Int64, size(allele_freqs_df, 1) / n_alleles)
freqs_mat = allele_freqs_df[:, 4:end]
freqs_vec = mean(freqs_mat, dims=2)
NU_mat = reshape(freqs_vec, n_alleles, n_loci)' #n_loci x n_alleles
NU_vec = sum(NU_mat .> 0.0, dims=2) #number of non-zero alleles per locus
NU_vec_df = DataFrames.DataFrame(CHR=reshape(allele_freqs_df[:,1], n_alleles, n_loci)'[:,1],
                                POS=reshape(allele_freqs_df[:,2], n_alleles, n_loci)'[:,1],
                                FREQ_A=NU_mat[:,1],
                                FREQ_T=NU_mat[:,2],
                                FREQ_C=NU_mat[:,3],
                                FREQ_G=NU_mat[:,4],
                                FREQ_N=NU_mat[:,5],
                                FREQ_DEL=NU_mat[:,6])
a_n = sum(1.00 ./ collect(1:(n-1)))
window_size = 10000000
THETA_DIFF = []
for chrom in unique(NU_vec_df.CHR)
    #test:
    # chrom=1
    SUB = NU_vec_df[NU_vec_df.CHR .== chrom, :]
    POS_MAX_MIN = SUB.POS[end] - SUB.POS[1]
    n_windows = convert(Int64, ceil(POS_MAX_MIN / window_size))
    windows_last_pos = cumsum(repeat([SUB.POS[1] + window_size], inner=n_windows+1))[2:end]
    for window in 1:n_windows
        #test:
        # window=1
        SUB_WINDOW = SUB[SUB.POS .<= windows_last_pos[window], :]
        # NU_final = DataFrames.DataFrame(k=collect(1:(n_alleles-1)), Nu_k=[sum(SUB_WINDOW.NU_vec .== (k+1)) for k in 1:(n_alleles-1)]) #added 1 to account for the ancestral (ref) allele
        ### THETA_w: Watterson's esimator
        # THETA_w = (1.00/sum(1:(n_alleles-1))) * sum(NU_final.Nu_k)
        THETA_w = nrow(SUB_WINDOW) / a_n
        ### THETA_pi: Tajima's pi
        # i_s = collect(1:(n_alleles - 1))
        # THETA_pi = (1.00/(factorial(n_alleles) / (factorial(n_alleles-2) * factorial(2)))) * sum( (i_s .* (n_alleles .- i_s)) .* NU_final.Nu_k )

        COUNTS_MAT = convert(Matrix{Int64}, round.(convert(Matrix{Float64}, SUB_WINDOW[:, 3:end]) .* n))
        S = []
        for i in 1:size(COUNTS_MAT, 1)
            #test:
            # i=1
            PROD = COUNTS_MAT[i, :] * COUNTS_MAT[i, :]'
            push!(S, sum(LinearAlgebra.UpperTriangular(PROD)) - sum(LinearAlgebra.diag(PROD)))
        end
        THETA_pi =  2*sum(S) / (n*(n-1))

        ### THETA_DIFF
        push!(THETA_DIFF, THETA_pi - THETA_w)
    end
end

using Plots; Plots.pyplot()
Plots.histogram(THETA_DIFF)
# D = mean(THETA_DIFF) / std(THETA_DIFF)
