###########################################################
###                                                     ###
### Modelling Lolium geneflow as a function of distance ###
###                                                     ###
###########################################################
# Pollen-mediated movement of herbicide resistance genes in Lolium rigidum
# 2016
# PLoS ONE
# Vol.11, Issue6
# Loureiro, Iñigo
# Escorial, María Concepción
# Chueca, María Cristina
# [@Loureiro2016a]
library(lme4)
setwd("/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE_GENETICS-combine_SNP_and_transcript_data/SRC/genomic_prediction/misc")
dat = read.csv("geneflow_modelling_Loureiro_etal_2016_data.csv")
str(dat)
par(mfrow=c(1,2))
plot(x=dat$DISTANCE, y=dat$POLLEN_FLOW_PERC, pch=20, col=rgb(0.5,0.5,0.5,alpha=0.5), xlab="Distance (m)", ylab="Pollen Flow (%)", main="Original Data")
AGGREGATE = aggregate(POLLEN_FLOW_PERC ~ DISTANCE, data=dat, FUN=mean)
points(x=AGGREGATE[,1], y=AGGREGATE[,2], type="b", pch=19, lty=1, lwd=2, col=rgb(0.9,0.2,0.1,alpha=0.5))
base=10
YEAR = dat$YEAR
DISTANCE = dat$DISTANCE
logPOLLENFLOW = log(dat$POLLEN_FLOW_PERC, base=base)
plot(x=DISTANCE, y=logPOLLENFLOW, pch=20, col=rgb(0.5,0.5,0.5,alpha=0.5), xlab="Distance (m)", ylab=paste0("log", base, "(Pollen Flow (%)"), main="Log-Transformed Percent Pollen Flow")
AGGREGATE_log = aggregate(logPOLLENFLOW ~ DISTANCE, FUN=mean)
points(x=AGGREGATE_log[,1], y=AGGREGATE_log[,2], type="b", pch=19, lty=1, lwd=2, col=rgb(0.9,0.2,0.1,alpha=0.5))

model = lmer(logPOLLENFLOW ~ (1|YEAR) + DISTANCE)
year_eff = ranef(model)
distance_eff = fixef(model)

NEW_DISTANCES = seq(from=0, to=1000, by=100) #0 to 1km
NEW_logPOLLENFLOW = distance_eff[1] + (distance_eff[2]*NEW_DISTANCES)
NEW_POLLENFLOWPERC = base^(NEW_logPOLLENFLOW)
par(mfrow=c(1,2))
plot(x=NEW_DISTANCES, y=NEW_logPOLLENFLOW, type="b")
plot(x=NEW_DISTANCES, y=NEW_POLLENFLOWPERC, type="b")
POLLEN_FLOW_PERC_AT_1KM = base^(distance_eff[1] + (distance_eff[2]*1000))
legend("topright", legend=paste0("Pollen Flow (%) at 1km = ", formatC(POLLEN_FLOW_PERC_AT_1KM, format="e", digits=2)))

############
### MISC ###
# ### log-tranforming a exponential looking curve
# a = 1
# b = -1
# x = 1:10
# y = a*exp(b*x)
# par(mfrow=c(1,2))
# plot(x, y, type="b")
# plot(x, log(y,base=10), type="b")
