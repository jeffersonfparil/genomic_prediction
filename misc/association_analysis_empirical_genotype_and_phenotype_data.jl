###############################################################
###															###
### 			Genomewide Association Analyses				###
### using 1001 Genomes Arabidopsis Genotypes and Phenotypes ###
###															###
###############################################################

##################################
### Load libraries and modules ###
##################################
# using CSV
using JLD2
using StatsKit
# using Statistics
# using DataFrames
# using Distributions
using LinearAlgebra
using Optim
using ProgressMeter
using ColorBrewer
using Plots; Plots.pyplot()
# include("src/hdf5_SNP_parsing_module.jl")
using RCall #use $ to get into R REPL; backspace to exit; $variable_name to use julia-defined variables within R REPL; and RData.@rget variable_name to get R-defined variable into julia! Awesome! :-D

#########################
### Define fuinctions ###
#########################
## Manhattan Plotting
function PLOT(LOD, CHROM)
	LOD_plot = Plots.plot([1, length(LOD)], [0, maximum(LOD)],
		seriestype=:scatter,
		marker=(:circle, 5, 0.5, :White, Plots.stroke(0, :white)),
		xlabel="SNP ID",
		ylabel="LOD",
		legend=false,
		size=(1200, 400));
	contigs = unique(CHROM)
	if length(contigs) > 9
		colours = ColorBrewer.palette("Pastel1", 9)
		colours_lab = ColorBrewer.palette("Set1", 9)
		for i in 1:floor(length(contigs)/9)
			append!(colours, ColorBrewer.palette("Pastel1", length(contigs)%6)) # mod 6 for 3 minimum colors in ColorBrewer
			append!(colours_lab, ColorBrewer.palette("Set1", length(contigs)%6)) # mod 6 for 3 minimum colors in ColorBrewer
		end
	else
		ncolours = length(contigs)
		colours = ColorBrewer.palette("Pastel1", ncolours)
		colours_lab = ColorBrewer.palette("Set1", ncolours)
	end
	i_loc = [0, 0] # counter and locus position
	for contig in contigs
		subset_LOD = LOD[CHROM .== contig]
		x0 = (i_loc[2]+1)
		x1 = (i_loc[2]+length(subset_LOD))
		Plots.plot!(LOD_plot, x0:x1, subset_LOD,
					seriestype=:scatter,
					marker=(:circle, 5, 0.5, colours[i_loc[1]+1], Plots.stroke(0, :white)),
					xlabel="SNP ID",
					ylabel="LOD",
					legend=false,
					size=(1700, 500));
		Plots.annotate!([(x0+((x1-x0)/2), 0, text(contig, 10, colours_lab[i_loc[1]+1], :center))]);
		i_loc[1] = i_loc[1]+1
		i_loc[2] = i_loc[2]+length(subset_LOD)
	end
	bonferroni_threshold = -log10(0.0001 / length(LOD))
	Plots.plot!(LOD_plot, [1,length(LOD)], [bonferroni_threshold, bonferroni_threshold], line=(:line, :dash, 0.5, 2, :red), label="Bonferroni threshold");
end

#########################
### Load genomic data ###
#########################
# fixed_hdf5_file = "/data/Lolium/Quantitative_Genetics/At_empirical/SNP_variants/1001_SNP_MATRIX/imputed_snps_binary-FIXED_FOR_JULIA.hdf5"
# ACC, CHROM, POS, X = hdf5_SNP_parsing_module.extract_SNP_info(fixed_hdf5_file)
# taxa, chrom, pos, ref, X = hdf5_SNP_parsing_module.extract_SNP_info_PANZEA("ZeaGBSv27_publicSamples_imputedV5_AGPv4-181023.h5")
# ### load jpd2 file for ease - pre-processed julia variables pre-processed:
# ### taxa, chrom, pos, ref, X
JLD2.@load "ZeaGBSv27_publicSamples_imputedV5_AGPv4-181023.jld2"

##################################
### Pull Panzea phenotype data ###
##################################
buckler_data = CSV.read("NAMSum0607FloweringTraitBLUPsAcross8Envs.txt"; delim="\t")

##############################################################
### Merge genotype and phenotype data based on common taxa ###
##############################################################
### download NAM Buckler_etal_2009_Science_flowering_time_data-090807.zip : iget /iplant/home/shared/panzea/phenotypes/Buckler_etal_2009_Science_flowering_time_data-090807.zip; unzip Buckler_etal_2009_Science_flowering_time_data-090807.zip; rm Buckler_etal_2009_Science_flowering_time_data-090807.zip
### or  download NAM Brown_etal_2011_PLoSGenet_pheno_data-120523.zip : iget /iplant/home/shared/panzea/phenotypes/Brown_etal_2011_PLoSGenet_pheno_data-120523.zip; unzip Brown_etal_2011_PLoSGenet_pheno_data-120523.zip; rm Brown_etal_2011_PLoSGenet_pheno_data-120523.zip
pheno_taxa = buckler_data.Geno_Code
pheno_idx = []
geno_idx = []
progress_bar = ProgressMeter.Progress(length(taxa), dt=1, barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow)
for i in 1:length(taxa)
	geno_id = split(taxa[i], ":")[1]
	for j in 1:length(pheno_taxa)
		pheno_id = pheno_taxa[j]
		if geno_id == pheno_id
			append!(geno_idx, i)
			append!(pheno_idx, j)
			taxa[i] = geno_id
		end
	end
	ProgressMeter.update!(progress_bar, i)
end

buckler_data = buckler_data[pheno_idx,:]
X = X[geno_idx,:]
taxa = taxa[geno_idx,:]

# #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# #further filtering by subpopulation of this NAM population
# sub_pop_idx = (buckler_data.pop .== 1)
# buckler_data = buckler_data[sub_pop_idx,:]
# X = X[sub_pop_idx,:]
# taxa = taxa[sub_pop_idx,:]
# n = length(taxa)
# #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

###########################################################################
### Define the specific phenotype to use from the phenotype file loaded ###
###########################################################################
y = buckler_data.Days_To_Anthesis_BLUP_Sum0607
# y = buckler_data.Days_To_Silk_BLUP_Sum0607

#standardize phenotypic values
mu_y = Statistics.mean(y)
sd_y = Statistics.std(y)
y_sub = (y .- mu_y) ./ sd_y
# Plots.histogram(y_sub)

##########################
### Filter loci by MAF ###
##########################
# var_loc = var(X, dims=1)
# mean_loc = mean(X, dims=1)
# maf = 0.0001
# loc_filter_idx = (var_loc .> 0) .& (mean_loc .> maf) .& (mean_loc .< (1-maf))
# X_filtered = X[:, loc_filter_idx[:]]
# chrom_filtered = chrom[loc_filter_idx[:]]
# pos_filtered = pos[loc_filter_idx[:]]
# JLD2.@save "ZeaGBSv27_publicSamples_imputedV5_AGPv4-181023_FILTERED_NAMSum0607FloweringTraitBLUPsAcross8Envs.jld2" X_filtered chrom_filtered pos_filtered
JLD2.@load "ZeaGBSv27_publicSamples_imputedV5_AGPv4-181023_FILTERED_NAMSum0607FloweringTraitBLUPsAcross8Envs.jld2"

# #################
# ### Assess LD ### very ROUGHLY!!!!
# #################
# s1 = 10000 # number of sample loci
# CORR = cor(X_filtered[:,sample(1:l, s1)])
# s2 = 20 # number of sample loci for plotting
# sample_rand_plot_idx = sample(1:s1, 20); sort!(sample_rand_plot_idx)
# heatmap(1:s2, 1:s2, CORR[sample_rand_plot_idx, sample_rand_plot_idx])

###########################################
### Correcting for population structure ###
###########################################
### using the population structure based on this "pop" variable in the phenotype file
n = size(X_filtered)[1]
pop_covariate = zeros(n,length(unique(buckler_data.pop)))
for i in 1:n
	pop_covariate[i, buckler_data.pop[i]] = 1
end
### or using the principal components
# RCall.R"X = matrix(as.numeric($X_filtered), nrow=$n, byrow=FALSE)"
# # ### or using just a subset of the genotype dataset
# # RCall.R"s = round( 0.25 * ncol(X) )"
# # RCall.R"X = X[,sample(1:ncol(X), size=s)]"
# RCall.R"PCA = prcomp(t(X))"
# # RCall.R"PC_covariates = PCA$rotation[,1:5]"
# ### or using a range of covariates until the cummulative variance explained is <= 50%
# RCall.R"PC_ROTATIONS = PCA$rotation"
# RCall.R"CUMM_VAR = cumsum( PCA$sdev^2 / sum(PCA$sdev^2) )"
# RCall.@rget PC_ROTATIONS CUMM_VAR
# # JLD2.@save "NAMSum0607FloweringTraitBLUPsAcross8Envs_PC1_to_PC5_covariates.jld2" PC_covariates #save the PC covariates since prcomp takes so much time to calculate!
# JLD2.@save "NAMSum0607FloweringTraitBLUPsAcross8Envs_PC_covariates.jld2" PC_ROTATIONS CUMM_VAR #save the PC covariates since prcomp takes so much time to calculate!
# # JLD2.@load "NAMSum0607FloweringTraitBLUPsAcross8Envs_PC1_to_PC5_covariates.jld2"
JLD2.@load "NAMSum0607FloweringTraitBLUPsAcross8Envs_PC_covariates.jld2"
PC_covariates = PC_ROTATIONS[:, CUMM_VAR .≤ 0.6 ]

# ### or using a derived kinship matrix based on SNP gneotype data
# RCall.R"library(popkin)"
# RCall.R"X = matrix(as.numeric($X_filtered), nrow=$n, byrow=FALSE)"
# RCall.R"K = popkin(t(X))"
# RCall.@rget K
# JLD2.@save "NAMSum0607FloweringTraitBLUPsAcross8Envs_KinshipMatrix.jld2" K
JLD2.@load "NAMSum0607FloweringTraitBLUPsAcross8Envs_KinshipMatrix.jld2"
### OR OR OR USING VanRaden, 2008 algorithm
### standardize X first
X_alelle_freqs = mean(X_filtered, dims=1) ./2 # 0,1,2 genotype codes NOT 0 & 1 only!!!
X_std = zeros(size(X_filtered)[1], size(X_filtered)[2]) # will be considered as random effect i.e. Z in the mixed model y = Xb + Zu + e
for i in 1:size(X_filtered)[2]
	X_std[:,i] = (X_filtered[:,i] .- X_alelle_freqs[i]) / sqrt(X_alelle_freqs[i]*(1-X_alelle_freqs[i]))
end

K = (X_std * X_std') ./ (4*sum( X_alelle_freqs .* (1 .- X_alelle_freqs) ))


###############################################
###								   			###
### 		GENOMEWIDE ASSOCIATION 			###
###								   			###
###############################################

# #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#20190329 tests with population subsets
pop_idx = buckler_data.pop .== 1
X_filtered = X_filtered[pop_idx,:]
y_sub = y_sub[pop_idx]
PC_covariates = PC_covariates[pop_idx,:]
pop_covariate = pop_covariate[pop_idx,:]
Km = K[pop_idx,pop_idx]
# #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
####################
### Iterative LS ###
####################
### with correction for NAM population structure via phenotype datset "pop" classification and/or the top PC's with some cummulative variance explained
n = size(X_filtered)[1]
l = size(X_filtered)[2]
BETA = []
# PVAL = []
LOD = []

function linereg_func(x, y_sub)
	# the SNP is at the SECOND column of x
	out_b_hat = []
	# out_p_val = []
	out_lod = []
	# error handling when inv(x' * x) does not exist
	try
		b_hat = (inv(x' * x)) * (x' * y_sub)
		y_hat = x * b_hat
		y_err = y_sub - y_hat
		Vy_err = 1/n * (y_err' * y_err)
		b_ster = sqrt(Vy_err * inv(x' * x)[2,2])
		# b_ster = sqrt(Vy_err * inv(x' * x)[end,2])
		t_val = abs(b_hat[2]) / b_ster
		# t_val = abs(b_hat[end]) / b_ster
		lod = -(log10(2) + (Distributions.logccdf(Distributions.TDist(n-1), t_val))) ### using log10 complement since calculating the pvalues first will lose a lot of precision with very small decimal places!
		# write
		append!(out_b_hat, b_hat[2])
		# append!(out_b_hat, b_hat[end])
		# append!(out_p_val, p_val)
		append!(out_lod, lod)
	catch
		b_hat = 0
		# p_val = 1

		lod = 0
		# write
		append!(out_b_hat, b_hat)
		# append!(out_p_val, p_val)
		append!(out_lod, lod)
	end
	# return([out_b_hat, out_p_val])
	return([out_b_hat, out_lod])
end

progress_bar = ProgressMeter.Progress(l, dt=1, desc="GWAS iterative: ",  barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow)
for i in 1:l
	# println(i)
	# x = hcat(ones(n, 1), X_filtered[:, i])
	# x = hcat(ones(n, 1), X_filtered[:, i], pop_covariate)
	# x = hcat(ones(n, 1), PC_covariates, X_filtered[:, i])
	x = hcat(ones(n, 1), X_filtered[:, i], PC_covariates)
	# x = hcat(ones(n, 1), X_filtered[:, i], Km)
	# x = hcat(ones(n, 1), X_filtered[:, i], K)
	# x = hcat(ones(n, 1), X_filtered[:, i], pop_covariate, PC_covariates)
	# b_hat, p_val = linereg_func(x, y_sub)
	b_hat, lod = linereg_func(x, y_sub)
	append!(BETA, b_hat)
	# append!(PVAL, p_val)
	append!(LOD, lod)
	ProgressMeter.update!(progress_bar, i)

end

PLOT(LOD, chrom_filtered .+ 1)
# QTL_chrom = chrom_filtered[LOD .> 60] .+ 1
# QTL_pos = pos_filtered[LOD .> 60]
Plots.histogram(BETA)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
##########################
### Linear Mixed Model ###
########################## probably incorrect...
# test my understanding 2019-01-12
### standardize X first
# X_alelle_freqs = mean(X_filtered, dims=1) ./2 # 0,1,2 genotype codes NOT 0 & 1 only!!!
# X_std = zeros(size(X_filtered)[1], size(X_filtered)[2]) # will be considered as random effect i.e. Z in the mixed model y = Xb + Zu + e
# for i in 1:size(X_filtered)[2]
# 	X_std[:,i] = (X_filtered[:,i] .- X_alelle_freqs[i]) / sqrt(X_alelle_freqs[i]*(1-X_alelle_freqs[i]))
# end
### standardized X does not seem to work --- it's spitting out NAs in the V matrix!
X_std = X_filtered

### define likelihood function (where par = [sd_a, sd_e])
function likelihood(par, Z, y, X_covar)
	Va = par[1]^2
	Ve = par[2]^2
	G = Va * I
	R = Ve * I
	Z = hcat(repeat([1], inner=length(y)), Z) #random intercept and the SNP or SNPs in question
	V = ( (Z*G) * Z' ) + R # the variance-covariance matrix --- additive genetic effects x genotype PLUS (blobby-assumed) error variance
	X = hcat(repeat([1], inner=length(y)), X_covar) # the mean is the only fixed effect
	if (isposdef(V)==true) # to prevent non-hermitian V
		V_inv = inv(V)
	else
		V_svd = LinearAlgebra.svd(V)
		V_inv = V_svd.V * inv(LinearAlgebra.Diagonal(V_svd.S)) * V_svd.U'
	end
	b = inv(X' * V_inv * X) * X' * V_inv * y # BLUEs!
	# loglik = (1/2)*( sum(log.(10,abs.(V))) + ((y - X*b)' * V_inv * (y - X*b)) )
	loglik = (1/2)*( sum(log10.(abs.(V))) + ((y - X*b)' * V_inv * (y - X*b)) )
	return(loglik)
end

# ### minimise the likelihood function to find the optimum Va and Ve given y and X (the fixed variable is just the intercept, i.e. vector of ones!)
# lower_limit = [1e-10, 1e-10]
# upper_limit = [1e+10, 1e+10]
# initial_par = [1.0, 1.0] # sd_additive_genetic & sd_error
# # initial_par = [1e-10, 1e-10] # sd_additive_genetic & sd_error
# # @time par = Optim.optimize(par->likelihood(initial_par, X_std, y_sub), lower_limit, upper_limit, initial_par)
# # @time par = Optim.optimize(OnceDifferentiable(par->likelihood(initial_par, X_std, y_sub, PC_covariates), ones(2); autodiff=:forward), initial_par, Fminbox{GradientDescent}()) #whatever the fudge this meas! hahaha!
# # @time par_opt = Optim.optimize(par->likelihood(par, X_std[:,1], y_sub, PC_covariates), lower_limit, upper_limit, initial_par)

### one time variance components estimation for all loci and then use these Va and Ve to estimate the BLUPs iteratively across all loci!
lower_limit = [1e-10, 1e-10]
upper_limit = [1e+10, 1e+10]
initial_par = [1.0, 1.0] # sd_additive_genetic & sd_error
# @time par_opt = Optim.optimize(par->likelihood(par, X_std, y_sub, PC_covariates), lower_limit, upper_limit, initial_par)
@time par_opt = Optim.optimize(par->likelihood(par, X_std, y_sub, Km), lower_limit, upper_limit, initial_par)
# @time par_opt = Optim.optimize(par->likelihood(par, X_std[:,1], y_sub, PC_covariates), initial_par)
Va = par_opt.minimizer[1]
Ve = par_opt.minimizer[2]
JLD2.@save "MLE_once_mixed_model_Va_Ve_iterative.jld2" Va Ve
JLD2.@load "MLE_once_mixed_model_Va_Ve_iterative.jld2"

# ### julia optimisation taking too long - trying R
# library(MASS)
# Z = matrix(as.numeric($X_std), nrow=$n, byrow=FALSE)
# y = as.vector(as.numeric($y_sub))
# likelihood <- function(par) {
# 	Va = par[1]
# 	Ve = par[2]
# 	V = ((Va * Z) %*% t(Z)) + Ve
# 	X = rep(1, times=length(y))
# 	try
# 		V_inv = solve(V)
# 	catch
# 		V_svd = ginv(V)
# 	end
# 	b = inv(X' * V_inv * X) * X' * V_inv * y # BLUEs!
# 	loglik = (1/2)*( sum(log.(10,abs.(V))) + ((y - X*b)' * V_inv * (y - X*b)) )
# 	return(loglik)
# }

# ### compute the BLUEs and BLUPs given the optime variance parameters Va and Ve
# D = Va * I
# R = Ve * I
# Z = hcat(repeat([1], inner=length(y)), X_std[:,1])
# V = (X_std * D * X_std') + R
# X = hcat(repeat([1], inner=length(y)), PC_covariates)
# if (isposdef(V)==true) # to prevent non-hermitian V
# 	V_inv = inv(V)
# else
# 	V_svd = LinearAlgebra.svd(V)
# 	V_inv = V_svd.V * inv(LinearAlgebra.Diagonal(V_svd.S)) * V_svd.U'
# end
# b = inv(X' * V_inv * X) * X' * V_inv * y # BLUEs!
# u = D * X_std' * V_inv * (y_sub - (X*b)) # BLUPs!

#iterative
y = y_sub
G = Va * I #assuming no inter-relatedness - which hopefully accounted for by the PC_covariates?!?!?!
R = Ve * I
X = hcat(repeat([1], inner=length(y)), PC_covariates)
U = []
progress_bar = ProgressMeter.Progress(l, dt=1, desc="GWAS iterative: ",  barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow)
for i in 1:l
	# par_opt = Optim.optimize(par->likelihood(par, X_std[:,i], y_sub, PC_covariates), lower_limit, upper_limit, initial_par)
	# Va = par_opt.minimizer[1]
	# Ve = par_opt.minimizer[2]
	# G = Va * I
	# R = Ve * I
	Z = hcat(repeat([1], inner=length(y)), X_std[:,i])
	V = (Z * G * Z') + R
	if (isposdef(V)==true) # to prevent non-hermitian V
		V_inv = inv(V)
	else
		V_svd = LinearAlgebra.svd(V)
		V_inv = V_svd.V * inv(LinearAlgebra.Diagonal(V_svd.S)) * V_svd.U'
	end
	b = inv(X' * V_inv * X) * X' * V_inv * y # BLUEs!
	u = G * Z' * V_inv * (y_sub - (X*b)) # BLUPs!
	append!(U, u[2])
	ProgressMeter.update!(progress_bar, i)
end


### model the BLUPs (SNP effects) as normally distributed and compute their corresponding p-values
# mu_u = mean(U) # or use MLE fo this as well
# sd_u = std(U) # or use MLE fo this as well
function u_likelihood(par, u) # find the optimum mean and sd of the BLUPs via MLE
	mu = par[1]
	sd = abs(par[2])
	out = -sum(Distributions.logpdf.(Distributions.Normal(mu, sd), u))
	return(out)
end
par_u = Optim.optimize(par->u_likelihood(par, U), [-1e-20, 1e-20], [1e+20, 1e+20], [0.0, 1.0])
mu_u = par_u.minimizer[1]
sd_u = par_u.minimizer[2]
function lod_extraction(x, mu, sd) # defining a conditional function where pval depends on whether the value is to the left or right of the mean
	if x < mu
		lod = log10(2) + Distributions.logcdf(Distributions.Normal(mu, sd), x)
	else
		lod = log10(2) + Distributions.logccdf(Distributions.Normal(mu, sd), x)
	end
	return(-lod)
end
lod = [lod_extraction(x, mu_u, sd_u) for x in U]
PLOT(lod, chrom_filtered)


################# COOL COOL!


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
########################
### Ridge regression ###
######################## something probably terribly wrongity wrong!
# ### standardize X first
# X_alelle_freqs = mean(X_filtered, dims=1)
# X_std = zeros(size(X_filtered)[1], size(X_filtered)[2])
# for i in 1:size(X_filtered)[2]
# 	X_std[:,i] = (X_filtered[:,i] .- X_alelle_freqs[i]) / sqrt(X_alelle_freqs[i]*(1-X_alelle_freqs[i]))
# end

### test non-standardized X
# X_std = X_filtered
X_std = hcat(X_filtered, PC_covariates)

### define the ridge estimator for beta (with exception handling when inv( (X * X') does not exist then calculate a pseudo-inverse via SVD instead)
function beta_estimator(y, X, lambda)
	beta = [] # beacuse of damned scoping!!!
	try
		append!(beta, X' * inv( (X * X') + (lambda * I) ) * y)
	catch
		SVD = LinearAlgebra.svd( (X * X') + (lambda * I) )
		U = SVD.U
		S = SVD.S
		V = SVD.V
		D = LinearAlgebra.Diagonal(SVD.S)
		append!(beta, X' * V * inv(D) * U' * y)
		# append!(beta, X' * pinv( (X * X') + (lambda * I) ) * y)
	end
	return(beta)
end

### cross validation to find the best lambda that minimizes residual variance
# lambda_limits, y, X, k=k-fold
function CV_lambda(lambda_limits, y, X, k)
	n = length(y)
	s = floor(n/k)
	lambda_min = lambda_limits[1]
	lambda_max = lambda_limits[2]
	# generate 100 lambdas to test
	lambda_interval = (lambda_max - lambda_min) / 100
	LAMBDAS = collect(lambda_min:lambda_interval:(lambda_max-lambda_interval))
	CV_ERROR = []
	pb = ProgressMeter.Progress(length(LAMBDAS)*k, dt=1, desc="Lambda CV: ", barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow)
	pb_counter = [0]
	for lambda in LAMBDAS
		residual_var = []
		for i in 1:k
			idx_validation = convert.(Int64, collect((((i-1)*s)+1):1:(i*s)))
			idx_training = convert.(Int64, filter!(x->x ∉ idx_validation, collect(1:1:n)))
			beta_hat = beta_estimator(y[idx_training], X[idx_training,:], lambda)
			append!(residual_var, sum((y[idx_validation] .- (X[idx_validation,:] * beta_hat)).^2) / length(y[idx_validation]))
		pb_counter[1] = pb_counter[1] + 1
		ProgressMeter.update!(pb, pb_counter[1])
		end
		append!(CV_ERROR, mean(residual_var))
	end
	# output the log10 lambdas and their corresponding error variances
	return([log.(10, LAMBDAS), CV_ERROR])
end

lambda_max = 10.0 * maximum(LinearAlgebra.svd(X_std * X_std').S)
lambda_min = 0.05*lambda_max
@time cv_lambda_list = CV_lambda([lambda_min, lambda_max], y_sub, X_std, 10)
# just looking at the lambdas and their corresponding cv error
plt = Plots.plot(cv_lambda_list[1], cv_lambda_list[2], seriestype=:scatter)

lambda = cv_lambda_list[1][cv_lambda_list[2] .== minimum(cv_lambda_list[2])][1]
# lambda = 0.5
beta = beta_estimator(y_sub, X_std, lambda)
beta = beta[1:size(X_filtered)[2]]

### is there an analytical way to get the p-values instead of this here below?!
mu = mean(beta)
sd = std(beta)
function lod_beta(b, m, s)
	if b < m
		lod = log10(2) + Distributions.logcdf(Distributions.Normal(m, s), b)
	else
		lod = log10(2) + (Distributions.logccdf(Distributions.Normal(m, s), b))
	end
	return(-lod)
end
lod = [lod_beta(x, mu, sd) for x in beta]

# ### well in fact there is!
# y_hat = X_std * beta
# y_err = y_sub .- y_hat
# Ve = (1/length(y_sub)) * sum( y_err' * y_err )
# Vbeta_sd = (X_std' * inv(X_std * X_std')) .* (inv(X_std * X_std')' * X_std)
# se_beta = sqrt.(Ve .* (Vb_sd))
# t_val = abs.(beta ./  se_beta)
# pval = 2 .* (1.00 .- Distributions.cdf.(Distributions.TDist(size(X_std)[1]-1), t_val))

PLOT(lod, chrom_filtered)
cor(BETA, beta)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#####################
### Pool-seq GWAS ###
#####################

###############
### GWAlpha ###
############### implement this or just call the existing GWAlpha module

##########
### RR ###
########## does this actually work?!?!?
sort_index = sortperm(y_sub)
y_pool = y_sub[sort_index]
X_pool = X_filtered[sort_index, :]

npools = 5
pool_size = convert(Int32, floor(length(y_pool)/npools))
remainder = length(y_pool) - (pool_size*npools)
X_freq = zeros(npools, size(X_pool)[2])
y_perc = []
for i in 1:npools
	idx1 = ((i-1) * pool_size) + 1
	idx2 = i * pool_size
	if i != npools
		X_freq[i, :] = mean(X_pool[idx1:idx2, :], dims=1) ./ 2
		append!(y_perc, maximum(y_pool[idx1:idx2]))
	else
		X_freq[i, :] = mean(X_pool[idx1:length(y_pool), :], dims=1) ./ 2
		append!(y_perc, maximum(y_pool[idx1:length(y_pool)]))
	end
end

# X_freq = hcat(X_freq, covariate, PC_covariates) ### correcting for population structure would be CHALLENGING UNDER POOL-SEQ!!!!
RCall.R"X = matrix(as.numeric($X_freq), nrow=$npools, byrow=FALSE)"
RCall.R"PCA = prcomp(t(X))"
RCall.R"PC_covar = PCA$rotation"
RCall.@rget PC_covar
X_freq = hcat(PC_covar, X_freq)

# y_perc = append!([minimum(y_pool)], y_perc[1:(end-1)]) + ( ((append!([minimum(y_pool)], y_perc[1:(end-1)]))+(y_perc))/2 )

# @time cv_lambda_list = CV_lambda([0, 20], y_perc, X_freq, 5)
# # just looking at the lambdas and their corresponding cv error
# plt = Plots.plot(cv_lambda_list[1], cv_lambda_list[2], seriestype=:scatter)

# lambda = cv_lambda_list[1][cv_lambda_list[2] .== minimum(cv_lambda_list[2])][1]
# lambda = 0.5 # need to find the best lambda via cross-validation <<<<<<<_-----------
lambda_max = 10.0 * maximum(LinearAlgebra.svd(X_freq * X_freq').S)
lambda_min = 0.05*lambda_max
@time cv_lambda_list = CV_lambda([lambda_min, lambda_max], y_perc, X_freq, 2)
# just looking at the lambdas and their corresponding cv error
plt = Plots.plot(cv_lambda_list[1], cv_lambda_list[2], seriestype=:scatter)
lambda_opt = cv_lambda_list[1][cv_lambda_list[2] .== minimum(cv_lambda_list[2])][1]
b = X_freq' * inv((X_freq * X_freq') + (lambda_opt * I)) * y_perc
beta = b[(npools+1):end]
# beta = b

### fitting beta ~ Norm(mu_beta, sd_beta) and calculating p-val from this distribution
mu = mean(beta)
sd = std(beta)
function lod_beta(b, m, s)
	if b < m
		lod = log10(2) + Distributions.logcdf(Distributions.Normal(m, s), b)
	else
		lod = log10(2) + (Distributions.logccdf(Distributions.Normal(m, s), b))
	end
	return(-lod)
end
lod = [lod_beta(x, mu, sd) for x in beta]
# lod = lod[(npools+1):end]

### analytica p-values
# y_hat = X_freq * beta
# y_err = y_perc .- y_hat
# Vy_err = 1/length(y_sub) * (y_err' * y_err)
# # Vbeta_sd = ((X' * inv(X*X')) * (inv(X*X')' * X))
# # b_ster = sqrt(Vy_err * [Vbeta_sd[i,i] for i in 1:length(beta)])
# b_ster = sqrt( Vy_err .* ((X' * inv(X*X')) .* (inv(X*X')' * X)) )
# t_val = abs(b_hat[2]) / b_ster
# p_val = 2*(1.00 - Distributions.cdf(Distributions.TDist(n-1), t_val))

PLOT(lod, chrom_filtered .+ 1)
Plots.plot(LOD, lod, seriestype=:scatter)
cor(LOD, lod)
StatsBase.corspearman(hcat(BETA, beta))

max_QTL_chr = chrom_filtered[maximum(lod) .== lod]
max_QTL_pos = POS_filtered[maximum(lod) .== lod]
