###################################################
###                                             ###
### EXTRACTING QUANTINEMO2 LD STATS (R²) OUTPUT ###
###                                             ###
###################################################

#####################
### load packages ###
#####################
using DelimitedFiles
using CSV
using DataFrames
using LinearAlgebra
using ProgressMeter
using Plots; Plots.pyplot()
using StatsBase
using Statistics
using Optim

#############
### input ###
#############
LD_fname = ARGS[1]
loci_spec_fname = ARGS[2]

#############################
### load the LD (R2) data ###
#############################
# vec_labels = DelimitedFiles.readdlm("LD_labels.temp", '\t')
# vec_LD = DelimitedFiles.readdlm("LD_data.temp", '\t')
vec_LD = DelimitedFiles.readdlm(LD_fname, '\t')
nLoci = convert(Int, round( ( -(-1) + sqrt((-1^2) - (4*(1)*(-2*size(vec_LD)[1]))) ) / (2*(1)) )) #quadratic formula solving {((l^2) - l) / 2} = nrow(vec_LD)

##########################
### load the loci info ###
##########################
# loci_spec = CSV.read("QUANTI_g140_p1_LOCI_SPEC.csv", delim=',')
loci_spec = CSV.read(loci_spec_fname, delim=',')

#########################################################################
### insert the LD (R2) values into an upper triangular matrix         ###
### while removing R2 between loci pairs across different chromosomes ###
### (convert them into NaN and exclude them from the vector output)   ###
#########################################################################
println("Parse the LD vector data into (1) a tringular matrix and (2) a new vector where pairwise R2 across chromosomes are removed!")
pb = ProgressMeter.Progress(nLoci-1, dt=1, barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow)
# mat_LD = LinearAlgebra.UpperTriangular( Array{Float64}(undef, nLoci, nLoci) )
mat_LD = Array{Float64}(undef, nLoci, nLoci)
vec_LD_NEW = []
ld_counter = [1]
chr_counter = [1]
for i in 1:(nLoci-1)
  for j in (i+1):nLoci
    if loci_spec.CHR[i] == chr_counter[1]
      if loci_spec.CHR[j] == chr_counter[1]
        mat_LD[i,j] = vec_LD[ld_counter[1]]
        append!(vec_LD_NEW, vec_LD[ld_counter[1]])
      else
        mat_LD[i,j] = NaN
      end
    else
      mat_LD[i,j] = vec_LD[ld_counter[1]]
      append!(vec_LD_NEW, vec_LD[ld_counter[1]])
      chr_counter[1] = chr_counter[1] + 1
    end
    ld_counter[1] = ld_counter[1] + 1
  end
  ProgressMeter.update!(pb, i)
end
# change diagonals into NaN
for i in 1:nLoci
  mat_LD[i,i] = NaN
end
# plot heatmap and save
Plots.heatmap(collect(1:nLoci), collect(1:nLoci), mat_LD, aspect_ratio=1, size=(3000,3000));
Plots.savefig("LD_heatmap.png")

######################################################################################
### compute the pairwise distances to match with the LD data vector PER CHROMOSOME ###
######################################################################################
println("Compute the pairwise distances per chromosome!")
pb = ProgressMeter.Progress(length(unique(loci_spec.CHR)), dt=1, barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow)
vec_chr = []
vec_dist = []
for chr in unique(loci_spec.CHR)
  subdat = loci_spec[loci_spec.CHR .== chr, :]
  for i in 1:(size(subdat)[1]-1)
    for j in (i+1):size(subdat)[1]
      append!(vec_chr, subdat.CHR[i])
      append!(vec_dist, subdat.POS[j] - subdat.POS[i])
    end
  end
  ProgressMeter.update!(pb, chr)
end
# merge distances and R2 into a dataframe and save as a csv file with header ###
LD = DataFrames.DataFrame(CHR=vec_chr, DIST=vec_dist, R2=vec_LD_NEW)
CSV.write("LD_data_parsed.csv", LD, delim=',')

##########################
### modelling LD decay ###
##########################
println("Modelling LD decay!")
println("and determining the distance at which R²=20%.")
# sample a some data points
idx_random_sample = StatsBase.sample(1:size(LD)[1], 5000, replace=false)
# idx_random_sample = 1:size(LD)[1]
x = LD.DIST[idx_random_sample] #remove missing R2 data
y = LD.R2[idx_random_sample]
x = x[.!isnan.(y)]
y = y[.!isnan.(y)]
# sort by increasing distance (for ease in the final steps)
idx_sort_by_distance = sortperm(x)
x = x[idx_sort_by_distance] ./ 1e3 # unit in kilobases (kb)
y = y[idx_sort_by_distance]
# fit the R² distribution on a generic exponential decay function
function FUNC_LD(par, x)
    a = par[1]
    b = par[2]
    c = par[3]
    # y = a .+ (b .* exp.(-c .* x))
    # y = a .+ (b ./ (c .* x))
    d = par[4]
    y = a .+ (b ./ (c .* (x .^ d)))
    # y = a .+ (b ./ (c .* exp.(d .* x)))
    # # y = a .+ (b .* x) .+ (c .* (x.^2))
    return(y)
end
function FUNCT_OPTIM_LD(par, x, y)
    y_bar = FUNC_LD(par, x)
    out = sum( abs.(y_bar .- y) )
    return(out)
end
par_ini = [1.0, 1.0, 1.0, 1.0]
vec_par = Optim.optimize(par->FUNCT_OPTIM_LD(par, x, y), par_ini, NelderMead()).minimizer
newx = collect(minimum(x):1:maximum(x))
newy = FUNC_LD(vec_par, newx)
### find the distance at with R2 == 0.20 (given he model in FUNC_LD: y = a .+ (b ./ (c .* (x .^ d))))
a, b, c, d = vec_par
x_decay = abs( convert(Complex, -b / (c*(0.2-a)) )^(1/d) )#unit in kilobases (kb) #converted to Complex number and then reduced to its absolute value to prevent Complex number error
# plot the LD decay as a function of distance (scatterplot)
Plots.plot( x,
            y,
            seriestype=:scatter,
            marker=(:circle, 4, 0.5, :green, Plots.stroke(0, :white)),
            xlabel="Distance (kb)",
            ylabel="R²",
            legend=false,
            size=(800,500)
            );
Plots.plot!(newx, newy, lw=2, color=:orange);
Plots.annotate!(3e5, 0.4, text(string("R²=0.20 at ", convert(Int, round(x_decay)), "kb"), :center));
Plots.savefig("LD_decay.svg")
### save the distance at which R2 goes down to 0.20
CSV.write("LD_decay_R2_20p.csv", DataFrames.DataFrame(Distance_kb_r2_20=x_decay))
