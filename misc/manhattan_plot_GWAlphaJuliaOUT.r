#!/usr/bin/env Rscript
ARGS = commandArgs(trailing=TRUE)
juliaGWAlpha_filename = ARGS[1]

### plot julia's GWAlpha output

library(ggplot2)

dat = read.csv(juliaGWAlpha_filename)
nsnp = nrow(dat)
dat$LOCUS = rep(1:(nsnp/6), each=6)
bonferroni_threshold = -log10(0.05 / nsnp)

filename_out = strsplit(strsplit(juliaGWAlpha_filename, "/")[[1]][length(strsplit(juliaGWAlpha_filename, "/")[[1]])], ".csv")[[1]]

jpeg(paste0(filename_out, ".jpeg"), quality=100, width=1500, height=500)
ggplot(data=dat, aes(x=LOCUS, y=LOD, group=ALLELE, color=ALLELE, fill=ALLELE)) +
	geom_point(alpha=0.5) +
	geom_hline(yintercept=bonferroni_threshold, color="black", linetype="dashed") +
	theme_linedraw()
dev.off()
