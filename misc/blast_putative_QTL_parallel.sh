#!/bin/bash

peaks_file=${1}
query_halflen=${2}
DB=${3}
REFORMATED_GENOME=${4}

# ### TEST:
# peaks_file=IS_pheno-FIXED_GWAlpha-PEAKS_ID.csv
# query_halflen=1500
# DB=/data/BlastDB/NCBI_NT0060
# REFORMATED_GENOME=REFORMATED_GENOME.fasta


### BUILD THE QUERY SEQUENCES
rm ${peaks_file}.query.fasta || \
touch ${peaks_file}.query.fasta
for locus in $(cat ${peaks_file})
do
  # locus=$(head -n1 ${peaks_file})
  chrom=$(cut -d, -f1 <<<$locus)
  pos=$(cut -d, -f2 <<<$locus)
  grep -A 1 ${chrom} ${REFORMATED_GENOME} | tail -n1 > ${peaks_file}.seq.temp
  bp=$(cat ${peaks_file}.seq.temp | wc -c)
  start=$(echo "${pos} - ${query_halflen}" | bc)
  end=$(echo "${pos} + ${query_halflen}" | bc)
  if [ $start -lt 1 ]
  then
    start=1
  fi
  if [ $end -gt $bp ]
  then
    end=${bp}
  fi
  echo -e "> ${chrom}:${pos}:${start}-${end}" >> ${peaks_file}.query.fasta
  cut -c${start}-${end} ${peaks_file}.seq.temp >> ${peaks_file}.query.fasta ||
  cut -c1-${end} ${peaks_file}.seq.temp >> ${peaks_file}.query.fasta ||
  cut -c${start}-${bp} ${peaks_file}.seq.temp >> ${peaks_file}.query.fasta ||
  cut -c1-${bp} ${peaks_file}.seq.temp >> ${peaks_file}.query.fasta
  rm ${peaks_file}.seq.temp
done

### BLAST 'EM!'
DIR=$(dirname $peaks_file)
FNAME=$(basename $peaks_file)
blastn -db ${DB} \
  -query ${peaks_file}.query.fasta \
  -perc_identity 90 \
  -outfmt '6 qseqid staxids pident evalue qcovhsp bitscore stitle' \
  -out ${DIR}/BLASTOUT-${FNAME}.txt
