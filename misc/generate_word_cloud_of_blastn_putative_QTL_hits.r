##################################################
### GENERATE WORD CLOUD OF BLAST HITS KEYWORDS ###
##################################################

args = commandArgs(trailing=TRUE)
BLAST_OUT_CONSOLIDATED_FNAME = args[1]
NTOP = as.numeric(args[2])
# BLAST_OUT_CONSOLIDATED_FNAME = "BLAST_OUT_CONSOLIDATED.txt"
# NTOP = as.numeric("5")

########################
### SAMPLE EXECUTION ###
# Rscript generate_word_cloud_of_blastn_putative_QTL_hits.r \
#   BLAST_OUT_CONSOLIDATED.txt \
#   5
########################

library(wordcloud)
library(RColorBrewer)

dat = read.delim(BLAST_OUT_CONSOLIDATED_FNAME, header=TRUE)
remove_these_strings = c("var\\.", "[Gg]enome", "[Gg]enomic", "[Ss]caffold", "subsp\\.", "[Cc]omplete", "DNA", "[Cc]hromosome",
"[Ss]train", "[Pp]lastid", "[Cc]omplex", "and", "[Gg]ene", "[Ss]equence", "[Ss]train", "[Cc]lone", "cds", "CDS", "[Pp]rotein",
"[Aa]ssembly", "[Pp]artial", "[Cc]ultivar", "\\(", "\\)", "\\;", "\\:", "\\,", "\\.")
# remove_these_strings = c(remove_these_strings, "NA", "[Cc]hinese", "Spring", "Triticum", "aestivum", "3B", "\\W*\\b\\w\\b\\W*")
# remove_these_strings = c(remove_these_strings, "NA", "[Cc]hinese", "Spring", "Triticum", "aestivum", "3B")

for (id in levels(dat$ID)){
  # id = levels(dat$ID)[1]
  sub = subset(dat, ID==id)
  sub = droplevels(sub)
  PUTATIVE_QTL_LIST = levels(sub$PUTATIVE_QTL)
  N_PUTATIVE_QTL = length(PUTATIVE_QTL_LIST)
  chrom = c(); pos = c(); range = c(); coverage = c(); bit_score = c(); hit = c()
  for (locus in PUTATIVE_QTL_LIST){
    # locus = PUTATIVE_QTL_LIST[1]
    qtl = subset(sub, PUTATIVE_QTL==locus)
    # qtl = qtl[order(qtl$BITSCORE, decreasing=TRUE), ]
    qtl = qtl[order(qtl$PERCENT_IDENTITY, decreasing=TRUE), ]
    if (is.na(NTOP) == TRUE){
      NTOP = nrow(qtl)
    }
    split = strsplit(locus, ":")[[1]]
    chrom = c(chrom, rep(split[1], times=NTOP))
    pos = c(pos, rep(split[2], times=NTOP))
    range = c(range, rep(split[3], times=NTOP))
    coverage = c(coverage, qtl$QUERY_COVERAGE[1:NTOP])
    bit_score = c(bit_score, qtl$BITSCORE[1:NTOP])
    hit = c(hit, as.character(qtl$HIT_INFO[1:NTOP]))
  }
  for (str in remove_these_strings){
    hit = gsub(str, "", hit)
    hit = gsub(" *\\b[[:alpha:]]{1,2}\\b *", " ", hit) # Remove 1-2 letter words
    hit = gsub("^ +| +$|( ) +", "\\1", hit) # Remove excessive spacing
  }
  eval(parse(text=paste0(id, "= data.frame(CHROM=chrom, POS=pos, RANGE=range, QUERY_COVERAGE=coverage, BITSCORE=bit_score, HIT_INFO=hit)")))
  TABLE = as.data.frame(table(strsplit(paste(eval(parse(text=paste0(id, "$HIT_INFO"))), collapse=" "), " ")[[1]]))
  if (as.character(TABLE$Var1[TABLE$Freq==max(TABLE$Freq)]) == ""){
    TABLE = TABLE[order(TABLE$Freq, decreasing=TRUE), ]
    TABLE = TABLE[2:nrow(TABLE), ]
  }
  TABLE = TABLE[TABLE$Var1 != "NA", ]
  ### Transform frequencies bounded between zero to one
  # TABLE$Freq = (TABLE$Freq - mean(TABLE$Freq)) / sd(TABLE$Freq)
  svg(paste0(id, "_PUTATIVE_QTL_BLASTN_WORDCLOUD.svg"), width=7, height=7)
  wordcloud(words=TABLE$Var1, freq=TABLE$Freq, random.order=FALSE, min.freq=1, rot.per=0.35, colors=brewer.pal(8, "Dark2"))
  dev.off()
}
