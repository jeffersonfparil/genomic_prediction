### Main repo moved to https://github.com/jeffersonfparil/violin_plotter
# parse_formula = function(formula, data=NULL, IMPUTE=FALSE, IMPUTE_METHOD=mean){
#   ### parse the input formula
#   response_var = as.character(unlist(as.list(attr(terms(formula), "variables"))[-1]))[1]
#   explanatory_var = as.character(unlist(as.list(attr(terms(formula), "term.labels"))))
#   ### build the dataframe with explicit interaction variables (columns) if included in the formula
#   non_interaction_terms = explanatory_var[!grepl(":", explanatory_var)]
#   interaction_terms = explanatory_var[grepl(":", explanatory_var)]
#   explanatory_list = list()
#   for (i in 1:length(c(non_interaction_terms, interaction_terms))){
#     # i = 1
#     term = c(non_interaction_terms, interaction_terms)[i]
#     explanatory_list[[i]] = eval(parse(text=paste0("paste(", paste(paste0("data$", unlist(strsplit(term, ":"))), collapse=","), ", sep=':')")))
#   }
#   # df =  eval(parse(text=paste0("data.frame(y=data$", response_var, ",", gsub("\"", "'", paste(paste(explanatory_list), collapse=", ")), ")")))
#   df =  eval(parse(text=paste0("data.frame(y=data$", response_var, ",", gsub("-", "_", gsub("\"", "'", paste(paste(explanatory_list), collapse=", "))), ")")))
#   ### impute missing response variable data?
#   if (IMPUTE == TRUE) {
#     idx_missing = is.na(df$y) | is.infinite(df$y)
#     df$y[idx_missing] = IMPUTE_METHOD(df$y[!idx_missing])
#     # eval(parse(text=paste0("data$", response_var, "[idx_missing] = IMPUTE_METHOD(df$", response_var, "[!idx_missing])")))
#   }
#   df = df[complete.cases(df), ]
#   colnames(df) = c(response_var, non_interaction_terms, interaction_terms)
#   return(df)
# }
#
# plot_violin = function(dat, response_variable_name, explanatory_variable_name, title="", xlab="", ylab="", COLOURS=c("#e0f3db", "#ccebc5", "#a8ddb5", "#7bccc4", "#4eb3d3", "#2b8cbe"), BAR_COLOURS=c("#636363", "#1c9099", "#de2d26"), XTICKS=TRUE, LOG=FALSE, BASE=10){
#   ### extract the dependent or response or y variable, as well as the independent or explanatory or x variable
#   x = eval(parse(text=paste0("dat$`", explanatory_variable_name, "`"))) ### numeric or categorical
#   x = as.factor(gsub("-", "", x)) ### remove "-" because it will conflict with the string splitting if performing TukeyHSD()
#   y = eval(parse(text=paste0("dat$`", response_variable_name, "`")))    ### numeric
#   # y = rnorm(length(y)) ### null test
#   ### merge them into a data frame for ease of handling
#   ### while converting the x variable into both categorical and numeric variables
#   df = tryCatch(
#           data.frame(y=y, x_categorical=as.factor(as.character(x)), x_numeric=as.numeric(as.character(x))),
#           warning=function(e){
#             data.frame(y=y, x_categorical=as.factor(as.character(x)), x_numeric=as.numeric(x))
#           }
#         )
#   df = df[complete.cases(df), ]
#   ### transform the x axis into log-scale for ease of viewing
#   if (LOG==TRUE){
#     df$x_numeric=log(df$x_numeric, base=BASE)
#     df$x_categorical=as.factor(round(df$x_numeric, 2))
#   }
#   ### extract the levels and unique values of the x variable
#   x_levels = levels(df$x_categorical)
#   x_numbers = tryCatch(
#                 as.numeric(as.character(x_levels)),
#                 warning=function(e){
#                   as.numeric(as.factor(x_levels))
#                 }
#               )
#   ### calculate the summary statistics of the x and y vairables
#   x_min = min(df$x_numeric)
#   x_max = max(df$x_numeric)
#   x_sd = sd(df$x_numeric)
#   y_min = min(df$y)
#   y_max = max(df$y)
#   y_sd = sd(df$y)
#   ### calculate the maximum interval between two levels of the x variable (divide by 2)
#   max_x_interval = min(x_numbers[order(x_numbers)][2:length(x_numbers)] - x_numbers[order(x_numbers)][1:length(x_numbers)-1]) / 2
#   ### repeat violin plot colours to fit the number of explanatory variable levels
#   COLOURS = rep(COLOURS, times=ceiling(length(x_levels)/length(COLOURS)))
#   ### define las: i.e. the orientation of the x-axis tick labels
#   ### as well as the plot margins and the x-axis label
#   max_nchar = max(unlist(lapply(x_levels, FUN=nchar)))
#   if (max_nchar > 10){
#     las = 2
#     par(mar=c(max_nchar*0.70, 5, 7, 2))
#     xlab=""
#   } else {
#     las =1
#     par(mar=c(5, 5, 7, 2))
#   }
#   ### initialize the plot with or without the x-axis
#   if (XTICKS==TRUE){
#     ### for strictly categorical explanatory variable
#     plot(x=c(x_min-max_x_interval, x_max+max_x_interval), y=c(y_min-y_sd, y_max+y_sd), type="n", main=title, xlab=xlab, ylab=ylab, las=las, xaxt="n")
#     axis(side=1, at=x_numbers, labels=x_levels, las=las)
#   } else {
#     ### for continuous explanatory variable
#     plot(x=c(x_min-max_x_interval, x_max+max_x_interval), y=c(y_min-y_sd, y_max+y_sd), type="n", main=title, xlab=xlab, ylab=ylab, las=las)
#   }
#   ### iteratively plot the density of each explanatory variable level
#   for (i in 1:length(x_levels)){
#     # i = 1
#     subdat = droplevels(subset(df, x_categorical==x_levels[i]))
#     ### calculate the summary statistics of the response variable (y): mean, standard deviation, standard error, and 95% confidence interval
#     mu = mean(subdat$y)
#     sigma = sd(subdat$y)
#     se = sd(subdat$y)/sqrt(nrow(subdat)-1)
#     ci = qnorm(((0.95)/2)+0.50) * se
#     ### calculate the density with binning adjustment proportional the number of observations divded by 1x10^5
#     d = density(subdat$y, adjust=max(c(1, nrow(subdat)/1e5)))
#     ### restrict the range of the response variable to the input dataframe
#     d$y = d$y[(d$x >= min(df$y)) & (d$x <= max(df$y))]
#     d$x = d$x[(d$x >= min(df$y)) & (d$x <= max(df$y))]
#     ### tranform the density (d$y) into the 0.00 to 1.00 range (in preparation to be mutiplied withe maximum interval variable: "max_x_interval")
#     d.y_min = min(d$y)
#     d.y_max = max(d$y)
#     d$y = (d$y - d.y_min) / (d.y_max - d.y_min)
#     ### define the x-axis points of the polygon defined as the density values left and right of the explanatory variable (df$x) level or value
#     poly_x = c(x_numbers[i]-rev(d$y*max_x_interval), x_numbers[i]+(d$y*max_x_interval))
#     ### define the y-axis points of the polygon defined as the range of values of the response variable (df$y)
#     poly_y = c(rev(d$x), d$x)
#     ### draw the polygon
#     polygon(x=poly_x, y=poly_y, border=NA, col=COLOURS[i])
#     ### plot the summary statistics
#     arrows(x0=x_numbers[i], y0=mu+sigma, y1=mu-sigma, angle=90, code=3, lwd=2, length=0.1, col=BAR_COLOURS[1])
#     arrows(x0=x_numbers[i], y0=mu+se, y1=mu-se, angle=90, code=3, lwd=2, length=0.1, col=BAR_COLOURS[2])
#     arrows(x0=x_numbers[i], y0=mu+ci, y1=mu-ci, angle=90, code=3, lwd=2, length=0.1, col=BAR_COLOURS[3])
#     points(x=x_numbers[i], y=mu, pch=20)
#     # print(x_levels[i])
#     # print(mean(subdat$y))
#   }
#   ### plot grid lines
#   grid()
#   ### show the summary statistics legend
#   legend("bottomright", inset=c(0, 1), xpd=TRUE, horiz=TRUE, bty="n", col=unlist(BAR_COLOURS), cex=0.75, lty=1, lwd=2, legend=c("Standard Deviation", "Standard Error", "95% Confidence Interval"))
#   ### return the levels and unique values of the x variable
#   return(list(x_levels=x_levels, x_numbers=x_numbers))
# }
#
# mean_comparison_HSD = function(formula, data=NULL, explanatory_variable_name, alpha=0.05, LOG=FALSE, BASE=10, x_levels=x_levels, x_numbers=x_numbers, PLOT=FALSE) {
#   # explanatory_variable_name = "x1"; alpha = 0.05
# 	# mod = aov(formula, data=data)
#   # anova_table = as.data.frame(anova(mod))
#   # if (anova_table$Pr[rownames(anova_table) == explanatory_variable_name] < alpha){
#   #   print(anova_table)
#   #   print(paste0(explanatory_variable_name, " has a significant effect on the response variable!"))
#   # } else {
#   #   print(anova_table)
#   #   print(paste0(explanatory_variable_name, " has a no significant effect on the response variable!"))
#   # }
#   ### parse the formula and generate the dataframe with explicit interaction terms if expressed in the formula
#   df = parse_formula(formula=formula, data=data, IMPUTE=FALSE, IMPUTE_METHOD=mean)
#   response_var = df[,1]; response_var_name = colnames(df)[1]
#   explanatory_var = df[,2:ncol(df)]; explanatory_var_name = colnames(df)[2:ncol(df)]
#   ### linear modelling
#   mod = aov(formula, data=df)
#   anova_table = as.data.frame(anova(mod))
#   if (anova_table$Pr[rownames(anova_table) == explanatory_variable_name] < alpha){
#     print(anova_table)
#     print(paste0(explanatory_variable_name, " has a significant effect on the response variable!"))
#   } else {
#     print(anova_table)
#     print(paste0(explanatory_variable_name, " has a no significant effect on the response variable!"))
#   }
#   ### computate the means per explanatory variable level
#   means = eval(parse(text=paste0("aggregate(",  response_var_name, "~ `", explanatory_variable_name, "`, data=df, FUN=mean)")))
#   means = means[order(means[,2], decreasing=TRUE), ]
#   ### compute the HSD pairwise comparison
#   hsd = eval(parse(text=paste0("as.data.frame(TukeyHSD(mod, conf.level=", 1.00-alpha, ")$`", explanatory_variable_name, "`)")))
#   ### add "LEVEL_" string to allow for explanatory variable that are originally numeric to be easily set as list names
#   factor_labels = matrix(paste0("LEVEL_", unlist(strsplit(rownames(hsd), "-"))), ncol=2, byrow=TRUE)
#   hsd$factor1 = factor_labels[,1]
#   hsd$factor2 = factor_labels[,2]
#   factors_all = paste0("LEVEL_", as.character(means[,1]))
#   ### initialize the list of HSD grouping of each response variable level
#   GROUPING_LIST = eval(parse(text=paste0("list('LEVEL_", paste(as.character(means$x), collapse="'=c(), 'LEVEL_"), "'=c())")))
#   ### generate the vector of letters and numbers for grouping
#   letters_vector = c(letters, LETTERS, 1:(nrow(hsd)^2))
#   ### iterate across response variable level
#   letter_counter = 1
#   for (f in factors_all){
#     # f = factors_all[1]
#     ### subset the current factor level
#     subhsd = hsd[(hsd$factor1==f) | (hsd$factor2==f), ]
#     ### identify the factor levels that are not significantly from the current factor level: f
#     nonsigfactors = unique(c(subhsd$factor1[subhsd$p > 0.05], subhsd$factor2[subhsd$p > 0.05]))
#     nonsigfactors = nonsigfactors[!(nonsigfactors %in% f)]
#     ### define the current letter grouping
#     letter_add = letters_vector[letter_counter]
#     new_letter_bool = 0 ### for testing if we need a new letter
#     ### iterate across non-significantly different factor levels to the current factor
#     for (g in nonsigfactors){
#       # g = nonsigfactors[1]
#       f_letters = eval(parse(text=paste0("GROUPING_LIST$`", f, "`"))) ### currect factor grouping
#       g_letters = eval(parse(text=paste0("GROUPING_LIST$`", g, "`"))) ### grouping of the non-siginificantly different factor level
#       ### test if the current factor level is the same as the non-siginificantly different factor level or if we are at the start
#       # if (paste(f_letters, collapse="") != paste(g_letters, collapse="") | is.null(f_letters)){
#       if ( !((sum(f_letters %in% g_letters)>0) | (sum(g_letters %in% f_letters)>0)) | is.null(f_letters) ) {
#         eval(parse(text=paste0("GROUPING_LIST$`", g, "` = c(", "GROUPING_LIST$`", g, "`, '", letter_add, "')")))
#         new_letter_bool = new_letter_bool + 1
#       }
#     }
#     ### add the current letter grouping
#     if ((new_letter_bool>0) | (length(nonsigfactors)==0)){
#       eval(parse(text=paste0("GROUPING_LIST$`", f, "` = c(", "GROUPING_LIST$`", f, "`, '", letter_add, "')")))
#       letter_counter = letter_counter + 1
#     }
#   }
#   ### prepare the grouping list
#   GROUPING_LIST = as.matrix(lapply(GROUPING_LIST, FUN=paste, collapse=""))
#   GROUPING_LIST = data.frame(LEVELS=gsub("LEVEL_", "", as.character(rownames(GROUPING_LIST))), GROUPING=as.character(GROUPING_LIST[,1]))
#   if (LOG==TRUE){
#     ### transform the level names into the corresponding level names we used previously (x_levels and x_numbers) because we will be merging dataframes below
#     GROUPING_LIST$LEVELS = as.factor(round(log(as.numeric(as.character(GROUPING_LIST$LEVELS)), base=BASE), 2))
#   }
#   ### prepare the explanatory variable names and corresponding numbers
#   X_LEVELS_AND_NUMBERS = data.frame(LEVELS=x_levels, NUMBERS=x_numbers)
#   ### merge and append the grouping letters together with the means
#   colnames(means) = c("LEVELS", "MEANS")
#   MERGE_GROUPING_DF = merge(merge(GROUPING_LIST, X_LEVELS_AND_NUMBERS, by="LEVELS"), means, by="LEVELS")
#   if(PLOT){
#     text(x=MERGE_GROUPING_DF$NUMBERS, y=max(response_var)+sd(response_var), lab=as.character(MERGE_GROUPING_DF$GROUPING))
#   }
#   return(MERGE_GROUPING_DF)
# }
#
# plot_regression_line = function(dat, response_variable_name, explanatory_variable_name, LOG=FALSE, BASE=10, x_levels=x_levels, x_numbers=x_numbers, PLOT=TRUE, LINE_COL="gray") {
#   # dat=df; response_variable_name=response_var_name; explanatory_variable_name = explanatory_var_names[i]; LINE_COL="gray"
#   dat_defined_x_levels = eval(parse(text=paste0("levels(dat$`", explanatory_variable_name, "`)")))
#   if (sum( dat_defined_x_levels == x_levels ) == length(x_levels)) {
#     eval(parse(text=paste0("levels(dat$`", explanatory_variable_name, "`) = x_numbers")))
#     x = eval(parse(text=paste0("as.numeric(as.character(dat$`", explanatory_variable_name, "`))")))
#     y = eval(parse(text=paste0("dat$`", response_variable_name, "`")))
#     mod = lm(y ~ x)
#     b0 = mod$coefficients[1]
#     b1 = mod$coefficients[2]
#     r2adj = summary(mod)$adj.r.squared
#     regress_out = c(b0, b1, r2adj); names(regress_out) = c("intercept", "slope", "R2adj")
#     x_new = seq(from=min(x)-sd(x), to=max(x)+sd(x), length.out=100)
#     y_pred = mod$coefficients[1] + (mod$coefficients[2] * x_new)
#     lines(x=x_new, y=y_pred, lty=2, lwd=2, col=LINE_COL)
#     legend("bottomright", legend=paste0(c("y-intercept=", "slope=", "R2_adjusted="), round(regress_out,2)), cex=0.75)
#   } else {
#     stop("The levels of the explanatory variable does not match x_levels. Check if you can actually convert this explanatory variable into a continuous numeric variable.")
#   }
#   return(regress_out)
# }
#
# violin_plotter = function(formula, data=NULL, TITLE="", XLAB="", YLAB="", VIOLIN_COLOURS=c("#e0f3db", "#ccebc5", "#a8ddb5", "#7bccc4", "#4eb3d3", "#2b8cbe"), ERROR_BAR_COLOURS=c("#636363", "#1c9099", "#de2d26"), XCATEGOR=TRUE, LOGX=FALSE, LOGX_BASE=1, HSDX=TRUE, ALPHA=0.05, REGRESSX=FALSE){
#   #' Violin plotter with mean comparison bars and optional HSD grouping and regression line
#   #'
#   #' @usage violinplot_func(dat, response_var, explanatory_var, title="", xlab="", ylab="", COLOURS=c("#e0f3db", "#ccebc5", "#a8ddb5", "#7bccc4", "#4eb3d3", "#2b8cbe"), BAR_COLOURS=c("#636363", "#1c9099", "#de2d26"), XTICKS=TRUE, LOG=FALSE, BASE=1, HSD=TRUE, REGRESS=FALSE)
#   #'
#   #' @param formula R's compact symbolic form to represent linear models with fixed additive and interaction effects (See ?formula for more information) [mandatory]
#   #' @param data data.frame containing the response and explantory variables which forms the formula above [default=NULL]
#   #' @param TITLE string or vector of strings corresponding to all the explanatory terms including additive and interaction terms in the formula [default=""]
#   #' @param XLAB string or vector of strings specifing the x-axis labels [default=column names of the explanatory variables (and their combinations) from data]
#   #' @param YLAB string or vector of strings specifing the y-axis labels [default=column names of the response variable from data]
#   #' @param VIOLIN_COLOURS vector of colors of the violin plots which are repeated if the length is less than the number of explanatory factor levels [default=c("#e0f3db", "#ccebc5", "#a8ddb5", "#7bccc4", "#4eb3d3", "#2b8cbe")]
#   #' @param ERROR_BAR_COLOURS vector of colours of standard deviation, standard error and 95 percent confidence interval error bars (error bar selection via leaving one of the three colors empty) [default=c("#636363", "#1c9099", "#de2d26")]
#   #' @param XCATEGOR logical or vector of logicals referring to whether the explanatory variable/s is/are strictly categorcial [default=TRUE]
#   #' @param LOGX logical or vector of logicals referring to whether to transform the explanatory variable/s into the logarithm scale [default=FALSE]
#   #' @param LOGX_BASE numeric or vector of numerics referring to the logarithm base to transform the explanatory variable/s with [default=1]
#   #' @param HSDX logical or vector of logicals referring to whether to perform Tukey's Honest Significance Grouping [default=TRUE]
#   #' @param ALPHA numeric significance level for the analysis of variance F-test and Tukey's mean comparison [default=0.05]
#   #' @param REGRESSX logical or vector of logicals referring to whether to regress the response variable against the explantory variable/s [default=FALSE]
#   #'
#   #' @return Violin plot/s with optional error bars, mean comparison grouping/s, and regression line/s
#   #' @return Mean comparison grouping/s based on Tukey's Hones significant difference and regression line statistics, if applicable
#   #'
#   #' @examples
#   #' x1 = rep(rep(rep(letters[1:5], each=5), times=5), times=5)
#   #' x2 = rep(rep(letters[6:10], each=5*5), times=5)
#   #' x3 = rep(letters[11:15], each=5*5*5)
#   #' y = rep(1:5, each=5*5*5) + rnorm(rep(1:5, each=5), length(x1)) ### x3 is the variable affecting y (see each=5*5*5)
#   #' data = data.frame(x1, x2, x3, y)
#   #' formula = y ~ x1 + x2 + x3 + (x2:x3)
#   #' OUT = violinplot_func(formula=formula, data=data)
#
#   ### parse the formula and generate the dataframe with explicit interaction terms if expressed in the formula
#   df = parse_formula(formula=formula, data=data, IMPUTE=FALSE, IMPUTE_METHOD=mean)
#   response_var = df[,1]
#   explanatory_var = df[,2:ncol(df)]
#   response_var_name = colnames(df)[1]
#   explanatory_var_names = colnames(df)[2:ncol(df)]
#
#   ### set axes labels and titles
#   if (YLAB==""){
#     YLAB = response_var_name
#   }
#   if ( (XLAB=="") | (length(XLAB) != ncol(df)-1) ){
#     XLAB = explanatory_var_names
#   }
#   if ( (TITLE=="") | (length(TITLE) != ncol(df)-1) ){
#     TITLE = paste0(YLAB, "\nX\n", XLAB)
#   }
#
#   ### Do we have to transform the explanatory variable/s into the log space?
#   if (length(LOGX) != ncol(df)-1) {
#     LOGX = rep(LOGX, times=ncol(df)-1)
#   }
#   if (length(LOGX_BASE) != ncol(df)-1) {
#     LOGX_BASE = rep(LOGX_BASE, times=ncol(df)-1)
#   }
#
#   ### Are the explanatory variable/s are strictly categorical?
#   if (length(XCATEGOR) != ncol(df)-1) {
#     XCATEGOR = rep(XCATEGOR, times=ncol(df)-1)
#   }
#
#   ### Do we have to regress the response variable against the explanatory variable/s?
#   if (length(REGRESSX) != ncol(df)-1) {
#     REGRESSX = rep(REGRESSX, times=ncol(df)-1)
#   }
#
#   ### define the layout of the plot space
#   MFROW = c(1)
#   for (i in 2:length(explanatory_var_names)){
#     if ( (length(explanatory_var_names) %% i)==0 ){
#       MFROW = c(MFROW, i)
#     }
#   }
#
#   ### iterate across explanatory variables defined by the formula
#   OUT = list()
#   par(mfrow=MFROW[c(ceiling(length(MFROW)/2), (floor(length(MFROW)/2)+1))])
#   for (i in 1:length(explanatory_var_names)){
#     # i = 4
#     print("======================================================")
#     print(paste0("Violin Plotting: ", explanatory_var_names[i]))
#     print("======================================================")
#     VIOPLOT_LETTERS2NUMS = plot_violin(dat=df,
#                                       response_variable_name=response_var_name,
#                                       explanatory_variable_name=explanatory_var_names[i],
#                                       title=TITLE[i],
#                                       xlab=XLAB[i],
#                                       ylab=YLAB,
#                                       COLOURS=VIOLIN_COLOURS,
#                                       BAR_COLOURS=ERROR_BAR_COLOURS,
#                                       XTICKS=XCATEGOR[i],
#                                       LOG=LOGX[i],
#                                       BASE=LOGX_BASE[i])
#     x_levels = VIOPLOT_LETTERS2NUMS$x_levels
#     x_numbers = VIOPLOT_LETTERS2NUMS$x_numbers
#     if (HSDX==TRUE){
#       print("======================================================")
#       print(paste0("HSD Grouping: ", explanatory_var_names[i]))
#       print("======================================================")
#       HSD_out = mean_comparison_HSD(formula=formula,
#                                     data=data,
#                                     explanatory_variable_name=explanatory_var_names[i],
#                                     alpha=ALPHA,
#                                     LOG=LOGX[i],
#                                     BASE=LOGX_BASE[i],
#                                     x_levels=x_levels,
#                                     x_numbers=x_numbers,
#                                     PLOT=TRUE)
#     } else {HSD_out = NULL}
#     if (REGRESSX==TRUE){
#       print("======================================================")
#       print(paste0("LiLnear Regressing: ", explanatory_var_names[i]))
#       print("======================================================")
#       REGRESS_out = plot_regression_line(dat=df,
#                                         response_variable_name=response_var_name,
#                                         explanatory_variable_name=explanatory_var_names[i],
#                                         LOG=LOGX[i],
#                                         BASE=LOGX_BASE[i],
#                                         x_levels=x_levels,
#                                         x_numbers=x_numbers,
#                                         PLOT=TRUE,
#                                         LINE_COL="gray")
#     } else {REGRESS_out = NULL}
#     OUT[[i]] = c(HSD_out=HSD_out, REGRESS_out=REGRESS_out)
#   }
#   return(OUT)
# }
#
# #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
# ### EXAMPLE INPUTS ###################################################################################################
# x1 = rep(rep(rep(letters[1:5], each=5), times=5), times=5)
# x2 = rep(rep(letters[6:10], each=5*5), times=5)
# x3 = rep(letters[11:15], each=5*5*5)
# y = rep(1:5, each=5*5*5) + rnorm(rep(1:5, each=5), length(x1)) ### x3 is the variable affecting y (see each=5*5*5)
# data = data.frame(x1, x2, x3, y)
# # formula = y ~ x1*x2*x3
# formula = y ~ x1 + x2 + x3 + (x2:x3)
# VIOLIN_COLOURS = RColorBrewer::brewer.pal(9, "GnBu")[c(2,3,4,5,6,7)]
# ERROR_BAR_COLOURS = c("#636363", "#1c9099", "#de2d26")
# TITLE = "" ### string or vector of strings
# XLAB = "" ### string or vector of strings
# YLAB = ""
# XCATEGOR = FALSE ### Is the explanatory variable strictly categorical? ### logical or vector of logicals
# LOGX=FALSE  ### logical or vector of logicals
# LOGX_BASE=10 ### numeric or vector of numerics
# HSDX=TRUE
# ALPHA=0.05
# REGRESSX=TRUE
# OUT = violin_plotter(formula=formula, data=data, TITLE=TITLE, XLAB=XLAB, YLAB=YLAB, VIOLIN_COLOURS=VIOLIN_COLOURS, ERROR_BAR_COLOURS=ERROR_BAR_COLOURS, XCATEGOR=XCATEGOR, LOGX=LOGX, LOGX_BASE=LOGX_BASE, HSDX=HSDX, ALPHA=ALPHA, REGRESSX=REGRESSX)
# #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
#
# ### test,csv
# data = read.csv("test.csv")
# str(data)
# formula=beauty ~ food*people
# data=data
# TITLE=""
# XLAB=""
# YLAB=""
# VIOLIN_COLOURS=c("#e0f3db", "#ccebc5", "#a8ddb5", "#7bccc4", "#4eb3d3", "#2b8cbe")
# ERROR_BAR_COLOURS=c("#636363", "#1c9099", "#de2d26")
# XCATEGOR=TRUE
# LOGX=FALSE
# LOGX_BASE=1
# HSDX=TRUE
# ALPHA=0.05
# REGRESSX=FALSE
# YD_OUT = violin_plotter(formula=formula, data=data)
