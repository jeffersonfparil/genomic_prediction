### PoolGPAS on the 60 SE Austalian Lolium populations

#############
### INPUT ###
JULIA_SCRIPT_HOME = ARGS[1]
filename_geno = ARGS[2]
filename_phen = ARGS[3]
MAF = parse(Float64, ARGS[4])
n_iterations = parse(Int64, ARGS[5])
filename_covariate = ARGS[6]
# ########################
# ### test:
# # # JULIA_SCRIPT_HOME = "/home/student.unimelb.edu.au/jparil/Documents/LOLSIM/Softwares/genomic_prediction/src/"
# # # filename_geno = "/home/student.unimelb.edu.au/jparil/LOLSIM/geno_ALLELEFREQ.csv"
# # # filename_phen = "/home/student.unimelb.edu.au/jparil/LOLSIM/pheno.csv"
# # # MAF = parse(Float64, "0.001")
# # JULIA_SCRIPT_HOME = "/mnt/Lolium/Softwares/genomic_prediction/src/"
# # filename_geno = "/mnt/GP_2019_60_SE_Australian_Lolium_populations/geno_ALLELEFREQ.csv"
# # filename_phen = "/mnt/GP_2019_60_SE_Australian_Lolium_populations/pheno.csv"
# # MAF = parse(Float64, "0.001")
# JULIA_SCRIPT_HOME = "/data/Lolium/Softwares/genomic_prediction/src/"
# filename_geno = "/data/Lolium/Quantitative_Genetics/GPWAS_60_SEAUS/geno.csv"
# MAF = parse(Float64, "0.01") ### 1/(2*42)
# n_iterations = parse(Int64, "10")
# # filename_phen = "/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/GP/pheno_CLETHODIM_RESISTANCE.csv"
# # filename_phen = "/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/GP/pheno_GLYPHOSATE_RESISTANCE.csv"
# # filename_phen = "/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/GP/pheno_SULFOMETURON_RESISTANCE.csv"
# filename_phen = "/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/GP/pheno_TERBUTHYLAZINE_RESISTANCE.csv"
# filename_covariate = "/data/Lolium/Quantitative_Genetics/GPWAS_60_SEAUS/PC1_Fst_cova.csv"
# ########################
#############

# ### PIN OLD GLM PACKAGE
# ]
# add GLM@1.3.3
# st
# pin GLM

######################
### LOAD LIBRARIES ###
using DelimitedFiles
using DataFrames
using CSV
using Statistics
using GLM
using GLMNet
using LinearAlgebra
using ProgressMeter
######################

###########################
### DEFINE SUB-FUNCTION ###
push!(LOAD_PATH, JULIA_SCRIPT_HOME)
using sync_parsing_module
using filter_sync_by_MAF_module
using LMM_module
using GP_module
### assess genomic prediction accuracy
function asses_model_accuracy_func(y_true, y_pred)
	if (var(y_pred) > 1.0e-10)
		# mean deviation of the predcited from the true phenotypic values
		MEAN_DEVIANCE = Statistics.mean(abs.(y_true .- y_pred)) #in percentage unit since the units we're using is in percentage eh!
		VAR_DEVIANCE = var(abs.(y_true .- y_pred)) #in percentage unit since the units we're using is in percentage eh!
		# Pearson' product moment correlation
		CORR = cor(y_true, y_pred)
		# modeling the true phenotypic values as a function of the predicted values
		data = DataFrames.DataFrame(y_true=convert(Array{Float64}, y_true), y_pred=convert(Array{Float64}, y_pred))
		# model = GLM.fit(GLM.LinearModel, GLM.@formula(y_true ~ 0 + y_pred), data) # fixed intercept at the origin because we assume a slope of one for perfect genomic prediction accuracy
		model = GLM.fit(GLM.LinearModel, GLM.@formula(y_true ~ y_pred), data)
		if length(coef(model)[:,1]) == 1
			local INTERCEPT = 0.0
		else
			local INTERCEPT = coef(model)[1,1]
		end
		SLOPE = coef(model)[end,1]
		R2 = var(GLM.predict(model)) / var(y_true) # not sure if this is correct
		RMSD = sqrt( (sum(y_pred .- y_true)^2)/length(y_true) ) #root mean suare deviation or RMSE (E for error)
	else
		MEAN_DEVIANCE = nothing
		VAR_DEVIANCE = nothing
		CORR = nothing
		INTERCEPT = nothing
		SLOPE = nothing
		R2 = nothing
		RMSD = nothing
	end
	# output: we want percent deviance to be zerol; correlation to be 1; intercept to be zero; and slope to be 1
	out = DataFrames.DataFrame(MEAN_DEVIANCE=MEAN_DEVIANCE, VAR_DEVIANCE=VAR_DEVIANCE, CORRELATION=CORR, INTERCEPT=INTERCEPT, SLOPE=SLOPE, R2=R2, RMSD=RMSD)
	return(out)
end
### genomic prediction models
function GWAlpha_GP(;X_no_int, y, MODEL="FIXED_RR", COVARIATE=nothing)
	n = size(X_no_int, 1)
	if COVARIATE == nothing
		INTERCEPT_AND_COVARIATE = ones(n)
	else
		INTERCEPT_AND_COVARIATE = hcat(ones(n), COVARIATE)
	end
	### concatenate the intercept or/and  covariates with the genotype datas
	X = hcat(INTERCEPT_AND_COVARIATE, X_no_int)
	#####################
	### MODEL FITTING ###
	#####################
	if MODEL == "FIXED_LS"
		#####################
		### Least Squares ###
		#####################
		b = X' * LMM_module.inverse(X * X')  * y
	elseif MODEL == "FIXED_RR"
		########################
		### Ridge regression ###
		########################
		ALPHA=0.00
		GLMNET_cv = try
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA)
			catch
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA, nfolds=(size(X, 1)-1))
			end
		idx_lambda = argmin(GLMNET_cv.meanloss)
		lambda = GLMNET_cv.lambda[idx_lambda]
		m = GLMNet.glmnet(X[:,2:end], y, standardize=false, lambda=[lambda], alpha=ALPHA)
		b = vcat(m.a0, m.betas[:,1])
	elseif MODEL == "FIXED_GLMNET"
		##############
		### GLMNET ###
		##############
		ALPHA=0.50
		GLMNET_cv = try
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA)
			catch
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA, nfolds=(size(X, 1)-1))
			end
		idx_lambda = argmin(GLMNET_cv.meanloss)
		lambda = GLMNET_cv.lambda[idx_lambda]
		m = GLMNet.glmnet(X[:,2:end], y, standardize=false, lambda=[lambda], alpha=ALPHA)
		b = vcat(m.a0, m.betas[:,1])
	elseif MODEL == "FIXED_LASSO"
		#############
		### LASSO ###
		#############
		ALPHA=1.00
		GLMNET_cv = try
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA)
			catch
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA, nfolds=(size(X, 1)-1))
			end
		idx_lambda = argmin(GLMNET_cv.meanloss)
		lambda = GLMNET_cv.lambda[idx_lambda]
		m = GLMNet.glmnet(X[:,2:end], y, standardize=false, lambda=[lambda], alpha=ALPHA)
		b = vcat(m.a0, m.betas[:,1])
	elseif MODEL == "MIXED_RR"
		INTCOVAR_EFF, EFF = try
				GP_module.GGMIX(X=X_no_int, y=y, Z=COVARIATE, alpha=0.0)
			catch
				(repeat([0.0], inner=(1+size(COVARIATE, 2))), repeat([0.0], inner=size(X_no_int, 2)))
			end
		b = vcat(INTCOVAR_EFF, EFF)
	elseif MODEL == "MIXED_GLMNET"
		INTCOVAR_EFF, EFF = try
				GP_module.GGMIX(X=X_no_int, y=y, Z=COVARIATE, alpha=0.5)
			catch
				(repeat([0.0], inner=(1+size(COVARIATE, 2))), repeat([0.0], inner=size(X_no_int, 2)))
			end
		b = vcat(INTCOVAR_EFF, EFF)
	elseif MODEL == "MIXED_LASSO"
		INTCOVAR_EFF, EFF = try
				GP_module.GGMIX(X=X_no_int, y=y, Z=COVARIATE, alpha=1.0)
			catch
				(repeat([0.0], inner=(1+size(COVARIATE, 2))), repeat([0.0], inner=size(X_no_int, 2)))
			end
		b = vcat(INTCOVAR_EFF, EFF)
	else
		println(string("AwWwwW! SowWwWyyyy! We have not implemented the model: ", MODEL, " yet."))
		println("¯\\_(๑❛ᴗ❛๑)_/¯ ʚ(´◡`)ɞ")
	end
	return(b)
end
###########################


##################
### LOAD FILES ###
# #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ### in bash: cut -d, -f1-3 geno.csv > col123.temp; cut -d, -f4- geno.csv > allele_freqs.temp
# ### in julia
# using DelimitedFiles
# using Statistics
# MAF = 0.001
# allele_freqs = mean(DelimitedFiles.readdlm("allele_freqs.temp", ','), dims=2)
# idx = (allele_freqs .>= MAF) .& (allele_freqs .< (1-MAF))
# DelimitedFiles.writedlm("LOCI_IDX_MAF_FILTERED.csv", idx, ',')
# exit()
# ### in julia again
# using DelimitedFiles
# idx = convert(Array{Bool}, DelimitedFiles.readdlm("LOCI_IDX_MAF_FILTERED.csv", ','))
# DelimitedFiles.writedlm("allele_freqs.csv", DelimitedFiles.readdlm("allele_freqs.temp", ',')[idx[:,1], :], ',')
# exit()
# ### in julia again
# using DelimitedFiles
# idx = convert(Array{Bool}, DelimitedFiles.readdlm("LOCI_IDX_MAF_FILTERED.csv", ','))
# DelimitedFiles.writedlm("col123.csv", DelimitedFiles.readdlm("col123.temp", ',')[idx[:,1], :], ',')
# exit()
# ### in bash
# paste -d, col123.csv allele_freqs.csv > geno_maf_filtered.csv
### in bash again to generate Fst matrix
# DIR=/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/
# cd $DIR
# touch pool_size.temp
# for i in $(seq 1 60)
# do
# 	echo "42" >> pool_size.temp
# done
# sed -i ':a;N;$!ba;s/\n/,/g' pool_size.temp
# time \
# julia /data/Lolium/Softwares/genomic_prediction/src/QUANTINEMO_04_Fst.jl \
# 	Lolium_2019_60pop.sync \
# 	1000 \
# 	$(cat pool_size.temp) \
# 	WeirCock
# rm pool_size.temp
# ### Fst PC
# # In R:
# fst = read.csv("/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/GP/Fst_covariate.csv", F)
# pc = prcomp(fst)
# pc1 = as.data.frame(pc$rotation[,1])
# # write.table(pc1, "/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/GP/PC1_Fst_cova.csv", row.names=FALSE, col.names=FALSE, sep=",")
# ### SVD
# # in julia
# using DelimitedFiles
# using LinearAlgebra
# filename_geno = "/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/GP/geno_ALLELEFREQ.csv"
# geno = DelimitedFiles.readdlm(filename_geno, ',')
# X = convert(Array{Float64}, copy(geno[idx, 4:end]))
# X_svd = LinearAlgebra.svd(X, full=false)
# PC = (X_svd.U * LinearAlgebra.Diagonal(X_svd.S))'
# DelimitedFiles.writedlm("PC1_X_cova.csv", PC[:,1], ',')
# DelimitedFiles.writedlm("PC1to5_X_cova.csv", PC[:,1:5], ',')
# DelimitedFiles.writedlm("PC1to10_X_cova.csv", PC[:,1:10], ',')
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
geno = DelimitedFiles.readdlm(filename_geno, ',')
phen = CSV.read(filename_phen, missingstring="NA")
Z = try
	DelimitedFiles.readdlm(filename_covariate, ',')
catch
	nothing
end
##################

######################
### MAF filetering ###
trait_vec = string.(names(phen))[2:end]
allele_freqs = mean(convert(Array{Float64}, copy(geno[:, 4:end])), dims=2)
idx = collect(1:size(geno, 1))[((allele_freqs .>= MAF) .& (allele_freqs .< (1-MAF)))[:,1]]
if sum(readdir() .== "LOCI_info.csv") == 0
	COL123 = DataFrames.DataFrame(CHROM=geno[idx,1], POS=geno[idx,2], ALLELE=geno[idx,3])
	CSV.write("LOCI_info.csv", COL123)
end
X = convert(Array{Float64}, copy(geno[idx, 4:end]))
geno = nothing
n, p = size(X)
######################

################################
### 05-FOLD CROSS-VALIDATION ###
n_folds = 5
# n_folds = minimum(convert(Array{Int}, floor.([sum(.!ismissing.(phen[:,i])) for i in 2:ncol(phen)] ./ 4)))

ITERATION_OUT = []
TRAIT_OUT = []
FOLD_OUT = []
TRAINING_SIZE_OUT = []
VALIDATION_SIZE_OUT = []
MODEL_OUT = []
N_PREDICTORS_OUT = []
MEAN_DEVIANCE_OUT = []
VAR_DEVIANCE_OUT = []
CORR_OUT = []
SLOPE_OUT = []
R2_OUT = []
RMSD_OUT = []
MODELS_LIST = ["FIXED_LS", "FIXED_RR", "FIXED_GLMNET", "FIXED_LASSO"]
# MODELS_LIST = ["FIXED_LASSO"]

pb = ProgressMeter.Progress(n_iterations * length(trait_vec) * n_folds * length(MODELS_LIST), 1, "Cross-validation in progress...", 50)
counter = [1]
for j in 1:n_iterations
	for k in 1:length(trait_vec)
		# k = 1
		trait = trait_vec[k]
		idx_nonmissing = collect(1:size(phen, 1))[.!isequal.(phen[:, k+1], missing)]
		sub_geno = X[:, idx_nonmissing]
		sub_phen = phen[idx_nonmissing, k+1]
		sub_cova = try
			Z[idx_nonmissing, :]
		catch
			nothing
		end
		pop_id_vec = rand(collect(1:length(sub_phen)), length(sub_phen))[1:(end - (length(sub_phen) % n_folds))] ### forcing the cross-validation matrix grouping rectangular
		n_sub = length(pop_id_vec)
		grouping_mat = reshape(pop_id_vec, n_folds, convert(Int, n_sub/n_folds))
		group_size = convert(Int, round(n_sub/n_folds))
		for l in 1:n_folds
			# l = 1
			idx_training = reshape(grouping_mat[(collect(1:n_folds) .!= l), :], group_size*(n_folds-1), 1)[:,1]
			idx_validation = grouping_mat[l, :]
			n_training = length(idx_training)
			n_validation = length(idx_validation)
			X_training = sub_geno[:, (idx_training)]'
			X_validation = sub_geno[:, (idx_validation)]'
			y_training = convert(Array{Float64}, sub_phen[idx_training])
			y_validation = convert(Array{Float64}, sub_phen[idx_validation])
			Z_training = try
				sub_cova[idx_training, :] ### NOTE: Z matrices are recomputed for the whole training and validation sets in teal-world applicatios since we're using the full relationships between training and cross-validation
			catch
				nothing
			end
			Z_validation = try
				sub_cova[idx_validation, :] ### NOTE: Z matrices are recomputed for the whole training and validation sets in teal-world applicatios since we're using the full relationships between training and cross-validation
			catch
				nothing
			end
			# TRAINING AND VALIDATION
			for model in MODELS_LIST
				ProgressMeter.update!(pb, counter[1])
				counter[1] = counter[1] + 1
				# model = "FIXED_RR"
				b = GWAlpha_GP(X_no_int=X_training, y=y_training, MODEL=model, COVARIATE=Z_training)
				io = open(string("CROSS_VALIDATION_5_FOLD_BETAS_", basename(filename_phen)), "a")
				DelimitedFiles.writedlm(io, b', ',')
				close(io)
				y_pred = try
					hcat(repeat([1.0], inner=n_validation), Z_validation, X_validation) * b
				catch
					hcat(repeat([1.0], inner=n_validation), X_validation) * b
				end
				ASSESSMENT = asses_model_accuracy_func(y_validation, y_pred)
				push!(ITERATION_OUT, j)
				push!(TRAIT_OUT, trait)
				push!(FOLD_OUT, l)
				push!(TRAINING_SIZE_OUT, n_training)
				push!(VALIDATION_SIZE_OUT, n_validation)
				push!(MODEL_OUT, model)
				push!(N_PREDICTORS_OUT, sum(abs.(b) .> 0.00)-1) #less intercept
				push!(MEAN_DEVIANCE_OUT, ASSESSMENT.MEAN_DEVIANCE[1])
				push!(VAR_DEVIANCE_OUT, ASSESSMENT.VAR_DEVIANCE[1])
				push!(CORR_OUT, ASSESSMENT.CORRELATION[1])
				push!(SLOPE_OUT, ASSESSMENT.SLOPE[1])
				push!(R2_OUT, ASSESSMENT.R2[1])
				push!(RMSD_OUT, ASSESSMENT.RMSD[1])
			end
		end
	end
end
# using JLD2
# JLD2.@load "CROSS_VALIDATION_5_FOLD_OUTPUT.jld2"
### convert nothing into missing
for i in [ITERATION_OUT, TRAIT_OUT, FOLD_OUT, TRAINING_SIZE_OUT, VALIDATION_SIZE_OUT, MODEL_OUT, N_PREDICTORS_OUT, MEAN_DEVIANCE_OUT, VAR_DEVIANCE_OUT, CORR_OUT, SLOPE_OUT, R2_OUT, RMSD_OUT]
 i[i .== nothing] .= missing
end
OUT = DataFrames.DataFrame(ITERATION=ITERATION_OUT, TRAIT=TRAIT_OUT, FOLD=FOLD_OUT, TRAINING_SIZE=TRAINING_SIZE_OUT, VALIDATION_SIZE=VALIDATION_SIZE_OUT,
							MODEL=MODEL_OUT, N_PREDICTORS=N_PREDICTORS_OUT, MEAN_DEVIANCE=MEAN_DEVIANCE_OUT, VAR_DEVIANCE=VAR_DEVIANCE_OUT,
							CORR=CORR_OUT, SLOPE=SLOPE_OUT, R2=R2_OUT, RMSD=RMSD_OUT)
CSV.write(string("CROSS_VALIDATION_5_FOLD_OUTPUT_", basename(filename_phen)), OUT)
################################

# ########################
# ### SAMPLE EXECUTION ###
# DIR=/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/GP
# cd ${DIR}
# cut -d, -f1 pheno.csv > col1.temp
# for i in 2 3 4 5
# do
# 	cut -d, -f${i} pheno.csv > col2.temp
# 	fname=$(head -n1 col2.temp | cut -d\" -f2)
# 	paste -d, col1.temp col2.temp > pheno_${fname}.csv
# 	echo $fname
# done
# rm *.temp
# ####################
# ### without covariate --> 100 iterations
# mkdir NO_COVARIATE
# cd NO_COVARIATE/
# time \
# parallel julia /data/Lolium/Softwares/genomic_prediction/misc/PoolGPAS_for_2019_60_SE_Australian_Lolium_populations.jl \
# /data/Lolium/Softwares/genomic_prediction/src/ \
# ${DIR}/geno_ALLELEFREQ.csv \
# {1} \
# 0.01 \
# 100 \
# not_an_existing_file.txt ::: $(ls ${DIR}/pheno_*.csv)
# cd ..
# ####################
# ### WITH PC1 of Fst as covariate --> 100 iterations
# mkdir PC1_FST_COVARIATE
# cd PC1_FST_COVARIATE/
# time \
# parallel julia /data/Lolium/Softwares/genomic_prediction/misc/PoolGPAS_for_2019_60_SE_Australian_Lolium_populations.jl \
# /data/Lolium/Softwares/genomic_prediction/src/ \
# ${DIR}/geno_ALLELEFREQ.csv \
# {1} \
# 0.01 \
# 100 \
# ${DIR}/PC1_Fst_cova.csv ::: $(ls ${DIR}/pheno_*.csv)
# cd ..
# ########################
