############################################################
###                                                      ###
### PARSING QuantiNemo2 SIMULATION PARAMETERS AND OUTPUT ###
###                                                      ###
############################################################

### NOTE: FOR MOST FUNCTIONS I HAVE HARD-CODED NUMBER OF ALLELES TO 5 --> NEED TO MAKE THIS MORE FLEXIBLE!!!!

################
#### inputs ####
################
#### dat_fname = quantinemo2 genotype FSTAT file outpu
#### ini_fname = quantinemo2 initiation file
#### genome_spec_fname = genome specificications: chromosome length in cM and bp
#### quanti_vs_ntrl = Did we simulate quantitative or just neutral loci? ("quanti" or "ntrl")
#### QTL_spec_fname = loci specifications: locus number, alleles, effect or value, mutation freq and initial freq
#################
#### outputs ####
#################
#### (1) Genotype matrix (nind x nloci*5) (NO HEADER)
#### (2) Loci information (nloci x 3) (HEADER: chromosome, position in cM, position in bp)
#### (3) Phenotype data (nind x 3) (HEADER: individual ID, untransformed phenotypic value, transformed phenotyp value to range from 0-1 based on possible extremes based on allele effects)
#### (4) Pooling percentiles (npools+1 x 1) (NO HEADER)
#### (5) Mean phenotypic values per pool (npools x 1) (NO HEADER)
#### (6) Allele frequency matrix (npools x nloci*5) (NO HEADER)
#### (7) QTL information (nQTL*5 x 4) (HEADER: chromosome,position in bp, allele, effect)
#### (8) Synchronized pileup file (nloci x npools+3) (NO HEADER)

### load libraries
using CSV
using DataFrames
using ProgressMeter
using DelimitedFiles
using Statistics
using StatsBase

### test
# cd("/data/Lolium/Quantitative_Genetics/genomic_prediction_simulations/QUANTINEMO2_SIM")
# outdir = readdir()[[match(Regex("lolium_"), x) != nothing for x in readdir()]][end]
# dat_fname = string(outdir, "/QUANTI_g140.dat")
# ini_fname = "lolium.ini"
# genome_spec_fname = "Lperenne_genome.spec"
# quanti_vs_ntrl = "quanti"
# QTL_spec_fname = "QTL.spec"
# QTL_phen_min_max_fname = "QTL_min_max_GEBV.spec"
# nAlleles = parse(Int64, split(open(readlines, dat_fname)[1], " ")[3])
dat_fname = ARGS[1]
ini_fname = ARGS[2]
genome_spec_fname = ARGS[3]
quanti_vs_ntrl = ARGS[4]
QTL_spec_fname = ARGS[5]
QTL_phen_min_max_fname = ARGS[6]
npools = parse(Int, ARGS[7])
nAlleles = parse(Int64, split(open(readlines, dat_fname)[1], " ")[3])

#################################################################################################################################
############################
### parse the FSTAT file ###
############################
println("Parsing the genotype data.")
function FUNC_PARSE_DAT(dat_fname)
    ### load the data in FSTAT format and extract the number of loci, number of alleles per locus and the number of individuals
    row1 = open(readlines, dat_fname)[1] # read the first row
    nloci = parse(Int64, split(row1, " ")[2])
    nalleles = parse(Int64, split(row1, " ")[3])
    nlines = countlines(dat_fname)
    header = ["IND"]
    append!(header, ["loc_"] .* string.(collect(1:nloci)))
    append!(header, ["DELETE_ME"]) #for the extra column weirdly included when reading with space as delimiter
    @time dat = CSV.read(dat_fname, delim=" ", header=header, datarow=nloci + 2)
    dat = dat[:,1:(nloci+1)] #remove the extra column
    nind = size(dat)[1]

    ### rename the individual column with consecutive numbers
    dat.IND = collect(1:nind)

    ### iterate across loci: convert the 2-digit numbers into strings, separate them and assign the corresponding SNP allele
    function num_2digit_2_bin_5allele(num_2digit)
        # #test
        # num_2digit = 23
        #initialize the ouput array of 5 elements corrensponding to the 5 possible SNP alleles: A,T,C,G and deletion
        out = [0, 0, 0, 0, 0]
        #convert the 2-digit number allele form into a string
        str_2digit = string(num_2digit)
        str_arr = split(str_2digit, "")
        for str in str_arr
            if str == "1"
                out = out .+ [1, 0, 0, 0, 0] #A
            elseif str == "2"
                out = out .+ [0, 1, 0, 0, 0] #T
            elseif str == "3"
                out = out .+ [0, 0, 1, 0, 0] #C
            elseif str == "4"
                out = out .+ [0, 0, 0, 1, 0] #G
            elseif str == "5"
                out = out .+ [0, 0, 0, 0, 1] #DEL
            end
        end
        return(out)
    end
    ###### execute the function iteratively
    GENO = Array{Int32}(undef, nind, nloci*5) #initialize the genotype matrix output
    progress_bar = ProgressMeter.Progress(nloci, dt=1, barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow) #progress bar
    for i in 1:nloci
        # i = 1
        geno = dat[:, i+1]
        for j in 1:length(geno) #iterate across individuals within the locus at hand
            # j = 1
            _start_ = (5*(i-1)) + 1
            _end_ = 5*i
            GENO[j, _start_:_end_] = num_2digit_2_bin_5allele(geno[j])
        end
        ProgressMeter.update!(progress_bar, i)
    end
    ### output the genotype matrix
    return(GENO)
end
### INPUT:
###### (1) FSTAT (*.dat) genotype data (filename)
### OUTPUT:
###### (1) Genotype matrix (nind x nloci*5) (NO HEADER)

### execute the fulnction and save the genotype matrix: nind x nloci*5
# dat_fname = "QUANTI_g140.dat"
GENO = FUNC_PARSE_DAT(dat_fname)
# @time DelimitedFiles.writedlm(string(split(dat_fname, ".dat")[1], "_GENO.csv"), GENO, ',') # delay writing out and merge with loci info first
#################################################################################################################################


#################################################################################################################################
###############################
### colect loci information ###
###############################
println("Collecting the loci information.")
function FUNC_PARSE_LOCI_SPEC(loci_info_fname, genome_chr_lengths_fname, loci_type)
    ### find the number of loci
    for i in 1:countlines(loci_info_fname)
        line = open(readlines, loci_info_fname)[i]
        if (match(Regex(string(loci_type, "_loci")), line) != nothing)
            global nloci = parse(Int64, split(line, " ")[end])
            break
        end
    end
    CHR = []
    POS = []
    counter = [0]
    ### find the lines specifying the loci positions in centiMorgan (cM)
    for i in 1:countlines(loci_info_fname)
        # println(i)
        # println(counter[1])
        # println(length(CHR))
        line = open(readlines, loci_info_fname)[i]
        if (match(Regex(string(loci_type, "_genome")), line) != nothing) #for the first chromosome
            num = split(split(split(line, "{")[end], "}")[end-1], " ")
            chr = parse(Int32, split(num[1], ":")[1])
            for j in 2:length(num)
                if num[j] != ""
                    push!(CHR, chr)
                    push!(POS, parse(Float64, num[j])) #for chromosomes without any loci captured
                end
                counter[1] = counter[1] + 1
            end
        elseif (counter[1] > 0) & (counter[1] < nloci) #for the  rest of the chromosomes
            num = split(split(split(line, "{")[end], "}")[1], " ")
            chr = parse(Int32, split(num[1], ":")[1])
            for j in 2:length(num)
                if num[j] != ""
                    push!(CHR, chr)
                    push!(POS, parse(Float64, num[j])) #for chromosomes without any loci captured
                end
                counter[1] = counter[1] + 1
            end
        else
            counter[1] = 0
        end
    end
    LOCI_SPEC = DataFrames.DataFrame(CHR=convert(Array{Int64}, CHR), r=convert(Array{Float64}, POS))
    ###  convert cM to bp
    genome_chr_lengths = CSV.read(genome_chr_lengths_fname, delim="\t")
    POS_bp = []
    for i in 1:nrow(LOCI_SPEC)
        chr = LOCI_SPEC.CHR[i]
        sub = genome_chr_lengths[genome_chr_lengths.Chrom .== chr, :]
        correction_factor = sub.Mbp*1e+6 / sub.cM
        # pos_bp = round.(LOCI_SPEC.r[i] * correction_factor)
        pos_bp = convert(Array{Int64}, round.(LOCI_SPEC.r[i] * correction_factor))
        append!(POS_bp, pos_bp)
    end
    LOCI_SPEC.POS = POS_bp
    return(LOCI_SPEC) #CHR: chromosome; r: recombination rate distance in centiMorgan (cM); POS: position in the chromosome in base pairs (bp)
end
### INPUTS:
###### (1) quantinemo2 simulation settings (*.ini) file (filename)
###### (2) chromosome lengths specifications (*.spec) for converting cM to bp (filename)
###### (3) loci type: quanti_genome or ntrl_genome (string)
### OUTPUT:
###### (1) Loci specifications dataframe: #CHR: chromosome; r: recombination rate distance in centiMorgan (cM); POS: position in the chromosome in base pairs (bp)
###### (WITH HEADER)

# excute and save the loci dataframe
# ini_fname = "../lolium.ini"
# genome_spec_fname = "../Lperenne_genome.spec"
# quanti_vs_ntrl = "quanti"
LOCI_SPEC = FUNC_PARSE_LOCI_SPEC(ini_fname, genome_spec_fname, quanti_vs_ntrl)
CSV.write(string(split(dat_fname, ".dat")[1], "_LOCI_SPEC.csv"), LOCI_SPEC, delim=',') # do not write out but merge with the genotype info
#################################################################################################################################


#################################################################################################################################
########################################################################
### merge loci info and genotype data for a consolidated GENO output ###
########################################################################
println("Merging the loci info and genotype data.")
CHROM = repeat(LOCI_SPEC.CHR, inner=nAlleles)
POS = repeat(LOCI_SPEC.POS, inner=nAlleles)
SNPs_alleles = ["A", "T", "C", "G", "DEL", "N"]
ALLELES = repeat(SNPs_alleles[1:nAlleles], outer=nrow(LOCI_SPEC))
GENO_PLUS_LOCI_SPEC = hcat(CHROM, POS, ALLELES, GENO')
@time DelimitedFiles.writedlm(string(split(dat_fname, ".dat")[1], "_GENO.csv"), GENO_PLUS_LOCI_SPEC, ',')
#################################################################################################################################


#################################################################################################################################
##############################
### collect phenotype data ###
##############################
println("Parsing the phenotype data.")
function FUNC_PARSE_PHENO(phe_fname, min_max_phe_fname)
    # phe_fname = "QUANTI_g140.phe"
    phe = CSV.read(phe_fname, header=["IND", "y_untr"], datarow=3, delim="\t")
    nind = nrow(phe)
    phe.IND = collect(1:nind)
    # min_max_phe_fname = "../QTL_min_max_GEBV.spec"
    min_max_phe = CSV.read(min_max_phe_fname, delim="\t")
    MIN_y = minimum([min_max_phe.MIN_GEBV[1], minimum(phe.y_untr)]) #extreme values based on both the predicted and observed phenotypes,
    MAX_y = maximum([min_max_phe.MAX_GEBV[1], maximum(phe.y_untr)]) #i.e. observed phenotypes may be lower than the expected minimum or higher than the expecter maximum
    y = ( phe.y_untr .- MIN_y ) ./ (MAX_y - MIN_y)
    phe.y = y
    return(phe)
end
### INPUTS:
###### (1) phenotype (*.phe) file (filename)allele_counter[1] > 0
###### (2) QTL specifications (*.spec) (filename)
### OUTPUT:
###### (1) phenotye data frame: IND (individual ID number); y_untr (untransformed phenotypic value); y (transformed phenotypic value ranging from 0 to 1 based on the minimum and maximum possible genotypic value based on the allele effects)
###### (WITH HEADER)

### execute and save
# QTL_phen_min_max_fname = "QTL_min_max_GEBV.spec"
phe_fname = string(split(dat_fname, ".dat")[1], ".phe")
PHENO = FUNC_PARSE_PHENO(phe_fname, QTL_phen_min_max_fname)
CSV.write(string(split(phe_fname, ".phe")[1], "_PHENO.csv"), PHENO, delim=',')
#################################################################################################################################

#################################################################################################################################
##########################################################################
### build the sync file and the calculate the mean phenotypes per pool ###
##########################################################################
println("Pooling.")
function FUNC_POOLING(GENO, PHENO, npools, pool_size)
    ### extract the dataset dimensionsallele_counter[1] > 0
    nind = size(GENO)[1]
    nloci = convert(Int64, size(GENO)[2] / 5)
    if (nind != nrow(PHENO))
        println("Oh no! The number of individuals do match between the genotype and phenotype data!")
        println("Exiting the function!")
        return(999)
    end
    ### set the number of pools to divide the simulated dataset and the pools sizes if you want it to be manually set
    # npools = 5
    # pool_size = convert(Int64, round(nind / npools)) #equally-sized pools
    # # pool_size = 100

    ### group the individuals into npools groups, each equally sized: pool_sizes
    PERCENTILES = append!([0.0], Statistics.quantile(PHENO.y, [1.0/npools] .* collect(1:npools)))
    PHENO_POOLS = Array{Float64}(undef, npools, 1)
    GENO_POOLS = Array{Float64}(undef, npools, nloci*5)
    for i in 1:npools
        test = (PHENO.y .>= PERCENTILES[i]) .& (PHENO.y .<= PERCENTILES[i+1])
        idx = StatsBase.sample(collect(1:nind)[test], pool_size, replace=true) #simulate random sampling during leaf sampling, DNA extraction, library preparation, and sequencing
        PHENO_POOLS[i] = Statistics.mean(PHENO.y[idx])
        GENO_POOLS[i,:] = Statistics.mean(GENO[idx,:], dims=1) ./ 2 #diploid that's why we need to divide by 2
    end

    ### simulate a synchronized pileup file (*.sync) using these pool datasets
    DEPTH = 100 #just a fixed arbitrary simulted sequencing or read depth
    chr = LOCI_SPEC.CHR
    pos = LOCI_SPEC.POS
    ref = []; alleles = ["A", "T", "C", "G", "DEL"]
    sync = Array{String}(undef, nloci, npools)
    pb = ProgressMeter.Progress(nloci, dt=1, barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow)
    for i in 1:nloci
        _start_ = (5*(i-1)) + 1
        _end_ = 5*i
        subdat = convert(Array{Int64}, round.( DEPTH .* GENO_POOLS[:, _start_:_end_]' )) #5 (A,T,C,G,DEL) x npools
        for j in 1:npools
            sync[i, j] = string(string(join(string.(subdat[1:end-1,j]), ":"), ":0"), ":", string.(subdat[end,j])) #insert N allele so that: A:T:C:G:N:DEL
        end
        # save the reference allele
        allele_sums = sum(subdat, dims=2)
        push!(ref, alleles[(maximum(allele_sums) .== allele_sums)[:]][end])
        ProgressMeter.update!(pb, i)
    end
    SYNC = hcat(chr, pos, ref, sync)
    return([PERCENTILES, PHENO_POOLS, GENO_POOLS, SYNC])
end
### INPUTS:
###### (1) Genotype matrix (nind x nloci*5) --> output of FUNC_PARSE_DAT()
###### (2) Phenotye data frame --> output of FUNC_PARSE_PHENO()
###### (3) number of pools
###### (4) size of each pool (equally-sized pools)
### OUTPUTS:
###### (1) phenotype percentiles per pool starting at 0.0 (npools+1 x 1) (NO HEADER)
###### (2) mean phenotype per pool (npools x 1) (NO HEADER)
###### (3) allele frequencies per pool matrix (npools x nloci*5) (NO HEADER)
###### (3) allele frequencies per pool in synchronized pileup format (nloci x npools+3) (*.sync) (NO HEADER)

### execute and save
nind = size(GENO)[1] #or nind = size(PHENO)[1]
pool_size = convert(Int, round(nind/npools))
PERCENTILES, PHENO_POOLS, GENO_POOLS, SYNC = FUNC_POOLING(GENO, PHENO, npools, pool_size)
DelimitedFiles.writedlm(string(split(dat_fname, ".dat")[1], "_POOLS_PERCENTILES.csv"), PERCENTILES, ',')
DelimitedFiles.writedlm(string(split(dat_fname, ".dat")[1], "_POOLS_PHENO.csv"), PHENO_POOLS, ',')
DelimitedFiles.writedlm(string(split(dat_fname, ".dat")[1], "_POOLS_GENO.csv"), GENO_POOLS, ',')
DelimitedFiles.writedlm(string(split(dat_fname, ".dat")[1], "_POOLS_GENO.sync"), SYNC, '\t')

### extra: build the GWAlpha.py phenotype input file:
Pheno_name = string("Pheno_name='pheno'", ";")
sig = string("sig=", sqrt(var(PHENO.y)), ";")
MIN = string("MIN=", minimum(PHENO.y), ";")
MAX = string("MAX=", maximum(PHENO.y), ";")
perc = string("perc=[", join(cumsum(repeat([1.0/npools], npools-1)), ","), "];")
q = string("q=[", join(PERCENTILES[2:end], ","), "];")
write_me_out = [Pheno_name, sig, MIN, MAX, perc, q]
DelimitedFiles.writedlm(string(split(dat_fname, ".dat")[1], "_POOLS_PHENO.py"), write_me_out, '\t')
#################################################################################################################################

#################################################################################################################################
###################################
### build QTL effects dataframe ###
###################################
println("Parsing the QTL data.")
function FUNC_PARSE_QTL_SPEC(QTL_spec_fname, nAlleles)
    # QTL_spec_fname = "../QTL.spec"
    # nAlleles = 5
    # load the loci effects specifications
    nlines = countlines(QTL_spec_fname)
        header = ["locus", "allele", "value", "mut_freq", "ini_freq"]
    qtl_spec_raw = CSV.read(QTL_spec_fname, delim='\t', header=header,datarow=10)
    # locate the QTL, i.e. the loci with !+ 0.0 mean allele values
    mean_per_loc = aggregate(qtl_spec_raw[:,[1,3]], :locus, mean)
    idx_qtl_loc = collect(1:nrow(qtl_spec_raw))[repeat(mean_per_loc.value_mean .!= 0.0, inner=nAlleles)]
    nQTL = convert(Int, length(idx_qtl_loc)/nAlleles)
    # extract the QTL specifications
    QTL_chr = LOCI_SPEC.CHR[convert.(Int64, ceil.(idx_qtl_loc/nAlleles))]
    QTL_pos = LOCI_SPEC.POS[convert.(Int64, ceil.(idx_qtl_loc/nAlleles))]
    QTL_all = repeat(["A", "T", "C", "G", "DEL"], outer=nQTL)
    QTL_eff = qtl_spec_raw.value[idx_qtl_loc]
    QTL_SPEC = DataFrames.DataFrame(chr=QTL_chr, pos=QTL_pos, allele=QTL_all, eff=QTL_eff)
    return(QTL_SPEC)
end
### INPUTS:
###### (1) Loci effects specifications i.e. quantitative loci additive genetic effects (or value)
###### (2) number of alleles per loci in the loci effects specifications file e.g. 5 for A, T, C, G, and DEL
### OUTPUT:
###### (1) QTL specifications: chr, pos, allele, and effects (nQTL*5 x 4) (WITH HEADER)

### execute and save
# QTL_spec_fname = "../QTL.spec"
# nAlleles = parse(Int64, split(open(readlines, dat_fname)[1], " ")[3])
QTL_SPEC = FUNC_PARSE_QTL_SPEC(QTL_spec_fname, nAlleles)
CSV.write(string(split(dat_fname, ".dat")[1], "_QTL_SPEC.csv"), QTL_SPEC, delim=',')
#################################################################################################################################

#################################################################################################################################
####################################################
### build a pileup file for the whole population ###
####################################################
println("Building a pileup file from the genotype data.")
# prepare loci info (first 4 columns: chr, pos, ref, and read_count)
nLoci = nrow(LOCI_SPEC)
col1_chr = LOCI_SPEC.CHR
col2_pos = LOCI_SPEC.POS
col3_ref = sample(["A", "T", "C", "G"], nLoci) #simulate random reference alleles: A,T,C & G

# prepare the reads info (columns 4, 5 and 6: reads, base_quality)
row1 = open(readlines, dat_fname)[1] # read the first row
header = ["IND"]
append!(header, ["loc_"] .* string.(collect(1:nloci)))
append!(header, ["DELETE_ME"]) #for the extra column weirdly included when reading with space as delimiter
dat = CSV.read(dat_fname, delim=" ", header=header, datarow=nloci + 2)[:,1:(nloci+1)] #remove the extra column
nInd = size(dat)[1]
col4_readcount = repeat([nInd*2], inner=nLoci) #simulate read count as just as the number of individuals *2 (perfect representation of each individuals haploid genome)

#############
# allele format conversion from number string to character string and vice versa
#############
function FUNC_allele_num_2_char_viceversa(num_or_char, num2char_char2num)
    if num2char_char2num == 0
    ### for character to number string conversion
        if num_or_char == "A"
            return("1")
        elseif num_or_char == "T"
            return("2")
        elseif num_or_char == "C"
            return("3")
        elseif num_or_char == "G"
            return("4")
        end
    elseif num2char_char2num == 1
    ### for number to UPPERCASE character string conversion
        if num_or_char == "1"
            return("A")
        elseif num_or_char == "2"
            return("T")
        elseif num_or_char == "3"
            return("C")
        elseif num_or_char == "4"
            return("G")
        end
    elseif num2char_char2num == 2
        ### for number to LOWERCASE character string conversion
            if num_or_char == "1"
                return("a")
            elseif num_or_char == "2"
                return("t")
            elseif num_or_char == "3"
                return("c")
            elseif num_or_char == "4"
                return("g")
            end
    end
end
# ### INPUTS:
# num_or_char = [A,T,C,G] or ["1","2","3","4"]
# num2char_char2num = [0 for char2num,
#                       1 num2char UPPERCASE,
#                       2 num2char LOWERCASE]

# convert fstat numeric format into pileup reads format
function FUNC_num_2digit_2_pileup_read(vec_num_2digit, ref, read_count)
    # #test
    # i = 1
    # vec_num_2digit = dat[:,i+1]
    # ref = FUNC_allele_num_2_char_viceversa(col3_ref[i], 0) #char to num string
    # read_count = col4_readcount[i]
    #initializ the ouput array of 5 elements corrensponding to the 5 possible SNP alleles: A,T,C,G and deletion
    out = repeat([".", ","], outer=convert(Int, read_count/2)) #alternating forward and reverse strands
    #convert the 2-digit number allele form into a string
    vec_str_2digit = string.(vec_num_2digit)
    mat_str_arr = split.(vec_str_2digit, "")
    # iterate across individuals
    for j in 1:convert(Int, read_count/2)
        # println(j)
        str_arr = mat_str_arr[j]
        idx_allele1 = (2*(j-1)) + 1
        idx_allele2 = 2*j
        # forward strand (first digit)
        if str_arr[1] != ref
            if str_arr[1] == "5" #if DEL
                out[idx_allele1] = "1" *  FUNC_allele_num_2_char_viceversa(ref, 1) #uppercase
            else
                out[idx_allele1] = FUNC_allele_num_2_char_viceversa(str_arr[1], 1) #uppercase
            end
        end
        # reverse strand (second digit)
        if str_arr[2] != ref
            if str_arr[2] == "5" #if DEL
                out[idx_allele2] = "1" *  FUNC_allele_num_2_char_viceversa(ref, 2) #lowercase
            else
                out[idx_allele2] = FUNC_allele_num_2_char_viceversa(str_arr[2], 2) #lowercase
            end
        end
    end
    return(join(out))
end
# ### INPUTS:
# vec_num_2digit = vector of the number string across individuals for the ith locus
# ref = reference allele in number string format for the ith locus
# read_count = read counts i.e. number of indiduals * 2 (diploid)
#############
#############
# convert fstat genotypes into pileup reads format
#############
col5_reads = Array{String}(undef, nLoci)
for i in 1:nLoci
    # println(i)
    vec_num_2digit = dat[:,i+1]
    ref = FUNC_allele_num_2_char_viceversa(col3_ref[i], 0) #char to num string
    read_count = col4_readcount[i]
    col5_reads[i] = FUNC_num_2digit_2_pileup_read(vec_num_2digit, ref, read_count)
end
#############
# simulate base quality column as constant PHRED=90
col6_quality = repeat([join(repeat(["c"], inner=nInd*2))], inner=nLoci)
# merge columns into the pileup output file and save as a tab-delimited pileup file
PILEUP = hcat(col1_chr, col2_pos, col3_ref, col4_readcount, col5_reads, col6_quality)
DelimitedFiles.writedlm(string(split(dat_fname, ".dat")[1], "_POPULATION.pileup"), PILEUP, '\t')
#################################################################################################################################
