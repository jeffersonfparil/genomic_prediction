GENOME = data.frame(Var1=as.character(read.table("GENOME_LOCI.info", header=FALSE)$V1))
SNP = as.data.frame(table(as.character(read.table("SYNC_LOCI.info", header=FALSE)$V1)))

DF = merge(GENOME, SNP, by="Var1", all=TRUE)
PERC_SCAFF_COVERED = sum(!is.na(DF$Freq))*100 / nrow(DF)

N_SCAFF = nrow(DF)
N_GROUPS = 8
GROUP_SIZE = round(N_SCAFF / N_GROUPS)
ADD_TO_LAST_GROUP = N_SCAFF - (GROUP_SIZE * N_GROUPS)
IDX_GROUPING = cumsum(rep(GROUP_SIZE, times=N_GROUPS))
IDX_GROUPING[N_GROUPS] = IDX_GROUPING[N_GROUPS] + ADD_TO_LAST_GROUP
IDX_GROUPING = c(0, IDX_GROUPING)

svg("SYNC_SNP_COVERAGE_Lolium2019_10X.svg", width=10, height=6)
par(mfrow=c(4, 2))
colors = RColorBrewer:::brewer.pal(N_GROUPS, "Set1")
for (i in 1:N_GROUPS){
  # i = 1
  start = IDX_GROUPING[i]+1
  end = IDX_GROUPING[i+1]
  df = DF[start:end, ]
  perc_scaff_covered = round(sum(!is.na(df$Freq))*100 / nrow(df), 2)
  barplot(df$Freq, xlab=paste0("Scaffold Group ", i), ylim=c(0, max(DF$Freq, na.rm=TRUE)), col=colors[i]) ### colors does not work for the number of levels I have
  legend("topleft", legend=paste0("Scaffolds Covered = ", perc_scaff_covered, "%"), bty="n")
}
dev.off()
