#!/usr/bin/env python

###########################################
### FIX 1001 GENOMES SNP FILE FOR JULIA ###
########################################### julia's HDF5 library does not cannot load the binary type SNP data

import sys
import h5py as h5
import numpy as np

HDF5_filename = sys.argv[1]

HDF5_file = h5.File(HDF5_filename,'r')
HDF5_file.keys()
acc = HDF5_file['accessions'][:]
pos = HDF5_file['positions'][:]
chrom = []
chrom_regions = HDF5_file['positions'].attrs['chr_regions']
for i in range(0, (len(chrom_regions))):
	print(i)
	rep = chrom_regions[i][1] - chrom_regions[i][0]
	chrom.extend([i+1]*rep)

X = HDF5_file['snps'][:]

HDF5_file.close()

#change types
acc = acc.astype('int32')
pos = pos.astype('int64')
chrom = np.asarray(chrom)
chrom = chrom.astype('int32')
X = X.astype('int8')

acc.shape
pos.shape
chrom.shape
X.shape

out = h5.File(".".join(HDF5_filename.split(".")[0:-1]) + "-FIXED_FOR_JULIA.hdf5", 'w')
out.create_dataset('accessions', data=acc)
out.create_dataset('positions', data=pos)
out.create_dataset('chromosomes', data=chrom)
out.create_dataset('snps', data=X)

out.close()
