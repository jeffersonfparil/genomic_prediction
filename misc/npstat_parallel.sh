#!/bin/bash
######################
### EXECUTE NPSTAT ###
######################
NPSTAT_DIR=${1}
nhaplo=${2}
mincov=${3}
maxcov=${4}
window=${5}
pileup=${6}
scaffo=${7}
${NPSTAT_DIR}/npstat  -n ${nhaplo} \
                      -l ${window} \
                      -mincov ${mincov} \
                      -maxcov ${maxcov} \
                      ${pileup}

tail -n+2 ${pileup}.stats > ${pileup}_NPSTAT.no_header ### remove header
for i in $(seq 1 $(cat ${pileup}_NPSTAT.no_header | wc -l)); do echo -e "$scaffo" >> ${pileup}_NPSTAT_COL1_POOL_ID.temp; done
paste ${pileup}_NPSTAT_COL1_POOL_ID.temp ${pileup}_NPSTAT.no_header > ${pileup}_NPSTAT.stats ### add chrom ID
rm ${pileup}.stats ${pileup}_NPSTAT.no_header ${pileup}_NPSTAT_COL1_POOL_ID.temp
