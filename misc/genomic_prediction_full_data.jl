JULIA_SCRIPT_HOME = ARGS[1]
filename_geno = ARGS[2]
filename_phen = ARGS[3]
MAF = parse(Float64, ARGS[4])
filename_covariate = ARGS[5]
MODELS=split(ARGS[6], ',')
# ### TEST
# JULIA_SCRIPT_HOME = "/data/Lolium/Softwares/genomic_prediction/src"
# filename_geno = "geno_ALLELEFREQ.csv"
# filename_phen = "pheno_CLETHODIM_RESISTANCE.csv"
# MAF = 0.001
# filename_covariate = "PC1to10_X_cova.csv"
# filename_covariate = "NULL"
# MODELS=split("FIXED_LS,FIXED_RR,FIXED_GLMNET,FIXED_LASSO,FIXED_ITERATIVE", ',')


using DelimitedFiles
using DataFrames
using CSV
using Statistics
using Distributions
using GLM
using GLMNet
using LinearAlgebra
using ProgressMeter
using Plots; Plots.pyplot()

push!(LOAD_PATH, JULIA_SCRIPT_HOME)
using sync_parsing_module
using filter_sync_by_MAF_module
using LMM_module
using GP_module

function GWAlpha_GP(;X_no_int, y, MODEL="FIXED_RR", COVARIATE=nothing)
	n = size(X_no_int, 1)
	if COVARIATE == nothing
		INTERCEPT_AND_COVARIATE = ones(n)
	else
		INTERCEPT_AND_COVARIATE = hcat(ones(n), COVARIATE)
	end
	### concatenate the intercept or/and  covariates with the genotype datas
	X = hcat(INTERCEPT_AND_COVARIATE, X_no_int)
	#####################
	### MODEL FITTING ###
	#####################
	if MODEL == "FIXED_LS"
		#####################
		### Least Squares ###
		#####################
		b = X' * LMM_module.inverse(X * X')  * y
	elseif MODEL == "FIXED_RR"
		########################
		### Ridge regression ###
		########################
		ALPHA=0.00
		GLMNET_cv = try
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA)
			catch
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA, nfolds=(size(X, 1)-1))
			end
		idx_lambda = argmin(GLMNET_cv.meanloss)
		lambda = GLMNET_cv.lambda[idx_lambda]
		m = GLMNet.glmnet(X[:,2:end], y, standardize=false, lambda=[lambda], alpha=ALPHA)
		b = vcat(m.a0, m.betas[:,1])
	elseif MODEL == "FIXED_GLMNET"
		##############
		### GLMNET ###
		##############
		ALPHA=0.50
		GLMNET_cv = try
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA)
			catch
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA, nfolds=(size(X, 1)-1))
			end
		idx_lambda = argmin(GLMNET_cv.meanloss)
		lambda = GLMNET_cv.lambda[idx_lambda]
		m = GLMNet.glmnet(X[:,2:end], y, standardize=false, lambda=[lambda], alpha=ALPHA)
		b = vcat(m.a0, m.betas[:,1])
	elseif MODEL == "FIXED_LASSO"
		#############
		### LASSO ###
		#############
		ALPHA=1.00
		GLMNET_cv = try
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA)
			catch
				GLMNet.glmnetcv(X[:,2:end], y, standardize=false, alpha=ALPHA, nfolds=(size(X, 1)-1))
			end
		idx_lambda = argmin(GLMNET_cv.meanloss)
		lambda = GLMNET_cv.lambda[idx_lambda]
		m = GLMNet.glmnet(X[:,2:end], y, standardize=false, lambda=[lambda], alpha=ALPHA)
		b = vcat(m.a0, m.betas[:,1])
	elseif MODEL == "MIXED_RR"
		INTCOVAR_EFF, EFF = try
				GP_module.GGMIX(X=X_no_int, y=y, Z=COVARIATE, alpha=0.0)
			catch
				(repeat([0.0], inner=(1+size(COVARIATE, 2))), repeat([0.0], inner=size(X_no_int, 2)))
			end
		b = vcat(INTCOVAR_EFF, EFF)
	elseif MODEL == "MIXED_GLMNET"
		INTCOVAR_EFF, EFF = try
				GP_module.GGMIX(X=X_no_int, y=y, Z=COVARIATE, alpha=0.5)
			catch
				(repeat([0.0], inner=(1+size(COVARIATE, 2))), repeat([0.0], inner=size(X_no_int, 2)))
			end
		b = vcat(INTCOVAR_EFF, EFF)
	elseif MODEL == "MIXED_LASSO"
		INTCOVAR_EFF, EFF = try
				GP_module.GGMIX(X=X_no_int, y=y, Z=COVARIATE, alpha=1.0)
			catch
				(repeat([0.0], inner=(1+size(COVARIATE, 2))), repeat([0.0], inner=size(X_no_int, 2)))
			end
		b = vcat(INTCOVAR_EFF, EFF)
	elseif MODEL == "FIXED_ITERATIVE"
		### test iterative OLS
	    b = [] ### actually LOD
	    pb = ProgressMeter.Progress(p, 1, "", 50)
	    for i in 1:p
	        # i = 1
	        x = hcat(ones(n), X[:, i])
	        A = pinv(x' * x)
	        b_hat = A * x' * y
	        y_hat = x * b_hat
			y_err = y - y_hat
			Vy_err = 1/n * (y_err' * y_err)
			b_ster = abs( sqrt(Vy_err * A)[end,end] ) #absolute value of the square root to prevent compplex numbers
			t_val = abs(b_hat[end]) / b_ster
			p_val = 2*(Distributions.ccdf(Distributions.TDist(n-1), t_val))
	        append!(b, -log(10, p_val))
	        ProgressMeter.update!(pb, i)
	    end

	else
		println(string("AwWwwW! SowWwWyyyy! We have not implemented the model: ", MODEL, " yet."))
		println("¯\\_(๑❛ᴗ❛๑)_/¯ ʚ(´◡`)ɞ")
	end
	return(b)
end

### EXECUTE
geno = DelimitedFiles.readdlm(filename_geno, ',')
phen = CSV.read(filename_phen, missingstring="NA")
idx_phen = .!(ismissing.(phen[:, 2]))
y = phen[idx_phen, 2]
Z = try
	DelimitedFiles.readdlm(filename_covariate, ',')
catch
	nothing
end
### MAF filetering
trait_vec = string.(names(phen))[2:end]
allele_freqs = mean(convert(Array{Float64}, copy(geno[:, 4:end])), dims=2)
idx = collect(1:size(geno, 1))[((allele_freqs .>= MAF) .& (allele_freqs .< (1-MAF)))[:,1]]
if sum(readdir() .== "LOCI_info.csv") == 0
	COL123 = DataFrames.DataFrame(CHROM=geno[idx,1], POS=geno[idx,2], ALLELE=geno[idx,3])
	CSV.write("LOCI_info.csv", COL123)
end
LOCI = copy(geno[idx, 1:3])
X = convert(Array{Float64}, copy(geno[idx, 4:end]))'[idx_phen, :]
# geno = nothing
n, p = size(X)

# MODELS = ["FIXED_LS", "FIXED_RR", "FIXED_GLMNET", "FIXED_LASSO", "MIXED_RR", "MIXED_GLMNET", "MIXED_LASSO", "FIXED_ITERATIVE"]
# MODELS = ["FIXED_LS", "FIXED_RR", "FIXED_GLMNET", "FIXED_LASSO", "FIXED_ITERATIVE"]
for model in MODELS
    # model = "FIXED_ITERATIVE"
    println(model)
    BETAS = GWAlpha_GP(X_no_int=X, y=y, MODEL=model, COVARIATE=Z)
	pval = []
	lod = []
	if model == "FIXED_ITERATIVE"
		try
			append!(lod, BETAS[(size(Z, 1)+1):end])
			append!(pval, 10 .^(-lod))
		catch
			append!(lod, BETAS)
			append!(pval, 10 .^(-lod))
		end

	else
	    b = BETAS[2:end]
	    b_mu = mean(b)
	    b_sd = std(b)
	    try
	    	append!(pval, ones(p+size(Z, 2)))
	    catch
	    	append!(pval, ones(p))
	    end
	    gaussD = Distributions.Normal(b_mu, b_sd)
	    idx_below_mu = b .< b_mu
	    idx_above_mu = b .>= b_mu
	    pval[idx_below_mu] .= Distributions.cdf.(gaussD, b[idx_below_mu])
	    pval[idx_above_mu] .= Distributions.ccdf.(gaussD, b[idx_above_mu])
	    append!(lod, -log.(10, pval))
	end
	# ### non-heuristic p-values
    # X_with_int = hcat(ones(n), X)
    # y_hat = X_with_int * BETAS
    # Ve = ((y .- y_hat)' * (y .- y_hat)) / (n-1)
    # A = X_with_int' * pinv(X_with_int * X_with_int')
    # B = pinv(convert(Matrix, X_with_int'))
    # Vb = []
    # pb = ProgressMeter.Progress(p, 1, "", 50)
    # for i in 1:(p+1)
    #     # i = 1
    #     append!(Vb, reshape(A[i, :], 1, n) * reshape(B[:, i], n, 1))
    #     ProgressMeter.update!(pb, i)
    # end
    # Vb = Vb .* Ve
    # tval = BETAS ./ sqrt.(abs.(Vb))
    # UnicodePlots.scatterplot(tval)
    # UnicodePlots.histogram(tval)
    # # tD = Distributions.TDist(p)
    # # pval = Distributions.ccdf.(tD, abs.(tval))
    # pval = Distributions.ccdf.(Distributions.Normal(mean(tval), std(tval)), abs.(tval))
    # lod = -log.(10, pval)
    # Plots.plot(lod, seriestype=:scatter)
    # ### test iterative OLS
    # lod = []
    # pb = ProgressMeter.Progress(p, 1, "", 50)
    # for i in 1:p
    #     # i = 1
    #     x = hcat(ones(n), X[:, i])
    #     A = pinv(x' * x)
    #     beta = A * x' * y
    #     y_hat = x * beta
    #     ve = ((y .- y_hat)' * (y .- y_hat)) / (n-1)
    #     vb = diag(A) .* ve
    #     tval = beta[end] / sqrt(vb[end])
    #     pval = Distributions.ccdf(Distributions.TDist(1), abs(tval))
    #     append!(lod, -log(10, pval))
    #     ProgressMeter.update!(pb, i)
    # end
    # Plots.plot(lod, seriestype=:scatter)
    Plots.plot(lod, seriestype=:scatter, size=(1000, 500))
    trait = split(split(basename(filename_phen), ".")[1], "_")[2]
    Plots.savefig(string("Manhattan_", trait, "_", model, ".png"))
	OUT_DF = DataFrames.DataFrame(SCAFF=LOCI[:,1], POS=LOCI[:,2], ALLELE=LOCI[:,3], PVAL=pval, LOD=lod)
	CSV.write(string("GWASOUT_", trait, "_", model, ".csv"), OUT_DF)
	#test bonferroni p-val adjustment
	using MultipleTesting
	pval_adj = MultipleTesting.adjust(convert(Array{Float64}, pval), MultipleTesting.Bonferroni()) ### or simple multiply these p-values by length(p-values) and truncate to maximum of 1.00
	pval_adj_test = pval .* length(pval); pval_adj_test[pval_adj_test .> 1.0] .= 1.0
	lod_adj = -log.(10, pval_adj)
	Plots.plot(lod_adj, seriestype=:scatter, size=(1000, 500))
end

# ### TEST and PLOTTING
# using UnicodePlots
# y_pred = hcat(ones(n), X) * LS_NONE
# cor(y, y_pred)
# MIN = 0.0
# MAX = maximum(vcat(y, y_pred))
# UnicodePlots.scatterplot(y, y_pred, xlim=(MIN, MAX), ylim=(MIN, MAX))
# UnicodePlots.histogram(b)
# UnicodePlots.scatterplot(lod)
# using Plots; Plots.pyplot()
# Plots.plot(lod, seriestype=:scatter)
