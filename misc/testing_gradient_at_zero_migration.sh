### TESTING THE RESISTANCE GRADIENT / INITIAL GENOTYPE INPUT FILE

DIR=/data/Lolium/Quantitative_Genetics/TEST_LANDSCAPING/LOLSIM_*/

### SETWD
cd $DIR
### MERGE PHENOTYPE DATA ACROSS GENERATIONS
echo -e "GEN\tPOP\tTRAIT_FGS\tTRAIT_BGS" >  PHENO_MERGED.txt
for phen in $(ls *.phe)
do
echo $phen
# phen=LOLIUM_10QTL_0.001mr_0.25fgs_-0.25bgs_g010.phe
geno=${phen%.phe*}.dat
### generation tim
TIME=$(echo ${phen%.phe*} | rev | cut -d"_" -f1 | rev)
### phenotype
tail -n+4 $phen | cut -f2- > phen.temp
### genotype labels
NLOCI=$(head -n1 $geno | cut -d" " -f2)
TAIL_HEADER_NLINES=$(echo "$NLOCI + 2" | bc)
cut -d" " -f1 ${phen%.phe*}.dat | tail -n+${TAIL_HEADER_NLINES} > pop_labels.temp
### merge generation time, pop_labels.temp, and phenotype data
rm ${TIME}.txt GEN_COL.temp
for j in $(seq 1 $(cat pop_labels.temp | wc -l)); do  echo "$TIME" >> GEN_COL.temp; done
sed -i s/g//g GEN_COL.temp
paste GEN_COL.temp pop_labels.temp phen.temp >> ${TIME}.txt
cat ${TIME}.txt >> PHENO_MERGED.txt
done

### in R:
# dat = read.table("PHENO_MERGED.txt", T)
# str(dat)
#
# AGG = aggregate(TRAIT_FGS ~ GEN + POP, data=dat, FUN=mean)
# boxplot(AGG$TRAIT_FGS ~ AGG$GEN)
#
# sub = subset(AGG, GEN==50)
# matrix(sub$TRAIT_FGS, ncol=sqrt(nrow(sub)), byrow=T)
