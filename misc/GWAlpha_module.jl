#######################################
###									###
### Julia implementation of GWAlpha ###
###									###
####################################### same module name and filename

module GWAlpha_module

#####################
###				  ###
### load packages ###
###				  ###
#####################
using DelimitedFiles
using Distributions
using Optim
using Plots; Plots.pyplot()
using ProgressMeter
using DataFrames
using CSV
using ColorBrewer

############################
###						 ###
### function definitions ###
###						 ###
############################
# neg_log_likelihood_pdfbeta(beta, data) = -sum(Distributions.logpdf.(Distributions.Beta(beta[1], beta[2]), data))
neg_log_likelihood_cdfbeta(beta, data_A, data_B) = -sum(
											 log.(10,
											 	 (Distributions.cdf.(Distributions.Beta(beta[1], beta[2]), data_A)) .-
											 	 (Distributions.cdf.(Distributions.Beta(beta[1], beta[2]), append!(zeros(1), data_A[1:(length(data_A)-1)])))
											     )
											 )     -sum(
											 log.(10,
											 	 (Distributions.cdf.(Distributions.Beta(beta[3], beta[4]), data_B)) .-
											 	 (Distributions.cdf.(Distributions.Beta(beta[3], beta[4]), append!(zeros(1), data_B[1:(length(data_B)-1)])))
											     )
											 )

#####################
###				  ###
### main function ###
###				  ###
##################### #inputs are the genotype sync filename and the phenotyp py filename
function GWAlpha(ARGS)
	### capture the input filename_sync
	if length(ARGS)!=3
		println("Please specify the sync allele counts file, the phenotype file and the minimum allele frequency threshold :-)")
		exit()
	end
	filename_sync = ARGS[1] #sync file of allele counts data per pool
	filename_phen = ARGS[2] #phenotype file with .py extension
	MAF = parse(Float64, ARGS[3])

	### test hardcoded input filenames:
	# cd("/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE_GENETICS-combine_SNP_and_transcript_data/SRC/genomic_prediction")
	# filename_sync = "res/GWAlpha_X.sync"; filename_phen = "res/GWAlpha_y.py"; MAF = 0.03
	# # filename_sync = "/volume1/SEQUENCES/DNA/20181106_Lolium_50_40pools_5popsX2/GWAS/UG.sync"; filename_phen = "/volume1/SEQUENCES/DNA/20181106_Lolium_50_40pools_5popsX2/GWAS/UG_pheno.py"; MAF = 0.03

	### load the sync and phenotype files
	sync = DelimitedFiles.readdlm(filename_sync, '\t')
	phen = DelimitedFiles.readdlm(filename_phen)

	### gather phenotype specifications
	NPOOLS = length(split(phen[5], ['=', ',', '[', ']', ';'])) - 3 #less the first leading and trailing elemenets
	if length(split(phen[1], ['=', '\"'])) < 3
		global NAME = split(phen[1], ['=', '\''])[3]
	else
		global NAME = split(phen[1], ['=', '\"'])[3]
	end
	SD = parse.(Float64, split(phen[2], ['=',';'])[2])
	MIN = parse.(Float64, split(phen[3], ['=',';'])[2])
	MAX = parse.(Float64, split(phen[4], ['=',';'])[2])
	PERC = parse.(Float64, split(phen[5], ['=', ',', '[', ']', ';'])[3:(NPOOLS+1)])
	QUAN = parse.(Float64, split(phen[6], ['=', ',', '[', ']', ';'])[3:(NPOOLS+1)])
	BINS = append!([x for x in PERC], 1) - append!(zeros(1), PERC)

	### gather genotype (allele frequency) specificications
	NSNP = size(sync)[1]
	n_pools_sync = size(sync)[2] - 3
	if NPOOLS != n_pools_sync
		println("The number of pools with phenotype data does not match the number of pools with allele frequency data!")
		println("Please check you input files :-)")
		println("Remove leading and intervening whitespaces in the phenotype file.")
		exit()
	else
		n_pools_sync = nothing #clear out contents of this redundun n_pools variable
	end

	### iterate across SNPs
	ALPHA_OUT = []
	ALLELE_FREQ = []
	ALLELE_ID = []
	LOCUS_ID = []
	LOCUS_W_EFF = [] # (1 for with effects, -999 for no effects of filtered out alleles) multiplier of ALPHA_OUT and LOD to exclude alleles with zero effects --> prevents the distortion of the distribution of alpha as affected by the zero effect alleles
	COUNTS = zeros(Int64, NPOOLS, 6) #nrow=n_pools and ncol=A,T,C,G,N,DEL
	progress_bar = ProgressMeter.Progress(NSNP, dt=1, desc="GWAlpha_ML Progress: ",  barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow) #progress bar
	for snp in 1:NSNP
		# println(snp)
		#parse allele counts from the sync file
		for i in 1:NPOOLS
			COUNTS[i,:] = [parse(Int64, x) for x in split.(sync[snp, 4:(NPOOLS+3)], [':'])[i]]
		end
		#convert to frequencies per pool
		FREQS = COUNTS ./ ( sum(COUNTS, dims=2) .+ 1e-10 ) #added 1e-10 to the denominator to avoid NAs in pools with no allele counts (zero depth; which should actually have been filtered out after mpileup using awk)
		allele_freqs = sum(FREQS .* BINS, dims=1)
		#iterate across alleles while filtering by MAF
		if (sum(COUNTS) != 0.0)
			if (minimum(allele_freqs[allele_freqs .!= 0.0]) >= MAF) & (maximum(allele_freqs) < (1.0 - MAF)) #locus filtering by mean MAF
				for allele in 1:6
					if (allele_freqs[allele] > 0.0) & (maximum(FREQS[:,allele]) < 0.999999)  #allele filtering remove alleles with no counts and that the number of pools with allele frequency close to one should not occur even once!
					# if (sum(FREQS[:,allele] .== 0.0) < NPOOLS) & (sum(FREQS[:,allele] .> 0.999999) < 1) #filter-out alleles with at least 1 pool fixed for that allele because it causes a failure in the optimization
						freqA = FREQS[:, allele]
						pA = sum(freqA .* BINS)
						pB = 1 - pA
						BINA = (freqA .* BINS) ./ pA
						BINB = ( (1 .- freqA) .* BINS ) ./ (1-pA)
						percA = cumsum(BINA)
						percB = cumsum(BINB)

						### optimize (minimize) -log-likelihood of these major allele frequencies modelled as a beta distribution
						# using Nelder-Mead optimization or Box minimisation (try-catch if one or the other fails with preference to Nelder-Mead)
						lower_limits = [1e-20, 1e-20, 1e-20, 1e-20]
						upper_limits = [1.0, 1.0, 1.0, 1.0]
						initial_values = [0.1, 0.1, 0.1, 0.1]
						BETA = try
							Optim.optimize(beta->neg_log_likelihood_cdfbeta(beta, percA, percB), initial_values, NelderMead())
						catch
							try
								Optim.optimize(beta->neg_log_likelihood_cdfbeta(beta, percA, percB), lower_limits, upper_limits, initial_values)
							catch ### lower limits of 1e-20 to 1e-6 causes beta dist parameter values to shrink to zero somehow - so we'r'e setting lower limits to 1e-5 instead
								lower_limits = [1e-5, 1e-5, 1e-5, 1e-5]
								Optim.optimize(beta->neg_log_likelihood_cdfbeta(beta, percA, percB), lower_limits, upper_limits, initial_values)
							end
						end
						MU_A = MIN + ((MAX-MIN)*BETA.minimizer[1]/(BETA.minimizer[1]+BETA.minimizer[2]))
						MU_B = MIN + ((MAX-MIN)*BETA.minimizer[3]/(BETA.minimizer[3]+BETA.minimizer[4]))

						### compute alpha
						W_PENAL = 2*sqrt(pA*pB)
						ALPHA = W_PENAL*(MU_A - MU_B) / SD
						append!(ALPHA_OUT, ALPHA)
						append!(ALLELE_ID, allele)
						append!(LOCUS_ID, snp)
						append!(LOCUS_W_EFF, 1)
						append!(ALLELE_FREQ, pA)
					end
				end
			end
		end
		# println("$snp")
		ProgressMeter.update!(progress_bar, snp)
	end

	### calculate p-values by assuming alpha's  are from a normal distribution
	alpha_mean = mean(ALPHA_OUT)
	alpha_sd = std(ALPHA_OUT)
	# define p-val conditional function
	function pval_N(x, mu, sd)
		if x < mu
			2*(Distributions.cdf(Distributions.Normal(mu, sd), x))
		else
			2*(1 - Distributions.cdf(Distributions.Normal(mu, sd), x))
		end
	end
	P_VALUES = [pval_N(x, alpha_mean, alpha_sd) for x in ALPHA_OUT]
	LOD = -Distributions.log.(10, P_VALUES)

##########################################################
########################################################## 2019-01-09
########################################################## NEED TO STREAMLINE OUTPUTTING AND
########################################################## LOOK FOR BUGS THAT MAY BE MASKING
########################################################## SIGNIFICANT LOCI!!!!
##########################################################


	### output
	filename = split(filename_phen, ['/', '.'])
		###### output:::csv file
	for i in 1:length(ALLELE_ID) #convert int allele ID into corresponting A, T, C, G, N, DEL
		if ALLELE_ID[i] == 1; ALLELE_ID[i] = "A"
		elseif ALLELE_ID[i] == 2; ALLELE_ID[i] = "T"
		elseif ALLELE_ID[i] == 3; ALLELE_ID[i] = "C"
		elseif ALLELE_ID[i] == 4; ALLELE_ID[i] = "G"
		elseif ALLELE_ID[i] == 5; ALLELE_ID[i] = "N"
		elseif ALLELE_ID[i] == 6; ALLELE_ID[i] = "DEL"
		end
	end
	OUT = DataFrames.DataFrame(CHROM=sync[LOCUS_ID,1], POS=sync[LOCUS_ID,2], ALLELE=ALLELE_ID, FREQ=ALLELE_FREQ, ALPHA=ALPHA_OUT, PVALUES=P_VALUES, LOD=LOD)
	alphas_fname = string(filename[length(filename)-1], "-GWAlpha_ML_Alphas.csv")
	CSV.write(alphas_fname, OUT)
		## manhattan plot
	LOD_plot = Plots.plot([1, length(LOD)], [0, maximum(LOD)],
		seriestype=:scatter,
		marker=(:circle, 5, 0.5, :White, Plots.stroke(0, :white)),
		xlabel="SNP ID",
		ylabel="LOD",
		legend=false,
		size=(1200, 400));
	contigs = unique(OUT.CHROM)
	nColours = length(contigs)
	colours = repeat(ColorBrewer.palette("Pastel1", 9 > nColours ? nColours : 9), outer=convert(Int, ceil(nColours/9)))
	colours_lab = repeat(ColorBrewer.palette("Set1", 9 > nColours ? nColours : 9), outer=convert(Int, ceil(nColours/9)))
	i_loc = [0, 0] # counter and locus position
	for contig in contigs
		subset_LOD = OUT[OUT.CHROM .== contig, :LOD]
		x0 = (i_loc[2]+1)
		x1 = (i_loc[2]+length(subset_LOD))
		Plots.plot!(LOD_plot, x0:x1, subset_LOD,
					seriestype=:scatter,
					marker=(:circle, 5, 0.5, colours[i_loc[1]+1], Plots.stroke(0, :white)),
					xlabel="SNP ID",
					ylabel="LOD",
					legend=false,
					size=(1700, 500));
		Plots.annotate!([(x0+((x1-x0)/2), 0, text(contig, 10, colours_lab[i_loc[1]+1], :center))]);
		i_loc[1] = i_loc[1]+1
		i_loc[2] = i_loc[2]+length(subset_LOD)
	end
	bonferroni_threshold = -log10(0.05 / length(LOD))
	Plots.plot!(LOD_plot, [1,length(LOD)], [bonferroni_threshold, bonferroni_threshold], line=(:line, :dash, 0.5, 2, :red), label="Bonferroni threshold");
	plot_fname = string(filename[length(filename)-1], "-GWAlpha_ML_Manhattan.png")
	Plots.savefig(plot_fname)


	println("Everything went well. Please check the output files:")
	println(alphas_fname)
	println(plot_fname)
	println("===============================================================")
 	return 0
end

end #end of GWAlpha module

### Sample usage:
# include("src/GWAlpha_module.jl")
# GWAlpha_module.GWAlpha( ["res/GWAlpha_X.sync", "res/GWAlpha_y.py", "0.03"] )

### This module can easily be converted into a self-standing script by
### removing the module header and end at the bottom here followed by
### adding the julia shebang on the first line and
### add GWAlpha(ARGS) at the end to execute the function
# ### compilation
# using PackageCompiler
# using ArgParse #precompile ArgParse for compiling with PackageCompiler
# PackageCompiler.build_executable("GWAlpha.jl", builddir="bin/GWAlpha")
