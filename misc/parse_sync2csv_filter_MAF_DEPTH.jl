### FILTER BY NUMBER OF READS PER LOCUS PER POPULATION AND
### CONVERT SYNC INTO ALLELEFREQ
using DelimitedFiles
using Statistics
using ProgressMeter
using DataFrames
using CSV

### FILTERING FUNCTION
function filter_sync_by_MAF_and_DEPTH(;filename_sync::String, MAF::Float64, DEPTH::Int64)
	### load the sync and phenotype files
	sync = DelimitedFiles.readdlm(filename_sync, '\t')

	### gather genotype (allele frequency) specificications
	NSNP = size(sync)[1]
	NPOOLS = size(sync)[2] - 3

	### iterate across SNPs
	OUT = repeat([false], inner=(NSNP*6))
	COUNTS = zeros(Int64, NPOOLS, 6) #nrow=n_pools and ncol=A,T,C,G,N,DEL
	progress_bar = ProgressMeter.Progress(NSNP, dt=1, desc="Filter sync by MAF Progress: ",  barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow) #progress bar
	for snp in 1:NSNP
        # snp = 1
		# println(snp)
		#parse allele counts from the sync file
		for i in 1:NPOOLS
			COUNTS[i,:] = [parse(Int64, x) for x in split.(sync[snp, 4:(NPOOLS+3)], [':'])[i]]
		end
        # test if all pools have the desired depth or higher
        if sum(sum(COUNTS, dims=2) .>= DEPTH) == NPOOLS
    		#convert to frequencies per pool
    		FREQS = COUNTS ./ ( sum(COUNTS, dims=2) .+ 1e-10 ) #added 1e-10 to the denominator to avoid NAs in pools with no allele counts (zero depth; which should actually have been filtered out after mpileup using awk)
    		allele_freqs = mean(FREQS, dims=1)
    		#iterate across alleles while filtering by MAF
    		if length(allele_freqs[allele_freqs .!= 0.0]) != 0
    			if (minimum(allele_freqs[allele_freqs .!= 0.0]) >= MAF) & (maximum(allele_freqs) < (1.0 - MAF)) #locus filtering by mean MAF
    				for allele in 1:6
    					if (allele_freqs[allele] > 0.0) & (maximum(FREQS[:,allele]) < 0.999999)  #allele filtering remove alleles with no counts and that the number of pools with allele frequency close to one should not occur even once!
    						OUT[((snp-1)*6) + allele] = true
    					end
    				end
    			end
    		end
    	end
        # println("$snp")
        ProgressMeter.update!(progress_bar, snp)
	end
	return(OUT)
end

### CONVERT SYNC TO ALLELEFREQ CSV
function sync_parse(;filename_sync::String)
	### test hardcoded input filenames:
	# cd("/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE_GENETICS-combine_SNP_and_transcript_data/SRC/genomic_prediction")
	# filename_sync = "GWAlpha_X.sync"; MAF=0.002
	# filename_sync = "IF.sync"; MAF=1/500.00

	### load the sync file
	sync = DelimitedFiles.readdlm(filename_sync, '\t')

	### gather genotype (allele frequency) specificications
	NSNP = size(sync)[1]
	NPOOLS = size(sync)[2] - 3

	### iterate across SNPs
	COUNTS = zeros(Int64, NPOOLS, 6) #nrow=n_pools and ncol=A,T,C,G,N,DEL
	# global OUT = DataFrames.DataFrame()
	progress_bar = ProgressMeter.Progress(NSNP, dt=1, desc="Progress: ",  barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow) #progress bar
	for snp in 1:NSNP
		# snp = 65
		# println(snp)
		#parse allele counts from the sync file
		for i in 1:NPOOLS
			COUNTS[i,:] = [parse(Int64, x) for x in split.(sync[snp, 4:(NPOOLS+3)], [':'])[i]]
		end
		#convert to frequencies per pool (FREQS: npools x (nalleles=6))
		FREQS = COUNTS ./ ( sum(COUNTS, dims=2) .+ 1e-10 ) #added 1e-10 to the denominator to avoid NAs in pools with no allele counts (zero depth; which should actually have been filtered out after mpileup using awk)
		# FREQS = COUNTS ./ sum(COUNTS, dims=2)
		allele_freq_across_pools = sum(FREQS, dims=1) ./ NPOOLS
		# #filter by MAF
		# non_zero_index = convert(Array{Bool}, allele_freq_across_pools .!= 0.0)
		# allele_freq_across_pools_non_zero = allele_freq_across_pools[non_zero_index]
		# if (isempty(allele_freq_across_pools_non_zero) == false) #for locui without any allele counts due to popoolations' filtering by read quality?! not sure but they exist!
		# 	if (minimum(allele_freq_across_pools_non_zero) > MAF) & (maximum(allele_freq_across_pools_non_zero) < (1.0 - MAF)) # filter by MAF and with allele freq ~1.0 that slipped
				# non_zero_nonref_index = convert(Array{Bool}, allele_freq_across_pools_non_zero .!= maximum(allele_freq_across_pools_non_zero))
				# OUT_FREQS = FREQS[:, non_zero_index[:]][:, non_zero_nonref_index[:]]' #transpose so we can append each locus (can have multiple alleles per locus) to the next line
				# OUT_FREQS = FREQS[:, non_zero_index[:]]' #transpose so we can append each locus including the reference allele!!! (can have multiple alleles per locus) to the next line
				OUT_FREQS = FREQS'
				# idx_remove_major_allele = allele_freq_across_pools_non_zero .!= maximum(allele_freq_across_pools_non_zero) #remove major allele to allocate 1 degree of freedom to the mean thereby reducing the dimesionality of the predictions substantially
				# OUT_FREQS = OUT_FREQS[idx_remove_major_allele, :]  #remove major allele to allocate 1 degree of freedom to the mean thereby reducing the dimesionality of the predictions substantially
				OUT_CHR = sync[snp, 1]
				OUT_POS = sync[snp, 2]
				# OUT_ALLELE = ["A", "T", "C", "G", "N", "DEL"][non_zero_index[:]]
				OUT_ALLELE = ["A", "T", "C", "G", "N", "DEL"]
				# OUT_ALLELE = ["A", "T", "C", "G", "N", "DEL"][non_zero_index[:]][idx_remove_major_allele]  #remove major allele to allocate 1 degree of freedom to the mean thereby reducing the dimesionality of the predictions substantially
				# OUT_REFALLELE = ["A", "T", "C", "G", "N", "DEL"][convert(Array{Bool}, allele_freq_across_pools .== maximum(allele_freq_across_pools))[:]]
				for a in 1:size(OUT_FREQS)[1] #iterate across alleles
					# global OUT = DataFrames.DataFrame(CHROM=OUT_CHR, POS=OUT_POS, REF=OUT_REFALLELE, ALLELE=OUT_ALLELE[a])
					global OUT = DataFrames.DataFrame(CHROM=OUT_CHR, POS=OUT_POS, ALLELE=OUT_ALLELE[a])
					# global OUT = DataFrames.DataFrame(CHROM=OUT_CHR, POS=OUT_POS, REF=OUT_REFALLELE, ALLELE=OUT_ALLELE[a], POOL1=OUT_FREQS[a,1])
					# insert!(OUT, OUT_CHR, :CHROM)
					# insert!(OUT, OUT_POS, :POS)
					# insert!(OUT, OUT_REFALLELE, :REF)
					# insert!(OUT, OUT_ALLELE[a], :ALLELE)
					# for p in 2:NPOOLS #add allele frequency per pool iteratively into the dataframe
					for p in 1:NPOOLS #add allele frequency per pool iteratively into the dataframe
						insert!(OUT, size(OUT)[2]+1, OUT_FREQS[a,p], Symbol("POOL", p))
					end
					CSV.write(string(join(split(filename_sync, ".")[1:(end-1)], '.'), "_ALLELEFREQ.csv"), OUT, append=true)
				end
		# 	end
		# end
		ProgressMeter.update!(progress_bar, snp)
	end
	println("===============================================================")
 	return 0
end

### MAIN FUNCTION
fname = ARGS[1]
maf = parse(Float64, ARGS[2])
depth = parse(Int64, ARGS[3])
# fname = "/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/Lolium_2019_60pop.sync"
# #fname = "/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/Lolium_2019_60pop_0.01MAF_filtered.sync"
# maf = 1/(2*42)
# depth = 10
@time LOCI = filter_sync_by_MAF_and_DEPTH(filename_sync=fname, MAF=maf, DEPTH=depth)
n_loci = convert(Int64, length(LOCI)/6)
LOCI_MAT = reshape(LOCI, 6, n_loci)'
IDX = collect(1:n_loci)[(mean(LOCI_MAT, dims=2))[:,1] .!= 0.0]
sync_in = DelimitedFiles.readdlm(fname, '\t')
sync_out = sync_in[IDX[:,1], :]
fname_out = string(split(basename(fname), ".")[1], "_", round(maf, digits=2), "MAF_filtered.sync")
try
    DelimitedFiles.writedlm(string(dirname(fname), "/", fname_out), sync_out, '\t')
catch
    DelimitedFiles.writedlm(fname_out, sync_out, '\t')
end
sync_parse(filename_sync=fname_out)
