########################
###					 ###
### Ridge Regression ###
###					 ###
######################## same module name and filename

module RR_module

#####################
###				  ###
### load packages ###
###				  ###
#####################
using MultivariateStats
using LinearAlgebra
using Distributions
using Optim
using ProgressMeter

############################
###						 ###
### function definitions ###
###						 ###
############################

### Tikhonov regularisation or linear reularisation using the L2-norm (squared Eucledan/distance norm) or ridge-regression
function RR_BLUE_func(y, X, lambda)
	n = size(X)[1]
	l = size(X)[2]
	if n < l
		b = X' * inv((X * X') + (LinearAlgebra.I * lambda)) * y
	else
		b = inv((X' * X) + (LinearAlgebra.I * lambda)) * (X' * y)
	end
	return(b)
end

### Lambda optimisation function (minimise)
function lambda_optim_func(lambda, y, X)
	b = RR_BLUE_func(y, X, lambda[1])
	dy = sum((y - (X * b)).^2)/length(y)
	return(dy)
end

#####################
###				  ###
### main function ###
###				  ###
#####################

function RR(X_raw, y, MAF; COVARIATE=nothing)
	# # ### test
	# X_raw = X_train
	# y = y_train
	# MAF = 0.01
	# COVARIATE=PC_train
	# ###########
	n = size(X_raw)[1]
	l = size(X_raw)[2]
	### filter loci by MAF
	LOC = collect(1:l)[ ((mean(X_raw, dims=1) ./ 2) .> MAF)[:] .& ((mean(X_raw, dims=1) ./ 2) .< (1.0-MAF))[:] ]
	X_filtered = X_raw[:, LOC]
	# ### add an intercept
	# X = hcat(ones(n), X_filtered)
	# ### MISC:
	# 	# # function RR_cost_function(beta, lambda, X, y)
	# 	# # 	RR_cost = sum(y - (X * beta)) + (lambda * (beta' * beta))
	# 	# # 	return(RR_cost)
	# 	# # end
	# 	# # beta = Optim.optimize(beta->RR_cost_function(beta, 0.5, X, y), zeros(l), Optim.NelderMead())
	# 	# beta = MultivariateStats.ridge(X[:,1:30000], y, 0.5)
	### build the intercept and covariate matrix to be concatenated (hcat) to the allele count vector
	if COVARIATE == nothing
		INTERCEPT_AND_COVARIATE = ones(n)
	else
		INTERCEPT_AND_COVARIATE = hcat(ones(n), COVARIATE)
	end
	### concatenate the intercept or/and  covariates with the genotype datas
	X = hcat(INTERCEPT_AND_COVARIATE, X_filtered)
	### Optimse the lambda for the L2 regularisation ###
	lower_limit=[-1.0e-10]; upper_limit=[1.0e10]
	lambda = Optim.optimize(par->lambda_optim_func(par, y, X), lower_limit, upper_limit, [1.0])
	### Ridge regression ###
	beta = RR_BLUE_func(y, X, lambda.minimizer[1])
	# INTERCEPT = beta[1]
	# EFF = beta[2:end]
	INTCOVAR_EFF = beta[1:size(INTERCEPT_AND_COVARIATE, 2)]
	EFF = beta[(size(INTERCEPT_AND_COVARIATE, 2)+1):end]
	PVAL = ones(length(LOC))
		idx_less_than_mean = EFF .< mean(EFF)
		idx_more_than_mean = EFF .> mean(EFF)
		PVAL[idx_less_than_mean] = Distributions.cdf.(Distributions.Normal(mean(EFF), std(EFF)), EFF[idx_less_than_mean])
		PVAL[idx_more_than_mean] = 1 .- Distributions.cdf.(Distributions.Normal(mean(EFF), std(EFF)), EFF[idx_more_than_mean])
	LOD = -log10.(PVAL .+ 1.0e-20) ### avoiding infinities
	return(LOC, INTCOVAR_EFF, EFF, PVAL, LOD)
end

end #end of RR module
