#############################################
###										  ###
### Multivariate Least Squares Esimtation ###
###			via QR Decomposition		  ###
###										  ###
############################################# same module name and filename

module LS_module

#####################
###				  ###
### load packages ###
###				  ###
#####################
using LinearAlgebra
using Distributions

#####################
###				  ###
### main function ###
###				  ###
#####################

function LS(X_raw, y, MAF; COVARIATE=nothing)
	# ### test
	# using JLD2
	# JLD2.@load("ZZZ_TEST_ACROSS_IND_DATA.jld2")
	# ###########
	n = size(X_raw)[1]
	l = size(X_raw)[2]
	### filter loci by MAF
	LOC = collect(1:l)[ ((mean(X_raw, dims=1) ./ 2) .> MAF)[:] .& ((mean(X_raw, dims=1) ./ 2) .< (1.0-MAF))[:] ]
	X_filtered = X_raw[:, LOC]
	### build the intercept and covariate matrix to be concatenated (hcat) to the allele count vector
	if COVARIATE == nothing
		INTERCEPT_AND_COVARIATE = ones(n)
	else
		INTERCEPT_AND_COVARIATE = hcat(ones(n), COVARIATE)
	end
	### concatenate the intercept or/and  covariates with the genotype datas
	X = hcat(INTERCEPT_AND_COVARIATE, X_filtered)
	### estimate the betas
	beta = X \ y
	INTCOVAR_EFF = beta[1:size(INTERCEPT_AND_COVARIATE, 2)]
	EFF = beta[(size(INTERCEPT_AND_COVARIATE, 2)+1):end]
	### compute the p-values
	PVAL = ones(length(LOC))
		idx_less_than_mean = EFF .< mean(EFF)
		idx_more_than_mean = EFF .> mean(EFF)
		PVAL[idx_less_than_mean] = Distributions.cdf.(Distributions.Normal(mean(EFF), std(EFF)), EFF[idx_less_than_mean])
		PVAL[idx_more_than_mean] = Distributions.cdf.(Distributions.Normal(mean(EFF), std(EFF)), EFF[idx_more_than_mean])
	LOD = -log10.(PVAL .+ 1.0e-20) ### avoiding infinities
	return(LOC, INTCOVAR_EFF, EFF, PVAL, LOD)
end

end #end of LS module
