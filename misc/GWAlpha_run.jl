#!/home/student.unimelb.edu.au/jparil/Downloads/SOFTWARES/julia-1.0.1/bin/julia

###############
###			###
### GWAlpha ###
###			###
###############
using Plots; Plots.pyplot(); ### to prevent incremental compiulation error

if length(ARGS)!=3
	println("===========================================================================")
	println("GWAlpha arguments:")
	println("\t Argument 1 is the genotype file in sync format.")
	println("\t Argument 2 is the phenotype file in python format.")
	println("\t Argument 3 is the minor allele frequency in decimal form.")
	println("\t Example: ./GWAlpha_run.jl res/GWAlpha_X.sync res/GWAlpha_y.py 0.03")
	println("===========================================================================")
else
	JULIA_SCRIPT_HOME = @__DIR__
	push!(LOAD_PATH, string(JULIA_SCRIPT_HOME, "/src"))
	using GWAlpha_module
	GWAlpha_module.GWAlpha(ARGS)
end
