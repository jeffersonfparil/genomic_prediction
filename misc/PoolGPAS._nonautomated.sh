### (1) prep reference genome
cd /data/Lolium/Genomics/SEQUENCES/DNA/REFERENCE_GENOMES/
wget http://185.45.23.197:5080/ryegrassdata/GENOMIC_ASSEMBLY/lope_V1.0.fasta
echo -e "
import os, sys
from Bio import SeqIO
INPUT_fasta = sys.argv[1]
OUTPUT_fasta = sys.argv[2]
SeqIO.convert(INPUT_fasta, 'fasta', OUTPUT_fasta, 'fasta')
" > fix_REF.py
python fix_REF.py lope_V1.0.fasta Reference.fasta
bwa index -p Reference -a bwtsw Reference.fasta # index the reference genome for bwa
samtools faidx Reference.fasta # index the reference genome for samtools

### (2) fastq to bam
cd /data/Lolium/Quantitative_Genetics
echo -e '#!/bin/bash
FNAME_READ1=$1
FNAME_READ2=$2
FNAME_REF=$3
MAPQ=$4
FNAME_OUT=$5
bwa mem ${FNAME_REF} ${FNAME_READ1} ${FNAME_READ2} | \
    samtools view -q ${MAPQ} -b | \
    samtools sort > ${FNAME_OUT}.bam
' > 01_fastq_to_bam.sh
chmod +x 01_fastq_to_bam.sh
REF=/data/Lolium/Genomics/SEQUENCES/DNA/REFERENCE_GENOMES/Reference
MAPQ=20
time \
parallel --link ./01_fastq_to_bam.sh \
            {1} \
            {2} \
            ${REF} \
            ${MAPQ} \
            {3} \
            ::: $(find PoolGPAS_*/FASTQ/*.fastq.gz | grep _R1 | sort) \
            ::: $(find PoolGPAS_*/FASTQ/*.fastq.gz | grep _R2 | sort) \
            ::: $(find PoolGPAS_*/FASTQ/*.fastq.gz | grep _R1 | sort | cut -d'/' -f3 | cut -d'_' -f1)
mkdir 01_BAM/
mv *.bam 01_BAM/

### (3) generate bam lists per experiment
cd /data/Lolium/Quantitative_Genetics
find 01_BAM/*.bam | grep ACC13 | grep Pool    | sort > CLETH_WITHIN_ACC13_bam.list
find 01_BAM/*.bam | grep ACC49 | grep Pool    | sort > CLETH_WITHIN_ACC49_bam.list
find 01_BAM/*.bam | grep ACC31 | grep Pool    | sort > GLYPH_WITHIN_ACC31_bam.list
find 01_BAM/*.bam | grep ACC62 | grep Pool    | sort > GLYPH_WITHIN_ACC62_bam.list
find 01_BAM/*.bam | grep IG                   | sort > GLYPH_WITHIN_INVER_bam.list
find 01_BAM/*.bam | grep UG                   | sort > GLYPH_WITHIN_URANA_bam.list
find 01_BAM/*.bam | grep ACC11 | grep Pool    | sort > SULFO_WITHIN_ACC11_bam.list
find 01_BAM/*.bam | grep ACC59 | grep Pool    | sort > SULFO_WITHIN_ACC59_bam.list
find 01_BAM/*.bam | grep IS                   | sort > SULFO_WITHIN_INVER_bam.list
find 01_BAM/*.bam | grep US                   | sort > SULFO_WITHIN_URANA_bam.list
find 01_BAM/*.bam | grep ACC09 | grep Pool    | sort > TERBU_WITHIN_ACC09_bam.list
find 01_BAM/*.bam | grep ACC54 | grep Pool    | sort > TERBU_WITHIN_ACC54_bam.list
find 01_BAM/*.bam | grep IT                   | sort > TERBU_WITHIN_INVER_bam.list
find 01_BAM/*.bam | grep UT                   | sort > TERBU_WITHIN_URANA_bam.list
find 01_BAM/*.bam | grep ACC   | grep -v Pool | sort > ACROSS_ACC_bam.list

### (4) bam to mpileup
cd /data/Lolium/Quantitative_Genetics
echo -e '#!/bin/bash
BAM_LIST=$1
FNAME_REF=$2
echo $BAM_LIST
samtools mpileup -aa \
    --fasta-ref ${FNAME_REF} \
    --bam-list ${BAM_LIST} > ${BAM_LIST%_bam.list*}.mpileup
' > 02_bam_to_mpileup.sh
chmod +x 02_bam_to_mpileup.sh
REF=/data/Lolium/Genomics/SEQUENCES/DNA/REFERENCE_GENOMES/Reference.fasta
time \
parallel ./02_bam_to_mpileup.sh {} ${REF} ::: $(ls *_bam.list)
mkdir 02_MPILEUP/
mv *.mpileup 02_MPILEUP/
mv *.list 01_BAM/

### (5) mpileup to sync
### install popoolation2
cd /data/Lolium/Softwares
wget https://sourceforge.net/projects/popoolation2/files/popoolation2_1201.zip
unzip popoolation2_1201.zip -d popoolation2_1201
rm popoolation2_1201.zip

cd /data/Lolium/Quantitative_Genetics
echo -e '#!/bin/bash
MPILEUP=$1
BASQ=$2
MEMGB=$3
NTHREADS=$4
java -ea -Xmx${MEMGB}g -jar \
/data/Lolium/Softwares/popoolation2_1201/mpileup2sync.jar \
    --input ${MPILEUP} \
    --output ${MPILEUP%.mpileup*}.sync \
    --fastq-type sanger \
    --min-qual ${BASQ} \
    --threads ${NTHREADS}
' > 03_mpileup_to_sync.sh
chmod +x 03_mpileup_to_sync.sh
BASQ=20
MEMGB=45
NTHREADS=12
for f in $(find 02_MPILEUP/*.mpileup)
do
    echo $f
    ./03_mpileup_to_sync.sh $f $BASQ $MEMGB $NTHREADS
done
mkdir 03_SYNC/
mv *.sync 03_SYNC/

### (6) sync to MAF-DEPTH-filtered sync
cd /data/Lolium/Quantitative_Genetics
### (6.1) test 50X minimum depth
echo "
using GWAlpha
GWAlpha.sync_processing_module.sync_filter(filename_sync=ARGS[1], MAF=0.001, DEPTH=50)
" > 04_sync_to_filtered_sync.jl
time \
parallel julia 04_sync_to_filtered_sync.jl {} ::: $(find 03_SYNC/*.sync)
### (6.2) test 10X minimum depth
echo "
using GWAlpha
GWAlpha.sync_processing_module.sync_filter(filename_sync=ARGS[1], MAF=0.001, DEPTH=10)
" > 04_sync_to_filtered_sync_10X.jl
time \
parallel -j11 julia 04_sync_to_filtered_sync_10X.jl {} ::: $(find 03_SYNC/*.sync | grep -v DEPTH)
mkdir 04_FILTERED/
mv 03_SYNC/*DEPTH*.sync 04_FILTERED/

### (7) increase the number of available SNPs for across population samples (04_FILTERED/ACROSS_*.sync)
###     to at least 100,000s by excluding some low coverag4e populations (see 01_BAM/)
cd /data/Lolium/Quantitative_Genetics
mkdir 05_EXCLUSION_ACROSS/
wc -l 04_FILTERED/ACROSS*.sync
### generate bam list after excluding bams based on a minimum bam size
ls -l 01_BAM/ | grep ACC | grep -v Pool | grep -v list | sed 's/ \+/\t/g' > 05_across_pop_bam.sizes
echo 'args = commandArgs(trailingOnly=TRUE)
# args = c("0.75e9") ### 0.75 Gigabytes
MINSIZE = as.numeric(args[1])
dat = read.table("05_across_pop_bam.sizes", header=FALSE)
colnames(dat) = c("PERMISSIONS", "NLINKS", "OWNER_NAME", "GROUP_NAME", "SIZE", "MONTH", "DAY", "TIME", "FNAME")
# hist(dat$SIZE)
fnames_out = paste0("01_BAM/", as.character(dat$FNAME[dat$SIZE >= MINSIZE]))
write.table(fnames_out, file=paste0("05_across_pop_MINSIZE-", MINSIZE, "_bam.list"), col.names=FALSE, row.names=FALSE, quote=FALSE)
' > 05_across_pop_bam.r
time \
parallel Rscript 05_across_pop_bam.r {} ::: 0.50e9 0.75e9 0.90e9 1.00e9
### mpileup from filtered bam list
REF=/data/Lolium/Genomics/SEQUENCES/DNA/REFERENCE_GENOMES/Reference.fasta
time \
parallel ./02_bam_to_mpileup.sh {} ${REF} ::: $(ls *_bam.list)
### mpileup to sync
BASQ=20
MEMGB=11
NTHREADS=12
time \
parallel ./03_mpileup_to_sync.sh {} $BASQ $MEMGB $NTHREADS ::: $(ls *.mpileup)
### filter sync by minimum depth
### DEPTH >= 50X
time \
parallel -j4 julia 04_sync_to_filtered_sync.jl {} ::: $(find 05_across_pop_MINSIZE-*.sync)
### DEPTH >= 10X
time \
parallel -j4 julia 04_sync_to_filtered_sync_10X.jl {} ::: $(find 05_across_pop_MINSIZE-*.sync | grep -v DEPTH)
### cleanup
mv 05_*.* 05_EXCLUSION_ACROSS/
mv 05_EXCLUSION_ACROSS/*.r .

#### (8) scp phenotype data from "/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE_GENETICS-combine_SNP_and_transcript_data/ALL_PoolGPAS_MERGED/Phenotypes"
####       into 06_PHENOTYPES/
mkdir 06_PHENOTYPES/
# cd "/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE_GENETICS-combine_SNP_and_transcript_data/ALL_PoolGPAS_MERGED/Phenotypes"
# ### scp the csv phenotype file for ACROSS population Pool-GPAS
# scp -o PreferredAuthentications=password ALL_PHENO.csv jparil@student.unimelb.edu.au@45.113.232.168:/data/Lolium/Quantitative_Genetics/06_PHENOTYPES/
# ### scp the python phenotype data for WITHIN population GWAlpha
# scp -o PreferredAuthentications=password *.py jparil@student.unimelb.edu.au@45.113.232.168:/data/Lolium/Quantitative_Genetics/06_PHENOTYPES/

### (9) FOR ACROSS POPULATION DATASETS:
###     (9.1) extract genoptype and phenotype data per herbicide treatment,
###     (9.2) filter-out populations with pool sizes less than 20, and
###     (9.3) parse sync into allele frequency and compuite Fst.
mkdir 07_ACROSS_POP_DATASETS/
echo '### input MINSIZE threshold in computer/scientific notation
      args = commandArgs(trailingOnly=TRUE)
      args = c("1e+09")
      ### input filenames
      fname_bam_list = paste0("05_EXCLUSION_ACROSS/05_across_pop_MINSIZE-", args[1], "_bam.list")
      fname_sync = paste0("05_EXCLUSION_ACROSS/05_across_pop_MINSIZE-", args[1], "_MAF0.001_DEPTH50.sync")
      fname_phen = "06_PHENOTYPES/ALL_PHENO.csv"
      ### load the population names
      pop_names = gsub(".bam", "", gsub("01_BAM/", "", read.delim(fname_bam_list, header=FALSE)[,1]))
      ### load genotype data
      sync = read.delim(fname_sync, header=FALSE)
      loci_ID = sync[,1:3]
      COUNTS = sync[,4:ncol(sync)]
      rm(sync); gc()
      ### load the phenotype data
      pop_pheno = read.csv(fname_phen, header=TRUE)
      ### filter by population names
      PHENO = droplevels(pop_pheno[(pop_pheno$SAMPLING == "ACROSS") & (pop_pheno$POPULATION %in% pop_names), ])
      ### extract genotype and phenotype data per herbicide treatment
      ### while filtering out populations with pool sizes < 20
      herbicides = unique(as.character(PHENO$HERBICIDE))
      for (herbi in herbicides){
          # herbi = herbicides[3]
          idx_pheno = (PHENO$HERBICIDE == herbi) & (PHENO$POOL_SIZE >= 20)
          sub_pheno = droplevels(PHENO[idx_pheno,])
          sub_pheno_out = cbind(sub_pheno$POOL_SIZE, sub_pheno$SURVIVAL_RATE)
          idx_geno = pop_names %in% sub_pheno$POPULATION
          sub_geno = cbind(loci_ID, COUNTS[, idx_geno])
          ### write-out
          fname_out_geno = paste0(substr(herbi, 1, 5), "_ACROSS_SEAUS_MAF0.001_DEPTH50.sync")
          fname_out_phen = paste0(substr(herbi, 1, 5), "_ACROSS_SEAUS_pheno.csv")
          fname_out_phen_all = paste0(substr(herbi, 1, 5), "_ACROSS_SEAUS_pheno.phen")
          write.table(sub_geno, file=fname_out_geno, sep="\t", col.names=FALSE, row.names=FALSE, quote=FALSE)
          write.table(sub_pheno_out, file=fname_out_phen, sep=",", col.names=FALSE, row.names=FALSE, quote=FALSE)
          write.table(sub_pheno, file=fname_out_phen_all, sep=",", col.names=TRUE, row.names=FALSE, quote=FALSE)
      }
' > 07_extract_geno_pheno_across.r
time \
for MINSIZE in "5e+08" "7.5e+08" "9e+08" "1e+09"
do
    # MINSIZE="1e+09"
    echo $MINSIZE
    Rscript 07_extract_geno_pheno_across.r $MINSIZE
    mkdir 07_ACROSS_POP_DATASETS/MINZISE-${MINSIZE}/
    mv *.sync 07_ACROSS_POP_DATASETS/MINZISE-${MINSIZE}/
    mv *_pheno.* 07_ACROSS_POP_DATASETS/MINZISE-${MINSIZE}/
done








### MAYBE DITCH THIS STEP AND PROCEED TO MERGING PRIOR TO FST CALCULATION AN LOOCV 2020-07-06
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

### parse sync and compute Fst
echo "
    using DelimitedFiles
    using Distributed
    Distributed.addprocs(Int(length(Sys.cpu_info())/4))
    @everywhere using GWAlpha
    # ARGS = [\"CLETH\", \"1e+09\"]
    cd(string(\"07_ACROSS_POP_DATASETS/MINZISE-\", ARGS[2]))
    sync_fname = string(ARGS[1], \"_ACROSS_SEAUS_MAF0.001_DEPTH50.sync\")
    phen_fname = string(ARGS[1], \"_ACROSS_SEAUS_pheno.csv\")
    pool_sizes = convert(Array{Int64}, DelimitedFiles.readdlm(phen_fname, ',')[:,1])
    ### parse
    GWAlpha.sync_processing_module.sync_parse(sync_fname)
    ### estimate Fst
    GWAlpha.relatedness_module.Fst_pairwise(filename_sync=sync_fname, window_size=100000, pool_sizes=pool_sizes, METHOD=\"WeirCock\")
" > 07_parse_and_fst_across.jl
time \
parallel -j 4 julia 07_parse_and_fst_across.jl {1} {2} ::: CLETH GLYPH SULFO TERBU ::: "5e+08" "7.5e+08" "9e+08" "1e+09"

### (10) FOR ACROSS POPULATION DATASETS:
###      Leave-one-out cross-validation (LOOCV)
echo '
    ### load libraries
    using GWAlpha
    using DelimitedFiles
    using RCall
    ### set working directory
    # ARGS = ["07_ACROSS_POP_DATASETS/MINZISE-1e+09/TERBU_ACROSS_SEAUS_MAF0.001_DEPTH50.sync"]
    cd(dirname(ARGS[1]))
    ### extract input filenames
    sync_fname = basename(ARGS[1])
    geno_fname = replace(sync_fname, ".sync" => "_ALLELEFREQ.csv")
    phen_fname = replace(sync_fname, "_MAF0.001_DEPTH50.sync" => "_pheno.csv")
    cova_fname = replace(sync_fname, ".sync" => "_COVARIATE_FST.csv")
    ### load genotype, phenotype, covariate input
    SYNC = DelimitedFiles.readdlm(sync_fname, `\t`); GWAlpha.sync_processing_module.sync_parse(sync_fname)
    GENO = DelimitedFiles.readdlm(geno_fname, `,`)
    PHEN = DelimitedFiles.readdlm(phen_fname, `,`)
    COVA = DelimitedFiles.readdlm(cova_fname, `,`)
    ### extract dataset dimesions
    n = size(PHEN,1)
    p = size(GENO,1)
    ### iterate across individuals for leave-one-out cross-validation
    MODEL = []
    Y_TRUE = []
    Y_PRED = []
    @time for i in 1:n
        ### divide into n-1 training datapoints and 1 validation datapoint
        idx = collect(1:n)[collect(1:n) .!= i]
        TRAIN_SYNC = hcat(SYNC[:, 1:3], SYNC[:, idx .+ 3])
        TRAIN_GENO = hcat(GENO[:, 1:3], GENO[:, idx .+ 3])
        TRAIN_PHEN = PHEN[idx, :]
        TRAIN_COVA = COVA[idx, idx]
        VALID_SYNC = hcat(SYNC[:, 1:3], SYNC[:, i + 3])
        VALID_GENO = hcat(GENO[:, 1:3], GENO[:, i + 3])
        VALID_PHEN = reshape(PHEN[i, :], 1, 2)
        ### filenames
        TRAIN_SYNC_fname = string("TRAIN-", sync_fname)
        TRAIN_GENO_fname = string("TRAIN-", geno_fname)
        TRAIN_PHEN_fname = string("TRAIN-", phen_fname)
        TRAIN_COVA_fname = string("TRAIN-", cova_fname)
        VALID_SYNC_fname = string("VALID-", sync_fname)
        VALID_GENO_fname = string("VALID-", geno_fname)
        VALID_PHEN_fname = string("VALID-", phen_fname)
        ### write-out
        DelimitedFiles.writedlm(TRAIN_SYNC_fname, TRAIN_SYNC)
        DelimitedFiles.writedlm(TRAIN_GENO_fname, TRAIN_GENO, `,`)
        DelimitedFiles.writedlm(TRAIN_PHEN_fname, TRAIN_PHEN, `,`)
        DelimitedFiles.writedlm(TRAIN_COVA_fname, TRAIN_COVA, `,`)
        DelimitedFiles.writedlm(VALID_SYNC_fname, VALID_SYNC)
        DelimitedFiles.writedlm(VALID_GENO_fname, VALID_GENO, `,`)
        DelimitedFiles.writedlm(VALID_PHEN_fname, VALID_PHEN, `,`)
        ### MIXED-REML-WEIRCOCK
        @time GWAlpha.PoolGPAS(filename_sync=TRAIN_SYNC_fname, filename_phen=TRAIN_PHEN_fname, maf=0.001, depth=50, model="MIXED", filename_random_covariate=TRAIN_COVA_fname, varcomp_est="REML")
        ### RIDGE
        @time GWAlpha.PoolGPAS(filename_sync=TRAIN_SYNC_fname, filename_phen=TRAIN_PHEN_fname, maf=0.001, depth=50, model="GLMNET", glmnet_alpha=0.00)
        ### LASSO
        @time GWAlpha.PoolGPAS(filename_sync=TRAIN_SYNC_fname, filename_phen=TRAIN_PHEN_fname, maf=0.001, depth=50, model="GLMNET", glmnet_alpha=1.00)
        ### validation
        BETA_MIXED_fname = string(replace(TRAIN_SYNC_fname, ".sync" => ""), "-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv")
        BETA_RIDGE_fname = string(replace(TRAIN_SYNC_fname, ".sync" => ""), "-GLMNET_ALPHA0.0-OUTPUT.csv")
        BETA_LASSO_fname = string(replace(TRAIN_SYNC_fname, ".sync" => ""), "-GLMNET_ALPHA1.0-OUTPUT.csv")
        for f in [BETA_MIXED_fname, BETA_RIDGE_fname, BETA_LASSO_fname]
            model = split(f, "-")[3]
            BETA = DelimitedFiles.readdlm(f, `,`)
            chrom=BETA[2:end,1]; pos=BETA[2:end,2]; allele=BETA[2:end,3]; effect=BETA[2:end,5]; intercept=BETA[2,5];
            @rput chrom; @rput pos; @rput allele; @rput effect; @rput intercept; @rput VALID_GENO;
            R"BETA = data.frame(chrom=unlist(as.character(chrom)), pos=unlist(as.numeric(pos)), allele=unlist(as.character(allele)), effect=unlist(as.numeric(effect)))
            VALID_GENO = as.data.frame(VALID_GENO)
            colnames(VALID_GENO) = c(`chrom`, `pos`, `allele`, `freq`)
            VALID_GENO$chrom = as.character(VALID_GENO$chrom)
            VALID_GENO$pos = as.numeric(VALID_GENO$pos)
            VALID_GENO$allele = as.character(VALID_GENO$allele)
            VALID_GENO$freq = as.numeric(VALID_GENO$freq)
            MERGED = merge(BETA, VALID_GENO, by=c(`chrom`, `pos`, `allele`))
            y_pred = intercept + sum(MERGED$freq * MERGED$effect)
            "
            @rget y_pred
            ### outputs
            push!(MODEL, model)
            push!(Y_TRUE, VALID_PHEN[2])
            push!(Y_PRED, y_pred)
        end
        ### clean-up
        rm(TRAIN_SYNC_fname)
        rm(TRAIN_GENO_fname)
        rm(TRAIN_PHEN_fname)
        rm(TRAIN_COVA_fname)
        rm(VALID_SYNC_fname)
        rm(VALID_GENO_fname)
        rm(VALID_PHEN_fname)
        rm(BETA_MIXED_fname)
        rm(BETA_RIDGE_fname)
        rm(BETA_LASSO_fname)
    end

    ### OUTPUT
    HERBI = split(sync_fname, "_")[1]
    @rput MODEL; @rput Y_TRUE; @rput Y_PRED; @rput HERBI;
    R"model = unlist(as.character(MODEL))
    y_true = unlist(as.numeric(Y_TRUE))
    y_pred = unlist(as.numeric(Y_PRED))
    DF = data.frame(MODEL=model, Y_TRUE=y_true, Y_PRED=y_pred)
    DF$MODEL = as.character(DF$MODEL)
    MODEL = c(); CORRELATION = c(); RMSE = c(); MEAN_ABS_DEV = c()

    png(paste0(HERBI, `_LOOCV.png`), width=800, height=800)
    plot(x=DF$Y_TRUE, y=DF$Y_PRED, type=`n`, main=HERBI, xlab=`Observed Resistance`, ylab=`Predicted Resistance`)
    colors_light=c(`#66c2a5`, `#fc8d62`, `#8da0cb`); colors_dark=c(`#1b9e77`, `#d95f02`, `#7570b3`)
    for (i in 1:length(unique(DF$MODEL))){
        model = unique(DF$MODEL)[i]
        sub = droplevels(DF[DF$MODEL==model, ])
        MODEL = c(MODEL, model)
        CORRELATION = c(CORRELATION, cor(sub$Y_TRUE, sub$Y_PRED))
        RMSE = c(RMSE, sqrt(mean((sub$Y_TRUE-sub$Y_PRED)^2)))
        MEAN_ABS_DEV = c(MEAN_ABS_DEV, mean(abs(sub$Y_TRUE-sub$Y_PRED)))
        points(x=sub$Y_TRUE, y=sub$Y_PRED, col=colors_light[i], pch=20)
        abline(lm(Y_PRED ~ Y_TRUE, data=sub), col=colors_dark[i])
    }
    legend(`topleft`, legend=unique(DF$MODEL), pch=20, lty=1, col=colors_light)
    dev.off()
    OUTPUT = data.frame(MODEL, CORRELATION, RMSE, MEAN_ABS_DEV)
    write.table(OUTPUT, file=paste0(HERBI, `_LOOCV.csv`), sep=`,`, quote=FALSE, row.names=FALSE)
    write.table(DF, file=paste0(HERBI, `_LOOCV_RAW.csv`), sep=`,`, quote=FALSE, row.names=FALSE)
    "
' > 08_LOOCV.jl
sed -i "s/\`/\'/g" 08_LOOCV.jl
time \
parallel julia 08_LOOCV.jl ::: $(find 07_ACROSS_POP_DATASETS/MINZISE-*/*_ACROSS_SEAUS_MAF0.001_DEPTH50.sync | grep -v TRAIN | grep -v VALID)

### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@










### merging across and within popualation data so that we have more data points for LOOCV
R
args = commandArgs(trailingOnly=TRUE)
# args = c("TERBU")
fname_list = c(system(paste("find 04_FILTERED/*WITHIN*DEPTH50.sync | grep ", args[1], "| grep DEPTH50"), intern=TRUE),
               system(paste("find 04_FILTERED/*WITHIN*DEPTH50.sync | grep ", args[1], "| grep DEPTH50"), intern=TRUE))
               






### merge filtered sync and phenotypes per herbicide
### NOTE1: The quantitative herbicide resistance data is used in TRIFLURALIN treatments because of everything was resistant.
### NOTE2: GLYPH_WITHIN_URANA* and TERBU_WITHIN_INVER* were excluded due to low coverage dragging the merged datasets to low common SNPs
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/01_SYNC_MAPQ20_BASQ20
echo 'args = commandArgs(trailingOnly=TRUE)
    # args = c("CLETH") # args = c("TRIFL")
    fname_list = system(paste("ls | grep ", args[1], "| grep DEPTH50"), intern=TRUE)
    ### merge genotype data
    for (f in fname_list){
        if (!exists("GENO")){
            GENO = read.delim(f, header=FALSE)
        } else {
            GENO = merge(GENO, read.delim(f, header=FALSE), by=c("V1", "V2", "V3"))
        }
    }
    GENO = droplevels(GENO)
    ### merge phenotype data
    all_pheno = read.csv("/data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/02_PHENOTYPES/ALL_PHENO.csv")
    names_list = strsplit(basename(fname_list), "_")
    for (i in 1:length(names_list)){
        herbicide = names_list[[i]][1]
        sampling = names_list[[i]][2]
        pop_id = names_list[[i]][3]
        if (!exists("PHENO")){
            PHENO = all_pheno[(substr(all_pheno$HERBICIDE, 1, 5)==herbicide) & (all_pheno$SAMPLING==sampling) & (all_pheno$POP_ID==pop_id), ]
        } else {
            PHENO = rbind(PHENO, all_pheno[(substr(all_pheno$HERBICIDE, 1, 5)==herbicide) & (all_pheno$SAMPLING==sampling) & (all_pheno$POP_ID==pop_id), ])
        }
    }
    PHENO = droplevels(PHENO)
    ### save
    write.table(GENO, file=paste0("MERGED_", args[1], "_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync"), sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)
    write.table(cbind(PHENO$POOL_SIZE, PHENO$SURVIVAL_RATE), file=paste0("MERGED_", args[1], "_pheno.csv"), sep=",", quote=FALSE, row.names=FALSE, col.names=FALSE)
    write.table(PHENO, file=paste0("MERGED_", args[1], ".pheno"), sep=",", quote=FALSE, row.names=FALSE, col.names=FALSE)
' > merge.r
time \
parallel Rscript merge.r {} ::: CLETH GLYPH SULFO TERBU TRIFL
cp MERGED_* /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
### remove populations with pool size less than 20
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
echo 'args = commandArgs(trailingOnly=TRUE)
      # args = c("CLETH", "20")
      herbi = args[1]
      pool_size_threshold = as.numeric(args[2])
      fname_phen = paste0("MERGED_", herbi, ".pheno")
      fname_phen_input = paste0("MERGED_", herbi, "_pheno.csv")
      fname_sync = paste0("MERGED_", herbi, "_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync")
      ### read main phenotype file
      PHEN = read.csv(fname_phen, header=FALSE)
      colnames(PHEN) = c("YEAR", "SAMPLING", "POP_GROUP", "POP", "NPOOLS", "POOLSIZE", "HERBICIDE", "QUANTRES", "SURVIVAL")
      idx = PHEN$POOLSIZE >= pool_size_threshold
      ### load sync and phenotype input data
      Y = read.csv(fname_phen_input, header=FALSE)
      SYNC = read.delim(fname_sync, header=FALSE)
      ## filter
      PHEN = PHEN[idx, ]
      Y = Y[idx, ]
      SYNC = SYNC[, c(rep(TRUE,3), idx)]
      ### overwrite unfiltered files
      write.table(PHEN, file=fname_phen, row.names=FALSE, col.names=FALSE, sep=",", quote=FALSE)
      write.table(Y, file=fname_phen_input, row.names=FALSE, col.names=FALSE, sep=",", quote=FALSE)
      write.table(SYNC, file=fname_sync, row.names=FALSE, col.names=FALSE, sep="\t", quote=FALSE)
' > filter_data_by_pool_sizes.r
time \
parallel Rscript filter_data_by_pool_sizes.r {} 20 ::: CLETH GLYPH SULFO TERBU
### Estimate pairwise Fst
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
echo "
    using DelimitedFiles
    using Distributed
    Distributed.addprocs(length(Sys.cpu_info()))
    @everywhere using GWAlpha
    # ARGS = [\"CLETH\"]
    sync_fname = string(\"MERGED_\", ARGS[1], \"_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync\")
    phen_fname = string(\"MERGED_\", ARGS[1], \"_pheno.csv\")
    pool_sizes = convert(Array{Int64}, DelimitedFiles.readdlm(phen_fname, ',')[:,1])
    GWAlpha.relatedness_module.Fst_pairwise(filename_sync=sync_fname, window_size=100000, pool_sizes=pool_sizes, METHOD=\"WeirCock\")
" > estimate_fst.jl
for herbi in CLETH GLYPH SULFO TERBU TRIFL
do
    echo $herbi
    julia estimate_fst.jl $herbi
done




### perform GWAS on each herbicide treatment
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
mkdir GWAS_PER_HERBI_PER_POP/   ### for SNPwise GWAlpha where this model is applicable WITHIN POPULATIONS ALONE!!!!
mkdir GWAS_PER_HERBI/           ### for non-iterative GWAlpha: LMM-Fst and glmnet models
echo "GWAlpha - iterative"
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/01_SYNC_MAPQ20_BASQ20
echo 'using Distributed
      Distributed.addprocs(length(Sys.cpu_info()))
      @everywhere using GWAlpha
      fname_sync = ARGS[1]
      maf = 0.001
      depth = 50
      fname_phen = string("/data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/02_PHENOTYPES/", split(basename(fname_sync), string("_MAF", maf, "_DEPTH", depth, ".sync"))[1], "_pheno.py")
      @time GWAlpha.PoolGPAS(filename_sync=fname_sync, filename_phen=fname_phen, maf=maf, depth=depth, plot=true)
' > gwalpha_snpwise.jl
### perform parallel iterative GWAlpha for each within population sample
for herbi in $(ls | grep WITHIN | grep MAF)
do
    # herbi=CLETH_WITHIN_ACC13_MAF0.001_DEPTH50.sync
    julia gwalpha_snpwise.jl $herbi
    mv *-GWAlpha* /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/GWAS_PER_HERBI_PER_POP/
done
echo "GWAlpha - non-iterative"
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
echo 'using GWAlpha
      herbi = ARGS[1]
      maf = 0.001
      depth = 50
      fname_sync = string("MERGED_", herbi, "_MAPQ20_BASQ20_MAF", maf, "_DEPTH", depth, ".sync")
      fname_phen = string("MERGED_", herbi, "_pheno.csv")
      fname_cova = string("MERGED_", herbi, "_MAPQ20_BASQ20_MAF", maf, "_DEPTH", depth, "_COVARIATE_FST.csv")
      @time GWAlpha.PoolGPAS(filename_sync=fname_sync, filename_phen=fname_phen, maf=maf, depth=depth, model="MIXED", filename_random_covariate=fname_cova, varcomp_est="REML", plot=true)
      @time GWAlpha.PoolGPAS(filename_sync=fname_sync, filename_phen=fname_phen, maf=maf, depth=depth, model="GLMNET", glmnet_alpha=0.00, plot=true)
      @time GWAlpha.PoolGPAS(filename_sync=fname_sync, filename_phen=fname_phen, maf=maf, depth=depth, model="GLMNET", glmnet_alpha=0.10, plot=true)
      @time GWAlpha.PoolGPAS(filename_sync=fname_sync, filename_phen=fname_phen, maf=maf, depth=depth, model="GLMNET", glmnet_alpha=0.50, plot=true)
      @time GWAlpha.PoolGPAS(filename_sync=fname_sync, filename_phen=fname_phen, maf=maf, depth=depth, model="GLMNET", glmnet_alpha=1.00, plot=true)
' > gwalpa_noniter.jl
### perform non-iterative GWAlpha for each merged herbicide treatment
time parallel julia gwalpa_noniter.jl {} ::: CLETH GLYPH SULFO TERBU TRIFL
mv *-OUTPUT* /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/GWAS_PER_HERBI/
### GWAS output analyses:
### (1) LOD correlations
### (2) Blasting peaks
### (3) Gene ontology thingies

### LOD correlations
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/
echo 'args = commandArgs(trailingOnly=TRUE)
      herbi = args[1] # herbi="CLETH"
      fnames_iterative = system(paste0("ls GWAS_PER_HERBI_PER_POP/*-OUTPUT.csv | grep ", herbi), intern=TRUE)
      fnames_noniterat = system(paste0("ls GWAS_PER_HERBI/*-OUTPUT.csv | grep -v RANEF | grep ", herbi), intern=TRUE)
      for (f in c(fnames_iterative, fnames_noniterat)) {
          if (dirname(f)=="GWAS_PER_HERBI_PER_POP") {
              model = "GWAlpha"
              pop = unlist(strsplit(basename(f), "_"))[3]
              id = paste0(model, "_", pop)
          } else {
              id = paste(unlist(strsplit(unlist(strsplit(basename(f), "_"))[6:7], "-"))[2:3], collapse="_")
          }
          if (exists("MERGED")==FALSE){
              MERGED = read.csv(f)
          } else {
              MERGED = merge(MERGED, read.csv(f), all=TRUE, by=c("CHROM", "POS", "ALLELE"))
          }
          colnames(MERGED)[(ncol(MERGED)-3):ncol(MERGED)] = paste0(id, c("_FREQ", "_EFF", "_PVAL", "_LOD"))
      }
      LOD_MATRIX = MERGED[, grepl("LOD", colnames(MERGED))]
      bonferroni_threshold = -log10(0.05/max(apply(MERGED[, grepl("LOD", colnames(MERGED))], MARGIN=2, FUN=function(x){sum(is.na(x))})))
      LOD_MATRIX = LOD_MATRIX[apply(LOD_MATRIX, MARGIN=1, FUN=max, na.rm=TRUE) >= bonferroni_threshold, ]
      CORR_RANK_LOD = cor(LOD_MATRIX, use="na.or.complete", method="spearman")
      write.csv(CORR_RANK_LOD, file=paste0(herbi, "_LOD_RANK_CORRELATION.csv"), quote=FALSE)
' > lod_rank_cor.r
time \
parallel Rscript lod_rank_cor.r {} ::: CLETH GLYPH SULFO TERBU
mkdir GWAS_ANALYSIS/
mv *_LOD_RANK_CORRELATION.csv GWAS_ANALYSIS/

### Blasting peaks
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/GWAS_ANALYSIS/
### download the Reference genome's (Lolium perenne) gene annotation data (within 100kb)
wget http://185.45.23.197:5080/ryegrassdata/GENE_ANNOTATIONS/Lp_Annotation_V1.1.mp.gff3 ### gff3 format: seqid, source, type, start, end, score, strand, phase, attributes
# extract neighboring gff3 names (100kb from the peaks)
echo 'args = commandArgs(trailingOnly=TRUE)
      # args=c("../GWAS_PER_HERBI_PER_POP/CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv", "1")
      f = args[1]
      LD_kb = as.numeric(args[2])
      GFF3 = read.delim("Lp_Annotation_V1.1.mp.gff3", skip=1, header=FALSE)
      colnames(GFF3) = c("seqid", "source", "type", "start", "end", "score", "strand", "phase", "attributes")
      GFF3$seqid = as.character(GFF3$seqid)
      print(f)
      dat = read.csv(f)
      dat = dat[order(dat$LOD, decreasing=TRUE), ]
      dat$CHROM = as.character(dat$CHROM)
      bonferroni_threshold = -log10(0.01/nrow(dat))
      dat = droplevels(dat[dat$LOD >= bonferroni_threshold, ])
      for (i in 1:nrow(dat)){
          # i = 1
          idx = (as.character(dat$CHROM[i]) == as.character(GFF3$seqid)) & ((dat$POS[i]-GFF3$start)<=(1000*LD_kb)) & ((dat$POS[i]-GFF3$end)<=(1000*LD_kb))
          if (exists("PEAKS")==FALSE){
              PEAKS = GFF3[idx, ]
              PEAKS$LOD = rep(dat$LOD[i], times=nrow(PEAKS))
          } else {
              peaks = GFF3[idx, ]
              peaks$LOD = rep(dat$LOD[i], times=nrow(peaks))
              PEAKS = rbind(PEAKS, peaks)
          }
          ### peaks with no nearby gff3 predicted gene
          if (sum(idx)==0){
            if (exists("NO_GFF3")==FALSE){
                NO_GFF3 = dat[i, ]
            } else {
                NO_GFF3 = rbind(NO_GFF3, dat[i, ])
            }
          } else {
            if (exists("YES_GFF3")==FALSE){
                YES_GFF3 = dat[i, ]
            } else {
                YES_GFF3 = rbind(YES_GFF3, dat[i, ])
            }
          }
      }
      PEAKS = PEAKS[PEAKS$type == "gene", ]
      PEAKS = PEAKS[!duplicated(PEAKS[,c(1,4,5)]), ]
      fname_out = sub("-OUTPUT.csv", "-GFF3_PEAKS.csv", basename(f))
      write.table(PEAKS, file=fname_out, sep=",", row.names=FALSE, quote=FALSE)
      
      ### peaks with and without nearby gff3 predicted gene
      fname_out = sub("-OUTPUT.csv", "-PEAKS_LIST_YES_GFF3.csv", basename(f))
      write.table(YES_GFF3, file=fname_out, sep=",", row.names=FALSE, quote=FALSE)
      fname_out = sub("-OUTPUT.csv", "-PEAKS_LIST_NO_GFF3.csv", basename(f))
      write.table(NO_GFF3, file=fname_out, sep=",", row.names=FALSE, quote=FALSE)
' > gff3_peaks.r
time \
parallel Rscript gff3_peaks.r {} 100 ::: $(find ../GWAS_PER_HERBI_PER_POP | grep "OUTPUT.csv")
# parallel Rscript gff3_peaks.r {} 1 ::: $(find ../GWAS_PER_HERBI_PER_POP | grep "OUTPUT.csv") $(find ../GWAS_PER_HERBI | grep -v RANEF | grep "OUTPUT.csv")
### and/or just extract the coordinates of the peaks
echo 'args = commandArgs(trailingOnly=TRUE)
      # args=c("../GWAS_PER_HERBI_PER_POP/CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv", "100")
      f = args[1]
      LD_kb = as.numeric(args[2])
      print(f)
      dat = read.csv(f)
      dat = dat[order(dat$LOD, decreasing=TRUE), ]
      dat$CHROM = as.character(dat$CHROM)
      bonferroni_threshold = -log10(0.01/nrow(dat))
      dat = droplevels(dat[dat$LOD >= bonferroni_threshold, ])
      out = data.frame(CHROM=dat$CHROM, POS=dat$POS, ALLELE=dat$ALLELE, POS_INIT=dat$POS-(LD_kb*1000), POS_SLUT=dat$POS+(LD_kb*1000))
      out$POS_INIT[out$POS_INIT<1] = 1
      fname_out = sub("-OUTPUT.csv", "-SEQ_PEAKS.csv", basename(f))
      write.table(out, file=fname_out, sep=",", row.names=FALSE, quote=FALSE)
' > seq_peaks.r
time \
parallel Rscript seq_peaks.r {} 10 ::: $(find ../GWAS_PER_HERBI_PER_POP | grep "OUTPUT.csv")

### Reformat the genome assembly (remove sequence wrapping) ###
grep "^>" Reference.fasta > SCAFFOLDS.temp
csplit Reference.fasta /\>/ '{*}'
ls xx* > SPLIT_FNAMES.temp
rm $(head -n1 SPLIT_FNAMES.temp) ### remove the first file which contains nada!
ls xx* > SPLIT_FNAMES.temp
rm REFORMATED_GENOME.fasta || touch REFORMATED_GENOME.fasta
time for i in $(seq 1 $(cat SPLIT_FNAMES.temp | wc -l))
do
    echo $i
    seq=$(head -n${i} SPLIT_FNAMES.temp | tail -n1)
    head -n1 ${seq} > temp_name
    tail -n+2 ${seq} > temp_seq
    sed -zi 's/\n//g' temp_seq
    cat temp_name >> REFORMATED_GENOME.fasta
    cat temp_seq >> REFORMATED_GENOME.fasta
    echo -e "" >> REFORMATED_GENOME.fasta ### insert newline character that was removed by sed
done
rm *temp* xx*

# ### blast gff3 peaks on TAIR10 genes instead of the full ncbi nucleotide database so we immediately get gene names recognized by GO enrichment tools
# ### download TAIR10 gff3
# wget https://www.arabidopsis.org/download_files/Genes/TAIR10_genome_release/TAIR10_gff3/TAIR10_GFF3_genes.gff
# ### merge into one annotation file with only the "gene" entries
# grep "gene" TAIR10_GFF3_genes.gff | sort | uniq > TAIR10.gff3
# rm TAIR10_GFF3_genes.gff
# ### download the TAIR10 genome assembly and remove sequence wrapping per chromosome
# wget https://www.arabidopsis.org/download_files/Genes/TAIR10_genome_release/TAIR10_chromosome_files/TAIR10_chr_all.fas
# grep "^>" TAIR10_chr_all.fas > CHROMS.temp
# csplit TAIR10_chr_all.fas /\>/ '{*}'
# ls xx* > SPLIT_FNAMES.temp
# rm $(head -n1 SPLIT_FNAMES.temp) ### remove the first file which contains nada!
# ls xx* > SPLIT_FNAMES.temp
# ### manually change chromosome names to be consistent with TAIR10.gff3
# sed -i "1s/.*/>Chr1/" xx01
# sed -i "1s/.*/>Chr2/" xx02
# sed -i "1s/.*/>Chr3/" xx03
# sed -i "1s/.*/>Chr4/" xx04
# sed -i "1s/.*/>Chr5/" xx05
# sed -i "1s/.*/>ChrM/" xx06
# sed -i "1s/.*/>ChrC/" xx07
# ### remove sequence wrapping within chromosome
# rm TAIR10.fasta || touch TAIR10.fasta
# time for i in $(seq 1 $(cat SPLIT_FNAMES.temp | wc -l))
# do
#     echo $i
#     seq=$(head -n${i} SPLIT_FNAMES.temp | tail -n1)
#     head -n1 ${seq} > temp_name
#     tail -n+2 ${seq} > temp_seq
#     sed -zi 's/\n//g' temp_seq
#     cat temp_name >> TAIR10.fasta
#     cat temp_seq >> TAIR10.fasta
#     echo -e "" >> TAIR10.fasta ### insert newline character that was removed by sed
# done
# rm *temp* xx*
# ### extract the sequences for each gene and build the blast database
# rm TAIR10.genes.fasta || touch TAIR10.genes.fasta
# time \
# while IFS= read -r i
# do
#     # i=$(head -n1 TAIR10.gff3)
#     chrom=$(cut -f1 <<<$i)
#     start=$(cut -f4 <<<$i)
#     end=$(cut -f5 <<<$i)
#     gene=$(cut -d";" -f1 <<<$(cut -f9 <<<$i) | sed 's/ID=//g')
#     grep -A 1 ${chrom} TAIR10.fasta | tail -n1 > seq.temp
#     echo -e "> ${gene}:${chrom}:${start}-${end}" >> TAIR10.genes.fasta
#     cut -c${start}-${end} seq.temp >> TAIR10.genes.fasta
#     rm seq.temp
# done < TAIR10.gff3
# ### generate the blast database
# time \
# makeblastdb -in TAIR10.genes.fasta \
#             -title "TAIR10.genes.blastdb" \
#             -dbtype nucl
# ### blasting (50% percent identity) NOTE THAT: we are blasting the  gff3 peaks on TAIR10 genes instead of the full ncbi nucleotide database so we immediately get gene names recognized by GO enrichment tools
# echo '#!/bin/bash
#     gff3_peaks_file=${1}
#     DB=${2}
#     REFORMATED_GENOME=${3}
#     # ### TEST:
#     # # gff3_peaks_file=CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-GFF3_PEAKS.csv
#     # # DB=/data/BlastDB/NCBI_NT0060
#     # gff3_peaks_file=CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-SEQ_PEAKS.csv
#     # DB=TAIR10.genes.fasta
#     # REFORMATED_GENOME=REFORMATED_GENOME.fasta
#     ### BUILD THE QUERY SEQUENCES
#     rm ${gff3_peaks_file}.query.fasta || touch ${gff3_peaks_file}.query.fasta
#     for locus in $(cat ${gff3_peaks_file} | tail -n+2)
#     do
#       # locus=$(cat ${gff3_peaks_file} | tail -n+2 | head -n1)
#       chrom=$(cut -d, -f1 <<<$locus)
#       start=$(cut -d, -f4 <<<$locus)
#       end=$(cut -d, -f5 <<<$locus)
#       grep -A 1 ${chrom} ${REFORMATED_GENOME} | tail -n1 > ${gff3_peaks_file}.seq.temp
#       if [ $end -gt $(tail -n1 ${gff3_peaks_file}.seq.temp | wc -c) ]
#       then
#         end=$(tail -n1 ${gff3_peaks_file}.seq.temp | wc -c)
#       fi
#       echo -e "> ${chrom}:${pos}:${start}-${end}" >> ${gff3_peaks_file}.query.fasta
#       cut -c${start}-${end} ${gff3_peaks_file}.seq.temp >> ${gff3_peaks_file}.query.fasta
#       rm ${gff3_peaks_file}.seq.temp
#     done
#     ### BLAST OFF!
#     DIR=$(dirname $gff3_peaks_file)
#     FNAME=$(basename $gff3_peaks_file)
#     blastn -db ${DB} \
#       -query ${gff3_peaks_file}.query.fasta \
#       -perc_identity 50 \
#       -outfmt "6 qseqid staxids pident evalue qcovhsp bitscore stitle" \
#       -out ${DIR}/BLASTOUT-${FNAME%.csv*}.txt
# ' > blast_gff3_peaks.sh
# chmod +x blast_gff3_peaks.sh
# time \
# parallel ./blast_gff3_peaks.sh {} TAIR10.genes.fasta REFORMATED_GENOME.fasta ::: $(ls *-GFF3_PEAKS.csv)
# # parallel ./blast_gff3_peaks.sh {} /data/BlastDB/NCBI_NT0060 REFORMATED_GENOME.fasta ::: $(ls *-GFF3_PEAKS.csv)
# time \
# parallel ./blast_gff3_peaks.sh {} TAIR10.genes.fasta REFORMATED_GENOME.fasta ::: $(ls *-SEQ_PEAKS.csv)

# for herbi in CLETH GLYPH SULFO TERBU
# do
#     echo $herbi-GFF3_PEAKS
#     cat BLASTOUT-${herbi}*GFF3_PEAKS.txt | cut -f 7 | cut -d":" -f1 | sort | uniq > ${herbi}-GFF3_PEAKS.blastout
#     echo $herbi-SEQ_PEAKS
#     cat BLASTOUT-${herbi}*SEQ_PEAKS.txt | cut -f 7 | cut -d":" -f1 | sort | uniq > ${herbi}-SEQ_PEAKS.blastout
# done
# ### GO term enrichment analysis with ShinyGO (http://bioinformatics.sdstate.edu/go/)
# ### using any combinations for cleth do not yield significant enrichment
# ### using gff3 for glyph does
# ### using seq only for sulfo does
# ### too few genes across both for terbu

### blast gff3 peaks on rice genes instead of TAIR10 or the full ncbi nucleotide database
### download rice ensemble genome and gff3
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/GWAS_ANALYSIS/
for i in 1 2 3 4 5 6 7 8 9 10 11 12 Mt Pt
do
    wget ftp://ftp.ensemblgenomes.org/pub/release-47/plants/fasta/oryza_sativa/dna/Oryza_sativa.IRGSP-1.0.dna.chromosome.${i}.fa.gz
    wget ftp://ftp.ensemblgenomes.org/pub/release-47/plants/gff3/oryza_sativa/Oryza_sativa.IRGSP-1.0.47.chromosome.${i}.gff3.gz
done
gunzip -d Oryza_sativa*.gz

### remove sequence wrapping within chromosome
rm ORYZA.fasta || touch ORYZA.fasta
time for i in 1 2 3 4 5 6 7 8 9 10 11 12 Mt Pt
do
    # i=1
    echo $i
    seq=Oryza_sativa.IRGSP-1.0.dna.chromosome.${i}.fa
    head -n1 ${seq} > temp_name
    tail -n+2 ${seq} > temp_seq
    sed -zi 's/\n//g' temp_seq
    cat temp_name >> ORYZA.fasta
    cat temp_seq >> ORYZA.fasta
    echo -e "" >> ORYZA.fasta ### insert newline character that was removed by sed
done
rm *temp*
### extract the sequences for each gene and build the blast database
cat Oryza_sativa*.gff3 | sort | uniq | grep gene_id > ORYZA.gff3
rm ORYZA.genes.fasta || touch ORYZA.genes.fasta
touch ORYZA.genes.fasta
time \
while IFS= read -r i
do
    # i=$(head -n1 ORYZA.gff3)
    chrom=$(cut -f1 <<<$i)
    start=$(cut -f4 <<<$i)
    end=$(cut -f5 <<<$i)
    gene=$(cut -d";" -f1 <<<$(cut -f9 <<<$i) | sed 's/ID=//g' | sed 's/gene://g')
    grep -A 1 "^>${chrom}" ORYZA.fasta | tail -n1 > seq.temp
    echo -e "> ${gene}:${chrom}:${start}-${end}" >> ORYZA.genes.fasta
    cut -c${start}-${end} seq.temp >> ORYZA.genes.fasta
    rm seq.temp
done < ORYZA.gff3
### generate the blast database
time \
makeblastdb -in ORYZA.genes.fasta \
            -title "ORYZA.genes.blastdb" \
            -dbtype nucl
### blasting (50% percent identity) NOTE THAT: we are blasting the  gff3 peaks on TAIR10 genes instead of the full ncbi nucleotide database so we immediately get gene names recognized by GO enrichment tools
echo '#!/bin/bash
    gff3_peaks_file=${1}
    DB=${2}
    REFORMATED_GENOME=${3}
    # ### TEST:
    # # gff3_peaks_file=CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-GFF3_PEAKS.csv
    # # DB=/data/BlastDB/NCBI_NT0060
    # gff3_peaks_file=CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-SEQ_PEAKS.csv
    # DB=TAIR10.genes.fasta
    # REFORMATED_GENOME=REFORMATED_GENOME.fasta
    ### BUILD THE QUERY SEQUENCES
    rm ${gff3_peaks_file}.query.fasta || touch ${gff3_peaks_file}.query.fasta
    for locus in $(cat ${gff3_peaks_file} | tail -n+2)
    do
      # locus=$(cat ${gff3_peaks_file} | tail -n+2 | head -n1)
      chrom=$(cut -d, -f1 <<<$locus)
      start=$(cut -d, -f4 <<<$locus)
      end=$(cut -d, -f5 <<<$locus)
      grep -A 1 ${chrom} ${REFORMATED_GENOME} | tail -n1 > ${gff3_peaks_file}.seq.temp
      if [ $end -gt $(tail -n1 ${gff3_peaks_file}.seq.temp | wc -c) ]
      then
        end=$(tail -n1 ${gff3_peaks_file}.seq.temp | wc -c)
      fi
      echo -e "> ${chrom}:${pos}:${start}-${end}" >> ${gff3_peaks_file}.query.fasta
      cut -c${start}-${end} ${gff3_peaks_file}.seq.temp >> ${gff3_peaks_file}.query.fasta
      rm ${gff3_peaks_file}.seq.temp
    done
    ### BLAST OFF!
    DIR=$(dirname $gff3_peaks_file)
    FNAME=$(basename $gff3_peaks_file)
    blastn -db ${DB} \
      -query ${gff3_peaks_file}.query.fasta \
      -perc_identity 90 \
      -outfmt "6 qseqid staxids pident evalue qcovhsp bitscore stitle" \
      -out ${DIR}/BLASTOUT-ORYZA-${FNAME%.csv*}.txt
' > blast_oryza_gff3_peaks.sh
chmod +x blast_oryza_gff3_peaks.sh
time \
parallel ./blast_oryza_gff3_peaks.sh {} ORYZA.genes.fasta REFORMATED_GENOME.fasta ::: $(ls *-GFF3_PEAKS.csv)
time \
parallel ./blast_oryza_gff3_peaks.sh {} ORYZA.genes.fasta REFORMATED_GENOME.fasta ::: $(ls *-SEQ_PEAKS.csv)

for herbi in CLETH GLYPH SULFO TERBU
do
    echo $herbi-GFF3_PEAKS
    cat BLASTOUT-ORYZA-${herbi}*GFF3_PEAKS.txt | cut -f 7 | cut -d":" -f1 | sort | uniq > ${herbi}-GFF3_PEAKS.oryza.blastout
    echo $herbi-SEQ_PEAKS
    cat BLASTOUT-ORYZA-${herbi}*SEQ_PEAKS.txt | cut -f 7 | cut -d":" -f1 | sort | uniq > ${herbi}-SEQ_PEAKS.oryza.blastout
done
rm *.temp
### GO term enrichment analysis with ShinyGO (http://bioinformatics.sdstate.edu/go/)

### detect common blast output per herbicide
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/GWAS_ANALYSIS/
echo 'args = commandArgs(trailingOnly=TRUE)
      # args = c("CLETH", "5")
      herbi = args[1]
      query_pcov = as.numeric(args[2]) ### minimum percent query coverage threshold
      # fnames_list = system(paste0("ls BLASTOUT-* | grep ", herbi), intern=TRUE)
      fnames_list = system(paste0("ls BLASTOUT-* | grep -v FILTERED | grep -v COMMON | grep -v GLMNET | grep ", herbi), intern=TRUE)
      for (f in fnames_list){
          # f = fnames_list[1]
          # f = fnames_list[6]
          names_list = unlist(strsplit(unlist(strsplit(f, "-"))[3], "_"))
          if (sum(grepl("MERGED", names_list))>0){
              name_dataset = paste(names_list[1:2], collapse="_")
              name_model = unlist(strsplit(f, "-"))[4]
          } else {
              name_dataset = paste(names_list[1:3], collapse="_")
              name_model = unlist(strsplit(f, "-"))[4]
          }
          dat = read.delim(f, header=FALSE)
          colnames(dat) = c("seqid", "staxids", "pident", "evalue", "qcovhsp", "bitscore", "stitle")
          dat = droplevels(dat[dat$qcovhsp >=query_pcov, ])
          seqid = levels(dat$seqid)
          staxids = c()
          pident = c()
          evalue = c()
          qcovhsp = c()
          bitscore = c()
          stitle = c()
          for (seq in seqid){
              # seq = seqid[1]
              sub = droplevels(dat[dat$seqid==seq, ])
              sub = sub[order(sub$bitscore, decreasing=TRUE), ]
              selected = droplevels(sub[1, ])
              staxids = c(staxids, selected$staxids)
              pident = c(pident, selected$pident)
              evalue = c(evalue, selected$evalue)
              qcovhsp = c(qcovhsp, selected$qcovhsp)
              bitscore = c(bitscore, selected$bitscore)
              stitle = c(stitle, as.character(selected$stitle))
          }
          herbicide = rep(herbi, times=length(seqid))
          dataset = rep(name_dataset, times=length(seqid))
          model = rep(name_model, times=length(seqid))
          OUT = data.frame(herbicide, dataset, model, seqid, staxids, pident, evalue, qcovhsp, bitscore, stitle)
          fname_out = paste0("BLASTOUT-FILTERED-", name_dataset, "-", name_model, ".txt")
          write.table(OUT, file=fname_out, sep="\t", row.names=FALSE, quote=FALSE)
          #### extract common locus/loci
          if (exists("SEQID")==FALSE){
              SEQID = data.frame(seqid)
          } else {
              SEQID = merge(SEQID, data.frame(seqid), by="seqid")
          }
          if (exists("RBOUND")==FALSE){
              RBOUND = OUT
          } else {
              RBOUND = rbind(RBOUND, OUT)
          }
      }
      COMMON_OUT = RBOUND[RBOUND$seqid %in% SEQID$seqid, ]
      write.table(COMMON_OUT, file=paste0("BLASTOUT-COMMON-", herbi, ".txt"), sep="\t", row.names=FALSE, quote=FALSE)
' > blast_filter_merge.r
time \
parallel Rscript blast_filter_merge.r {} 1 ::: CLETH GLYPH SULFO TERBU
# ### Merging including all bast hits per herbicide resistance trait (WITHIN POPULATION GWAS ONLY!!!!)
# for herbi in CLETH GLYPH SULFO TERBU
# do
#     # herbi=CLETH
#     echo $herbi
#     head -n1 $(ls BLASTOUT-FILTERED-${herbi}*-GWAlpha.txt | head -n1) > BLASTOUT_${herbi}.txt
#     touch BLASTOUT-${herbi}.temp
#     for f in $(ls BLASTOUT-FILTERED-${herbi}*-GWAlpha.txt)
#     do
#         tail -n+2 $f >> BLASTOUT_${herbi}.temp
#     done
#     sort BLASTOUT_${herbi}.temp | uniq >> BLASTOUT_${herbi}.txt
#     rm BLASTOUT_${herbi}.temp
# done
### MERGE all blast hits per herbicide resistance trait but only for within population Pool-GWAWS experiments
for herbi in CLETH GLYPH SULFO TERBU
do
    echo -e "herbicide\tdataset\tmodel\tseqid\tstaxids\tpident\tevalue\tqcovhsp\tbitscore\tstitle" > BLASTOUT_${herbi}.txt
    cat BLASTOUT-ORYZA-${herbi}_WITHIN_* | sort | uniq >> BLASTOUT_${herbi}.txt
done

### Extracting scaffold names containing the blast hits for popgen ###(+ 100 neutral scaffolds too --> not actually needed because we're simulating the neutral expectations with ms)
for herbi in CLETH GLYPH SULFO TERBU
do
    # herbi=CLETH
    echo $herbi
    tail -n+2 BLASTOUT_${herbi}.txt | cut -d: -f1 | sort | uniq > BLASTOUT-${herbi}-SCAFFNAMES.txt
    # grep -A1 -f BLASTOUT-${herbi}-SCAFFNAMES.txt REFORMATED_GENOME.fasta | grep -v "--" - > BLASTOUT-${herbi}-SEQUENCES.fasta
done
# ### randomly sampling neutral scaffolds
# cut -f1 ../MERGED_ALL_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync | sort | uniq > merged_scaffnames.txt
# grep '^>scaffold' BLASTOUT-*-SEQUENCES.fasta | sed 's/>//g' - | cut -d: -f2 | sort | uniq > scaffold_names_non_neutral.txt
# grep -v -f scaffold_names_non_neutral.txt merged_scaffnames.txt | shuf | head -n50 > BLASTOUT-NEUTRAL-SCAFFNAMES.txt
# grep -A1 -f BLASTOUT-NEUTRAL-SCAFFNAMES.txt REFORMATED_GENOME.fasta | grep -v "--" - > BLASTOUT-NEUTRAL-SEQUENCES.fasta
# ### extracting mpileup per BLASTOUT-*-SCAFFNAMES
# for herbi in CLETH GLYPH SULFO TERBU NEUTRAL
# do
#     # herbi=CLETH
#     echo $herbi
#     grep -f BLASTOUT-${herbi}-SCAFFNAMES.txt ../../00_MPILEUP_AA_MAPQ20/CLETH_ACROSS_SEAUS.mpileup > BLASTOUT-${herbi}-MPILEUP.mpileup
# done

### plotting BLAST output into the manhattan plots
echo '
### plotting BLAST output into the manhattan plots

### set the working directory
setwd("/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE_GENETICS-combine_SNP_and_transcript_data/ALL_PoolGPAS_MERGED/GWAS/GWAS_ANALYSIS")
# setwd("/data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/")
### load GWAS ouput files
print("Merging the GWAS outputs:")
fnames_list = system("find ../GWAS_PER_HERBI_PER_POP/*-GWAlpha-OUTPUT.csv | grep -v TRIFL", intern=TRUE)
# fnames_list = system("find GWAS_PER_HERBI_PER_POP/*-GWAlpha-OUTPUT.csv | grep -v TRIFL", intern=TRUE)
herbi_list = c()
popnames_list = c()
for (f in fnames_list){
    herbi = unlist(strsplit(basename(f), "_"))[1]
    popname = unlist(strsplit(basename(f), "_"))[3]
    herbi_list = c(herbi_list, herbi)
    popnames_list = c(popnames_list, popname)
    print(paste0(herbi, "-", popname))
    dat = read.csv(f)
    colnames(dat)[4:ncol(dat)] = paste0(herbi, "_", popname, "_", colnames(dat)[4:ncol(dat)])
    if (!exists("GWAS")){
        GWAS = dat
    } else {
        GWAS = merge(GWAS, dat, by=c("CHROM", "POS", "ALLELE"), all=TRUE)
    }
}
GWAS = GWAS[order(GWAS$ALLELE), ]
GWAS = GWAS[order(GWAS$POS), ]
GWAS = GWAS[order(GWAS$CHROM), ]
GWAS$LOCUS = paste(GWAS$CHROM, GWAS$POS, sep="-")
GWAS$IDX = 1:nrow(GWAS)
write.table(GWAS, file="MERGED_ALL_GWAS_OUTPUT.csv", sep=",", row.names=FALSE, quote=FALSE)

##################
### JUST PEAKS ###
##################
### plot manhattan plots per population  highlight the peaks and 
### zoom in per scaffold for the top 5% scaffolds with max(-log10(p-value)) above the Bonferroni threshold
print("Plotting:")
colors_points = c("#41b6c4", "#a1dab4")
colors_lines = c("red", rgb(0.2, 0.2, 0.2, alpha=0.10), "#feb24c")
for (i in 1:length(herbi_list)){
    # i = 1
    herbi = herbi_list[i]
    popname = popnames_list[i]
    print(paste0(herbi, "-", popname))
    LOD=eval(parse(text=paste0("GWAS$", herbi, "_", popname, "_LOD")))
    gwas = data.frame(CHROM=GWAS$CHROM, POS=GWAS$POS, IDX=GWAS$IDX, LOD=LOD)
    nloci = length(unique(paste(GWAS$LOCUS[!is.na(LOD)])))
    p = sum(!is.na(gwas$LOD)) ### total number of alleles across loci (total number of predictors)
    alpha = 0.05
    bon_thresh = -log10(alpha/p)
    agg = aggregate(LOD ~ CHROM, data=gwas, FUN=max)
    agg = agg[order(agg$LOD, decreasing=TRUE), ]
    idx = agg$LOD >= bon_thresh
    scaff_above_thresh_0.05 = agg$CHROM[idx][1:round(sum(idx)*0.05)]
    agg = agg[order(agg$CHROM), ]
    agg$IDX = 1:nrow(agg)
     ### load blasted GWAS peaks
    BLASTOUT = read.delim(paste0("BLASTOUT_", herbi, ".txt"), header=TRUE)
    BLASTOUT$CHROM = matrix(unlist(strsplit(as.character(BLASTOUT$seqid), "::")), ncol=2, byrow=TRUE)[,1]
    BLASTOUT$POS_INIT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(BLASTOUT$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 1])
    BLASTOUT$POS_SLUT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(BLASTOUT$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 2])
    idx_scaff_with_peaks = gwas$CHROM %in% gwas$CHROM[(gwas$LOD >= bon_thresh) & !is.na(gwas$LOD)]
    GWAS_SCAFF_PEAKS = droplevels(gwas[idx_scaff_with_peaks, ])
    GWAS_SCAFF_PEAKS = aggregate(LOD ~ CHROM +  POS, data=GWAS_SCAFF_PEAKS, FUN=max)
    min_pos = aggregate(POS ~ CHROM, dat=data.frame(CHROM=c(as.character(GWAS_SCAFF_PEAKS$CHROM), rep(as.character(BLASTOUT$CHROM), times=2)), POS=c(GWAS_SCAFF_PEAKS$POS, BLASTOUT$POS_INIT, BLASTOUT$POS_SLUT) ), FUN=min)
        colnames(min_pos)[2] = "MIN_POS"
    max_pos = aggregate(POS ~ CHROM, dat=data.frame(CHROM=c(as.character(GWAS_SCAFF_PEAKS$CHROM), rep(as.character(BLASTOUT$CHROM), times=2)), POS=c(GWAS_SCAFF_PEAKS$POS, BLASTOUT$POS_INIT, BLASTOUT$POS_SLUT) ), FUN=max)
        colnames(max_pos)[2] = "MAX_POS"
    pos_df = merge(min_pos, max_pos, by="CHROM")
    pos_df = pos_df[order(pos_df$CHROM), ]
    # cumsum_delta = cumsum(pos_df$MAX_POS - pos_df$MIN_POS)
    cumsum_delta = cumsum(pos_df$MAX_POS)
    pos_df$ADD_POS = c(0, cumsum_delta[1:(length(cumsum_delta)-1)])
    GWAS_SCAFF_PEAKS = merge(GWAS_SCAFF_PEAKS, pos_df, by="CHROM")
    min_LOD = min(GWAS_SCAFF_PEAKS$LOD,na.rm=TRUE)
    max_LOD = max(GWAS_SCAFF_PEAKS$LOD,na.rm=TRUE)
    n_loci_above_thresh = sum((GWAS_SCAFF_PEAKS$LOD>=bon_thresh) & !is.na(GWAS_SCAFF_PEAKS$LOD))
    n_scaff_with_blast_hits =  sum(BLASTOUT$CHROM %in% GWAS_SCAFF_PEAKS$CHROM)


    jpeg(paste0(herbi, "_", popname, "_MANHATTAN_PLOTS.jpeg"), quality=100, width=1000, height=900)
    par(mfrow=c(3,1))
    ### plot LOD of each allele across loci across scaffolds
    plot(x=gwas$IDX, y=gwas$LOD, type="n", xlab="Locus", ylab="-log10(p-value)", main=paste0("Number of loci = ", nloci, "\nNumber of alleles x loci = ", p), sub=paste0("Bonferroni threshold (alpha=", alpha, ") = ", round(bon_thresh, 2)))
    grid(col="gray")
    for (j in 1:nlevels(gwas$CHROM)){
        # j = 1
        sub = droplevels(gwas[gwas$CHROM==levels(gwas$CHROM)[j], ])
        points(x=sub$IDX, y=sub$LOD, col=colors_points[(j%%2)+1], pch=20)
    }
    abline(h=bon_thresh, col=colors_lines[1], lty=2, lwd=2)
    ### plot max(LOD) per scaffold
    plot(x=agg$IDX, y=agg$LOD, type="n", xlab="Scaffold", ylab="max(-log10(p-value))", main=paste0("Number of scaffolds = ", nrow(agg)))
    grid(col="gray")
    abline(v=agg$IDX[agg$CHROM %in% scaff_above_thresh_0.05], lty=1, lwd=6, col=colors_lines[2])
    points(x=agg$IDX, y=agg$LOD, pch=20, col=colors_points)
    abline(h=bon_thresh, col=colors_lines[1], lty=2, lwd=2)
    ### plot only scaffolds with peaks and overlay the blast hits
    plot(x=c(0,max(cumsum_delta)), y=c(min_LOD, max_LOD), type="n", main=paste0("Number of loci above the Bonferroni threshold = ", n_loci_above_thresh, "\nNumber of scaffolds with loci peaks within 1kb of a blast hit = ", n_scaff_with_blast_hits), xlab="Loci", ylab="-log10(p-value)")
    grid(col="gray")
    for (l in 1:length(unique(GWAS_SCAFF_PEAKS$CHROM))){
        # l = 1
        scaff = unique(GWAS_SCAFF_PEAKS$CHROM)[l]
        sub = droplevels(GWAS_SCAFF_PEAKS[GWAS_SCAFF_PEAKS$CHROM==scaff, ])
        blast = droplevels(BLASTOUT[BLASTOUT$CHROM==scaff, ])
        if (nrow(blast)>0){
            for (m in 1:nrow(blast)){
                xleft = (blast$POS_INIT[m] - sub$MIN_POS[m]) + sub$ADD_POS[m]
                xright = (blast$POS_SLUT[m] - sub$MIN_POS[m]) + sub$ADD_POS[m]
                rect(xleft=xleft, xright=xright, ybottom=min_LOD, ytop=max_LOD, density=NA, col=colors_lines[3])
            }
        }
        x = (sub$POS - sub$MIN_POS) + sub$ADD_POS
        y = sub$LOD
        points(x, y, col=colors_points[(l%%2)+1], pch=20)
    }
    abline(h=bon_thresh, col=colors_lines[1], lty=2, lwd=2)
    dev.off()
    ### plot top 5% peaks
    layout_width = ceiling(sqrt(length(scaff_above_thresh_0.05)))
    layout_height = ceiling(length(scaff_above_thresh_0.05)/layout_width)
    svg(paste0(herbi, "_", popname, "_MANHATTAN_PEAKS.svg"), width=layout_width*4, height=layout_height*2.5)
    par(mfrow=c(layout_height, layout_width))
    for (k in 1:length(scaff_above_thresh_0.05)){
        # k = 1
        scaff = scaff_above_thresh_0.05[k]
        sub = droplevels(gwas[gwas$CHROM==scaff, ])
        plot(x=c(min(sub$POS), max(sub$POS)), y=c(min(gwas$LOD, na.rm=TRUE), max(gwas$LOD, na.rm=TRUE)), type="n", main=scaff, xlab="Position (bp)", ylab="-log10(p-value)")
        grid(col="gray")
        points(x=sub$POS, y=sub$LOD, col=colors_points[(k%%2)+1], pch=20)
        abline(h=bon_thresh, col=colors_lines[1], lty=2, lwd=2)
    }
    dev.off()
}

###########################################
### merge peaks per herbicide treatment ###
###########################################
for (i in 1:length(unique(herbi_list))){
    # i = 1
    herbi = unique(herbi_list)[i]
    popnames = popnames_list[herbi_list==herbi]
    print(paste(c(herbi, popnames), collapse="-"))
    for (j in 1:length(popnames)){
        # j = 1
        popname = popnames[j]
        print(paste0(herbi, "-", popname))
        if (exists("LOD")==FALSE){
            LOD=eval(parse(text=paste0("GWAS$", herbi, "_", popname, "_LOD")))
        } else {
            LOD = cbind(LOD, eval(parse(text=paste0("GWAS$", herbi, "_", popname, "_LOD"))))
        }
    }
    gwas = cbind(data.frame(GWAS$CHROM, GWAS$POS), LOD)
    colnames(gwas) = c("CHROM", "POS", paste0("LOD_", popnames))
    idx_2d = LOD >= matrix(rep(-log10(0.01/colSums(!is.na(LOD))), times=nrow(gwas)), nrow=nrow(gwas), byrow=TRUE)
    idx_2d[is.na(idx_2d)] = FALSE
    idx = (rowSums(idx_2d) == ncol(LOD)) & !is.na(apply(LOD, MARGIN=1, FUN=sum))
    gwas = gwas[idx, ]
    write.table(gwas, file=paste0(herbi, "_MERGED_ALL_PEAKS.csv"), sep=",", row.names=FALSE, col.names=TRUE, quote=FALSE)
}

########################
### pairwise merging ###
######################## and highlight blast hits per scaffold
for (i in 1:length(unique(herbi_list))){
    # i = 1
    herbi = unique(herbi_list)[i]
    popnames = popnames_list[herbi_list==herbi]
    blastout = read.delim(paste0("BLASTOUT_", herbi, ".txt"), header=TRUE)
    blastout$CHROM = matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,1]
    blastout$POS_INIT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 1])
    blastout$POS_SLUT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 2])
    for (j in 1:(length(popnames)-1)){
        # j = 1
        popname1 = popnames[j]
        for (k in (j+1):length(popnames)){
            # k = j+2
            popname2 = popnames[k]
            print(paste(c(herbi, popname1, popname2), collapse="-"))
            LOD = cbind(eval(parse(text=paste0("GWAS$", herbi, "_", popname1, "_LOD"))),
                        eval(parse(text=paste0("GWAS$", herbi, "_", popname2, "_LOD"))))
            gwas = cbind(data.frame(GWAS$CHROM, GWAS$POS), LOD)
            colnames(gwas) = c("CHROM", "POS", paste0("LOD_", c(popname1, popname2)))
            idx_2d = LOD >= matrix(rep(-log10(0.01/colSums(!is.na(LOD))), times=nrow(gwas)), nrow=nrow(gwas), byrow=TRUE)
            idx_2d[is.na(idx_2d)] = FALSE
            idx = (rowSums(idx_2d) == ncol(LOD)) & !is.na(apply(LOD, MARGIN=1, FUN=sum))
            gwas = gwas[idx, ]
            write.table(gwas, file=paste0(herbi, "_MERGED_PAIR_PEAKS-", popname1, "_", popname2, ".csv"), sep=",", row.names=FALSE, col.names=TRUE, quote=FALSE)

            ### plot manhattan plots per scaffold and highlight blast hits
            if (nrow(gwas)>0){
                scaff_list = unique(gwas$CHROM)
                df = data.frame(CHROM=GWAS$CHROM, POS=GWAS$POS, LOD_1=LOD[,1], LOD_2=LOD[,2])
                layout_width = ceiling(sqrt(length(scaff_list)))
                layout_height = ceiling(length(scaff_list)/layout_width)
                svg(paste0(herbi, "_MANHATTAN_PAIR_MERGED_", popname1, "-", popname2, ".svg"), width=layout_width*4, height=layout_height*2.5)
                par(mfrow=c(layout_height, layout_width))
                for (l in 1:length(scaff_list)){
                    # l = 1
                    scaff = scaff_list[l]
                    sub = df[df$CHROM==scaff, ]
                    blastout_sub = blastout[blastout$CHROM==scaff, ]
                    plot(x=c(0,max(c(sub$POS, blastout_sub$POS_SLUT))), y=c(min(LOD,na.rm=TRUE), max(LOD,na.rm=TRUE)), type="n", main=scaff, xlab="Position (bp)", ylab="-log10(p-value)")
                    grid(col="gray")
                    points(x=sub$POS, y=sub$LOD_1, col=colors_points[1], pch=20)
                    points(x=sub$POS, y=sub$LOD_2, col=colors_points[2], pch=20)
                    if(nrow(blastout_sub)>0){
                        for (m in 1:nrow(blastout_sub)){
                            rect(xleft=blastout_sub$POS_INIT[m], xright=blastout_sub$POS_SLUT[m], ybottom=min(LOD,na.rm=TRUE), ytop=max(LOD,na.rm=TRUE), density=NA, col=colors_lines[3])
                        }
                    }
                }
                dev.off()
            }
        }
    }
}

### plot scaffolds with blast hits (blast hist from peaks)
for (i in 1:length(unique(herbi_list))){
    # i = 1
    herbi = unique(herbi_list)[i]
    popnames = popnames_list[herbi_list==herbi]
    blastout = read.delim(paste0("BLASTOUT_", herbi, ".txt"), header=TRUE)
    blastout$CHROM = matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,1]
    blastout$POS_INIT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 1])
    blastout$POS_SLUT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(blastout$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 2])
    print(paste(c(herbi, popnames), collapse="-"))
    rm(LOD)
    for (j in 1:length(popnames)){
        # j = 1
        popname = popnames[j]
        print(paste0(herbi, "-", popname))
        if (exists("LOD")==FALSE){
            LOD=eval(parse(text=paste0("GWAS$", herbi, "_", popname, "_LOD")))
        } else {
            LOD = cbind(LOD, eval(parse(text=paste0("GWAS$", herbi, "_", popname, "_LOD"))))
        }
    }
    gwas = cbind(data.frame(GWAS$CHROM, GWAS$POS), LOD)
    colnames(gwas) = c("CHROM", "POS", paste0("LOD_", popnames))
    scaff_list = unique(blastout$CHROM)
    layout_width = ceiling(sqrt(length(scaff_list)))
    layout_height = ceiling(length(scaff_list)/layout_width)
    svg(paste0(herbi, "_", popname, "_MANHATTAN_BLAST_HITS.svg"), width=layout_width*4, height=layout_height*2.5)
    par(mfrow=c(layout_height, layout_width))
    for (l in 1:length(scaff_list)){
        # l = 1
        scaff = scaff_list[l]
        sub = gwas[df$CHROM==scaff, ]
        blastout_sub = blastout[blastout$CHROM==scaff, ]
        plot(x=c(0,max(c(sub$POS, blastout_sub$POS_SLUT))), y=c(min(LOD,na.rm=TRUE), max(LOD,na.rm=TRUE)), type="n", main=scaff, xlab="Position (bp)", ylab="-log10(p-value)")
        grid(col="gray")
        points(x=rep(sub$POS, each=(ncol(gwas)-2)), y=as.vector(t(sub[,3:ncol(sub)])), col=colors_points, pch=20)
        if(nrow(blastout_sub)>0){
            for (m in 1:nrow(blastout_sub)){
                rect(xleft=blastout_sub$POS_INIT[m], xright=blastout_sub$POS_SLUT[m], ybottom=min(LOD,na.rm=TRUE), ytop=max(LOD,na.rm=TRUE), density=NA, col=colors_lines[3])
            }
        }
    }
    dev.off()
}

### GO term enrichment analysis
# if (system("ls ncbi_gene_names.txt | wc -l", intern=TRUE) == 0) {
#     system("wget https://ftp.ncbi.nih.gov/gene/DATA/gene2accession.gz")
#     system("gunzip gene2accession.gz")
#     system("cut -f16 gene2accession > ncbi_gene_names.temp")
#     system("split -b500000000 ncbi_gene_names.temp")
#     system("touch ncbi_gene_names.temp2")
#     system("for f in $(ls x*); do echo $f; sort $f | uniq >> ncbi_gene_names.temp2; done")
#     system("sort ncbi_gene_names.temp2 | uniq > ncbi_gene_names.txt")
#     system("rm ncbi_gene_names.temp ncbi_gene_names.temp2 x*")
#     herbi=CLETH
#     touch GENE_NAME_COLUMN.temp
#     for i in $(seq 2 $(wc -l BLASTOUT-${herbi}.txt))
#     do
#         i=2
#         head -n${i} BLASTOUT-${herbi}.txt | tail -n1 | cut -f10 > blastout_description.temp
#         grep -F -f ncbi_gene_names.txt blastout_description.temp
#     done
# }
# ### manually fill-up the paste0("BLASTOUT_", herbi, ".txt") a column with arabidopsis gene names
# for (i in 1:length(unique(herbi_list))){
#     # i = 1
#     herbi = unique(herbi_list)[i]
#     BLASTOUT = read.delim(paste0("BLASTOUT_", herbi, ".txt"), header=TRUE)
#     BLASTOUT$CHROM = matrix(unlist(strsplit(as.character(BLASTOUT$seqid), "::")), ncol=2, byrow=TRUE)[,1]
#     BLASTOUT$POS_INIT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(BLASTOUT$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 1])
#     BLASTOUT$POS_SLUT = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(BLASTOUT$seqid), "::")), ncol=2, byrow=TRUE)[,2], "-")), ncol=2, byrow=TRUE)[, 2])
# }
# ### NOTABLE OBSERVATIONS: Some of the manhattan peaks do not have corresponding blast output.
# ###                       This is probably due to the highly fragmented genome assembly, i.e.
# ###                       The peaks are actually linked to some annotated genes but the scaffolds containing
# ###                       the peak and the gene are separate.
' > plot_bastout_manahttan.r















# ### BUT FIRST COLLAPSE MUTIALLELIOC ALLELE FREQUENCY DATA INTO BIALLELIC 2020-06-04
# echo '
#     ### load modules
#     using Distributed
#     using SharedArrays
#     Distributed.addprocs(length(Sys.cpu_info())-1)
#     @everywhere using DelimitedFiles
#     @everywhere using DataFrames
#     @everywhere using CSV
#     @everywhere using Statistics

#     ### Input herbicide resistance trait preffix-o
#     # ARGS = ["CLETH"]
#     HERBI = ARGS[1]

#     @everywhere function collapse_func(HERBI)
#         ### load the allelee frequency data
#         geno_fname = string("MERGED_", HERBI, "_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv")
#         GENO = DelimitedFiles.readdlm(geno_fname, @@@,@@@)
#         ### extract loci names
#         loci_names = unique(string.(GENO[:,1], "-", GENO[:,2]))
#         ### extract data dimensions
#         n = size(GENO,2) - 3
#         p = length(loci_names)
#         ### calculate mean allele frquencies
#         mean_allele_freq = Statistics.mean(GENO[:,4:end], dims=2)
#         ### intialize output allele frequency with the first column being the allele ID as integers because SharedArrays can only be numeric
#         OUT = SharedArrays.SharedArray{Float64,2}(p, n+1)
#         ### map the allele ID from letters to integers
#         allele_to_int = Dict("A" => 1, "T" => 2, "C" => 3, "G" => 4, "N" => 5, "DEL" => 6)
#         function allele_to_int_func(x)
#             allele_to_int[x]
#         end
#         ### parallel processing by selecting the major allele
#         @time @sync @distributed for i in 1:p
#             # i = 1
#             locus = loci_names[i]
#             scaff = split(locus, "-")[1]
#             pos   = parse(Int64, split(locus, "-")[2])
#             idx_row = (GENO[:,1] .== scaff) .& (GENO[:,2] .== pos)
#             geno =  GENO[idx_row, :]
#             geno[:,3] = map(allele_to_int_func, geno[:,3])
#             q = mean_allele_freq[idx_row,1]
#             idx_allele = sortperm(q)[end]
#             OUT[i, :] = geno[idx_allele, 3:end]
#         end
#         return(OUT)
#     end
#     ### execute
#     out = collapse_func(HERBI)
#     ### reopen the allele frequncy data to extract loci info
#     geno_fname = string("MERGED_", HERBI, "_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv")
#     GENO = DelimitedFiles.readdlm(geno_fname, @@@,@@@)
# ' | sed "s/@@@/'/g" > collapse_to_biallelic_freqs.jl

# time for herbi in CLETH GLYPH SULFO TERBU
# do
#     echo $herbi
#     julia collapse_to_biallelic_freqs.jl $herbi
#     mv MERGED_${herbi}_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv ALLELEFREQ_MULTIALLELIC/
#     mv MERGED_${herbi}_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ_BIALLELIC.csv MERGED_${herbi}_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv
# done


### GPAS odd-one-out cross-validation because of we have a limited number of samples
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
time \
parallel julia /data/Lolium/Softwares/genomic_prediction/misc/PoolGPAS._nonautomated_LOOCV.jl {} ::: CLETH GLYPH SULFO TERBU TRIFL
### plot predicted values against observed values for each herbicide resistance trait
echo '
    args = commandArgs(trailingOnly=TRUE)
    # args = c("CLETH")
    herbi = args[1]
    if (herbi=="CLETH"){
        herbi_name = "Clethodim"
    } else if (herbi=="GLYPH"){
        herbi_name = "Glyphosate"
    } else if (herbi=="SULFO"){
        herbi_name = "Sulfometuron"
    } else if (herbi=="TERBU"){
        herbi_name = "Terbuthylazine"
    }
    fname = paste0(herbi, "_LOOCV_RAW.csv")
    dat = read.csv(fname)
    levels(dat$MODEL) = c("RIDGE", "LASSO", "MIXED_FST")
    ### restrict predicted values between 0 and 1
    dat$Y_PRED[dat$Y_PRED < 0] = 0
    dat$Y_PRED[dat$Y_PRED > 1] = 1
    ### colors
    colors_points = c("#7bccc4", "#fecc5c", "#bae4b3")
    colors_lines = c("#0868ac", "#fd8d3c", "#31a354")
    ### plot
    svg(paste0(herbi, "_LOOCV.svg"), width=7, height=7)
    plot(x=dat$Y_TRUE, y=dat$Y_PRED, type="n", xlab=paste0("Observed ", herbi_name, " Resistance"), ylab=paste0("Predicted ", herbi_name, " Resistance"))
    grid(col="gray")
    R2=c(); CORR=c(); MAD=c(); RMSE=c()
    for (i in 1:nlevels(dat$MODEL)){
        # i = 1
        mod = levels(dat$MODEL)[i]
        sub = dat[dat$MODEL==mod, ]
        points(x=sub$Y_TRUE, y=sub$Y_PRED, pch=20, col=colors_points[i])
        reg_mod = lm(sub$Y_PRED ~ sub$Y_TRUE)
        abline(reg_mod, col=colors_lines[i], lwd=2)
        R2 = c(R2, summary(reg_mod)$adj.r.sq)
        CORR = c(CORR, cor(sub$Y_PRED, sub$Y_TRUE))
        MAD = c(MAD, mean(abs(sub$Y_PRED-sub$Y_TRUE)))
        RMSE = c(RMSE, mean(sqrt((sub$Y_PRED-sub$Y_TRUE)^2)))
    }
    legend("topleft", legend=paste0(levels(dat$MODEL), ": R2=", round(R2*100, 2), "%; RMSE=", round(RMSE,4)), fill=colors_points, bord=colors_lines)
    dev.off()
' > anal_loocv.r
time \
parallel Rscript anal_loocv.r {} ::: CLETH GLYPH SULFO TERBU

### analysis with bootstrapping to generate variance estimates
head -n1 <$(ls *_LOOCV_RAW.csv | head -n1) > ALL_LOOCV.csv
for f in $(ls *_LOOCV_RAW.csv)
do
    tail -n+2 $f >> ALL_LOOCV.csv
done
echo '### load violinplotter library
library(violinplotter)
### load data
dat = read.csv("ALL_LOOCV.csv")
levels(dat$MODEL) = c("RIDGE", "LASSO", "MIXED_FST")
hist(dat$Y_PRED)
### restrict predicted values between 0 and 1
dat$Y_PRED[dat$Y_PRED < 0] = 0
dat$Y_PRED[dat$Y_PRED > 1] = 1
### prelim scatterplot
plot(dat$Y_TRUE, dat$Y_PRED, xlab="Observed Resistance", ylab="Predicted Resistance", main="All Data", pch=20, col="#43a2ca")
### bootstrapping bvy random sampling across Y_TRUE
y_true_list = unique(dat$Y_TRUE)
n_samples = 10
nIter = 10000
MODEL=c(); CORR=c(); RMSE=c(); MAD=c()
for (i in 1:nIter){
    rand_y_true = sample(y_true_list, size=n_samples)
    sub = droplevels(dat[dat$Y_TRUE %in% rand_y_true, ])
    for (model in c("RIDGE", "LASSO", "MIXED_FST")){
        sub_model = droplevels(sub[sub$MODEL==model, ])
        MODEL = c(MODEL, model)
        CORR = c(CORR, cor(sub_model$Y_TRUE, sub_model$Y_PRED))
        RMSE = c(RMSE, sqrt(mean((sub_model$Y_TRUE - sub_model$Y_PRED)^2)))
        MAD = c(MAD, mean(abs(sub_model$Y_TRUE - sub_model$Y_PRED)))
    }
}
BOOT = data.frame(MODEL, CORR, RMSE, MAD)
write.table(BOOT, "ALL_LOOCV_BOOTOUT.csv", sep=",", row.names=FALSE, quote=FALSE)
svg("ALL_LOOCV_PLOTS.svg", width=10, height=10)
par(mfrow=c(2,2))
plot(dat$Y_TRUE, dat$Y_PRED, xlab="Observed Resistance", ylab="Predicted Resistance", main="", pch=20, type="n")
colors = c("#66c2a5", "#fc8d62", "#8da0cb")
for (i in 1:nlevels(dat$MODEL)){
    model = levels(dat$MODEL)[i]
    points(dat$Y_TRUE[dat$MODEL==model], dat$Y_PRED[dat$MODEL==model], pch=20, col=paste0(colors[i], "80"))
    abline(lm(dat$Y_PRED[dat$MODEL==model] ~ dat$Y_TRUE[dat$MODEL==model]), lty=1, lwd=2, col=colors[i])
}
legend("topleft", legend=levels(dat$MODEL), fill=colors)
violinplotter(RMSE ~ MODEL, data=BOOT, VIOLIN_COLOURS=colors)
violinplotter(MAD ~ MODEL, data=BOOT, VIOLIN_COLOURS=colors)
violinplotter(CORR ~ MODEL, data=BOOT, VIOLIN_COLOURS=colors)
dev.off()
' > bootanal.r
Rscript bootanal.r
mkdir PREDICTION_LOOCV/
mv *_LOOCV.* PREDICTION_LOOCV/
mv *_LOOCV_RAW.csv* PREDICTION_LOOCV/
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### GPAS 10-fold cross-validation (round(n/k)={cleth=7, glyph=7, sulfo=7, terbu=4}))
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
time \
parallel julia /data/Lolium/Softwares/genomic_prediction/misc/PoolGPAS._nonautomated_10fold_CV.jl {} 100 ::: CLETH GLYPH SULFO TERBU
echo 'library(violinplotter)
      fname_list = system("ls *10-FOLDCV.csv | grep -v HSD", intern=TRUE)
      metrics = c("RMSE", "CORR", "MAD", "R2")
      colors = c("#c2a5cf", "#fdae61", "#abd9e9", "#f4a582")
      for (m in metrics){
          svg(paste0(m, "_VIOLINPLOTS_10-FOLDCV.svg"), width=10, height=10)
          par(mfrow=c(2,2))
          for (i in 1:length(fname_list)){
              f = fname_list[i]
              herbi = unlist(strsplit(f, "_"))[2]
              dat = read.csv(f)
              idx = (dat$VAR_TRAIN > 0.00) & (dat$RMSE <= 1)
              dat = droplevels(dat[idx, ])
              levels(dat$MODEL) = c("RIDGE", "LASSO", "MIXED_FST")
' > anal_10_FOLDCV.r
echo -e "     hsd = eval(parse(text=paste0('violinplotter(', m, '~MODEL, data=dat, TITLE=\"', herbi, ' Resistance\", YLAB=\"', m, '\", VIOLIN_COLOURS=colors[i])')))" >> anal_10_FOLDCV.r
echo '        models = hsd[[1]]$HSD_out.LEVELS
              groupings = hsd[[1]]$HSD_out.GROUPING
              means = hsd[[1]]$HSD_out.MEANS
              OUT = data.frame(MODEL=models, HSD_GROUPING=groupings, MEAN=means)
              write.csv(OUT, file=paste0(m, "_", herbi, "_HSD_10-FOLDCV.csv"))
          }
          dev.off()
      }
' >> anal_10_FOLDCV.r
Rscript anal_10_FOLDCV.r
mkdir PREDICTION_10-FOLDCV/
mv *_10-FOLDCV.* PREDICTION_10-FOLDCV/


# #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ### using GWAlpha-SNPwise-derived allelic effects to compute polygenic scores 
# ### and converted into predicited resistance levels
# cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
# mkdir PREDICTION_POLYGENIC_SCORES/

# ### parse sync files of training populations into allele freq csv files
# cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/01_SYNC_MAPQ20_BASQ20/
# echo 'using GWAlpha
#       # ARGS = ["CLETH_WITHIN_ACC13_MAF0.001_DEPTH50.sync"]
#       f = ARGS[1]
#       GWAlpha.sync_processing_module.sync_parse(f)
# ' > parse_for_polygenic_scores_gp.jl
# time \
# parallel julia parse_for_polygenic_scores_gp.jl {} ::: $(ls *DEPTH50.sync)
# mv *ALLELEFREQ.csv /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/GWAS_PER_HERBI_PER_POP
# ### train polygenic risk scores-to-trait_values linear model
# cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/GWAS_PER_HERBI_PER_POP
# R
# args = commandArgs(trailingOnly=TRUE)
# # args = c("CLETH_WITHIN_ACC13_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv")
# # args = c("CLETH_WITHIN_ACC49_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv")
# # args = c("SULFO_WITHIN_INVER_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv")
# # args = c("GLYPH_WITHIN_INVER_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv")
# # args = c("GLYPH_WITHIN_ACC31_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv")
# # args = c("GLYPH_WITHIN_ACC62_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv")
# # args = c("TERBU_WITHIN_INVER_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv")
# fname_TRAIN_GWAS = args[1] # fname_TRAIN_GWAS = "GLYPH_WITHIN_ACC31_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv"
# fname_TRAIN_GENO = sub("-GWAlpha-OUTPUT.csv", "_ALLELEFREQ.csv", fname_TRAIN_GWAS)
# sampling = unlist(strsplit(fname_TRAIN_GWAS, "_"))[2]
# herbi = unlist(strsplit(fname_TRAIN_GWAS, "_"))[1]
# population = unlist(strsplit(fname_TRAIN_GWAS, "_"))[3]
# fname_VALID_GENO = paste0("../MERGED_", herbi, "_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv") # fname_VALID_GENO = "GLYPH_WITHIN_ACC62_MAF0.001_DEPTH50_ALLELEFREQ.csv"
# fname_VALID_PHEN = paste0("../MERGED_", herbi, "_pheno.csv")

# ### load data
# TRAIN_GWAS = read.csv(fname_TRAIN_GWAS)
# TRAIN_GENO = read.csv(fname_TRAIN_GENO, header=FALSE)
# all_phen = read.csv("../../02_PHENOTYPES/ALL_PHENO.csv")
# TRAIN_PHEN = all_phen[(all_phen$SAMPLING==sampling) & (grepl(herbi, all_phen$HERBICIDE)) & grepl(population, all_phen$POPULATION), ]
# TRAIN_PHEN = TRAIN_PHEN[order(TRAIN_PHEN$POOL, decreasing=FALSE), ]
# TRAIN_y = TRAIN_PHEN$SURVIVAL_RATE
# TRAIN_n = length(TRAIN_y)
# VALID_GENO = read.csv(fname_VALID_GENO, header=FALSE)
# VALID_PHEN = read.csv(fname_VALID_PHEN, header=FALSE)
# VALID_y = VALID_PHEN[,2]
# VALID_n = length(VALID_y)

# ### merge TRAIN_GWAS, TRAIN_GENO, and VALID_GENO
# colnames(TRAIN_GENO)[1:3] = c("CHROM", "POS", "ALLELE")
# colnames(VALID_GENO)[1:3] = c("CHROM", "POS", "ALLELE")
# MERGED = merge(merge(TRAIN_GWAS, TRAIN_GENO, by=c("CHROM", "POS", "ALLELE")), VALID_GENO, by=c("CHROM", "POS", "ALLELE"))
# b = MERGED$ALPHA
# TRAIN_X = t(as.matrix(MERGED[,8:(8+TRAIN_n-1)]))
# VALID_X = t(as.matrix(MERGED[,(8+TRAIN_n):ncol(MERGED)]))

# ### training TRAIN_y ~ TRAIN_polygenic_score
# TRAIN_polygenic_score = TRAIN_X %*% b
# mod = lm(TRAIN_y ~ TRAIN_polygenic_score)
# # plot(x=TRAIN_polygenic_score, y=TRAIN_y); abline(mod, col="red", lwd=2)
# a0 = coef(mod)[1]
# a1 = coef(mod)[2]
# ### prediction
# VALID_polygenic_score = VALID_X %*% b
# VALID_y_pred = a0 + (a1 * VALID_polygenic_score)
# plot(x=VALID_y, y=VALID_y_pred)
# ### NOTE: UTTERLY WORSE PERFORMANCE NYEEEE!!!!!





###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

### Next we'll try exploring filtering alternatives because we may be excluding the QTL
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/01_SYNC_MAPQ20_BASQ20
### mpileup to sync
### install popoolation2
wget https://sourceforge.net/projects/popoolation2/files/popoolation2_1201.zip
unzip popoolation2_1201.zip -d popoolation2_1201
mv popoolation2_1201/popoolation2_1201/* popoolation2_1201
rm -R popoolation2_1201.zip popoolation2_1201/popoolation2_1201/
### synchronize mpileup
BASQ=20
time \
java -ea -Xmx40g -jar popoolation2_1201/mpileup2sync.jar \
    --input /data/Lolium/Population_Genetics/POPGEN-MODELS/ACC.mpileup \
    --output ACC.sync \
    --fastq-type sanger \
    --min-qual ${BASQ} \
    --threads 12
### sync to MAF-DEPTH-filtered sync
### NOTE: LOWERING THE DEPTH TO 10X instead of 50X (2020-06-16)
echo "
using GWAlpha
# GWAlpha.sync_processing_module.sync_filter(filename_sync=ARGS[1], MAF=0.001, DEPTH=50)
GWAlpha.sync_processing_module.sync_filter(filename_sync=ARGS[1], MAF=0.001, DEPTH=10)
" > sync2filteredsync.jl
### split the large sync file into similarly sized pieces (e.g. 45,000,000 lines each and not splitting into a specific number of chunks because the lines get truncated)
### then filter in parallel
split -d -l 45000000 ACC.sync ACC_split_
for f in $(ls ACC_split_*)
do
    mv $f ${f}.sync
done
time parallel julia sync2filteredsync.jl {} ::: $(ls ACC_split_*.sync)
### concatenate the peices back into 1
cat ACC_split_*_MAF0.001_DEPTH10.sync > ACC_MAF0.001_DEPTH10.sync
rm ACC_split_*
### split filtered sync and phenotypes per herbicide
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/01_SYNC_MAPQ20_BASQ20
echo '
    ### load sync and phenotype files
    all_geno = read.delim("ACC_MAF0.001_DEPTH10.sync", sep="\t", header=FALSE)
    all_pheno = read.csv("../02_PHENOTYPES/ALL_PHENO.csv")
    ### label the sync file to correspont to the populations in the phenotype file
    all_population_names = sort(unique(as.character(all_pheno$POPULATION)))
    colnames(all_geno) = c("CHROM", "POS", "ALLELE", all_population_names[grepl("ACC", all_population_names)])
    ### include only across population data for the 60SEAu populations
    sub_pheno = droplevels(all_pheno[(all_pheno$SAMPLING == "ACROSS") & grepl("ACC", as.character(all_pheno$POPULATION)), ])
    ###  split data per herbicide treatment
    for (herbi in levels(sub_pheno$HERBICIDE)){
        # herbi = levels(sub_pheno$HERBICIDE)[1]
        print(herbi)
        sub_herbi_pheno = droplevels(sub_pheno[sub_pheno$HERBICIDE==herbi, ])
        ### key step here is to sort by population names so that the phenotype and genotype data correspont to each other
        sub_herbi_pheno = sub_herbi_pheno[order(as.character(sub_herbi_pheno$POPULATION))]
        sub_herbi_geno = all_geno[, (colnames(all_geno) %in% c("CHROM", "POS", "ALLELE", as.character(sub_herbi_pheno$POPULATION)))]
        ### write-out
        write.table(sub_herbi_geno, file=paste0("MERGED_", herbi, "_MAPQ20_BASQ20_MAF0.001_DEPTH10.sync"), sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)
        write.table(sub_herbi_pheno, file=paste0("MERGED_", herbi, ".pheno"), sep=",", quote=FALSE, row.names=FALSE, col.names=FALSE)
        write.table(cbind(sub_herbi_pheno$POOL_SIZE, sub_herbi_pheno$SURVIVAL_RATE), file=paste0("MERGED_", herbi, "_pheno.csv"), sep=",", quote=FALSE, row.names=FALSE, col.names=FALSE)
    }
' > split_by_herbi.r
Rscript split_by_herbi.r
cp MERGED_* /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA

### remove populations with pool size less than 10 #20
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
echo 'args = commandArgs(trailingOnly=TRUE)
      # args = c("CLETH", "20")
      herbi = args[1]
      pool_size_threshold = as.numeric(args[2])
      fname_phen = paste0("MERGED_", herbi, ".pheno")
      fname_phen_input = paste0("MERGED_", herbi, "_pheno.csv")
      fname_sync = paste0("MERGED_", herbi, "_MAPQ20_BASQ20_MAF0.001_DEPTH10.sync")
      ### read main phenotype file
      PHEN = read.csv(fname_phen, header=FALSE)
      colnames(PHEN) = c("YEAR", "SAMPLING", "POP_GROUP", "POP", "NPOOLS", "POOLSIZE", "HERBICIDE", "QUANTRES", "SURVIVAL")
      idx = PHEN$POOLSIZE >= pool_size_threshold
      ### load sync and phenotype input data
      Y = read.csv(fname_phen_input, header=FALSE)
      SYNC = read.delim(fname_sync, header=FALSE)
      ## filter
      PHEN = PHEN[idx, ]
      Y = Y[idx, ]
      SYNC = SYNC[, c(rep(TRUE,3), idx)]
      ### overwrite unfiltered files
      write.table(PHEN, file=fname_phen, row.names=FALSE, col.names=FALSE, sep=",", quote=FALSE)
      write.table(Y, file=fname_phen_input, row.names=FALSE, col.names=FALSE, sep=",", quote=FALSE)
      write.table(SYNC, file=fname_sync, row.names=FALSE, col.names=FALSE, sep="\t", quote=FALSE)
' > filter_data_by_pool_sizes.r
time \
parallel Rscript filter_data_by_pool_sizes.r {} 10 ::: CLETHODIM GLYPHOSATE SULFOMETURON TERBUTHYLAZINE
# parallel Rscript filter_data_by_pool_sizes.r {} 20 ::: CLETHODIM GLYPHOSATE SULFOMETURON TERBUTHYLAZINE

### Estimate pairwise Fst
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
echo "
    using DelimitedFiles
    using Distributed
    Distributed.addprocs(length(Sys.cpu_info()))
    @everywhere using GWAlpha
    # ARGS = [\"CLETH\"]
    sync_fname = string(\"MERGED_\", ARGS[1], \"_MAPQ20_BASQ20_MAF0.001_DEPTH10.sync\")
    phen_fname = string(\"MERGED_\", ARGS[1], \"_pheno.csv\")
    pool_sizes = convert(Array{Int64}, DelimitedFiles.readdlm(phen_fname, ',')[:,1])
    GWAlpha.relatedness_module.Fst_pairwise(filename_sync=sync_fname, window_size=100000, pool_sizes=pool_sizes, METHOD=\"WeirCock\")
" > estimate_fst.jl
for herbi in CLETHODIM GLYPHOSATE SULFOMETURON TERBUTHYLAZINE
do
    echo $herbi
    julia estimate_fst.jl $herbi
done

### GPAS odd-one-out cross-validation because of we have a limited number of samples
### but first modify the script to recognize 10X depth instead of 50X depth
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
sed 's/DEPTH50/DEPTH10/g' /data/Lolium/Softwares/genomic_prediction/misc/PoolGPAS._nonautomated_LOOCV.jl | \
    sed 's/depth=50/depth=10/g' >  loocv_depth10x.jl
time \
parallel julia loocv_depth10x.jl {} \
    ::: CLETHODIM GLYPHOSATE SULFOMETURON TERBUTHYLAZINE
### plot predicted values against observed values for each herbicide resistance trait
echo '
    args = commandArgs(trailingOnly=TRUE)
    # args = c("CLETH")
    herbi = args[1]
    if (herbi=="CLETH"){
        herbi_name = "Clethodim"
    } else if (herbi=="GLYPH"){
        herbi_name = "Glyphosate"
    } else if (herbi=="SULFO"){
        herbi_name = "Sulfometuron"
    } else if (herbi=="TERBU"){
        herbi_name = "Terbuthylazine"
    }
    fname = paste0(herbi, "_LOOCV_RAW.csv")
    dat = read.csv(fname)
    levels(dat$MODEL) = c("RIDGE", "LASSO", "MIXED_FST")
    ### restrict predicted values between 0 and 1
    dat$Y_PRED[dat$Y_PRED < 0] = 0
    dat$Y_PRED[dat$Y_PRED > 1] = 1
    ### colors
    colors_points = c("#7bccc4", "#fecc5c", "#bae4b3")
    colors_lines = c("#0868ac", "#fd8d3c", "#31a354")
    ### plot
    svg(paste0(herbi, "_LOOCV.svg"), width=7, height=7)
    plot(x=dat$Y_TRUE, y=dat$Y_PRED, type="n", xlab=paste0("Observed ", herbi_name, " Resistance"), ylab=paste0("Predicted ", herbi_name, " Resistance"))
    grid(col="gray")
    R2=c(); CORR=c(); MAD=c(); RMSE=c()
    for (i in 1:nlevels(dat$MODEL)){
        # i = 1
        mod = levels(dat$MODEL)[i]
        sub = dat[dat$MODEL==mod, ]
        points(x=sub$Y_TRUE, y=sub$Y_PRED, pch=20, col=colors_points[i])
        reg_mod = lm(sub$Y_PRED ~ sub$Y_TRUE)
        abline(reg_mod, col=colors_lines[i], lwd=2)
        R2 = c(R2, summary(reg_mod)$adj.r.sq)
        CORR = c(CORR, cor(sub$Y_PRED, sub$Y_TRUE))
        MAD = c(MAD, mean(abs(sub$Y_PRED-sub$Y_TRUE)))
        RMSE = c(RMSE, mean(sqrt((sub$Y_PRED-sub$Y_TRUE)^2)))
    }
    legend("topleft", legend=paste0(levels(dat$MODEL), ": R2=", round(R2*100, 2), "%; RMSE=", round(RMSE,4)), fill=colors_points, bord=colors_lines)
    dev.off()
' > anal_loocv.r
time \
parallel Rscript anal_loocv.r {} ::: CLETH GLYPH SULFO TERBU

###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@








#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### proper validation
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/01_SYNC_MAPQ20_BASQ20
### MERGE EVERYTHING (except the low coverage libraries: (1) TRIFL_*, (2) GLYPH_WITHIN_URANA* and (3) TERBU_WITHIN_INVER*)
echo '
    fname_list = system(paste("ls | grep DEPTH50 | grep -v TRIFL"), intern=TRUE)
    ### merge genotype data
    for (f in fname_list){
        if (!exists("GENO")){
            GENO = read.delim(f, header=FALSE)
        } else {
            GENO = merge(GENO, read.delim(f, header=FALSE), by=c("V1", "V2", "V3"))
        }
    }
    GENO = droplevels(GENO)
    ### merge phenotype data
    all_pheno = read.csv("/data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/02_PHENOTYPES/ALL_PHENO.csv")
    names_list = strsplit(basename(fname_list), "_")
    for (i in 1:length(names_list)){
        herbicide = names_list[[i]][1]
        sampling = names_list[[i]][2]
        pop_id = names_list[[i]][3]
        if (!exists("PHENO")){
            PHENO = all_pheno[(substr(all_pheno$HERBICIDE, 1, 5)==herbicide) & (all_pheno$SAMPLING==sampling) & (all_pheno$POP_ID==pop_id), ]
        } else {
            PHENO = rbind(PHENO, all_pheno[(substr(all_pheno$HERBICIDE, 1, 5)==herbicide) & (all_pheno$SAMPLING==sampling) & (all_pheno$POP_ID==pop_id), ])
        }
    }
    PHENO = droplevels(PHENO)
    ### save
    write.table(GENO, file=paste0("MERGED_ALL_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync"), sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)
    write.table(PHENO, file=paste0("MERGED_ALL.phe"), sep=",", quote=FALSE, row.names=FALSE, col.names=TRUE)
' > merge_all.r
Rscript merge_all.r
mv MERGED_ALL* /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
### prediction
cd /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA
mkdir PREDICTIONS_PROPER_7k_LOCI/
time \
parallel julia /data/Lolium/Softwares/genomic_prediction/misc/PoolGPAS._nonautomated_proper_prediction.jl {} ::: CLETHODIM GLYPHOSATE SULFOMETURON TERBUTHYLAZINE
### clean-up
mv *PREDICTIONS.csv PREDICTIONS_PROPER_7k_LOCI
rm *TRAIN* *VALID*
