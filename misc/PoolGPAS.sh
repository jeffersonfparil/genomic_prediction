#!/bin/bash
#######################################################################################
###                                                                                 ###
### Genomic prediction modeling and association analyses using Pool-sequencing data ###
###                                   (PoolGPAS)                                    ###
###                                                                                 ###
#######################################################################################

###################
### USER INPUTS ###
###################
DIR=${1}              ### working directory where the fastq files are located
RGRP=${2}             ### text file indicating the identiying labels for read1 and read2 (e.g. line1:_R1 and line2:_R2) ### NOTE: It's nice to ad whatever delimiter/s flanking these identifiers
PGRP=${3}             ### text file indicating the identiying labels for pool groups (e.g. line1:IF, line2:IG, ...)
MAPQ=${4}             ### minimum mapping quality threshold
BASQ=${5}             ### minimum base quality threshold
REFLINK=${6}          ### download link to the reference genome in fasta format
POPOOLATION_DIR=${7}  ### directory of the popoolation2 software
SRC_DIR=${8}          ### location of genomic_prediction/src
MAF=${9}              ### minimum allele frequency threshold
DEPTH=${10}           ### minimum depth threshold
POOLSIZES=${11}       ### pool sizes per pool grouping: e.g. IT\t100,100,100,100,100\nUG\t100,100,100,100,100\n
GENOME=${12}          ### reference genome #should be ${DIR}/REF/Reference by default as genererated by SETUP
DB=${13}              ### blast database/s
SETUP=${14}
ALIGN=${15}
PILEUP=${16}
SYNC=${17}
FILTER=${18}
POOLGPAS=${19}
PEAKS=${20}
POOLGPCV=${21}

#@@@@@@@@@@@@@@@@@@@@@@@
### TEST:
DIR=/data/Lolium/Quantitative_Genetics/PoolGPAS_08SEAu
RGRP=${DIR}/read_grouping.txt
PGRP=${DIR}/pool_grouping.txt
MAPQ=20
BASQ=20
REFLINK=http://185.45.23.197:5080/ryegrassdata/GENOMIC_ASSEMBLY/lope_V1.0.fasta
POPOOLATION_DIR=/data/Lolium/Softwares/popoolation2_1201
SRC_DIR=/data/Lolium/Softwares/genomic_prediction/src
MAF=0.001
DEPTH=50
POOLSIZES=${DIR}/pool_sizes_per_grouping.txt
GENOME=${DIR}/REF/lope_V1.0.fasta
DB=/data/BlastDB/NCBI_NT0060
SETUP=FALSE
ALIGN=FALSE
PILEUP=FALSE
SYNC=FALSE
FILTER=TRUE
POOLGPAS=TRUE
PEAKS=TRUE
POOLGPCV=TRUE
#@@@@@@@@@@@@@@@@@@@@@@@@@@

cd ${DIR} ### navigate to the working directory
#####################################
### (0) SETUP WORKING DIRECTORIES ###
#####################################
if [ ${SETUP} == TRUE ]
then
  echo -e "################################################"
  echo -e "(0) SETUP WORKING DIRECTORIES"
  echo -e "################################################"
  # (0a) test if there are paired-end fastq files (qzipped or ungzipped) in the working directory
  if [ $(ls ${DIR}/*.fastq* | wc -l) -lt 2 ]
  then
    echo -e "The paired-end reads (*.fastq or *.fastq.gz) files not found in the directory:"
    echo -e "${DIR}"
    echo -e "Exiting now!"
    exit
  fi
  # (0b) set working directories
  mkdir ${DIR}/FASTQ/; mv ${DIR}/*.fastq* ${DIR}/FASTQ/ ### moving fastq files into FASTQ/ directory
  mkdir ${DIR}/REF/   ### dowload reference genome here
  mkdir ${DIR}/SAM/   ### for storing BAM alignments
  mkdir ${DIR}/VCF/   ### for storing mpileups and sync files
  mkdir ${DIR}/GPAS/   ### for storing mpileups and sync files
  # (0c) dowload the fasta reference genome and fix the format
  if [ $(ls ${DIR}/REF/$(basename $REFLINK) | wc -l) == 0  ]
  then
    cd ${DIR}/REF/
    wget ${REFLINK}
    echo -e "
    import os, sys
    from Bio import SeqIO
    INPUT_fasta = sys.argv[1]
    OUTPUT_fasta = sys.argv[2]
    SeqIO.convert(INPUT_fasta, 'fasta', OUTPUT_fasta, 'fasta')
    " > fix_REF.py
    python fix_REF.py ${DIR}/REF/$(basename $REFLINK) ${DIR}/REF/Reference.fasta || \
    echo -e "Please check your reference fasta.\nExiting now!" | exit
    bwa index -p Reference -a bwtsw Reference.fasta # index the reference genome for bwa
    samtools faidx ${DIR}/REF/Reference.fasta # index the reference genome for samtools
    # mv REF/Reference.fasta.fai REF/Reference_genome.fai
    cd -
  fi
fi

#################
### (1) ALIGN ###
#################
if [ ${ALIGN} == TRUE ]
then
  echo -e "################################################"
  echo -e "(1) ALIGN"
  echo -e "################################################"
  READ1=$(head -n1 $RGRP)
  READ2=$(tail -n1 $RGRP)
  echo -e "
  bwa mem ${DIR}/REF/Reference ${DIR}/FASTQ/\${1} ${DIR}/FASTQ/\${2} | \\
  samtools view -q ${MAPQ} -b | \\
  samtools sort > ${DIR}/SAM/\${3%.fastq*}.bam
  " > align.sh
  chmod +x align.sh
  time \
  parallel --link ./align.sh {1} {2} {3} ::: \
  $(ls ${DIR}/FASTQ/ | grep "${READ1}") ::: \
  $(ls ${DIR}/FASTQ/ | grep "${READ2}") ::: \
  $(ls ${DIR}/FASTQ/ | grep "${READ1}" | sed s/"$READ1"/""/g)
  rm align.sh
fi

##################
### (2) PILEUP ###
##################
if [ ${PILEUP} == TRUE ]
then
  echo -e "################################################"
  echo -e "(2) PILEUP"
  echo -e "################################################"
  # (2a) generate bam list
  for i in $(cat $PGRP)
  do
    echo $i
    ls ${DIR}/SAM/${i}*.bam > ${DIR}/bam_list_${i}.txt
  done
  # (2b) pileup alignments with minimum mapping and base qualities set to MAPQ ans BASQ,respectively
  echo -e "
  samtools mpileup \\
  --max-depth 10000 \\
  --min-MQ ${MAPQ} \\
  --min-BQ ${BASQ} \\
  --fasta-ref ${DIR}/REF/Reference.fasta \\
  --bam-list ${DIR}/bam_list_\${1}.txt  > ${DIR}/VCF/\${1}.mpileup
  " > mpileup.sh
  chmod +x mpileup.sh
  time \
  parallel ./mpileup.sh {} ::: $(cat $PGRP)
  rm mpileup.sh
fi

LD=FALSE
if [ ${LD} == TRUE ]
then
  echo -e "###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###"
  echo -e "### LD ESTIMATTION: TEST TEST TEST TEST 20191203 ###"
  echo -e "###@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@###"
  # (2c) estimate linkage disequilibrium ### WRONGITY WRONG ASSUMPTION FOR POOL-SEQ DATA! HahAhahAAAhAAA!
  # # parallel bcftools call -mv ${DIR}/VCF/{}.mpileup -o ${DIR}/VCF/{}.vcf ::: $(cat $PGRP) ## multi-alleleic variant calling
  # # vcftools --vcf ${DIR}/VCF/{}.vcf --hap-r2 --ld-window-bp 10000 --out ${DIR}/VCF/{}_LD ::: $(cat $PGRP)
  # bcftools mpileup --max-depth 10000 --min-MQ ${MAPQ} --min-BQ ${BASQ} -f ${DIR}/REF/Reference.fasta file.bam --bam-list ${DIR}/bam_list_{}.txt | bcftools call -vm > ${DIR}/VCF/{}.vcf
  echo -e "
  bcftools mpileup \\
  --max-depth 10000 \\
  --min-MQ ${MAPQ} \\
  --min-BQ ${BASQ} \\
  --fasta-ref ${DIR}/REF/Reference.fasta \\
  --bam-list ${DIR}/bam_list_\${1}.txt | \\
  bcftools call -vm > ${DIR}/VCF/\${1}.vcf
  " > variant_calling.sh
  # echo -e "vcftools --vcf ${DIR}/VCF/\${1}.vcf \" > LD_estim.sh
  # echo -e "--geno-r2 \" >> LD_estim.sh
  # echo -e "--ld-window-bp 10000 \" >> LD_estim.sh
  # echo -e "--max-alleles 256 \" >> LD_estim.sh
  # echo -e "--min-alleles 2 \" >> LD_estim.sh
  # echo -e "--out ${DIR}/VCF/\${1}_LD" >> LD_estim.sh
  chmod +x variant_calling.sh
  # chmod +x LD_estim.sh
  time \
  parallel ./variant_calling.sh {} ::: $(cat $PGRP)
  # for i in $(cat $PGRP)
  # do
  #   echo $i
  #   ./LD_estim.sh $i
  # done
  rm variant_calling.sh
  # rm LD_estim.sh
  ### USING [LDx](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0048588)
  touch LD_MERGED.txt
  for f in $(cat $PGRP)
  do
    # f=UG
    bcftools sort ${DIR}/VCF/${f}.vcf > ${DIR}/VCF/${f}_sorted.vcf
    for i in $(seq 1 5)
    do
      # i=1
      echo ${f}-${i}
      ### only works when both -l=1 and -i=1
      samtools view -h -o ${DIR}/SAM/${f}${i}.sam ${DIR}/SAM/${f}${i}_combined.bam
      ${SRC_DIR}/LDx.pl \
        -l 1 \
        -h 10000 \
        -s 300 \
        -q ${BASQ} \
        -a ${MAF} \
        -i 1 \
        ${DIR}/SAM/${f}${i}.sam \
        ${DIR}/VCF/${f}_sorted.vcf >> LD_MERGED.txt
    done
    rm -Rf /tmp/bcftools*
  done
  ### LD decay plotting across
  echo -e '
  library(mgcv)
  dat = read.table("LD_MERGED.txt", header=FALSE)
  colnames(dat) = gsub("\\\(", "", gsub(")", "", gsub(" ", "_",
                  c("Location of SNP1",
                    "Location of SNP2",
                    "Number of pairs observed with x_11",
                    "Number of pairs observed with x_12",
                    "Number of pairs observed with x_21",
                    "Number of pairs observed with x_22",
                    "Estimate for allele frequency of allele A",
                    "Estimate for allele frequency of allele B",
                    "Read depth for SNP1",
                    "Read depth for SNP2",
                    "Intersecting read depth",
                    "Approx MLE R2 (low end of interval)",
                    "Approx MLE estimate",
                    "Approx MLE (high end of interval)",
                    "Direction Computation R2",
                    "A",
                    "a *",
                    "B",
                    "b"))))
  dat$Distance = abs(dat$Location_of_SNP1 - dat$Location_of_SNP2)
  TRAITS = c("Approx_MLE_R2_low_end_of_interval", "Approx_MLE_high_end_of_interval", "Approx_MLE_estimate")
  COLORS_POINTS = RColorBrewer::brewer.pal(3, "Pastel1")
  COLORS_LINES = RColorBrewer::brewer.pal(3, "Set1")
  jpeg("Linkage_disequilibrium_decay_poolseq_per_read.jpg", width=1500, height=600)
  par(mfrow=c(1,3), cex=1.5)
  for (i in 1:length(TRAITS)){
    # i = 1
    trait = TRAITS[i]
    x=dat$Distance
    y=eval(parse(text=paste0("dat$", trait)))
    ### model selection maximim R2
    mod1 = gam(y ~ s(x))
    mod2 = gam(y ~ te(x))
    mod3 = lm(y ~ poly(x,1))
    mod4 = lm(y ~ poly(x,2))
    mod5 = lm(y ~ poly(x,3))
    r.sq = c()
    for (j in 1:5){
      r.sq = c(r.sq, summary(eval(parse(text=paste0("mod", j))))$r.sq)
    }
    mod_id = c(1:length(r.sq))[r.sq == max(r.sq)][1]
    mod = eval(parse(text=paste0("mod", mod_id)))
    new_x = seq(from=min(x), to=max(x), by=0.01)
    y_pred = predict(mod, newdata=data.frame(x=new_x))
    plot(x=x, y=y, xlab="Distance (bp)", ylab="R2", main=trait, pch=19, col=COLORS_POINTS[i])
    lines(x=new_x, y=y_pred, lwd=4, col=COLORS_LINES[i])
  }
  dev.off()
  ' > LD_MERGED.r
  Rscript LD_MERGED.r
  rm LD_MERGED.r
  echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
fi

#######################
### (3) SYNCHRONIZE ###
#######################
if [ ${SYNC} == TRUE ]
then
  echo -e "################################################"
  echo -e "(3) SYNCHRONIZE MPILEUP"
  echo -e "################################################"
  # (3a) set up popoolation2
  if [ $(ls ${POPOOLATION_DIR} | wc -l) == 0 ]
  then
    cd ${POPOOLATION_DIR%popoolation2_1201*}
    wget https://sourceforge.net/projects/popoolation2/files/popoolation2_1201.zip
    unzip popoolation2_1201.zip -d popoolation2_1201
    rm popoolation2_1201.zip
    cd -
  fi
  # (3b) synchronize mpileup/s
  USABLE_RAM=$(echo "($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1000000) - 2" | bc)
  NCORES=$(grep -c ^processor /proc/cpuinfo)
  NPROCS=$(cat $PGRP | wc -l)
  if [ $NCORES -gt $NPROCS ]
  then
    MEMORY=$(echo $USABLE_RAM / $NPROCS | bc)
    NTHREADS=$(echo $NCORES / $NPROCS | bc)
  else
    MEMORY=$(echo $USABLE_RAM / $NCORES | bc)
    NTHREADS=1
  fi
  time \
  parallel \
  java -ea -Xmx${MEMORY}g -jar ${POPOOLATION_DIR}/mpileup2sync.jar \
    --input ${DIR}/VCF/{}.mpileup \
    --output ${DIR}/VCF/{}_MAPQ${MAPQ}_BASQ${BASQ}.sync \
    --fastq-type sanger \
    --min-qual ${BASQ} \
    --threads ${NTHREADS} ::: $(cat $PGRP)
fi

###################################################################
### (4) FILTER BY MAF AND DEPTH AND CALCULTE ALLELE FREQUENCIES ###
###################################################################
if [ ${FILTER} == TRUE ]
then
  echo -e "################################################"
  echo -e "(4) FILTER BY MAF AND EXTRACT ALLELE FREQUENCIES"
  echo -e "################################################"
  # filter sync by MAF and DEPTH and calculate the alee frequencies
  echo -e '# ARGS = ["/data/Lolium/Quantitative_Genetics/PoolGPAS_08SEAu/VCF/ACC11_MAPQ20_BASQ20_SPLIT01.sync", "0.001", "50"]
    ### Inputs
    FNAME_SYNC = ARGS[1]
    MAF = parse(Float64, ARGS[2])
    DEPTH = parse(Int64, ARGS[3])
    ### Outputs
    ### (1) filtered sync file with the suffix: `string("_MAF", MAF, "_DEPTH", DEPTH, ".sync")`
    ### (2) Allele frequency csv file with the filename: `string(join(split(string("_MAF", MAF, "_DEPTH", DEPTH, ".sync"), ".")[1:(end-1)], '.'), "_ALLELEFREQ.csv")`
    ### Load GWAlpha
    using GWAlpha
    ### Filter sync by MAF and DEPTH
    GWAlpha.sync_processing_module.sync_filter(filename_sync=FNAME_SYNC, MAF=MAF, DEPTH=DEPTH)
    FILTERED_SYNC = string(join(split(FNAME_SYNC, ".")[1:(end-1)], "."), "_MAF", MAF, "_DEPTH", DEPTH, ".sync")
    ### Parse the filtered sync into an allele frequency csv file
    GWAlpha.sync_processing_module.sync_parse(FILTERED_SYNC)
  ' > SYNC_FILTER_AND_PARSE.jl
  time \
  parallel julia SYNC_FILTER_AND_PARSE.jl \
                        ${DIR}/VCF/{1}_MAPQ${MAPQ}_BASQ${BASQ}.sync \
                        ${MAF} \
                        ${DEPTH} \
                        ::: $(cat $PGRP)
  rm SYNC_FILTER_AND_PARSE.jl
fi

##################################
### (5) POOL-GWAS AND POOL-GP  ###
##################################
if [ ${POOLGPAS} == TRUE ]
then
  echo -e "################################################"
  echo -e "(5) POOL-GPAS"
  echo -e "################################################"
  # (5a) Pool-GWAS and Pool-GP
  echo -e '# ARGS = ["VCF/ACC59_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync", "ACC59_pheno.py", "ACC59_pheno.csv"]
    filename_sync_filtered = ARGS[1] ### USE MAF-DEPTH filtered sync file
    filename_phen_py = ARGS[2]
    filename_phen_csv = ARGS[3]
    ## extract the name of the unfiltered sync file and the correspoding maf and depth threshold to use in GWAlpjha.PoolGPAS()
    ## this is in order to avoid re-filtering!
    ## NOTE: (1) use filename_sync for METHO="GWAlpha", while
    ##       (2) use filename_sync_filtered for METHOD!="GWAlpha"
    split_fname= convert(Array{String,1}, split(filename_sync_filtered, "_"))
    # filename_sync = string(join(split_fname[isnothing.(match.(r"MAF", split_fname)) .& isnothing.(match.(r"DEPTH", split_fname))], "_"), ".sync")
    MAF = parse(Float64, split(split_fname[.!isnothing.(match.(r"MAF", split_fname))][1], "MAF")[2])
    DEPTH = parse(Int64, split(split(split_fname[.!isnothing.(match.(r"DEPTH", split_fname))][1], "DEPTH")[2], ".")[1])
    ### load the GWAlpha.jl library for parallel execution
    using Distributed
    Distributed.addprocs(length(Sys.cpu_info()))
    @everywhere using GWAlpha
    ### NOTE: All models except the SNPwise uses the Fst (WeirCock) random covariate
    ### (1) GWAlpha SNPwise
    @time GWAlpha.PoolGPAS(filename_sync=filename_sync_filtered, filename_phen=filename_phen_py, maf=MAF, depth=DEPTH, model="GWAlpha", fpr=0.01, plot=true)
    ### (2) GWAlpha MIXED (REML variance components estimation with FST-WEIRCOCK as the random covariate)
    @time GWAlpha.PoolGPAS(filename_sync=filename_sync_filtered, filename_phen=filename_phen_csv, maf=MAF, depth=DEPTH, model="MIXED", varcomp_est="REML", fpr=0.01, plot=true)
    ### (3) GWAlpha RR (alpha = 0.00)
    @time GWAlpha.PoolGPAS(filename_sync=filename_sync_filtered, filename_phen=filename_phen_csv, maf=MAF, depth=DEPTH, model="GLMNET", glmnet_alpha=0.00, fpr=0.01, plot=true)
    ### (4) GWAlpha GLMNET at alpha = 0.50
    @time GWAlpha.PoolGPAS(filename_sync=filename_sync_filtered, filename_phen=filename_phen_csv, maf=MAF, depth=DEPTH, model="GLMNET", glmnet_alpha=0.50, fpr=0.01, plot=true)
    ### (5) GWAlpha LASOO (alpha = 1.00)
    @time GWAlpha.PoolGPAS(filename_sync=filename_sync_filtered, filename_phen=filename_phen_csv, maf=MAF, depth=DEPTH, model="GLMNET", glmnet_alpha=1.00, fpr=0.01, plot=true)
    ' > POOL_GWAS_GP.jl
  time \
  for i in $(cat $PGRP)
  do
    echo $i
    julia POOL_GWAS_GP.jl \
              ${DIR}/VCF/${i}_MAPQ${MAPQ}_BASQ${BASQ}_MAF${MAF}_DEPTH${DEPTH}.sync \
              ${DIR}/${i}_pheno.py \
              ${DIR}/${i}_pheno.csv
    mv ${DIR}/VCF/*OUTPUT* ${DIR}/GPAS/
    mv ${DIR}/VCF/*COVARIATE* ${DIR}/GPAS/
  done
  rm POOL_GWAS_GP.jl
fi

#########################
### (6) PEAK ANALYSIS ###
#########################
if [ ${PEAKS} == TRUE ]
then
  echo -e "################################################"
  echo -e "(6) PEAK ANALYSIS"
  echo -e "################################################"
  echo -e ' #ARGS = c("GPAS/ACC62_MAPQ20_BASQ20_MAF0.001_DEPTH50-REML_RR_FST-OUTPUT.csv", "100")
    ARGS = commandArgs(trailing=TRUE)
    fname_input = ARGS[1]       ### GWAlpha (PoolGPAS.sh) output csv file
    ntop = as.numeric(ARGS[2])  ### number of loci with the highest LOD
    fname_vec_dot_split = unlist(strsplit(fname_input, "[.]"))
    ### load PoolGPAS output
    dat = read.csv(fname_input)
    ### exit if there are no peaks detected
    if ( (var(dat$LOD[!is.na(dat$LOD)])==0) | is.na(var(dat$LOD[!is.na(dat$LOD)])==0) ){
      quit()
    }
    ### sort by chromosome (scaffold ID) and insert a locus counter column
    str(dat)
    dat = droplevels(dat[dat$CHROM != "Intercept", ])
    chrom_sorter_idx = as.numeric(matrix(unlist(strsplit(matrix(unlist(strsplit(as.character(dat$CHROM), "[|]")), nrow=2)[1,], "_")), nrow=2)[2,])
    dat = dat[order(chrom_sorter_idx), ]
    colnames(dat) = c("CHROM", "POS", "ALLELE", "FREQ", "ALPHA", "PVALUES", "LOD")
    #### Trying to detect peaks
    AGGREGATED_BY_LOCUS = aggregate(LOD ~ CHROM + POS, FUN=max, na.action=na.omit, data=dat)
    AGGREGATED_BY_LOCUS = AGGREGATED_BY_LOCUS[order(AGGREGATED_BY_LOCUS$LOD, decreasing=TRUE), ]
    TOP_PEAKS = head(AGGREGATED_BY_LOCUS, n=ntop)
    TOP_PEAKS = TOP_PEAKS[with(TOP_PEAKS, order(CHROM, POS)), ] # sort
    OUT = data.frame(CHROM=TOP_PEAKS$CHROM, POS=TOP_PEAKS$POS)
    fname_output = paste0(unlist(strsplit(fname_input, "_Alphas.csv"))[1], "-PEAKS_ID.csv")
    write.table(OUT, file=fname_output, row.names=FALSE, col.names=FALSE, sep=",", quote=FALSE)
  ' > extract_peaks.r
  time \
  parallel Rscript extract_peaks.r {1} 100 ::: $(ls ${DIR}/GPAS/*-OUTPUT.csv | grep -v "RANEF")

  ###########################################
  ### BLASTING PUTATIVE QTL FROM CV TESTS ###
  cd ${DIR}/GPAS/
  query_halflen=1500 ### in bp: 1500*2 = 3kb
  ###########################################################
  ### Reformat the genome assembly (no sequence wrapping) ###
  if [ $(ls ${DIR}/REF/REFORMATED_GENOME.fasta | wc -l) == 0 ]
  then
    grep "^>" ${GENOME} > SCAFFOLDS.temp
    csplit ${GENOME} /\>/ '{*}'
    ls xx* > SPLIT_FNAMES.temp
    rm $(head -n1 SPLIT_FNAMES.temp) ### remove the first file which contains nada!
    ls xx* > SPLIT_FNAMES.temp

    rm REFORMATED_GENOME.fasta
    touch REFORMATED_GENOME.fasta
    # for i in $(seq 1 $(cat SCAFFOLDS.temp | wc -l))
    time for i in $(seq 1 $(cat SPLIT_FNAMES.temp | wc -l))
    do
      echo $i
      seq=$(head -n${i} SPLIT_FNAMES.temp | tail -n1)
      head -n1 ${seq} > temp_name
      tail -n+2 ${seq} > temp_seq
      sed -zi 's/\n//g' temp_seq
      cat temp_name >> ${DIR}/REF/REFORMATED_GENOME.fasta
      cat temp_seq >> ${DIR}/REF/REFORMATED_GENOME.fasta
      echo -e "" >> ${DIR}/REF/REFORMATED_GENOME.fasta ### insert newline character that was removed by sed
    done
    rm *temp* xx*
  fi
  REFORMATED_GENOME=${DIR}/REF/REFORMATED_GENOME.fasta
  ################
  ### Blasting ###
  echo '#!/bin/bash
    peaks_file=${1}
    query_halflen=${2}
    DB=${3}
    REFORMATED_GENOME=${4}
    # ### TEST:
    # peaks_file=IS_pheno-FIXED_GWAlpha-PEAKS_ID.csv
    # query_halflen=1500
    # DB=/data/BlastDB/NCBI_NT0060
    # REFORMATED_GENOME=REFORMATED_GENOME.fasta
    ### BUILD THE QUERY SEQUENCES
    rm ${peaks_file}.query.fasta || \
    touch ${peaks_file}.query.fasta
    for locus in $(cat ${peaks_file})
    do
      # locus=$(head -n1 ${peaks_file})
      chrom=$(cut -d, -f1 <<<$locus)
      pos=$(cut -d, -f2 <<<$locus)
      grep -A 1 ${chrom} ${REFORMATED_GENOME} | tail -n1 > ${peaks_file}.seq.temp
      bp=$(cat ${peaks_file}.seq.temp | wc -c)
      start=$(echo "${pos} - ${query_halflen}" | bc)
      end=$(echo "${pos} + ${query_halflen}" | bc)
      if [ $start -lt 1 ]
      then
        start=1
      fi
      if [ $end -gt $bp ]
      then
        end=${bp}
      fi
      echo -e "> ${chrom}:${pos}:${start}-${end}" >> ${peaks_file}.query.fasta
      cut -c${start}-${end} ${peaks_file}.seq.temp >> ${peaks_file}.query.fasta ||
      cut -c1-${end} ${peaks_file}.seq.temp >> ${peaks_file}.query.fasta ||
      cut -c${start}-${bp} ${peaks_file}.seq.temp >> ${peaks_file}.query.fasta ||
      cut -c1-${bp} ${peaks_file}.seq.temp >> ${peaks_file}.query.fasta
      rm ${peaks_file}.seq.temp
    done
    ### BLAST OFF!
    DIR=$(dirname $peaks_file)
    FNAME=$(basename $peaks_file)
    blastn -db ${DB} \
      -query ${peaks_file}.query.fasta \
      -perc_identity 90 \
      -outfmt "6 qseqid staxids pident evalue qcovhsp bitscore stitle" \
      -out ${DIR}/BLASTOUT-${FNAME}.txt
  ' > blast_putative_QTL_parallel.sh
  chmod +x blast_putative_QTL_parallel.sh
  parallel ./blast_putative_QTL_parallel.sh \
  {1} \
  ${query_halflen} \
  ${DB} \
  ${REFORMATED_GENOME} ::: $(ls *-PEAKS_ID.csv)
  ### consolidate by first identifier across all models
  for ID in $(ls BLASTOUT* | cut -d"-" -f2-3)
  do
    # ID=$(ls BLASTOUT* | cut -d"-" -f2-3 | head -n1)
    echo $ID
    echo -e "ID\tMODEL\tPUTATIVE_QTL\tSUBJECT_ID\tPERCENT_IDENTITY\tE_VALUE\tQUERY_COVERAGE\tBITSCORE\tHIT_INFO" > ${ID}_BLAST_OUT_CONSOLIDATED.txt
    for i in $(ls BLASTOUT-${ID}*)
    do
      # i=$(ls BLASTOUT-${ID}* | head -n1)
      MODEL=$(cut -d"-" -f3 <<<$i)
      nLines=$(cat $i | wc -l)
      touch temp
      for j in $(seq 1 $nLines); do echo -e "${ID}\t${MODEL}" >> temp; done
      paste temp $i >> ${ID}_BLAST_OUT_CONSOLIDATED.txt
      rm temp
    done
  done
  head -n1 $(ls *_BLAST_OUT_CONSOLIDATED.txt | head -n1) > BLAST_OUT_CONSOLIDATED.txt
  for f in  $(ls *_BLAST_OUT_CONSOLIDATED.txt)
  do
    tail -n+2 $f >> BLAST_OUT_CONSOLIDATED.txt
  done
  ### convert tabs in the last column into spaces
  cp BLAST_OUT_CONSOLIDATED.txt BK_BLAST_OUT_CONSOLIDATED.txt.temp ### testing
  cut -f9- BLAST_OUT_CONSOLIDATED.txt > col9.temp
  sed -iz "s/\t/ /g" col9.temp
  cut -f1-8 BLAST_OUT_CONSOLIDATED.txt > col128.temp
  paste col128.temp col9.temp > colsall.temp
  mv colsall.temp BLAST_OUT_CONSOLIDATED.txt
  ### create word clouds
  echo '
    ##################################################
    ### GENERATE WORD CLOUD OF BLAST HITS KEYWORDS ###
    ##################################################
    args = commandArgs(trailing=TRUE)
    # args = c("BLAST_OUT_CONSOLIDATED.txt", "10")
    BLAST_OUT_CONSOLIDATED_FNAME = args[1]
    NTOP = as.numeric(args[2])
    ########################
    ### SAMPLE EXECUTION ###
    # Rscript generate_word_cloud_of_blastn_putative_QTL_hits.r \
    #   BLAST_OUT_CONSOLIDATED.txt \
    #   5
    ########################
    library(wordcloud)
    library(RColorBrewer)
    dat = read.delim(BLAST_OUT_CONSOLIDATED_FNAME, header=TRUE)
    dat$ID = as.character(dat$ID)
    dat$ID = sub("-", "_", dat$ID)
    remove_these_strings = c("var\\.", "[Gg]enome", "[Gg]enomic", "[Ss]caffold", "subsp\\.", "[Cc]omplete", "DNA", "[Cc]hromosome",
    "[Ss]train", "[Pp]lastid", "[Cc]omplex", "and", "[Gg]ene", "[Ss]equence", "[Ss]train", "[Cc]lone", "cds", "CDS", "[Pp]rotein",
    "[Aa]ssembly", "[Pp]artial", "[Cc]ultivar", "\\(", "\\)", "\\;", "\\:", "\\,", "\\.")
    # remove_these_strings = c(remove_these_strings, "NA", "[Cc]hinese", "Spring", "Triticum", "aestivum", "3B", "\\W*\\b\\w\\b\\W*")
    # remove_these_strings = c(remove_these_strings, "NA", "[Cc]hinese", "Spring", "Triticum", "aestivum", "3B")
    for (id in unique(dat$ID)){
      # id = unique(dat$ID)[1]
      sub = droplevels(subset(dat, ID==id))
      PUTATIVE_QTL_LIST = levels(sub$PUTATIVE_QTL)
      N_PUTATIVE_QTL = length(PUTATIVE_QTL_LIST)
      chrom = c(); pos = c(); range = c(); coverage = c(); bit_score = c(); hit = c()
      for (locus in PUTATIVE_QTL_LIST){
        # locus = PUTATIVE_QTL_LIST[1]
        qtl = subset(sub, PUTATIVE_QTL==locus)
        # qtl = qtl[order(qtl$BITSCORE, decreasing=TRUE), ]
        qtl = qtl[order(qtl$PERCENT_IDENTITY, decreasing=TRUE), ]
        if (is.na(NTOP) == TRUE){
          NTOP = nrow(qtl)
        }
        split = strsplit(locus, ":")[[1]]
        chrom = c(chrom, rep(split[1], times=NTOP))
        pos = c(pos, rep(split[2], times=NTOP))
        range = c(range, rep(split[3], times=NTOP))
        coverage = c(coverage, qtl$QUERY_COVERAGE[1:NTOP])
        bit_score = c(bit_score, qtl$BITSCORE[1:NTOP])
        hit = c(hit, as.character(qtl$HIT_INFO[1:NTOP]))
      }
      for (str in remove_these_strings){
        hit = gsub(str, "", hit)
        hit = gsub(" *\\b[[:alpha:]]{1,2}\\b *", " ", hit) # Remove 1-2 letter words
        hit = gsub("^ +| +$|( ) +", "\\1", hit) # Remove excessive spacing
      }
      eval(parse(text=paste0(id, "= data.frame(CHROM=chrom, POS=pos, RANGE=range, QUERY_COVERAGE=coverage, BITSCORE=bit_score, HIT_INFO=hit)")))
      TABLE = as.data.frame(table(strsplit(paste(eval(parse(text=paste0(id, "$HIT_INFO"))), collapse=" "), " ")[[1]]))
      if (as.character(TABLE$Var1[TABLE$Freq==max(TABLE$Freq)]) == ""){
        TABLE = TABLE[order(TABLE$Freq, decreasing=TRUE), ]
        TABLE = TABLE[2:nrow(TABLE), ]
      }
      TABLE = TABLE[TABLE$Var1 != "NA", ]
      ### Transform frequencies bounded between zero to one
      # TABLE$Freq = (TABLE$Freq - mean(TABLE$Freq)) / sd(TABLE$Freq)
      svg(paste0(id, "_PUTATIVE_QTL_BLASTN_WORDCLOUD.svg"), width=7, height=7)
      wordcloud(words=TABLE$Var1, freq=TABLE$Freq, random.order=FALSE, min.freq=1, rot.per=0.35, colors=brewer.pal(8, "Dark2"))
      dev.off()
    }
  ' > generate_word_cloud_of_blastn_putative_QTL_hits.r
  Rscript generate_word_cloud_of_blastn_putative_QTL_hits.r BLAST_OUT_CONSOLIDATED.txt 10
  ### OUTPUT:
  # (n) ${ID}_PUTATIVE_QTL_BLASTN_WORDCLOUD.svg
  ### clean up
  # rm BLASTOUT* *_BLAST_OUT_CONSOLIDATED.txt col9.temp

  ########################################################
  ### RE-MANHATTAN AND LABELLING PEAKS WITH BLAST HITS ###
  ### generating a consolidated loci x-axis for plotting the-log10(p-values)
  echo '
    ### GENERATE MAHATTAN PLOTS
    ### TOP (HIGHEST LOD OR -log10(p-value)) BLASTN HITS (NOTE: not all peaks have blastn hits)
    ### inputs:
    args = commandArgs(trailingOnly=TRUE)
    # args = c("LOCI_CONSOLIDATED.csv", "BLAST_OUT_CONSOLIDATED.txt", "/data/Lolium/Quantitative_Genetics/PoolGPAS_08SEAu/GPAS/")
    fname_loci_consolidated = args[1]
    fname_blastout_consolidated = args[2]
    DIR = args[3]
    ### consolidated loci, blastn hits across samples and models
    LOCI_INFO = read.csv(fname_loci_consolidated)
    LOCI_INFO = LOCI_INFO[with(LOCI_INFO, order(CHROM, POS)), ]
    LOCI_INFO$LOCI_IDX = 1:nrow(LOCI_INFO)
    BLAST_OUT = read.delim(fname_blastout_consolidated, sep="\t", header=TRUE)
    # GFF3_PRED = read.delim("CONSOLIDATED-GFF3-HITS.txt", sep="\t", header=TRUE)
    ### set working directory where the *ALphas.csv files are located
    setwd(DIR)
    ALPHAS_LIST = system("ls *-OUTPUT.csv | grep -v RANEF", intern=TRUE)
    ALPHAS_MATRIX = matrix(unlist(strsplit(ALPHAS_LIST, "-")), ncol=3, byrow=TRUE)[,1:2]
    ALPHAS_MATRIX[,1] = paste0( ALPHAS_MATRIX[,1], "-",  ALPHAS_MATRIX[,2])
    for (counter in 1:nrow(ALPHAS_MATRIX)){
      ### sample- and model-specific alphas file (GPAS allelic effects file)
    # counter = 1
      id = ALPHAS_MATRIX[counter, 1]
      model = ALPHAS_MATRIX[counter, 2]
      FNAME_ALPHAS = paste0(id, "-OUTPUT.csv")
      dat = read.csv(FNAME_ALPHAS)
      X = aggregate(LOD ~ CHROM + POS, data=dat, FUN=max)
      X = X[with(X, order(CHROM, POS)), ]
      sub_blast = subset(subset(BLAST_OUT, ID==id), MODEL==model)
      if (!is.na(var(X$LOD, na.rm=TRUE)) & (var(X$LOD, na.rm=TRUE) > 0.0) & (length(sub_blast$PUTATIVE_QTL) > 0)){
        print(FNAME_ALPHAS)
        MERGED = merge(LOCI_INFO, X, by=c("CHROM", "POS"), all=TRUE)
        ### plot tests
        # svg(paste0("Manhattan_plot-", id, "-", model, ".svg"), width=10, height=3)
        png(paste0("Manhattan_plot-", id, "-", model, ".png"), width=2400, height=1400)
        # plot(x=MERGED$LOCI_IDX, y=MERGED$LOD, xlab="LOCI", ylab="-log10(p-value)", main=paste0(id, "-", model), type="p", pch=20, col=rgb(0.33, 0.58, 0.92, alpha=0.5))
        ### testing 2-alternating colors per scaffold
        bicolors = c(rgb(0.31,0.70,0.83,alpha=0.75), rgb(0.66,0.87,0.71,alpha=0.75))
        layout(matrix(c(1,1,2), nrow=3, ncol=1))
        par(cex=3, mar=c(4,4.5,3,1))
        plot(x=MERGED$LOCI_IDX, y=MERGED$LOD, xlab="LOCI", ylab=expression(-log[10]("p-value")), main=paste0(id, "-", model), type="n")
        pb = txtProgressBar(min=0, max=nlevels(MERGED$CHROM), style=3, width=30)
        for (i in 1:nlevels(MERGED$CHROM)){
          # i = 1
          sub_by_chrom = subset(MERGED, CHROM==as.character(levels(MERGED$CHROM)[i]))
          points(x=sub_by_chrom$LOCI_IDX, y=sub_by_chrom$LOD, pch=20, col=bicolors[(i %% 2)+1])
          setTxtProgressBar(pb, i)
        }
        close(pb)
        ### adding threshold line
        bonferroni_threshold = -log10(1e-6 / sum(!is.na(MERGED$LOD))) ### significance at 99.9999% or FPR = 0.000001 = 1e-6
        abline(h=bonferroni_threshold, lty=2, lwd=4, col=rgb(0.25,0.25,0.25,alpha=0.75))
        ### appending top blast hits
        sub_blast_chrom_pos_range = matrix(unlist(strsplit(as.character(sub_blast$PUTATIVE_QTL), ":")), ncol=3, byrow=TRUE)
        sub_blast$CHROM = sub_blast_chrom_pos_range[,1]
        sub_blast$POS = as.numeric(sub_blast_chrom_pos_range[,2])
        sub_blast = merge(LOCI_INFO, sub_blast, by=c("CHROM", "POS"), all=FALSE)
        sub_blast = droplevels(sub_blast)
        chrom_blast = c()
        pos_blast = c()
        idx_blast = c()
        lod_blast = c()
        labs_blast = c()
        for (i in levels(sub_blast$PUTATIVE_QTL)){
          # i = levels(sub_blast$PUTATIVE_QTL)[1]
          # print(i)
          sub_sub = subset(sub_blast, PUTATIVE_QTL==i)
          sub_sub = sub_sub[with(sub_sub, order(BITSCORE, decreasing=TRUE)), ]
          chrom_blast = c(chrom_blast, as.character(sub_sub$CHROM[1]))
          pos_blast = c(pos_blast, sub_sub$POS[1])
          idx_blast = c(idx_blast, sub_sub$LOCI_IDX[1])
          lod_blast = c(lod_blast, X$LOD[(as.character(X$CHROM)==tail(chrom_blast,1)) & (X$POS==tail(pos_blast,1))])
          labs_blast = c(labs_blast, as.character(sub_sub$HIT_INFO[1]))
        }
        ### top 5 peaks with BLAST hits in terms of LOD sorry -log10(p-value) scores (NOTE: some of the peaks may not have blast hits)
        blast_hits_points = data.frame(CHROM=chrom_blast, POS=pos_blast, LOCI_IDX=idx_blast, LOD=lod_blast, HIT_INFO=labs_blast)
        hits_table = table(blast_hits_points$HIT_INFO)
        idx_peaks_blast = c()
        lod_peaks_blast = c()
        lab_peaks_blast = c()
        for (i in names(hits_table[order(hits_table, decreasing=TRUE)])[1:5]){
          sub_sub = subset(blast_hits_points, HIT_INFO==i)
          sub_sub = sub_sub[with(sub_sub, order(LOD, decreasing=TRUE)),][1,]
          idx_peaks_blast = c(idx_peaks_blast, sub_sub$LOCI_IDX)
          lod_peaks_blast = c(lod_peaks_blast, sub_sub$LOD)
          lab_peaks_blast = c(lab_peaks_blast, as.character(sub_sub$HIT_INFO))
        }
        top5_peaks = data.frame(LOCI_IDX=idx_peaks_blast, LOD=lod_peaks_blast, HIT_INFO=lab_peaks_blast)
        top5_peaks = top5_peaks[order(top5_peaks$LOCI_IDX, decreasing=FALSE), ]
        ### label the points with BLAST hits
        points(x=top5_peaks$LOCI_IDX, y=top5_peaks$LOD, cex=2.5, pch=1, col=rgb(0.03,0.41,0.67,alpha=0.9))
        text(x=top5_peaks$LOCI_IDX, y=top5_peaks$LOD, label=as.character(1:nrow(top5_peaks)), pos=4, col=rgb(0.25,0.25,0.25,alpha=0.9))
        # text(x=top5_peaks$LOCI_IDX, y=top5_peaks$LOD, label=top5_peaks$HIT_INFO, pos=4)
        ### add legend to list the BLAST hits as a new plot hahaha
        par(mar=c(1,4,1,1), cex=1.5)
        plot(x=seq(from=0, to=1, length=nrow(top5_peaks)+1), y=0:nrow(top5_peaks), type="n", xlab="", ylab="", main="", xaxt="n", yaxt="n", bty="n")
        text(x=0, y=1:nrow(top5_peaks), lab=rev(paste(c(1:nrow(top5_peaks)), top5_peaks$HIT_INFO, sep=": ")), pos=4)
        dev.off()
      }
    }
  ' > generate_manahattan_plots_with_blastn_putative_QTL_hits.r
  head -n1 $(ls *-OUTPUT.csv | grep -v "RANEF" | head -n1) | cut -d, -f1,2 > LOCI_CONSOLIDATED.temp
  for f in $(ls *-OUTPUT.csv | grep -v "RANEF")
  do
    # f=$(ls *_Alphas.csv | head -n1)
    echo $f
    echo $f > fname.temp
    if [ $(grep "FIXED_GWAlpha" fname.temp | wc -l) -eq 1 ]
    then
      tail -n+2 $f | cut -d, -f1,2 - | uniq >> LOCI_CONSOLIDATED.temp
    else
      tail -n+3 $f | cut -d, -f1,2 - | uniq >> LOCI_CONSOLIDATED.temp
    fi
  done
  head -n1 LOCI_CONSOLIDATED.temp > LOCI_CONSOLIDATED.csv
  tail -n+2 LOCI_CONSOLIDATED.temp | sort > temp.temp
  uniq temp.temp >> LOCI_CONSOLIDATED.csv
  rm *.temp
  Rscript generate_manahattan_plots_with_blastn_putative_QTL_hits.r \
          LOCI_CONSOLIDATED.csv \
          BLAST_OUT_CONSOLIDATED.txt \
          ${DIR}/GPAS/
  ### OUTPUT:
  # (n) Manhattan_plot_${ID}_${MODEL}.svg
fi

###############################################
### (7) GENOMIC PREDICTION CROSS-VALIDATION ###
###############################################
if [ ${POOLGPCV} == TRUE ]
then
  echo -e "################################################"
  echo -e "(7) GENOMIC PREDICTION CROSS-VALIDATION"
  echo -e "################################################"
    ### Julia scrip for cross-validation
    ### test
    # ARGS = ["/data/Lolium/Quantitative_Genetics/PoolGPAS_08SEAu/GPAS/ACC62_MAPQ20_BASQ20_MAF0.001_DEPTH50-GWAlpha-OUTPUT.csv",
    #         "/data/Lolium/Quantitative_Genetics/PoolGPAS_08SEAu/VCF/ACC62_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv",
    #         "/data/Lolium/Quantitative_Genetics/PoolGPAS_08SEAu/ACC62_pheno.csv",
    #         "/data/Lolium/Quantitative_Genetics/PoolGPAS_08SEAu/GPAS/ACC62_MAPQ20_BASQ20_MAF0.001_DEPTH50-GWAlpha-OUTPUT_ACC31_MAPQ20_BASQ20_VALIDATE_ALLELEFREQ.csv",
    #         "/data/Lolium/Quantitative_Genetics/PoolGPAS_08SEAu/ACC31_pheno.csv"]
  echo "
    ### load libraries
    using DelimitedFiles
    using DataFrames
    using CSV
    using Statistics
    ### load input
    filename_train_gwas = ARGS[1]
    filename_train_geno = ARGS[2]
    filename_train_pheno = ARGS[3]
    filename_test_geno = ARGS[4]
    filename_test_pheno = ARGS[5]
    train_gwas = CSV.read(filename_train_gwas, header=true)
    train_geno = CSV.read(filename_train_geno, header=false)
    train_pheno = CSV.read(filename_train_pheno, header=false)
    test_geno = CSV.read(filename_test_geno, header=false)
    test_pheno = CSV.read(filename_test_pheno, header=false)
    ### names
    name_train_pop = split(split(basename(filename_train_gwas), \"-\")[1], \"_\")[1]
    name_train_model = split(basename(filename_train_gwas), \"-\")[2]
    name_test_pop = split(split(basename(filename_test_geno), \"-\")[end], \"_\")[2]
    ### merge gwas, train_geno, and test_geno data for the correct allele/snp ordering
    train_npools = size(train_pheno,1)
    test_npools = size(test_pheno,1)
    train_geno_names = vcat([\"CHROM\", \"POS\", \"ALLELE\", string.(repeat([\"TRAIN_POOL_\"],train_npools), collect(1:train_npools))]...)
    test_geno_names = vcat([\"CHROM\", \"POS\", \"ALLELE\", string.(repeat([\"TEST_POOL_\"],test_npools), collect(1:test_npools))]...)
    names!(train_geno, Symbol.(train_geno_names))
    names!(test_geno, Symbol.(test_geno_names))
    MERGED = join(train_gwas, train_geno, test_geno, on=Symbol.([\"CHROM\", \"POS\", \"ALLELE\"]))
    ### extract sorted genotype
    X_train = convert(Array{Float64}, MERGED[:, 8:7+train_npools])'
    X_test =  convert(Array{Float64}, MERGED[:, 8+train_npools:end])'
    ### extract phenotype data
    y_train = train_pheno[:,2]
    y_test = test_pheno[:,2]
    if name_train_model == \"GWAlpha\"
      ### extract allelic effects
      b_hat = MERGED.ALPHA
      ### using the training population regress the polygenic score on the phenotypic values
      polygenic_score_train = X_train * b_hat
      p0, p1 = hcat(repeat([1], length(polygenic_score_train)), polygenic_score_train) \ y_train
      ### calculate the polygenic score of the test population
      polygenic_score_test = X_test * b_hat
      ### predict the phenotypic values
      y_pred = p0 .+ (polygenic_score_test .* p1)
    else
      ### extract intercept and allelic effects
      b0 = train_gwas.ALPHA[train_gwas.CHROM .== \"Intercept\"]
      b_hat = vcat([b0, MERGED.ALPHA]...)
      ### prediction
      y_pred = hcat(repeat([1.0], test_npools), X_test) * b_hat
    end
    ### metrics
    CORR = cor(y_test, y_pred)
    RMSE = sqrt(mean((y_test .- y_pred).^2))
    # ### MISC: plot
    # using UnicodePlots
    # UnicodePlots.scatterplot(y_test, y_pred)
    ### ouput
    filename_output = string(name_train_pop, \"-\", name_train_model, \"-\", name_test_pop, \".rmse\")
    DelimitedFiles.writedlm(filename_output, hcat(name_train_pop, name_train_model, name_test_pop, CORR, RMSE), ',')
    ### output: TRAIN_POP, TRAIN_MODEL, TEST_POP, CORR, RMSE
  " > predict.jl
  ### parse the testing population"s subsetted sync file into:
  echo "filename_sync_test = ARGS[1]
    using GWAlpha
    GWAlpha.sync_processing_module.sync_parse(filename_sync_test)
  " > parse.jl
  ### parallelizable script
  echo '#!/bin/bash
    ### inputs
    POP_TRAIN=$1
    POP_TEST=$2
    MODEL=$3
    DIR=$4
    # ### test
    # POP_TRAIN=ACC62
    # POP_TEST=ACC31
    # MODEL=GWAlpha
    # DIR=/data/Lolium/Quantitative_Genetics/PoolGPAS_08SEAu/
    ### filenames
    TRAIN_GWAS=${DIR}/GPAS/${POP_TRAIN}_MAPQ20_BASQ20_MAF0.001_DEPTH50-${MODEL}-OUTPUT.csv
    TRAIN_SYNC=${DIR}/VCF/${POP_TRAIN}_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync
    TRAIN_PHENO=${DIR}/${POP_TRAIN}_pheno.csv
    TEST_UNFLITERED_SYNC=${DIR}/VCF/${POP_TEST}_MAPQ20_BASQ20.sync
    TEST_PHENO=${DIR}/${POP_TEST}_pheno.csv
    if [ ! -f $TRAIN_GWAS ]
    then
      ### exit if the GWAS file does not exist
      exit 1
    fi
    ### subset unfiltered sync file of the test population based on the
    ### loci present in the training population"s Pool-GPAS model
    cut -d, -f1-2 ${TRAIN_GWAS} | grep -v "CHROM" | grep -v "Intercept" | sort | uniq | sed s/,/"\t"/g \
      > ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*}).grepper
    grep -f ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*}).grepper ${TEST_UNFLITERED_SYNC} | sort \
      > ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_VALIDATE.sync
    ### parse the testing population"s subsetted sync file into:
    julia parse.jl \
      ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_VALIDATE.sync
    ### In the test population: grep the corresponding alleles from the training population"s GWAS output
    cut -d, -f1-3 ${TRAIN_GWAS} | grep -v "CHROM" | grep -v "Intercept" | sort | uniq \
      > ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*}).allele.grepper
    grep -f ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*}).allele.grepper ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_VALIDATE_ALLELEFREQ.csv | sort \
      > ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_VALIDATE_ALLELEFREQ.csv.temp
    mv ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_VALIDATE_ALLELEFREQ.csv.temp \
      ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_VALIDATE_ALLELEFREQ.csv
    ### In the training population: grep the corresponding alleles from the training population"s GWAS output
    grep -f ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*}).allele.grepper ${TRAIN_SYNC%.sync*}_ALLELEFREQ.csv | sort \
      > ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_TRAINING_ALLELEFREQ.csv
    ### cross-validation
    julia predict.jl \
            ${TRAIN_GWAS} \
            ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_TRAINING_ALLELEFREQ.csv \
            ${TRAIN_PHENO} \
            ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_VALIDATE_ALLELEFREQ.csv \
            ${TEST_PHENO}
    ### clean-up
    rm ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*}).grepper
    rm ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*}).allele.grepper
    rm ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_VALIDATE.sync
    rm ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_VALIDATE_ALLELEFREQ.csv
    rm ${TRAIN_GWAS%.csv*}_$(basename ${TEST_UNFLITERED_SYNC%.sync*})_TRAINING_ALLELEFREQ.csv
  ' > predict.sh
  chmod +x predict.sh
  time \
  parallel ./predict.sh {1} {2} {3} ${DIR}\
                  ::: $(cat $PGRP) \
                  ::: $(cat $PGRP) \
                  ::: GWAlpha MIXEDREML_FST GLMNET_ALPHA0.0 GLMNET_ALPHA0.5 GLMNET_ALPHA1.0
fi
