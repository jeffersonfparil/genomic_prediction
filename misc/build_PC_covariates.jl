using DelimitedFiles
using LinearAlgebra

filename_geno = ARGS[1]
# filename_geno = "/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/GP/geno.csv"
geno = DelimitedFiles.readdlm(filename_geno, ',')
X = convert(Array{Float64}, copy(geno[:, 4:end]))
X_svd = LinearAlgebra.svd(X, full=false)
PC = (X_svd.U * LinearAlgebra.Diagonal(X_svd.S))'
DelimitedFiles.writedlm("PC1_X_cova.csv", PC[:,1], ',')
DelimitedFiles.writedlm("PC1to5_X_cova.csv", PC[:,1:5], ',')
DelimitedFiles.writedlm("PC1to10_X_cova.csv", PC[:,1:10], ',')
