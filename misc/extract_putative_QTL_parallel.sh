### EXTRACT PUTATIVE QTL PARALLEL RUN

DIR=${1}
SRC_DIR=${2}
CORR=${3}
covariate=${4}
herbi=${5}
model=${6}
# DIR=/data/Lolium/Quantitative_Genetics/GPWAS_60_SEAUS
# SRC_DIR=/data/Lolium/Softwares/genomic_prediction/misc
# CORR=0.95
# covariate=NO_COVARIATE
# herbi=CLETHODIM_RESISTANCE
# model=FIXED_LS

### separate beta and stats by covariate, herbi and model
grep -n ${model} <<<$(tail -n+2 ${DIR}/${covariate}/CROSS_VALIDATION_5_FOLD_OUTPUT_pheno_${herbi}.csv) | cut -d: -f1 > idx-${covariate}-${herbi}-${model}.temp
awk 'NR == FNR{a[$0]; next};FNR in a' idx-${covariate}-${herbi}-${model}.temp  ${DIR}/${covariate}/CROSS_VALIDATION_5_FOLD_BETAS_pheno_${herbi}.csv > BETAS-${covariate}-${herbi}-${model}.csv
head -n1 ${DIR}/${covariate}/CROSS_VALIDATION_5_FOLD_OUTPUT_pheno_${herbi}.csv > STATS-${covariate}-${herbi}-${model}.csv
grep ${model} ${DIR}/${covariate}/CROSS_VALIDATION_5_FOLD_OUTPUT_pheno_${herbi}.csv >> STATS-${covariate}-${herbi}-${model}.csv

### extract putative QTL
julia ${SRC_DIR}/extract_putative_QTL.jl \
    ${DIR} \
    ${DIR}/${covariate}/LOCI_info.csv \
    ${CORR} \
    BETAS-${covariate}-${herbi}-${model}.csv \
    STATS-${covariate}-${herbi}-${model}.csv \
    "false"

### message and cleanup
echo "${covariate}-${herbi}-${model}: DONE!"
echo "########################################"
rm idx-${covariate}-${herbi}-${model}.temp
rm BETAS-${covariate}-${herbi}-${model}.csv
rm STATS-${covariate}-${herbi}-${model}.csv
