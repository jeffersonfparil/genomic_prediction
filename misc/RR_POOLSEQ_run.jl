#!/home/student.unimelb.edu.au/jparil/Downloads/SOFTWARES/julia-1.0.1/bin/julia

###############
###			###
### GWAlpha ###
###			###
###############
if length(ARGS)!=3
	println("===========================================================================")
	println("RR-Pool-seq arguments:")
	println("\t Argument 1 is the genotype file in sync format.")
	println("\t Argument 2 is the phenotype file in python format.")
	println("\t Argument 3 is the minor allele frequency in decimal form.")
	println("\t Example: ./GWAlpha_run.jl res/GWAlpha_X.sync res/GWAlpha_y.py 0.03")
	println("===========================================================================")
else
	include("src/RR_POOLSEQ_module.jl")
	RR_POOLSEQ_module.RR_POOLSEQ(ARGS)
end
