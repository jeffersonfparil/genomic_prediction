###########################
### Building covariates ###
########################### same module name and filename

module build_covariates_module

#####################
###				  ###
### load packages ###
###				  ###
#####################
using Statistics
using LinearAlgebra
using ProgressMeter
using DataFrames
using CSV

#####################
###				  ###
### main function ###
###				  ### #inputs are the genoype matrix of allele dose (0,1,2), the covariate you wish to generate: PC or K, and the filename of the output file to store the covariate
##################### #outputs are the covariate matrix as julia array and as a csv file
function build_covariates(X_raw, MAF; covariate_out="PC", filename_out=string("COVARIATE_OUT_", time(), ".csv"))
	### MAF filtering
	n = size(X_raw)[1]
	l = size(X_raw)[2]
	if covariate_out == "PC"
		### principal components
		### filter loci by MAF
		LOC = collect(1:l)[ ((mean(X_raw, dims=1) ./ 2) .> MAF)[:] .& ((mean(X_raw, dims=1) ./ 2) .< (1.0-MAF))[:] ]
		X = X_raw[:, LOC]
		COVARIATE = LinearAlgebra.svd(X).U
	elseif covariate_out == "K"
		### kinship matrix (equation 1 of Goudet, Kay & Weir, 2018)
		idx_biallelic = [] # using only biallelic loci
		for i in 1:convert(Int, round(size(X_raw,2)/5))
			allele_sums = sum( X_raw[:, (((i-1)*5)+1):(i*5)], dims=1 )
			to_retain_1_allele = collect(1:5) .* (allele_sums .== maximum(allele_sums))[1,:]
			idx = to_retain_1_allele .== maximum(to_retain_1_allele)
			append!(idx_biallelic, idx)
		end
		X_biallelic = X_raw[:, convert(Array{Bool}, idx_biallelic)]
		### MAF filtering
		l = size(X_biallelic, 2)
		LOC = collect(1:l)[ ((mean(X_biallelic, dims=1) ./ 2) .> MAF)[:] .& ((mean(X_biallelic, dims=1) ./ 2) .< (1.0-MAF))[:] ]
		X_biallelic_filtered = X_biallelic[:, LOC]
		n = size(X_biallelic_filtered, 1)
		M = Array{Float64}(undef, n, n)
		# pb = ProgressMeter.Progress(n; dt=0.1, desc="Building Kinship matrix: ", barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow)
		for i in 1:n
			for j in 1:n
				M[i,j] = sum( (1 .+ ( (X_biallelic_filtered[i,:] .- 1) .* (X_biallelic_filtered[j,:] .- 1) )) ./ 2 )
			end
			# ProgressMeter.update!(pb, i)
		end
		M_nodiag = copy(M)
		M_nodiag[diagind(M_nodiag)] .= 0
		Ms = sum(M_nodiag) / (n*(n-1))
		K = (M .- Ms) ./ (1 .- Ms)
		COVARIATE = K
	else
		println("Please specify a covariate_out.")
		println("Choose 'PC' for principal componenets of eigenvectors or 'K' for kinship matrix")
		COVARIATE = nothing
		exit()
	end
	try
		rm(filename_out)
	catch
		println("---------------------------------------------------------------------")
	end
	CSV.write(filename_out, DataFrames.DataFrame(COVARIATE); append=true)
	return(COVARIATE)
end

end #end of GWAlpha module

### Sample usage:
# n=100
# l=500
# X_raw = reshape(rand([0.0,1.0,2.0], n*l), n,l)
# MAF = 0.001
# COVARIATE = build_covariates_module.build_covariates(X_raw; covariate_out="PC", filename_out="ZZZ_covariate_test.csv")
