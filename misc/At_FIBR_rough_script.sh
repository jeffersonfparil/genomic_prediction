#############
### setwd ###
#############
DIR=/mnt/Lolium/Quantitative_Genetics/LOLSIM_2019/arabidopsis_GPAS_ABC/FIBR_LIMIX_VS_GEMMA
SRC_DIR=/mnt/Lolium/Softwares
cd ${DIR}

###################
### input files ###
###################
ls -lh FIBR_PTUtB_PC1.csv
ls -lh FIBR_PTUtB_PC2.csv
ls -lh FIBR_PTUtB_Clusters.csv
ls -lh k2029_MAF0.01_SNPS.bed
ls -lh k2029_MAF0.01_SNPS.bim
ls -lh k2029_MAF0.01_SNPS.fam

#########################################################
#### plink filtering by available FIBR phenotype data ###
#########################################################
cut -d, -f1 FIBR_PTUtB_PC1.csv | tail -n+2 | sort -g > FIBR_filtering_sample_ID_PC1.temp
cut -d, -f1 FIBR_PTUtB_PC2.csv | tail -n+2 | sort -g > FIBR_filtering_sample_ID_PC2.temp
cut -d, -f1 FIBR_PTUtB_Clusters.csv | tail -n+2 | sort -g > FIBR_filtering_sample_ID_Clusters.temp
cat FIBR_filtering_sample_ID_*.temp | sort -g | uniq > FIBR_filtering_sample_ID.txt
${SRC_DIR}/plink2 --bed k2029_MAF0.01_SNPS.bed \
                  --bim k2029_MAF0.01_SNPS.bim \
                  --fam k2029_MAF0.01_SNPS.fam \
                  --keep-fam FIBR_filtering_sample_ID.txt \
                  --snps-only \
                  --maf 0.01 \
                  --threads 12 \
                  --make-bed \
                  --out FIBR_k2029_MAF0.01

##############################
### prepare kinship matrix ###
##############################
${SRC_DIR}/plink  --bfile FIBR_k2029_MAF0.01 \
                              --recode12 \
                              --output-missing-genotype 0 \
                              --transpose \
                              --out FIBR_k2029_MAF0.01_transposed
${SRC_DIR}/emmax-kin-intel64 -v -s -d 10 FIBR_k2029_MAF0.01_transposed
### output: FIBR_k2029_MAF0.01_transposed.aIBS.kinf
mv FIBR_k2029_MAF0.01_transposed.aIBS.kinf FIBR_k2029_MAF0.01.kinship
### output: FIBR_k2029_MAF0.01.kinship

###################
### prepare fam ###
###################
echo -e '
args = commandArgs(trailingOnly=TRUE)
# args = c("FIBR_k2029_MAF0.01.fam", "FIBR_PTUtB_PC1.csv")
fam = read.delim(args[1], header=FALSE, sep="\t")[,1:5]
pc = read.csv(args[2])
colnames(pc) = c("V1", "PC")
out = merge(fam, pc, by="V1")
write.table(out, file=args[1], quote=FALSE, sep="\t", row.names=FALSE, col.names=FALSE)
' > prep_fam.R

##################
### limix GWAS ###
##################
echo -e '
import sys
import limix
from glimix_core.lmm import LMM
from os import getcwd
from os.path import join
from pandas_plink import get_data_folder
import numpy as np
from numpy_sugar.linalg import economic_qs_linear
import dask as da
import matplotlib.pyplot as plt
import scipy.stats as stat
import pandas as pd
import gc
bimbam_prefix = sys.argv[1]
kinship_file = sys.argv[2]
outname = sys.argv[3]
# bimbam_prefix = "FIBR_k2029_MAF0.01"
# kinship_file = "FIBR_k2029_MAF0.01_transposed.aIBS.kinf"
# suffix = "PC1"
### load bim (loci info), fam (family ID + phenotype), and bed (genotype scores; loaded as a dask array) data
(bim, fam, bed) = limix.io.plink.read(join(getcwd(), bimbam_prefix), verbose=False)
ibs = limix.io.csv.read(kinship_file, header=False)
idx = np.arange(0, fam.shape[0], 1)[fam.loc[:, "trait"] != -9]
y = fam.loc[:, "trait"][idx].to_numpy()
X_raw = bed[0:bed.shape[0], idx].transpose()
MAF_thresh = np.min(limix.qc.compute_maf(bed.transpose()))
MAF = limix.qc.compute_maf(X_raw)
X = X_raw[0:X_raw.shape[0], MAF >= MAF_thresh]
bim = bim.iloc[MAF >= MAF_thresh, :]
K = ibs.iloc[idx, idx].to_numpy()
# limix.plot.kinship(K); plt.show()
### using QTL SCAN
out = limix.qtl.scan(X, y, "normal", K=K)
pval = out.stats.pv20.to_numpy()
lod = -np.log10(pval)
df = pd.DataFrame({"chrom":bim.iloc[:, 0].to_numpy(), "pos":bim.iloc[:, 3].to_numpy(), "pv":pval, "lod":lod})
kinship_file_split = kinship_file.split("_")
df.to_csv(outname + ".csv")
' > limix_GWAS.py

##################
### gemma GWAS ###
##################
echo -e '#!/bin/bash
BFILE=$1
KFILE=$2
OUTNAME=$3
SRC_DIR=$4
# BFILE=FIBR_k2029_MAF0.01
# KFILE=FIBR_k2029_MAF0.01.kinship
# SUFFIX=FIBR_gemma-PC1
${SRC_DIR}/gemma-0.98.1-linux-static  -bfile ${BFILE} \
                                      -k ${KFILE} \
                                      -lmm 4 \
                                      -o ${OUTNAME}
mv output/${OUTNAME}.assoc.txt ${OUTNAME}.txt
rm -R output/
' > gemma_GWAS.sh
chmod +x gemma_GWAS.sh

##########################
### manhattan plotting ###
##########################
echo -e '
args = commandArgs(trailingOnly=TRUE)
# args = c("FIBR_limix-PC1.csv")
# args = c("FIBR_gemma-PC1.txt")
if (grepl("limix", args[1])){
  dat = read.csv(args[1], header=TRUE)
  str(dat)
  colnames(dat) = c("IDX", "CHROM", "POS", "PVAL", "LOD")
  dat$ID = paste(dat$CHROM, dat$POS, sep="_")
} else if (grepl("gemma", args[1])){
  dat = read.delim(args[1], header=TRUE, sep="\t")
  str(dat)
  colnames(dat) = c("CHROM", "ID", "POS", "NMISS", "ALT", "REF", "FREQ", "BETA", "SE", "LLH1", "LREML", "LMLE", "PWALD", "PLRT", "PVAL")
  dat$IDX = 1:nrow(dat)
  dat$LOD = -log(dat$PVAL, base=10)
}
# calculate Bonferroni and Banjamini-Hochberg threshold at alpha=1%
a = 0.01
THRESHOLD = -log10(a/nrow(dat))
P_k = dat$PVAL[order(dat$PVAL, decreasing=FALSE)]
k = 1:length(P_k)
m = length(P_k)
k_max = max(k[apply(cbind(P_k, k), MAR=1, FUN=function(x){x[1] <= x[2]*a/m})])
FDR_THRESHOLD = -log10(P_k[k_max]) ### lower threshold fro PC1 at least 2020-03-18
# manhattan plotting
COLORS = rep(RColorBrewer::brewer.pal(9, "GnBu")[c(3, 7)], times=length(unique(dat$CHROM)))
png(paste0(gsub(".txt", "", gsub(".csv", "", args[1])), ".png"), width=2000, height=900)
layout(matrix(c(1,1,1,2), nrow=1, byrow=TRUE))
par(cex=2)
plot(x=dat$IDX, y=dat$LOD, col=COLORS[dat$CHROM], pch=20, main=args[1], xlab="Loci", ylab="-log10(p-values)")
abline(h=THRESHOLD, lwd=1, lty=2, col="gray")
grid()
### qq plotting
observed_pval = dat$PVAL[order(dat$PVAL, decreasing=FALSE)]
# calculate the pdf of the observed p-values, i.e. the probability density for a p-value interval which corresponds to each observed p-value
observed_pval_density = density(observed_pval, n=length(observed_pval), from=0, to=1) ### calculate the density of the observed p-values between 0 and 1 because we are dealing with p-values which are probabilities!
# calculate the cummulative probabilities of the observed p-values based on the pdf: where Prob(pvalue) = Density(pvalue | pvalue_interval) * pvalue_interval
observed_pval_cumprob = cumsum(observed_pval_density$y * (observed_pval_density$x[2]-observed_pval_density$x[1]))
# calculate the expected quantiles based on the cummulative probabilities of the observed p-values
expected_pval = qunif(p=observed_pval_cumprob, min=0, max=1) ### calculate the expected quantiles based on the observed cummulative probabilities
# transform into -log10 scale
observed_lod = -log10(observed_pval + 1e-200)
expected_lod = -log10(expected_pval + 1e-200)
# plot
plot(x=c(min(observed_lod), max(observed_lod)), y=c(min(observed_lod), max(observed_lod)), type="n", , main="QQ Plot", xlab="Expected -log10(p-value)", ylab="Observed -log10(p-value)")
points(x=expected_lod, y=observed_lod, type="p", pch=20, col=COLORS[2])
lines(x=c(0, max(observed_lod)), y=c(0, max(observed_lod)), lty=2, lwd=2, col="gray")
grid()
dev.off()
' > manhattan_plotting.R

###############
### EXECUTE ###
###############
for i in PC1 PC2 Clusters
do
  # i=PC1
  echo $i
  Rscript prep_fam.R FIBR_k2029_MAF0.01.fam FIBR_PTUtB_${i}.csv
  python3 limix_GWAS.py FIBR_k2029_MAF0.01 FIBR_k2029_MAF0.01.kinship FIBR_limix-${i}
  ./gemma_GWAS.sh FIBR_k2029_MAF0.01 FIBR_k2029_MAF0.01.kinship FIBR_gemma-${i} ${SRC_DIR}
  Rscript manhattan_plotting.R FIBR_limix-${i}.csv
  Rscript manhattan_plotting.R FIBR_gemma-${i}.txt
done
