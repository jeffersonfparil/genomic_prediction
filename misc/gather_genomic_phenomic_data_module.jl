#################################
###							  ###
###   Simulate_phenotypes	  ###
###   or load pre-existing	  ###
### genotype & phenotype data ###
###							  ###
################################# same module name and filename

module gather_genomic_phenomic_data_module

using PyCall
using HDF5
using CSV
using HTTP
using StatsKit
using ProgressMeter
using JLD2

############################
### Arabidopsis thaliana ###
############################
function __download_Arabidopsis_thaliana_data__()
	### downlload SNP (0=ref; 1=alt) and kinship data
	run(`wget http://1001genomes.org/data/GMI-MPI/releases/v3.1/SNP_matrix_imputed_hdf5/1001_SNP_MATRIX.tar.gz`)
	run(`tar -xvzf 1001_SNP_MATRIX.tar.gz`)
	run(`mv 1001_SNP_MATRIX/imputed_snps_binary.hdf5 At_SNPs_binary.hdf5`)
	run(`mv 1001_SNP_MATRIX/kinship_ibs_mac5.hdf5 At_kinship.hdf5`)
	run(`rm -R 1001_SNP_MATRIX 1001_SNP_MATRIX.tar.gz`)
	### fix hdf5 to be Julia compatible (stanadlone script hereL: src/hdf5_SNP_fix_for_julia.py)
	py"""
	import sys
	import h5py as h5
	import numpy as np
	HDF5_filename = "At_SNPs_binary.hdf5"
	HDF5_file = h5.File(HDF5_filename,'r')
	HDF5_file.keys()
	acc = HDF5_file['accessions'][:]
	pos = HDF5_file['positions'][:]
	chrom = []
	chrom_regions = HDF5_file['positions'].attrs['chr_regions']
	for i in range(0, (len(chrom_regions))):
		print(i)
		rep = chrom_regions[i][1] - chrom_regions[i][0]
		chrom.extend([i+1]*rep)

	X = HDF5_file['snps'][:]
	HDF5_file.close()
	#change types
	acc = acc.astype('int32')
	pos = pos.astype('int64')
	chrom = np.asarray(chrom)
	chrom = chrom.astype('int32')
	X = X.astype('int8')
	acc.shape
	pos.shape
	chrom.shape
	X.shape
	out = h5.File(".".join(HDF5_filename.split(".")[0:-1]) + "-FIXED_FOR_JULIA.hdf5", 'w')
	out.create_dataset('accessions', data=acc)
	out.create_dataset('positions', data=pos)
	out.create_dataset('chromosomes', data=chrom)
	out.create_dataset('snps', data=X)
	out.close()
	"""
	### import SNP data
	At_SNP_hdf5 = HDF5.h5open("At_SNPs_binary-FIXED_FOR_JULIA.hdf5")
	names(At_SNP_hdf5)
	taxa = HDF5.read(At_SNP_hdf5["accessions"])
	pos = HDF5.read(At_SNP_hdf5["positions"])
	X = HDF5.read(At_SNP_hdf5["snps"])
	HDF5.close(At_SNP_hdf5)

	### import kinship data
	At_kinship = HDF5.h5open("At_kinship.hdf5")
	names(At_kinship)
	K = HDF5.read(At_kinship["kinship"])

	### download and load phenotype data
	phenum = [1,2,3,4,12,16,18,19,22]
	for i in 1:length(phenum)
		Y_temp = CSV.read(IOBuffer(String(HTTP.get(string("https://arapheno.1001genomes.org:443/rest/study/", phenum[i], "/values/")).body)))
		if (i == 1)
			global Y = Y_temp
		else
			global Y = DataFrames.join(Y, Y_temp, on=[:accession_id, :replicate_id], kind=:outer)
		end
	end

	### output
	JLD2.@save("At_DATA.jld2", taxa, pos, X, K, Y)
	return(taxa, pos, X, K, Y)
end
### execute & use the jld2 data
# taxa, pos, X, K, Y = __download_Arabidopsis_thaliana_data__()
# JLD2.@load("At_DATA.jld2")

###############################
### Drosophila melanogaster ###
###############################
function __download_Drosophila_melanogaster_data__()
	### download SNP data (0=ref; 2=alt; -=missing)
	run(`wget http://dgrp2.gnets.ncsu.edu/data/website/dgrp2.tgeno`)
	run(`wget http://dgrp2.gnets.ncsu.edu/data/website/freeze2.common.rel.mat`)
	run(`mv dgrp2.tgeno Dm_SNPs_integer.txt`)
	run(`mv freeze2.common.rel.mat Dm_kinship.txt`)
	run(`sed -i 's/-/ /g' Dm_SNPs_integer.txt`) #remove missing data
	# ### John Pool resources Wisconsin look for them (African populations) as well as the corresponding phenotype data from
	# run(`wget http://pooldata.genetics.wisc.edu/dpgp3_sequences.tar.bz2`)
	# run(`tar -xvzf dpgp3_sequences.tar.bz2`)
	# run(`wget http://pooldata.genetics.wisc.edu/DGN11/POOL_sequences.tar.gz`)
	# run(`wget -O Pool2017_MBE_Dm_cold.zip "https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/mbe/34/2/10.1093_molbev_msw232/15/msw232_Supp.zip?Expires=1556863118&Signature=zh1G7enBUxHASIv1f0R-bFVUpIAUFTy6GMdsqaXEZsuCpkq0DIBX8vLYGnydsMG5l7aVOYHC9dG6ugEFHVfpJm9mSzpPplSmQqX6SZnaMtoOCHrdhxbPKOyYg34cNln4K4m-q9MZK~dnvHpfG4MbL8dmisbfvmAShoZagBxj8NTsJxi-UsSdaBNEAXrQ3RLSXkivxsPO0VIHA4faJbOZvJ84hvi91e0OLTZiEs8v2OZfa95klsvnEbUD7WYfjg0gKLCbevMXHMcjgsAl5CTM0fjQsCLMhLIBjL~rFJn-zPoWubLyQ5y-XpEf2mEFgfHWYyiVmiSkCjO71VyxvnvZCg__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA"`)
	# run(`unzip Pool2017_MBE_Dm_cold.zip; rm Pool2017_MBE_Dm_cold.zip`)
	# run(`cd dpgp3_sequences; for i in $(ls *.tar); do tar -xvf $i; done`)

	### load genotype and kinship DataFrames
	X_allmeta = CSV.read("Dm_SNPs_integer.txt", delim=' ')
	K_allmeta = CSV.read("Dm_kinship.txt", delim='\t')

	### crude imputation (mean per locus) of the genotype data
	taxa = string.(names(X_allmeta)[10:end])
	chr = X_allmeta.chr
	pos = X_allmeta.pos
	X_temp = convert(Array, X_allmeta[:,10:end])'
	impute_ave = []
	# impute_ave = Array{Union{Missing, Float64}}(missing, size(X_temp)[2], 1)
	pb = ProgressMeter.Progress(size(X_temp)[2], dt=1, barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow)
	for i in 1:size(X_temp)[2]
		# println(i)
		try
			# append!(impute_ave, mean(skipmissing(X_temp[:,i:i])))
			X_temp[ismissing.(X_temp[:,i]),i] .= mean(skipmissing(X_temp[:,i:i]))
		catch
			# append!(impute_ave, 0)
			X_temp[ismissing.(X_temp[:,i]),i] .= 0.0
		end
		# mu = mean(X_temp[:,i:i])
		# append!(impute_ave, mu[1])
		# impute_ave[i,1] = mu
		ProgressMeter.update!(pb, i)
	end
	# convert into Int32 array, and save the imputed genotype data
	X = convert(Array{Int32}, X_temp)
	# JLD2.@save("Dm_SNPs_imputed.jld2", X)
	# JLD2.@load("Dm_SNPs_imputed.jld2")

	### convert into Float64 and save the kinship matrix
	K = convert(Array{Float64}, K_allmeta[:,2:end])

	### dowload and load phenotype data
	fnames = ["http://dgrp2.gnets.ncsu.edu/data/website/survival.paraquat.female.csv", "http://dgrp2.gnets.ncsu.edu/data/website/benz.female.csv", "http://dgrp2.gnets.ncsu.edu/data/website/etoh.sensitivity.female.csv"]
	mnames = ["http://dgrp2.gnets.ncsu.edu/data/website/survival.paraquat.male.csv", "http://dgrp2.gnets.ncsu.edu/data/website/benz.male.csv", "http://dgrp2.gnets.ncsu.edu/data/website/etoh.sensitivity.male.csv"]
	for i in 1:length(fnames)
		fname = fnames[i]
		mname = mnames[i]
		Yf = CSV.read(IOBuffer(String(HTTP.get(fname).body)), header=false)
		Ym = CSV.read(IOBuffer(String(HTTP.get(mname).body)), header=false)
		sex = repeat(["female"], inner=DataFrames.nrow(Yf))
		append!(sex, repeat(["male"], inner=DataFrames.nrow(Ym)))
		line = Yf[:,1]
		append!(line, Ym[:,1])
		y = Yf[:,2]
		append!(y, Ym[:,2])
		Y_temp1 = DataFrames.DataFrame(sex=sex, line=line, y=y)
		if (i==1)
			global Y = DataFrames.DataFrame(sex=sex, line=line, paraquat=y)
			# names!(Y_temp1, Symbol.(["sex", "line", "paraquat"]))
		else
			Y_temp1 = DataFrames.DataFrame(sex=sex, line=line, y=y)
			names!(Y_temp1, Symbol.(["sex", "line", split(split(fname, "/")[end], ".")[1]]))
			# rename!(Y_temp, :y=>Symbol(split(split(fname, "/")[end], ".")[1]))
			global Y = DataFrames.join(Y, Y_temp1, on=[:sex,:line], kind=:outer)
		end
		# Y = DataFrames.join(Y, Y_temp1, on=[:sex,:line])
	end

	### output
	JLD2.@save("Dm_DATA.jld2", taxa, chr, pos, X, K, Y)
	return(taxa, chr, pos, X, K, Y)
end
### execute & use the jld2 data
# taxa, chr, pos, X, K, Y = __download_Drosophila_melanogaster_data__()
# JLD2.@load("Dm_DATA.jld2")


################
### Zea mays ###
################
function __download_Zea_mays_data__()
	### download AGP version 4 imputed 20181023
	#### register to cyverse: https://user.cyverse.org/register
	### confirm account
	### download iCommands: wget ftp://ftp.renci.org/pub/irods/releases/4.1.10/ubuntu14/irods-icommands-4.1.10-ubuntu14-x86_64.deb
	### install iCommands: sudo dpkg -i irods-icommands-4.1.10-ubuntu14-x86_64.deb; sudo apt install -f
	### login to cyVerse and browse: http://datacommons.cyverse.org/browse/iplant/home/shared/panzea
	### setup iCommands: $ iinit
	###						data.cyverse.org
	###						1247
	###						jparil
	###						iplant
	###						qwertyuiop0987654321
	### dowload the GBS genotype data v2.7: iget /iplant/home/shared/panzea/genotypes/GBS/v27/ZeaGBSv27_publicSamples_imputedV5_AGPv4-181023.h5
	panzea_hdf5_file = "ZeaGBSv27_publicSamples_imputedV5_AGPv4-181023.h5"
	f = HDF5.h5open(panzea_hdf5_file, "r")
	names(f)
	X_temp = HDF5.read(f["Genotypes"])

	### taxa (we could've used the "Taxa" data from the hdf5 data structure but I want to be sure the genotype data correspond perfectly with e taxa names)
	taxa = fill(convert(String, "abc"), length(keys(X_temp)))
	i = [1] # iterator for appending into taxa array
	for k in keys(X_temp)
		# println(k)
		# println(String(join(k)))
		tmp = String(join(k))
		# println(tmp)
		# append!(taxa, tmp)
		taxa[i[1]] = tmp
		i[1] = i[1] + 1
	end

	### chrom,  pos and reference alleles
	chrom = HDF5.read(f["Positions"])["ChromosomeIndices"]
	pos = HDF5.read(f["Positions"])["Positions"]
	ref = HDF5.read(f["Positions"])["ReferenceAlleles"]

	### X
	n = length(taxa)
	l = length(X_temp[taxa[1]]["calls"])
	X = zeros(UInt8, (n, l))
	# X = Array{Union{Missing, UInt8}}(missing, n, l)

	progress_bar = ProgressMeter.Progress(n, dt=1, barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow) #progress bar
	for i in 1:n
		if (taxa[i] != "AlleleStates") && (taxa[i] != "_Descriptors")
			x0 = X_temp[taxa[i]]["calls"]
			x1 = fill(convert(UInt8, 255), length(x0)) ### because geting error with NN: -1
			x1[x0 .>  -1] = convert(Array{UInt8}, x0[x0 .> -1])
			# sequence every signed 8-bits or every 16th number of base 8: ACGT+-ZN
			# A_: 0x00-0x0f: AA AC AG AT A+ A- AZ AN
			# C_: 0x10-0x1f: CA CC CG CT C+ C- CZ CN
			# G_: 0x20-0x2f: GA GC GG GT G+ G- GZ GN
			# T_: 0x30-0x3f: TA TC TG TT T+ T- TZ TN
			# +_: 0x40-0x4f: +A +C +G +T ++ +- +Z +N
			# -_: 0x50-0x5f: -A -C -G -T -+ -- -Z -N
			# Z_: 0xe0-0xef: ZA ZC ZG ZT Z+ Z- ZZ ZN
			# N_: 0xf0-0xff: NA NC NG NT N+ N- NZ NN
			X[i, :] = x1
		end
		# println(i)
		ProgressMeter.update!(progress_bar, i)
	end
	close(f)

	### summarize into 0, 1, 2 matrix assuming biallelic loci everywhere
	# BUT BUT BUT!!! WHAT TO DO WITH Ns AND Zs AND MULTIPLE ALLELES?!?!?!
	# NEW PLAN: use ref::: count number of times ref is and set 0xff as missing and of course all the while accounting for heterozygotes
	function bit8_to_refcount(xij, refj)
		# missing
		if xij == 0xff
			out = 0
		# non-missing
		else
			# first allele
			if floor(xij / 0x10) == refj
				out = 1
			else
				out = 0
			end
			# second allele
			if (xij % 0x10) == refj
				out = out + 1
			end
		end
		return(out)
	end
	progress_bar = ProgressMeter.Progress(n*l, dt=1, barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow); counter=[1]
	for i in 1:n
		for j in 1:l
			xij = X[i,j]
			X[i,j] = bit8_to_refcount(xij, ref[j])
			ProgressMeter.update!(progress_bar, counter[1]); counter[1] = counter[1] + 1
		end
	end
	# # convert from 8-bit into float64 ---> No need actually just convert after filtering or as we are filtering
	# X_out = convert(Array{Float64}, X) #fails du to insufficient memory

	### download phenotype data
	### NAM Buckler_etal_2009_Science_flowering_time_data-090807.zip: iget /iplant/home/shared/panzea/phenotypes/Buckler_etal_2009_Science_flowering_time_data-090807.zip; unzip Buckler_etal_2009_Science_flowering_time_data-090807.zip; rm Buckler_etal_2009_Science_flowering_time_data-090807.zip
	### NAM Brown_etal_2011_PLoSGenet_pheno_data-120523.zip: iget /iplant/home/shared/panzea/phenotypes/Brown_etal_2011_PLoSGenet_pheno_data-120523.zip; unzip Brown_etal_2011_PLoSGenet_pheno_data-120523.zip; rm Brown_etal_2011_PLoSGenet_pheno_data-120523.zip
	Y = CSV.read("NAMSum0607FloweringTraitBLUPsAcross8Envs.txt"; delim=",")

	### output
	JLD2.@save "Zm_DATA.jld2" taxa chrom pos ref X Y
	return(taxa, chrom, pos, ref, X, Y)
end
### execute & use the jld2 data
# taxa, chr, pos, ref, X, Y = __download_Zea_mays_data__()
# JLD2.@load("Zm_DATA.jld2")

end #end of gather_genomic_phenomic_data_module
