###############################################
###											###
### Simple iterative genomewide association ### with PC covariates
###											###
############################################### same module name and filename

module GWAS_PC_module

#####################
###				  ###
### load packages ###
###				  ###
#####################
using LinearAlgebra
using Distributions
using ProgressMeter

#####################
###				  ###
### main function ###
###				  ###
#####################

function GWAS_PC(X_raw, y, MAF, nPC=3)
	######################
	# ### test
	# X_raw = X_test
	# y = y_test
	# MAF = 0.005
	# nPC = 3
	######################
	n = size(X_raw)[1]
	l = size(X_raw)[2]
	### filter loci by MAF
	LOC = collect(1:l)[ ((mean(X_raw, dims=1) ./ 2) .> MAF)[:] .& ((mean(X_raw, dims=1) ./ 2) .< (1.0-MAF))[:] ]
	X = X_raw[:, LOC]
	l = size(X)[2]
	### PCA
	m = mean(X, dims=2)
	Z = (X .- m) ./ std(X, dims=2) #each row (individual) is standardized across loci
	U, s, V = LinearAlgebra.svd(Z)
	PC_scores = U * LinearAlgebra.Diagonal(s)
	PC_axes = V
	### essentially equivalent!:
	### sum((U * LinearAlgebra.Diagonal(s) * V' .- Z ) .< 1.0*10^(-10)) == (size(Z, 1) * size(Z, 2))
	### initialize output arrays
	EFF = []
	PVAL = []
	progress_bar = ProgressMeter.Progress(l, dt=1, desc="GWA Progress: ",  barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow) #progress bar
	for i in 1:l
		x = hcat((zeros(n) .+ 1), PC_scores[:, 1:nPC], X[:,i])
		b_hat = (inv(x' * x)) * (x' * y)
		y_hat = x * b_hat
		y_err = y - y_hat
		# Vy_err = var(y_err)
		Vy_err = 1/n * (y_err' * y_err)
		b_ster = sqrt(Vy_err * inv(x' * x)[nPC+2, nPC+2])
		# t_val = abs(mean(y) - b_hat[2]) / b_ster
		t_val = abs(b_hat[nPC+2]) / b_ster
		t_distn = Distributions.TDist(n-1)
		p_val = 2*(1.00 - Distributions.cdf(t_distn, t_val))
		append!(EFF, b_hat[nPC+2])
		append!(PVAL, p_val)
		# println("$i : $p_val")
		ProgressMeter.update!(progress_bar, i)
	end
	# LOD = -log.(10, PVAL)
	LOD = -log.(10, PVAL .+ 1.0e-20)
	return(LOC, EFF, PVAL, LOD)
end
# l = @layout [a; b]
# p1 = plot(LOD)
# p2 = plot(LOD_noPC)
# plot(p1, p2, layout=l)
end #end of GWAS_PC module
