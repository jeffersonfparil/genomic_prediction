####################################################################
###                                                              ###
###       Convert quantinemo2_geno_sim_01a_OUTPUTPARSE.jl        ###
### genotype output files into a single synchronized pileup file ###
###         to simulation Population-level Pool-seq              ###
###                                                              ###
####################################################################

################
#### inputs ####
################
#### fname_list_txt = text file listing the filenames of the genotype data output of quantinemo2_geno_sim_01a_OUTPUTPARSE.jl
#### nLoci = number of loci in the genotype files
#### nAlleles = number of alleles per loci in the genotype files
#################
#### outputs ####
#################
#### (1) Synchronized pileup file (nLoci x nPop+3) (NO HEADER)

### load libraries
using DelimitedFiles
using Statistics


### load the text file listing the genotype data and the other input paramteres
fname_list_txt = ARGS[1]
nLoci = parse(Int64, ARGS[2])
nAlleles = parse(Int64, ARGS[3])
### test
# nLoci = 2000
# nAlleles = 5
# fname_list_txt = "fname_list.txt"
geno_list = DelimitedFiles.readdlm(fname_list_txt)
nPop = length(geno_list)

### iterate across populations
SYNC = Array{String}(undef, nLoci, nPop)
for i in 1:nPop
    f = geno_list[i]
    dat = DelimitedFiles.readdlm(f, ',')
    sums = Statistics.sum(dat[:,4:end], dims=2) #generate a vector of sums across alleles and loci
    sums = reshape(sums, (nAlleles, nLoci))' #reshape into a matrix of m=nLoci and n=5 alleles: A-T-C-G-DEL
    sums = hcat(sums, sums[:,end]) #add the N allele such that we have A-T-C-G-N-DEL
    sums[:,5] .= 0 #set 0 to the whole N column
    ### concatenate allele counts per locus
    sync_column = Array{String}(undef, nLoci)
    for i in 1:length(sync_column)
        sync_column[i] = join(string.(sums[i,:]), ":")
    end
    SYNC[:,i] = sync_column
end

### output sync file
CHR = reshape(DelimitedFiles.readdlm(geno_list[1], ',')[:,1], (nAlleles, nLoci))'[:,1] #extract chromosome info
POS = reshape(DelimitedFiles.readdlm(geno_list[1], ',')[:,2], (nAlleles, nLoci))'[:,1] #extract position info
REF = repeat(["A"], inner=nLoci) #set the reference allele as just all A for simplicity eh!
OUT = hcat(CHR, POS, REF, SYNC)
DelimitedFiles.writedlm(string(split(geno_list[1], "_p1_GENO.csv")[1], "_ALLPOP_GENO.sync"), OUT, '\t')
