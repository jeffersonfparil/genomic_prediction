#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_cdf.h>

///////////////
//			 //
// constants //
//			 //
///////////////
const int STEP_SIZE = 1000; //extension size of our character pointers pointer pointing to the lines we read through th stream

/////////////////////
//				   //
// data structures //
//				   //
/////////////////////
struct PHE { //whole phenotype data description
	char *name;		//phenotype name
	double sd;		//standard deviation of the phenotype
	double min;		//minimum value of the phenotype
	double max;		//maximum value of the phenotype
	double *perc;	//array of upper limits of the 1st, 2nd, 3rd, ... ,(npools-1)th percentile of the inverse quantile-normalized phenotypic values (ranges 0.0 to 1.0)
	double *quan;	//array of upper limits of the 1st, 2nd, 3rd, ... ,(npools-1)th percentile of the untransformed pehnotypic values
};

struct SNP { //individual SNP data description
	char *chrom;
	int pos;
	int *allele_counts; //array of allele counts without fixed size so that it can be dynamically allocated according to pool size
	double *allele_freq; //array of allele frequencies without fixed size so that it can be dynamically allocated according to pool size
};

/////////////////////////
//					   //
// function prototypes //
//					   //
/////////////////////////
struct PHE load_pheno(char *p_filename, int npools);
char **load_sync(char *filename, int *length);
int *int_array_duplicate(int *int_array, int size);
double *double_array_duplicate(double *double_array, int size);
int int_array_sum(int *int_array, int size);
double double_array_sum(double *double_array, int size);
struct SNP parse_snp_loc_freq(char *p_snp, int npools);

/////////////////////////////
//						   //
// main execution function //
//						   //
/////////////////////////////
//argc is the number of elements entered where the first one is the name of the program and the proceeding elements are the input file and/or arguments
//argv is the character pointer for the filename i.e. strings are usually stored as character pointers
int main(int argc, char *argv[]) {
	
	//check if input files are supplied
	if (argc ==1) {
		printf("No input files supplied!\n");
		exit(1);
	}

	//load phenotype file
	struct PHE phenotype = load_pheno(argv[2], 5);
	//check if we have successfully inserted the phenotype file data into the PHE file structure
	printf("Phenotype name: %s\n", phenotype.name);
	printf("Standard deviation: %f\n", phenotype.sd);
	printf("Minimum: %f\n", phenotype.min);
	printf("Maximum: %f\n", phenotype.max); 
	printf("Percentiles of the inverse quantile-normalized phenotypic values\n");
		for (int j=0; j<4; j++){
			printf("%f:", phenotype.perc[j]);
		}
		printf("\n");
	printf("Quantiles of the untransformed phenotypic values\n");
		for (int j=0; j<4; j++){
			printf("%f:", phenotype.quan[j]);
		}
		printf("\n");

	//load sync file
	int length = 0; //counter of how many lines we have in the file
	char **pp_snp = load_sync(argv[1], &length); //load each line into memory, generate an array of pointers pointing to each line and output a pointer of that array of pointers
	printf("Your file has %i lines.\n", length);

	//count the number of columns and pools in the sync file
	int ncols = 0;
	char *p_head, *p_line_1;
	p_head = strdup(pp_snp[0]);
	while((p_line_1 = strsep(&p_head, "\t")) != NULL) {
		ncols ++;
	}
	int npools = ncols - 3; //number of pools
	printf("We have %i columns.\n", ncols);
	printf("We have %i pools.\n", npools);

	//iterate over SNPs
	char **pp_chrom = (char **)malloc(length * sizeof(char *)); //character pointer to point to the chromosome identity of each SNP
	char **pp_pos = (char **)malloc(length * sizeof(char *)); //chacter pointer to point to the position per chromosome of each SNP	
	// char *p_chrom = (char *)malloc(1 * sizeof(char));
	// char *p_pos = (char *)malloc(1 * sizeof(char));
	for (int i=0; i<length; i++){
		//parse each snp: store chrom and pos info + extract the major allele frequencies across pools
		struct SNP ALLELE = parse_snp_loc_freq(pp_snp[i], npools);
			//check output of line above/////////////////////////////////////////////////
			printf("CHROM ID: %s and POS: %i\n", ALLELE.chrom, ALLELE.pos);
			//check allele counts
			int *DUPLICATE = int_array_duplicate(ALLELE.allele_counts, npools);
			for (int t=0; t<npools; t++){printf("%i:", DUPLICATE[t]);}
			printf("\n");
			//check output of int_array_sum
			int SUM_TEST_COUNTS = int_array_sum(ALLELE.allele_counts, npools);
			printf("The sum of the allele counts is: %i\n", SUM_TEST_COUNTS);
			//check allele frequencies
			for (int t=0; t<npools; t++){printf("%f:", ALLELE.allele_freq[t]);}
			printf("\n");
			//check output of double_array_sum
			double SUM_TEST_FREQ = double_array_sum(ALLELE.allele_freq, npools);
			printf("The sum of the allele frequencies is: %f\n", SUM_TEST_FREQ);
			////////////////////////////////////////////////////////////////////////////
		//determine the size of each pool and multiple with respective allele frequency to get
	 }

	return 0;
}

//////////////////////////
//						//
// function definitions //
//						//
//////////////////////////
struct PHE load_pheno(char *p_filename, int npools) {
	//define variables
	char *name;
	double sd;
	double min;
	double max;
	double *perc = (double *)malloc((npools-1)*sizeof(double));
	double *quan = (double *)malloc((npools-1)*sizeof(double));
	struct PHE OUT;

	//open the file
	FILE *p_pheno = fopen(p_filename, "r");

	//create a buffer for temporarily storing each line
	char buffer[1000];
	char *buffer_duplicate;
	char *buffer_token;

	//read file line by line
	int line_counter=0;
	while (fgets(buffer, 1000, p_pheno)) {
		buffer_duplicate = strdup(buffer);
		if(line_counter<4) { //for the first 4 items i.e. name, sd, min and max
			int word_counter=0;
			while((buffer_token = strsep(&buffer_duplicate, "=;[]"))!=NULL){
				if(line_counter==0 & word_counter==1){
					// printf("Name: %s\n", buffer_token);
					name = strdup(buffer_token);
				} else if(line_counter==1 & word_counter==1){
					// printf("SD: %f\n", atof(buffer_token));
					sd = atof(buffer_token);
				} else if(line_counter==2 & word_counter==1){
					// printf("MIN: %f\n", atof(buffer_token));
					min = atof(buffer_token);
				} else if(line_counter==3 & word_counter==1){
					// printf("MAX: %f\n", atof(buffer_token));
					max = atof(buffer_token);
				}
				word_counter++;
			}
		} else if (line_counter==4) { //for the percentile array (transformed)
			int word_counter=0;
			int array_element_counter=0;
			while((buffer_token = strsep(&buffer_duplicate, "=;[,]"))!=NULL){
				if (word_counter>1 & word_counter<(npools+1)) {//capture the 3rd to (npool+1)th array element
					perc[array_element_counter] = atof(buffer_token);
					// printf("ARRAY_PERC_TEST: %f\n", perc[array_element_counter]);
					array_element_counter++;
				}
				word_counter++;
			}
		} else if (line_counter==5) { //for the quantile array (untransformed)
			int word_counter=0;
			int array_element_counter=0;
			while((buffer_token = strsep(&buffer_duplicate, "=;[,]"))!=NULL){
				if (word_counter>1 & word_counter<(npools+1)) {//capture the 3rd to (npool+1)th array element
					quan[array_element_counter] = atof(buffer_token);
					// printf("ARRAY_QUAN_TEST: %f\n", quan[array_element_counter]);
					array_element_counter++;
				}
				word_counter++;
			}
		}
		line_counter++;
	}
	//dummy output
	OUT.name = strdup(name);
	OUT.sd = sd;
	OUT.min = min;
	OUT.max = max;
	OUT.perc = (double *)malloc((npools-1) * sizeof(double));
	OUT.quan = (double *)malloc((npools-1) * sizeof(double));
	OUT.perc = double_array_duplicate(perc, npools-1);
	OUT.quan = double_array_duplicate(quan, npools-1);

	return OUT;
}

char **load_sync(char *p_filename, int *p_length) {
	FILE *p_sync = fopen(p_filename, "r");

	//if the file cannot be openned
	if (!p_sync) {
		printf("Cannot open the sync file, %s for reading.\n", p_filename);
		return NULL;
	}

	//define array length
	int array_length = 0;

	//generate the pointer of our array of pointers by initially allocating memory of 100 character pointers
	char **pp_lines = NULL;

	//create a buffer (character array) to temporary store the lines with over-estimated size
	char buffer[2000];
	int i = 0; //counter for where to add the new line into the character pointers pointer
	while (fgets(buffer, 2000, p_sync)){
		//if our pplines if full extend with realloc
		if (i==array_length) {
			array_length += STEP_SIZE;
			char **pp_newlines = realloc(pp_lines, array_length * sizeof(char *));
			//what if we ran out of memory?
			if (!pp_newlines) {
				printf("Cannot reallocate! We've ran out of memory! Dang!\n");
				exit(1);
			}
			pp_lines = pp_newlines;
		}
		//trim off newline character
		buffer[strlen(buffer)-1] = '\0';
		//get length of the buffer
		int line_length = strlen(buffer);
		//define the character pointer (line) to hold the line from the buffer with its exact size plus a newline character
		char *p_line = (char *)malloc((line_length + 1) * sizeof(char));
		//copy the line from the buffer (buffer) to the line pointer (pline)
		strcpy(p_line, buffer);
		//attach the character pointer (pline) pointing to the line to the character pointer pointer (pplines)
		pp_lines[i] = p_line;
		//iterate
		i++;
	}
	*p_length = i; //set length to the value of the counter i
	return pp_lines;
}

int *int_array_duplicate(int *int_array, int size){
	int *out = malloc(size * sizeof(int));
	memcpy(out, int_array, size * sizeof(int));
	return(out);
}

double *double_array_duplicate(double *double_array, int size){
	double *out = malloc(size * sizeof(double));
	memcpy(out, double_array, size * sizeof(double));
	return(out);
}

int int_array_sum(int *int_array, int size){
	int out = 0;
	for (int i=0; i<size; i++){
		out = out + int_array[i];
	}
	return out;
}

double double_array_sum(double *double_array, int size){
	double out = 0.0;
	for (int i=0; i<size; i++){
		out = out + double_array[i];
	}
	return out;
}

struct SNP parse_snp_loc_freq(char *p_snp, int npools) {
	//define variables
	char *p_line;
	char *p_token;
	int col_counter;
	char *p_chrom;
	int pos;
	int pool_counter;
	int A[npools];
	int T[npools];
	int C[npools];
	int G[npools];
	int N[npools];
	int DEL[npools];
	int *ALLELE_COUNTS = malloc(npools * sizeof(int)); //major allele for which the tests will be based on (most of the SNPs are assumed to be biallelic)
	double *ALLELE_FREQ = malloc(npools * sizeof(double)); //major allele for which the tests will be based on (most of the SNPs are assumed to be biallelic)
	int sum_allele; //for testing which is the major allele
	int sum_new; //for testing which is the major allele
	char *p_count_per_pool;
	char *p_token_per_pool;
	struct SNP OUT;
	OUT.allele_counts = (int *)malloc(npools * sizeof(int)); //set the size
	OUT.allele_freq = (double *)malloc(npools * sizeof(double)); //set the size
	int allele_sum;

	//copy the row of snp data
	p_line = strdup(p_snp);
	//iterate across tab-delimited columns
	col_counter = 0;
	while((p_token = strsep(&p_line, "\t")) != NULL){
		// printf("Col%i:%s\n", col_counter, p_token);
		if (col_counter == 0){ //chromosome column
			p_chrom = malloc(strlen(p_token) * sizeof(char)); 
			p_chrom = strdup(p_token);
		}
		// if (col_counter == 0){p_chrom = p_token;} //chromosome column
		else if (col_counter == 1){pos = atoi(p_token);} //position per chromosome column
		else if (col_counter >= 3){ //allele counts per pool
			p_count_per_pool = strdup(p_token);
			//iterate across allele counts per pool delimited by ":"
			pool_counter = 0;
			while ((p_token_per_pool = strsep(&p_count_per_pool, ":")) != NULL) {
				if(pool_counter == 0) {A[col_counter-3] = atoi(p_token_per_pool);}
				else if(pool_counter == 1) {T[col_counter-3] = atoi(p_token_per_pool);}
				else if(pool_counter == 2) {C[col_counter-3] = atoi(p_token_per_pool);}
				else if(pool_counter == 3) {G[col_counter-3] = atoi(p_token_per_pool);}
				else if(pool_counter == 4) {N[col_counter-3] = atoi(p_token_per_pool);}
				else if(pool_counter == 5) {DEL[col_counter-3] = atoi(p_token_per_pool);}
				// printf("TOKEN_PER_POOL: %s\n", p_token_per_pool);
				pool_counter++;
			}
		}
		col_counter++;
	}
	
	//determine the major allele
	sum_allele = 0;
	for (int j=0; j<6; j++) { //find the sum of allele counts per allele: A,T,C,G,N, & DEL
		switch (j) {
			case 0: //A allele
				sum_new = 0;
				for (int k=0; k<npools; k++){sum_new += A[k];}
				if (sum_new >= sum_allele) {
					sum_allele = sum_new;
					// for (int l=0; l<npools; l++){ALLELE[l] = A[l];}
					ALLELE_COUNTS = int_array_duplicate(A, npools);
				}
				break;
			case 1: //T allele
				sum_new = 0;
				for (int k=0; k<npools; k++){sum_new += T[k];}
				if (sum_new >= sum_allele) {
					sum_allele = sum_new;
					ALLELE_COUNTS = int_array_duplicate(T, npools);
				}
				break;
			case 2: //C allele
				sum_new = 0;
				for (int k=0; k<npools; k++){sum_new += C[k];}
				if (sum_new >= sum_allele) {
					sum_allele = sum_new;
					ALLELE_COUNTS = int_array_duplicate(C, npools);
				}
				break;
			case 3: //G allele
				sum_new = 0;
				for (int k=0; k<npools; k++){sum_new += G[k];}
				if (sum_new >= sum_allele) {
					sum_allele = sum_new;
					ALLELE_COUNTS = int_array_duplicate(G, npools);
				}
				break;
			case 4: //N allele
				sum_new = 0;
				for (int k=0; k<npools; k++){sum_new += N[k];}
				if (sum_new >= sum_allele) {
					sum_allele = sum_new;
					ALLELE_COUNTS = int_array_duplicate(N, npools);
				}
				break;
			case 5: //DEL allele
				sum_new = 0;
				for (int k=0; k<npools; k++){sum_new += DEL[k];}
				if (sum_new >= sum_allele) {
					sum_allele = sum_new;
					ALLELE_COUNTS = int_array_duplicate(DEL, npools);
				}
				break;
		}

		// printf("%s\n", pp_snp[i]);
	}
	//convert allele counts into allele frequencies summing to 1.00 across pools
	allele_sum = int_array_sum(ALLELE_COUNTS, npools);
	for (int i=0; i<npools; i++){
		ALLELE_FREQ[i] = (ALLELE_COUNTS[i]*1.0)/allele_sum; //multiple by 1.0, an explicit double precision number to prevent from translting into int type
	}
	// free(ALLELE_COUNTS);

	// OUTPUT
	OUT.chrom = p_chrom;
	OUT.pos = pos;
	OUT.allele_counts = int_array_duplicate(ALLELE_COUNTS, npools);
	OUT.allele_freq = double_array_duplicate(ALLELE_FREQ, npools);
	return OUT;
}