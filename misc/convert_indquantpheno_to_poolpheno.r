# #!/usr/bin/env/ Rscript

### Convert individual plant quantitative  phenotypes and pooling specifications into per pool phenotypes

args = commandArgs(trailing=TRUE)

dat = read.csv(args[1]) # FORMAT: POPULA, HERBI, PLANT, RESIS, POOL
# # test:
# dat = read.csv("[2]EXP19_2popx4her_quantindivipheno.csv")
attach(dat)
for (i in levels(POPUL)){
  for (j in levels(HERBI)){
    sub = subset(subset(dat, POPUL==i), HERBI==j)
    OUT = aggregate(sub$RESIS ~ sub$POOL, FUN=mean)
    write.table(OUT, file=paste0(i, j, "_y.csv"), sep=",", row.names=FALSE, col.names=FALSE)
  }
}
