#!/bin/bash

##########################################################
### SNP distribution across the Lolium genome assembly ###
##########################################################

GENOME_FASTA=/data/Lolium/Genomics/SEQUENCES/DNA/REFERENCE_GENOMES/Reference.fasta
SNP_SYNC=/data/Lolium/Population_Genetics/2018November_Ryegrass_Collection/VCF/Lolium2019_10X_30PHRED_filtered.sync

DIR=/data/Lolium/Quantitative_Genetics/GWAS_GP_2019_SE_Australia/GP/FULL_DATA_NO_CV
cd $DIR

grep "^>" ${GENOME_FASTA} | sort > GENOME_LOCI.info
sed -i 's/>//g' GENOME_LOCI.info
cut -f1-3 ${SNP_SYNC} > SYNC_LOCI.info

Rscript SNP_distribution_stats_plots.sh
### Output:
# SYNC_SNP_COVERAGE_Lolium2019_10X.svg
