#!/bin/bash

###############################################
###   ARABIDOPSIS THALIANA 2029 GENOTYPES   ###
###           GPAS MODELLING AND            ###
### ABC OPTIMIZATION OF RESOURCE ALLOCATION ###
###############################################


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### DEFINE GLOBAL VARIABLES
DIR=/data/Lolium/Quantitative_Genetics/LOLSIM_2019/arabidopsis_GPAS_ABC
SRC_DIR=/data/Lolium/Softwares
GEN_PRED_SRC_DIR=/data/Lolium/Softwares/genomic_prediction/src
sudo apt install datamash
julia
using Pkg
Pkg.add(PackageSpec(url="https://github.com/jeffersonfparil/GWAlpha.jl.git", rev="master"))
using GWAlpha
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### SETUP SOFTWARES
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
echo -e "SETUP SOFTWARES"
echo -e "$(date)"
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
cd ${SRC_DIR}
# download the softwares
wget http://s3.amazonaws.com/plink2-assets/alpha2/plink2_linux_x86_64.zip
wget http://s3.amazonaws.com/plink1-assets/plink_linux_x86_64_20200121.zip
wget http://mouse.cs.ucla.edu/emma/emma_1.1.2.tar.gz
wget http://csg.sph.umich.edu//kang/emmax/download/emmax-intel-binary-20120210.tar.gz
wget https://data.broadinstitute.org/alkesgroup/BOLT-LMM/downloads/BOLT-LMM_v2.3.4.tar.gz
wget https://github.com/genetics-statistics/GEMMA/releases/download/0.98.1/gemma-0.98.1-linux-static.gz
wget https://cnsgenomics.com/software/gcta/bin/gcta_1.93.0beta.zip
git clone https://github.com/bulik/ldsc.git
wget http://dougspeed.com/wp-content/uploads/ldak.4.9.zip
pip install scipy pandas numpy bitarray nose cython pybedtools --user
# extract or install
unzip plink2_linux_x86_64.zip
unzip plink_linux_x86_64_20200121.zip
unzip ldak.4.9.zip

R CMD INSTALL emma_1.1.2.tar.gz
tar -xvzf emmax-intel-binary-20120210.tar.gz
tar -xvzf BOLT-LMM_v2.3.4.tar.gz
gunzip gemma-0.98.1-linux-static.gz; chmod +x gemma-0.98.1-linux-static
unzip gcta_1.93.0beta.zip
pip3 install limix
# cleanup
rm plink2_linux_x86_64.zip
rm plink_linux_x86_64_20200121.zip
rm emma_1.1.2.tar.gz
rm emmax-intel-binary-20120210.tar.gz
rm BOLT-LMM_v2.3.4.tar.gz
rm gcta_1.93.0beta.zip
rm ldak.4.9.zip
# install some R packages
sudo apt install libcurl4-openssl-dev libssl-dev
Rscript -e "install.packages('ggmap')"
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### SETUP DATASETS
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
echo -e "SETUP DATASETS"
echo -e "$(date)"
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
cd ${DIR}
mkdir LOCATION_DATA
mkdir GENOTYPE_DATA
mkdir PHENOTYPE_DATA
mkdir POPULATION_STRUCTURE_DATA
mkdir NUMPY_ARRAY_DATA
mkdir MISC
# scp files from a unimelb vm

# move files into their respective directories
mv  k2029_accessions_coord.csv LOCATION_DATA/
mv *.bed GENOTYPE_DATA/ ### biallelic genotype binary file
mv *.bim GENOTYPE_DATA/ ### *.bed file's corresponding loci info
mv *.ped GENOTYPE_DATA/ ### standard pedigree and genotype headerless tab-delimited file
mv *.map GENOTYPE_DATA/ ### *.ped file's corresponding loci info
mv *.mibs* POPULATION_STRUCTURE_DATA/ ### identity-by-state matrix headerless and tab-delimited file
cp *.*fam POPULATION_STRUCTURE_DATA/ ### *.bed file's corresponding family info + phenotype data
mv *.*fam PHENOTYPE_DATA/ ### *.bed file's corresponding family info + phenotype data
mv *.npy NUMPY_ARRAY_DATA/ ### numpy arrays
mv *.* MISC/ ### unclassified data
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### SANITY CHECKS
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
echo -e "SANITY CHECK OF THE DATASET"
echo -e "$(date)"
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
### before we do our population clustering, GPAS modelling and ABC optimization
# assess individual sample data
cd ${DIR}/PHENOTYPE_DATA
echo -e '
  dat = read.table("k2029.fam", header=FALSE, na.string=-9)
  str(dat)
  colnames(dat) = c("Family_ID", "Within_Family_ID", "Within_Family_Sire_ID", "Within_Family_Dam_ID", "Sex", paste0("Phenotype_", 1:(ncol(dat)-5)))
  print(summary(dat))
  print(length(unique(dat$Family_ID)))
  print(length(unique(dat$Within_Family_ID)))
  print(sum(!is.na(dat$Phenotype_1)))
  print(sum(dat$Family_ID == dat$Phenotype_1))
  print(sum(dat$Within_Family_ID == dat$Phenotype_1))
' > assess_pheno_data.R
Rscript assess_pheno_data.R
# findings:
# (1) Family_ID == Within_Family_ID == Phenotype_1
# (2) Within_Family_Sire_ID, Within_Family_Dam_ID, and Sex are unknown
# (3) Phenotype_2, and Phenotype_3 is either 0.00 or 1.00
# (4) Phenotype_4 ranges from approximately -2 to +2
# (5) Only Phenotype 4 or the last column seem to be usable
# (6) No pedigree or family level family structure encoded in the *.fam file
cut -d' ' -f 1,2,3,4,5,9 ${DIR}/PHENOTYPE_DATA/k2029.fam > ${DIR}/PHENOTYPE_DATA/k2029_1pheno.fam
# assess geographical info
cd ${DIR}/LOCATION_DATA
echo -e '
  library(ggmap)
  # load accession coordinates file with messed up coordinates
  dat = read.csv("k2029_accessions_coord.csv")
  colnames(dat) = c("ECOTYPE", "NAME", "COUNTRY", "LAT_MESS", "LON_MESS")
  # dat = dat[!duplicated(dat$ECOTYPE), ]
  dat$LAT_MESS = as.character(dat$LAT_MESS)
  dat$LON_MESS = as.character(dat$LON_MESS)
  dat = dat[!duplicated(dat$ECOTYPE), ]
  lat_locs = c("", " east side", "Buckhorn Pass", " Guntschnaberg", "Burghaun/Rhon", "Da(1)-12", "Dresden", "Erlangen", "Gullabo", "Hodja-Obi-Garm", "Isenburg/Neuwied", "Knox", "Ler", "Ripon", "RRS", "S96", "Sn(5)-1", "Tamm", "Tol", "Wil-1-Dean-Lab", "Wordsworth Trust", "Ws")
  lon_locs = c("", " nr Grasmere", "CZE", "LTU", "TJK", "UNK")
  idx_missing_coor = (dat$LAT_MESS %in% lat_locs) | (dat$LON_MESS %in% lon_locs)
  dat$LAT_MESS[idx_missing_coor] = NA
  dat$LON_MESS[idx_missing_coor] = NA
  dat$LAT_MESS = as.numeric(dat$LAT_MESS)
  dat$LON_MESS = as.numeric(dat$LON_MESS)
  # load manually downloaded accession coordinates from 1001 genomes and the 1307 regional map accessions
  dat2 = read.csv("ecotypes_coordinates_1001genomes_1307regmap.csv")
  # dat2$LATITUDE = as.character(dat2$LATITUDE)
  # dat2$LONGITUDE = as.character(dat2$LONGITUDE)
  dat2 = dat2[!duplicated(dat2$ECOTYPE), ]
  dat2 = dat2[dat2$ECOTYPE %in% dat$ECOTYPE, ]
  ### merge and fill-out missing coordinates
  MERGED = merge(dat, dat2, by="ECOTYPE", all=TRUE)
  MERGED$LATITUDE[is.na(MERGED$LATITUDE)] = MERGED$LAT_MESS[is.na(MERGED$LATITUDE)]
  MERGED$LONGITUDE[is.na(MERGED$LONGITUDE)] = MERGED$LON_MESS[is.na(MERGED$LONGITUDE)]
  # save filtered and polished accession coordinates where unknown collection site sent to null lat=0, lon=0
  DATA = data.frame(ECOTYPE=MERGED$ECOTYPE, NAME=MERGED$NAME.x, COUNTRY=MERGED$COUNTRY, LATITUDE=MERGED$LATITUDE, LONGITUDE=MERGED$LONGITUDE)
  write.csv(DATA, file="k2029_accessions_coord_RECTIFIED.csv")
  # plot
  map_coor = c(min(DATA$LONGITUDE), min(DATA$LATITUDE), max(DATA$LONGITUDE), max(DATA$LATITUDE))
  map = get_map(location=map_coor, source="stamen", maptype="watercolor", crop=FALSE)
  ggmap(map) +
  geom_point(aes(x=LONGITUDE, y=LATITUDE), data=DATA, alpha=0.50)
  ggsave(filename="k2029_accessions_coord_FILTERED.png")
' > accession_locations.R
Rscript accession_locations.R
# findings:
# (1) Accessions spans the globe at temperate/sub-temperate regions
# (2) Some accessions have unknown site of collection
# (3) Western Europe qualitatively by ocular inspection seem to have been disproportionately sampled
# assess raw genotype data
NLOCI=$(wc -l ${DIR}/GENOTYPE_DATA/k2029.bim | cut -d' ' -f1)
echo -e "k2029.bim has $NLOCI loci!"
# finding:
# (1) 10,709,466 loci
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### FILTER GENOTYPE DATA
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
echo -e "FILTER GENOTYPE DATA"
echo -e "$(date)"
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
cd $DIR
# bed filtering by MAF=0.01 with plink 2.0 (we can use plink 1.9 but I just can't be bothered right now... 2020-01-30)
MAF=0.01
${SRC_DIR}/plink2 --bed ${DIR}/GENOTYPE_DATA/k2029.bed \
                  --bim ${DIR}/GENOTYPE_DATA/k2029.bim \
                  --fam ${DIR}/PHENOTYPE_DATA/k2029_1pheno.fam \
                  --snps-only \
                  --maf ${MAF} \
                  --threads 12 \
                  --make-bed \
                  --out ${DIR}/k2029_MAF0.01_SNPS
# OUTPUTS:
# (1) ${DIR}/k2029_MAF0.01_SNPS.bed
# (1) ${DIR}/k2029_MAF0.01_SNPS.bim
# (1) ${DIR}/k2029_MAF0.01_SNPS.fam
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### GENERATE KINSHIP MATRICES
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
echo -e "GENERATE KINSHIP MATRICES"
echo -e "$(date)"
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
cd $DIR
# EMMAX: ${DIR}/k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf
${SRC_DIR}/plink  --bfile ${DIR}/k2029_MAF0.01_SNPS \
                  --recode12 \
                  --output-missing-genotype 0 \
                  --transpose \
                  --out ${DIR}/k2029_MAF0.01_SNPS_transposed
${SRC_DIR}/emmax-kin-intel64 -v -s -d 10 ${DIR}/k2029_MAF0.01_SNPS_transposed
cp ${DIR}/k2029_MAF0.01_SNPS_transposed.aIBS.kinf ${DIR}/k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf
# GEMMA: ${DIR}/output/k2029_MAF0.01_KINSHIP_gemma.sXX.txt
${SRC_DIR}/gemma-0.98.1-linux-static -bfile k2029_MAF0.01_SNPS -gk 2 -o k2029_MAF0.01_KINSHIP_gemma
# GCTA: ${DIR}/k2029_MAF0.01_KINSHIP_gcta_sparseGRM
cp ${DIR}/k2029_MAF0.01_SNPS.fam ${DIR}/k2029_MAF0.01_SNPS.fam.bk
cut -f1 ${DIR}/k2029_MAF0.01_SNPS.fam.bk > col1.temp
cut -f5 ${DIR}/k2029_MAF0.01_SNPS.fam.bk | tail -n+2 > col5.temp
echo -e "1" >> col5.temp ### dummy male to prevent: "Empty sample_subset is not currently permitted" error!
cut -f6 ${DIR}/k2029_MAF0.01_SNPS.fam.bk > col6.temp
paste col1.temp \
      col1.temp \
      col1.temp \
      col1.temp \
      col5.temp \
      col6.temp > ${DIR}/k2029_MAF0.01_SNPS.fam
rm *.temp
${SRC_DIR}/gcta_1.93.0beta/gcta64 --bfile ${DIR}/k2029_MAF0.01_SNPS \
                                  --make-grm \
                                  --autosome \
                                  --thread-num 10 \
                                  --out ${DIR}/k2029_MAF0.01_KINSHIP_gcta
${SRC_DIR}/gcta_1.93.0beta/gcta64 --grm ${DIR}/k2029_MAF0.01_KINSHIP_gcta \
                                  --make-bK-sparse 0.05 \
                                  --thread-num 10 \
                                  --out ${DIR}/k2029_MAF0.01_KINSHIP_gcta_sparse
mv ${DIR}/k2029_MAF0.01_SNPS.fam ${DIR}/k2029_MAF0.01_SNPS.fam.gcta
mv ${DIR}/k2029_MAF0.01_SNPS.fam.bk ${DIR}/k2029_MAF0.01_SNPS.fam
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
echo -e "PERFORM GWAS WITH SIMULATED QTL AND PHENOTYPES"
echo -e "Using plink2, EMMAx, GCTA, LIMIX and GEMMA softwares"
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
cd $DIR
### QTL and phenotype simulation script
echo -e '
### load libraries
import sys
import limix
import os
import numpy as np
import dask as da
import pandas as pd
import matplotlib.pyplot as plt
plt.switch_backend("TkAgg")
### arguments
bimbam_prefix = sys.argv[1]
nQTL = int(sys.argv[2])
QTL_DIST = int(sys.argv[3]) ###0=uniform; 1=chi-square; 2=normal
### load plink files
(bim, fam, bed) = limix.io.plink.read(os.path.join(os.getcwd(), bimbam_prefix), verbose=False)
X_raw = bed.transpose()
### filter by MAF
MAF = limix.qc.compute_maf(X_raw)
X = X_raw[0:X_raw.shape[0], MAF >= 0.01]
bim = bim.iloc[MAF >= 0.01, :]
### simulate QTL
idx = np.sort(np.random.randint(0, bim.shape[0], size=nQTL))
beta = np.zeros(bim.shape[0])
if QTL_DIST == 0:
  # random uniform
  beta[idx] = np.random.uniform(0, 1, size=nQTL)
elif QTL_DIST == 1:
  # chi-square
  beta[idx] = np.random.chisquare(df=2, size=nQTL)
else:
  # Gaussian
  beta[idx] = np.random.normal(0, 1, size=nQTL)

h2 = 0.5 ### 50% heritability
y_immature = X.dot(beta).compute()
Vg = np.var(y_immature)
Ve = (Vg/h2) - Vg
y = y_immature + np.random.normal(loc=0.0, scale=np.sqrt(Ve), size=X.shape[0])
# y_out = (y - np.mean(y)) / np.std(y) ### standardize
y_out = (y - minimum(y)) / (maximum(y) - minimum(y)) ### limit between 0 and 1
bim["beta"]=beta
bim.to_csv(bimbam_prefix + "_SIMULATED_QTL.csv")
np.savetxt(bimbam_prefix + "_SIMULATED_PHENOTYPE.csv", np.transpose(np.vstack((range(X.shape[0]), y_out))), delimiter=",", fmt="%1.4f")
' > simulate_QTL_and_pheno.py
### Limix GWAS script
echo -e '
### load libraries
import sys
import limix
from glimix_core.lmm import LMM
from os import getcwd
from os.path import join
from pandas_plink import get_data_folder
import numpy as np
from numpy_sugar.linalg import economic_qs_linear
import dask as da
import matplotlib.pyplot as plt
plt.switch_backend("TkAgg")
import scipy.stats as stat
import pandas as pd
import gc
### inputs
bimbam_prefix = sys.argv[1]
kinship_file = sys.argv[2]
# bimbam_prefix = "k2029_MAF0.01_SNPS"
# kinship_file = "k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf"
### load bim (loci info), fam (family ID + phenotype), and bed (genotype scores; loaded as a dask array) data
(bim, fam, bed) = limix.io.plink.read(join(getcwd(), bimbam_prefix), verbose=False)
ibs = limix.io.csv.read(kinship_file, header=False)
idx = np.arange(0, fam.shape[0], 1)[fam.loc[:, "trait"] != -9]
y = fam.loc[:, "trait"][idx].to_numpy()
X_raw = bed[0:bed.shape[0], idx].transpose()
### filter by MAF
MAF_thresh = np.min(limix.qc.compute_maf(bed.transpose()))
MAF = limix.qc.compute_maf(X_raw)
X = X_raw[0:X_raw.shape[0], MAF >= MAF_thresh]
bim = bim.iloc[MAF >= MAF_thresh, :]
K = ibs.iloc[idx, idx].to_numpy()
# limix.plot.kinship(K); plt.show()
### using QTL SCAN
out = limix.qtl.scan(X, y, "normal", K=K)
pval = out.stats.pv20.to_numpy()
lod = -np.log10(pval)
df = pd.DataFrame({"chrom":bim.iloc[:, 0].to_numpy(), "pos":bim.iloc[:, 3].to_numpy(), "pv":pval, "lod":lod})
kinship_file_split = kinship_file.split("_")
df.to_csv("limix_K" + kinship_file_split[len(kinship_file_split)-1].split(".")[0] + ".csv")
# limix.plot.manhattan(df)
# plt = limix.plot.get_pyplot()
# _ = plt.axhline(-np.log10(1e-7), color="red")
# _ = plt.ylim(2, plt.ylim()[1])
# plt.show()
' > limix_GWAS.py
### Manhattan plotting scripts
echo -e '
  ### read user inputs
  args = commandArgs(trailingOnly=TRUE)
  # args = c("plink2_PCA.PHENO1.glm.linear", "k2029_MAF0.01_SNPS_SIMULATED_QTL.csv")
  # args[1] = "emmax_Kemmax.ps"
  # args[1] = "emmax_Kgemma.ps"
  # args[1] = "gemma_lmm_Kemmax.assoc.txt"
  # args[1] = "gemma_lmm_Kgemma.assoc.txt"
  # args[1] = "gcta_Kgcta.fastGWA"
  # args[1] = "gcta_Kgcta.fastGWA"
  # args[1] = "limix_Kemmax.csv"
  # args[1] = "limix_Kgemma.csv"
  # args = c("ROI_Across_Indseq_GEMMA.assoc.txt", "ROI_Across_Indseq_SIMULATED_QTL.csv")
  # args = c("ROI_Across_Poolseq-FIXED_GWAlpha_Alphas.csv", "ROI_Across_Indseq_SIMULATED_QTL.csv")
  # args = c("ROI_Across_Poolseq_POOLSIZE_AND_PHENOTYPE-FIXED_RR_Alphas.csv", "ROI_Across_Indseq_SIMULATED_QTL.csv")
  # args = c("OUT-GWAS-Across_Poolseq-FIXED_GLMNET_Alphas.csv", "ROI_Across_Indseq_SIMULATED_QTL.csv")
  # args = c("OUT-GWAS-Within_Indseq_CLUSTER_1-FIXED_GLMNET_Alphas.csv", "ROI_Across_Indseq_SIMULATED_QTL.csv")
  names_list = unlist(strsplit(basename(args[1]), "_"))
  if ((names_list[1] == "plink2") | (names_list[1] == "emmax") | (names_list[1] == "gemma") | (names_list[1] == "gcta") | (names_list[1] == "limix")){
    software_used = names_list[1]
  } else if (names_list[length(names_list)] == "GEMMA.assoc.txt"){
    software_used = "gemma"
  } else if (names_list[length(names_list)] == "LIMIX.csv"){
    software_used = "limix"
  } else if (names_list[length(names_list)] == "Alphas.csv"){
    software_used = "GWAlpha"
  }
  # print(software_used)
  ### load bim file: including the simulated QTL effects
  bim = read.csv(args[2], header=TRUE)
  colnames(bim) = c("ID", "CHROM", "ID_AGAIN", "cM", "POS", "REF", "ALT", "IDX", "BETA_SIMULATED")
  ### read the GWAS output files
  if (software_used == "plink2"){
    dat = read.delim(args[1], header=TRUE)
    colnames(dat) = c("CHROM", "POS", "ID", "REF", "ALT", "A1", "TEST", "OBS_CT", "BETA", "SE", "T_STAT", "PVAL")
  } else if (software_used == "emmax"){
    dat = read.delim(args[1], header=FALSE)
    colnames(dat) = c("ID", "BETA", "SE", "PVAL")
    CHROM_POS = matrix(unlist(strsplit(as.character(dat$ID), "_")), nrow=nrow(dat), byrow=TRUE)
    dat$CHROM = as.numeric(CHROM_POS[, 1])
    dat$POS = as.numeric(CHROM_POS[, 2])
  } else if (software_used == "gemma"){
    dat = read.delim(args[1], header=TRUE)
    colnames(dat) = c("CHROM", "ID", "POS", "NMISS", "REF", "ALT", "AF", "BETA", "SE", "LOGLIK", "REML", "ML", "PWALD", "PLRT", "PVAL")
    if (length(dat$PVAL) == 0){
      params = optim(c(0.0, 1.0), fn=function(par){sum(-dnorm(dat$alpha, mean=par[1], sd=par[2], log=TRUE))})$par
      dat$p_score = pnorm(dat$alpha, lower.tail=TRUE, log.p=FALSE)
      dat$p_score[dat$alpha > params[1]] = pnorm(dat$alpha[dat$alpha > params[1]], mean=params[1], sd=params[2], lower.tail=FALSE, log.p=FALSE)
    }
  } else if (software_used == "gcta"){
    dat = read.table(args[1], header=TRUE)
    colnames(dat) = c("CHROM", "ID", "POS", "A1", "A2", "N", "AF1", "BETA", "SE", "PVAL")
  } else if (software_used == "limix"){
    dat = read.csv(args[1], header=TRUE)
    colnames(dat) = c("idx", "CHROM", "POS", "PVAL", "LOD")
    dat$ID = paste(dat$CHROM, dat$POS, sep="_")
  } else if (software_used == "GWAlpha"){
    dat = read.csv(args[1], header=TRUE)
    dat = droplevels(dat[complete.cases(dat), ])
    tryCatch(
        ### For Pool-GPAS output
      {
        colnames(dat) = c("idx", "CHROM", "POS", "ALLELE", "FREQ", "ALPHA", "PVAL", "LOD")
      },
      error=function(e){
        ### For Indi-GPAS output
        colnames(dat) = c("idx", "CHROM", "POS", "FREQ", "ALPHA", "PVAL", "LOD")
      }
    )
    dat$ID = paste(dat$CHROM, dat$POS, sep="_")
  }
  ### compute the -log10(PVAL)
  dat$NEGLOG10PVAL = -log10(dat$PVAL + 1e-200) ### added a small number to prevent infinities
  ### merge withe the simulated QTL dataframe by CHROM_POS ID
  DATA = merge(dat, bim[, c(1, 8, 9)], by="ID")
  ### extract the simulated QTL dataframe
  QTL_DF = DATA[DATA$BETA_SIMULATED != 0.00, ]
  ### compute the Bonferroni threshold
  THRESHOLD = -log10(0.01/nrow(DATA))
  ### calculate true positive rate (TPR) and false discovery rate (FDR)
  putative_QTL_DF = droplevels(DATA[DATA$NEGLOG10PVAL >= THRESHOLD, ])
  if (nrow(putative_QTL_DF)>0){
    ### If these are putative QTL
    n_true_QTL = nrow(QTL_DF)
    n_putative_QTL = nrow(putative_QTL_DF)
    KB = 10000 ### LB BLOCK SIZE HARDCODED TO 10 kb
    true_QTL_detected = c()
    linked_QTL_detected = c()
    for (i in 1:n_true_QTL){
      detected = putative_QTL_DF$ID[(putative_QTL_DF$CHROM == QTL_DF$CHROM[i]) & (putative_QTL_DF$POS <= (QTL_DF$POS[i] + KB)) & (putative_QTL_DF$POS >= (QTL_DF$POS[i] - KB))]
      if (length(detected)>0){
        true_QTL_detected = c(true_QTL_detected, QTL_DF$ID[i])
        linked_QTL_detected = c(linked_QTL_detected, detected)
      }
    }
    n_detected_true_QTL = length(unique(true_QTL_detected))
    n_detected_linked_to_true_QTL = length(unique(linked_QTL_detected))
    TPR = n_detected_true_QTL / n_true_QTL
    FDR = (n_putative_QTL - n_detected_linked_to_true_QTL) / n_putative_QTL
  } else {
    ### If there are no putative QTL
    TPR = 0
    FDR = 0
  }
  write.table(data.frame(MODEL=basename(args[1]), TPR=TPR, FDR=FDR), file=paste0(args[1], ".TPR_FDR.csv"), row.names=FALSE, quote=FALSE, sep=",")
  ### manhattan plotting with the simulated QTL
  COLORS = rep(RColorBrewer::brewer.pal(9, "GnBu")[c(3, 7)], times=length(unique(DATA$CHROM)))
  jpeg(paste0(args[1], ".manhattan.jpg"), quality=100, width=3000, height=800)
  layout(matrix(c(1,1,1,2), nrow=1, byrow=TRUE))
  par(cex=2)
  # plot manhattan plot
  plot(x=DATA$IDX, y=DATA$NEGLOG10PVAL, col=COLORS[DATA$CHROM], pch=20, main=args[1], xlab="Loci", ylab="-log10(p-values)")
  abline(h=THRESHOLD, lwd=1, lty=2, col="gray")
  points(x=QTL_DF$IDX, y=QTL_DF$NEGLOG10PVAL, pty=20, col="red")
  grid()
  # QQ plot (p-values are assumed to be uniformly distributed)
  # observed_pval = runif(100); observed_pval = observed_pval[order(observed_pval, decreasing=FALSE)] ### TEST
  # sort the observed p-values
  observed_pval = DATA$PVAL[order(DATA$PVAL, decreasing=FALSE)]
  # calculate the pdf of the observed p-values, i.e. the probability density for a p-value interval which corresponds to each observed p-value
  observed_pval_density = density(observed_pval, n=length(observed_pval), from=0, to=1) ### calculate the density of the observed p-values between 0 and 1 because we are dealing with p-values which are probabilities!
  # calculate the cummulative probabilities of the observed p-values based on the pdf: where Prob(pvalue) = Density(pvalue | pvalue_interval) * pvalue_interval
  observed_pval_cumprob = cumsum(observed_pval_density$y * (observed_pval_density$x[2]-observed_pval_density$x[1]))
  # calculate the expected quantiles based on the cummulative probabilities of the observed p-values
  expected_pval = qunif(p=observed_pval_cumprob, min=0, max=1) ### calculate the expected quantiles based on the observed cummulative probabilities
  # transform into -log10 scale
  observed_lod = -log10(observed_pval + 1e-200)
  expected_lod = -log10(expected_pval + 1e-200)
  # plot
  plot(x=c(min(observed_lod), max(observed_lod)), y=c(min(observed_lod), max(observed_lod)), type="n", , main="QQ Plot", xlab="Expected -log10(p-value)", ylab="Observed -log10(p-value)")
  points(x=expected_lod, y=observed_lod, type="p", pch=20, col=COLORS[2])
  lines(x=c(0, max(observed_lod)), y=c(0, max(observed_lod)), lty=2, lwd=2, col="gray")
  grid()
  dev.off()
' > manhattan_plotter.r
echo -e '#!/bin/bash\nRscript manhattan_plotter.r $1 k2029_MAF0.01_SNPS_SIMULATED_QTL.csv' > manhattan_plotter_parallelizer.sh
### GWAS script
echo -e '#!/bin/bash
  DIR=$1
  SRC_DIR=$2
  nQTL=$3
  QTL_dist=$4 ### 0=uniform(0,1); 1=Chi-square(2); and 2=Normal(0,1)
  REP=$5
  echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
  echo -e "SIMULATE QTL AND PHENOTYPE"
  echo -e "$(date)"
  echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
  python3 simulate_QTL_and_pheno.py k2029_MAF0.01_SNPS ${nQTL} ${QTL_dist}
  if [ $(ls k2029_MAF0.01_SNPS.fam.bk | wc -l) -eq 0 ]
  then
  mv k2029_MAF0.01_SNPS.fam k2029_MAF0.01_SNPS.fam.bk
  fi
  cut -f1 k2029_MAF0.01_SNPS.fam.bk > col1.temp
  cut -f1-5 k2029_MAF0.01_SNPS.fam.bk > col1to5.temp
  cut -d, -f2 k2029_MAF0.01_SNPS_SIMULATED_PHENOTYPE.csv > col6.temp
  paste col1to5.temp col6.temp > k2029_MAF0.01_SNPS.fam
  paste col1.temp col1.temp col6.temp > k2029_MAF0.01_SNPS_SIMULATED_PHENOTYPE.phe
  rm *.temp
  echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
  echo -e "PERFORM GWAS"
  echo -e "$(date)"
  echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
  #####################
  ### (1) PLINK 2.0 ###
  #####################
  echo -e "########################"
  echo -e "PLINK 2.0"
  echo -e "$(date)"
  echo -e "########################"
  # GWAS with PC covariate:${DIR}/plink2_PCA.PHENO1.glm.linear
  time \
  ${SRC_DIR}/plink2 --bed ${DIR}/k2029_MAF0.01_SNPS.bed \
                    --bim ${DIR}/k2029_MAF0.01_SNPS.bim \
                    --fam ${DIR}/k2029_MAF0.01_SNPS.fam \
                    --nonfounders \
                    --snps-only \
                    --maf 0.01 \
                    --threads 12 \
                    --glm \
                    --pca \
                    --out ${DIR}/plink2_PCA
  ################
  ### (2) EMMA ###
  ################ 2020-01-30: TOOOOOOOOOOOOOO MEMORY INTESIVE IN R!!!!!
  #################
  ### (3) EMMAx ###
  #################
  echo -e "########################"
  echo -e "EMMAx"
  echo -e "$(date)"
  echo -e "########################"
  cd ${DIR}
  for K in k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf output/k2029_MAF0.01_KINSHIP_gemma.sXX.txt
  do
  # K=${DIR}/k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf
  # K=${DIR}/output/k2029_MAF0.01_KINSHIP_gemma.sXX.txt
  LAST=$(echo $K | tr "_" "\n" | tail -n1)
  Kmethod=$(echo ${LAST%.*.*})
  echo $Kmethod
  ${SRC_DIR}/emmax-intel64 -d 10 \
                           -t ${DIR}/k2029_MAF0.01_SNPS_transposed \
                           -p ${DIR}/k2029_MAF0.01_SNPS_SIMULATED_PHENOTYPE.phe \
                           -k ${K} \
                           -o ${DIR}/emmax_K${Kmethod}
  done
  # ################# PERFORMING GEMMA SEPARATELY WHEN ALL QTL AND PHENOTYPES HAVE BEEN SIMULATED 2020-02-27
  # ### (4) GEMMA ###
  # #################
  # echo -e "########################"
  # echo -e "GEMMA"
  # echo -e "$(date)"
  # echo -e "########################"
  # cd ${DIR}
  # # for K in k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf output/k2029_MAF0.01_KINSHIP_gemma.sXX.txt
  # # do
  # # # K=${DIR}/k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf
  # # # K=${DIR}/output/k2029_MAF0.01_KINSHIP_gemma.sXX.txt
  # # LAST=$(echo $K | tr "_" "\n" | tail -n1)
  # # Kmethod=$(echo ${LAST%.*.*})
  # # echo $Kmethod
  # # ### univariate linear mixed model using the kinship matrix covariate
  # # ${SRC_DIR}/gemma-0.98.1-linux-static  -bfile k2029_MAF0.01_SNPS \
  # #                                       -k ${K} \
  # #                                       -lmm 4 \
  # #                                       -silence \
  # #                                       -o gemma_lmm_K${Kmethod}
  # # # ### Bayesian sparse linear mixed model with the kinship matrix as the covariate ### ERROR: GSL error failed to allocate space for block data in init_source.c at line 40 errno 8
  # # # ${SRC_DIR}/gemma-0.98.1-linux-static  -bfile k2029_MAF0.01_SNPS \
  # # #                                       -k ${K} \
  # # #                                       -bslmm 2 \
  # # #                                       -o gemma_bslmm_K${Kmethod}
  # # mv output/gemma_lmm_K${Kmethod}.assoc.txt .
  # # done
  # K=$1
  # SRC_DIR=$2
  # # K=${DIR}/k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf
  # # K=${DIR}/output/k2029_MAF0.01_KINSHIP_gemma.sXX.txt
  # LAST=$(echo $K | tr "_" "\n" | tail -n1)
  # Kmethod=$(echo ${LAST%.*.*})
  # echo $Kmethod
  # ### univariate linear mixed model using the kinship matrix covariate
  # ${SRC_DIR}/gemma-0.98.1-linux-static  -bfile k2029_MAF0.01_SNPS \
  #                                       -k ${K} \
  #                                       -lmm 4 \
  #                                       -silence \
  #                                       -o gemma_lmm_K${Kmethod}
  # # ### Bayesian sparse linear mixed model with the kinship matrix as the covariate ### ERROR: GSL error failed to allocate space for block data in init_source.c at line 40 errno 8
  # # ${SRC_DIR}/gemma-0.98.1-linux-static  -bfile k2029_MAF0.01_SNPS \
  # #                                       -k ${K} \
  # #                                       -bslmm 2 \
  # #                                       -o gemma_bslmm_K${Kmethod}
  # mv output/gemma_lmm_K${Kmethod}.assoc.txt .
  ####################
  ### (5) BOLT-LMM ###
  # not really appropriate for a dataset this small [https://data.broadinstitute.org/alkesgroup/BOLT-LMM/downloads/BOLT-LMM_v2.3.4_manual.pdf]
  #################### ERROR RUNNING OUT OF RAM DURING LD SCORE ESTIMATION
  ################
  ### (6) GCTA ###
  ################
  echo -e "########################"
  echo -e "GCTA"
  echo -e "$(date)"
  echo -e "########################"
  cd $DIR
  mv ${DIR}/k2029_MAF0.01_SNPS.fam ${DIR}/k2029_MAF0.01_SNPS.fam.bk
  mv ${DIR}/k2029_MAF0.01_SNPS.fam.gcta ${DIR}/k2029_MAF0.01_SNPS.fam
  ${SRC_DIR}/gcta_1.93.0beta/gcta64 --bfile ${DIR}/k2029_MAF0.01_SNPS \
                                    --grm-sparse ${DIR}/k2029_MAF0.01_KINSHIP_gcta_sparse \
                                    --fastGWA-mlm \
                                    --pheno ${DIR}/k2029_MAF0.01_SNPS_SIMULATED_PHENOTYPE.phe \
                                    --threads 10 \
                                    --out ${DIR}/gcta_Kgcta
  mv ${DIR}/k2029_MAF0.01_SNPS.fam ${DIR}/k2029_MAF0.01_SNPS.fam.gcta
  mv ${DIR}/k2029_MAF0.01_SNPS.fam.bk ${DIR}/k2029_MAF0.01_SNPS.fam
  #################
  ### (7) LIMIX ###
  #################
  echo -e "########################"
  echo -e "LIMIX"
  echo -e "$(date)"
  echo -e "########################"
  for K in ${DIR}/k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf ${DIR}/output/k2029_MAF0.01_KINSHIP_gemma.sXX.txt
  do
    echo $K
    python3 limix_GWAS.py k2029_MAF0.01_SNPS ${K}
  done
  ##############################
  ### (8) MANHATTAN PLOTTING ###
  ##############################
  echo -e "########################"
  echo -e "MAHATTAN PLOTTING"
  echo -e "$(date)"
  echo -e "########################"
  cd $DIR
  chmod +x manhattan_plotter_parallelizer.sh
  # parallel ./manhattan_plotter_parallelizer.sh {} ::: plink2_PCA.PHENO1.glm.linear emmax_Kemmax.ps emmax_Kgemma.ps gemma_lmm_Kemmax.assoc.txt gemma_lmm_Kgemma.assoc.txt gcta_Kgcta.fastGWA gcta_Kgcta.fastGWA limix_Kemmax.csv limix_Kgemma.csv
  parallel ./manhattan_plotter_parallelizer.sh {} ::: plink2_PCA.PHENO1.glm.linear emmax_Kemmax.ps emmax_Kgemma.ps gcta_Kgcta.fastGWA gcta_Kgcta.fastGWA limix_Kemmax.csv limix_Kgemma.csv
  #############################################################################
  ### (9) CLEAN-UP AND MOVE SIMULATED DATA AND GWAS OUTPUT INTO A DIRECTORY ###
  #############################################################################
  echo -e "########################"
  echo -e "CLEAN-UP"
  echo -e "$(date)"
  echo -e "########################"
  mkdir Athaliana_SIM_GWAS_${nQTL}QTL_${QTL_dist}DIST_${REP}REP/
  head -n1 plink2_PCA.PHENO1.glm.linear.TPR_FDR.csv > Athaliana_SIM_GWAS_${nQTL}QTL_${QTL_dist}DIST_${REP}REP/MERGED_TPR_FDR.csv
  # for i in plink2_PCA.PHENO1.glm.linear emmax_Kemmax.ps emmax_Kgemma.ps gemma_lmm_Kemmax.assoc.txt gemma_lmm_Kgemma.assoc.txt gcta_Kgcta.fastGWA gcta_Kgcta.fastGWA limix_Kemmax.csv limix_Kgemma.csv
  for i in plink2_PCA.PHENO1.glm.linear emmax_Kemmax.ps emmax_Kgemma.ps gcta_Kgcta.fastGWA limix_Kemmax.csv limix_Kgemma.csv
  do
    echo $i
    mv ${i}* Athaliana_SIM_GWAS_${nQTL}QTL_${QTL_dist}DIST_${REP}REP/
    tail -n+2 Athaliana_SIM_GWAS_${nQTL}QTL_${QTL_dist}DIST_${REP}REP/${i}.TPR_FDR.csv >> Athaliana_SIM_GWAS_${nQTL}QTL_${QTL_dist}DIST_${REP}REP/MERGED_TPR_FDR.csv
  done
  mv  k2029_MAF0.01_SNPS_SIMULATED_QTL.csv Athaliana_SIM_GWAS_${nQTL}QTL_${QTL_dist}DIST_${REP}REP/
  mv  k2029_MAF0.01_SNPS_SIMULATED_PHENOTYPE.csv Athaliana_SIM_GWAS_${nQTL}QTL_${QTL_dist}DIST_${REP}REP/
' > arabSim.sh
chmod +x arabSim.sh
### EXECUTE: simulate GWAS using 2,029 Arabidopsis thaliana genotype data
### NOTE: Prequisite is to run the SNP data filtering and Kinship matrices calculations above!
cd $DIR
for nQTL in 5 10 100
do
  for QTL_dist in 0 1 2
  do
    for REP in $(seq 1 10)
    do
      # nQTL=10
      # QTL_dist=0
      # REP=1
      echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
      echo ${nQTL} - ${QTL_dist} - ${REP}
      ./arabSim.sh ${DIR} ${SRC_DIR} ${nQTL} ${QTL_dist} ${REP}
      echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    done
  done
done
#################
### (4) GEMMA ### PARALLEL COMPUTATION AFTER SIMULATING ALL THE DIFFERENT TRAIT ARCHITECTURES WITH RERPLICATIONS
################# (This is done because gemma in the provided executable downloadable file does not use multiple threads but it can if we re-build it with options specific for parallel processing)
echo -e "########################"
echo -e "GEMMA"
echo -e "$(date)"
echo -e "########################"
cd ${DIR}
echo -e '#!/bin/bash
  PHEN_DIR=$1
  K=$2
  # SRC_DIR=/data/Lolium/Softwares/
  SRC_DIR=/data/cephfs/punim0543/jparil/Softwares/ #### on spartan 20200303
  module load R/3.5.2-GCC-6.2.0 ## on spartan 20200303
  # ### TEST
  # PHEN_DIR=/data/Lolium/Quantitative_Genetics/LOLSIM_2019/arabidopsis_GPAS_ABC/Athaliana_SIM_GWAS_5QTL_0DIST_1REP
  # K=${DIR}/k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf
  # K=${DIR}/output/k2029_MAF0.01_KINSHIP_gemma.sXX.txt
  ### setting up the bim-bam files
  cp k2029_MAF0.01_SNPS.bed ${PHEN_DIR}/
  cp k2029_MAF0.01_SNPS.bim ${PHEN_DIR}/
  cp k2029_MAF0.01_SNPS.fam ${PHEN_DIR}/
  cd ${PHEN_DIR}
  cut -f1 k2029_MAF0.01_SNPS.fam > col1.temp
  cut -f1-5 k2029_MAF0.01_SNPS.fam > col1to5.temp
  cut -d, -f2 k2029_MAF0.01_SNPS_SIMULATED_PHENOTYPE.csv > col6.temp
  paste col1to5.temp col6.temp > k2029_MAF0.01_SNPS.fam
  rm *.temp
  cd -
  ### identify the kinship matrix
  LAST=$(echo $K | tr "_" "\n" | tail -n1)
  Kmethod=$(echo ${LAST%.*.*})
  echo $Kmethod
  ### univariate linear mixed model using the kinship matrix covariate
  cd ${PHEN_DIR}/
  ${SRC_DIR}/gemma-0.98.1-linux-static  -bfile k2029_MAF0.01_SNPS \
                                        -k ../${K} \
                                        -lmm 4 \
                                        -o gemma_lmm_K${Kmethod}
  cd -
  mv ${PHEN_DIR}/output/gemma_lmm_K${Kmethod}.assoc.txt ${PHEN_DIR}/
  rm -R ${PHEN_DIR}/output/
  ### manhattan plot and append TPR and FDR results
  for i in ${PHEN_DIR}/gemma_lmm_Kemmax.assoc.txt ${PHEN_DIR}/gemma_lmm_Kgemma.assoc.txt
  do
    echo $i
    Rscript manhattan_plotter.r ${i} ${PHEN_DIR}/k2029_MAF0.01_SNPS_SIMULATED_QTL.csv
    tail -n+2 ${i}.TPR_FDR.csv >> ${PHEN_DIR}/MERGED_TPR_FDR.csv
  done
' > gemma_parallelizer.sh
chmod +x gemma_parallelizer.sh
nohup time parallel ./gemma_parallelizer.sh {} k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf ::: $(ls | grep Athaliana_SIM_GWAS_) &
mv nohup.out nohup_Kemmax.txt
nohup time parallel ./gemma_parallelizer.sh {} output/k2029_MAF0.01_KINSHIP_gemma.sXX.txt ::: $(ls | grep Athaliana_SIM_GWAS_) &
mv nohup.out nohup_Kgemma.txt
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
echo -e "DEFINE POPULATIONS BY GEODESIC DISTANCE-BASED K-MEANS CLUSTERING"
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
cd ${DIR}
echo -e '
  ### arguments to define the region of interest (ROI)
  args = commandArgs(trailingOnly=TRUE)
  lat_min = as.numeric(args[1])
  lat_max = as.numeric(args[2])
  lon_min = as.numeric(args[3])
  lon_max = as.numeric(args[4])
  # # eg: Europe with a bit of North Africa and West Asia
  # lat_min = 25
  # lat_max = 72
  # lon_min = -22
  # lon_max = 90
  ### load libraries
  library(geodist)
  library(doParallel)
  library(cluster)
  library(sp)
  library(rgdal) #sudo apt install libgdal-dev
  library(raster)
  # library(spatstat)  # Used for the dirichlet tessellation function
  # library(maptools)  # Used for conversion from SPDF to ppp
  # library(tmap) # for plotting
  # library(ggplot2) # for plotting
  ### load the fam file and the corresponding kinship matrix
  fam = read.delim("k2029_MAF0.01_SNPS.fam", header=FALSE)
  colnames(fam) = c("ECOTYPE", "ECOTYPE2", "MALE_ID", "FEMALE_ID", "SEX", "PHENO")
  fam$SORTER = 1:nrow(fam) ### for sorting so that we have the same order as the kinship file after all the filtering
  MAT_kinship = as.matrix(read.delim("k2029_MAF0.01_KINSHIP_emmax.aIBS.kinf", sep="\t", header=FALSE))
  ### load the coordinates file
  DF_coordinates = read.csv("k2029_accessions_coord_RECTIFIED.csv")
  ### merge the fam and coordinates by ECOTYPE ID
  MERGED_ORDER = merge(fam, DF_coordinates, by="ECOTYPE")
  MERGED_ORDER = MERGED_ORDER[order(MERGED_ORDER$SORTER, decreasing=FALSE), ]
  ### save and load the world map (using the 110-m resolution)
  WORLD = tryCatch(
    rgdal::readOGR("ne_110m_land.shp"),
    error = function(e){
      system("wget https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/110m/physical/ne_110m_land.zip")
      system("unzip ne_110m_land.zip")
      rgdal::readOGR("ne_110m_land.shp")
    }
    )
  ### shape files:
  #(1) .shp (main file and mandatory) - spatial vector data (not plain text)
  #(2) .shx (mandatory) shape index position (not plain text)
  #(3) .dbf (mandatory) database file (plain-ish text)
  #(4) .prj (optional) cooridinate and projection systems associated with the 3 main files: .shp, .shx, and .bdf (plain-ish text)
  #(5) .cpg (optional) code page file describes the encoding of the main file .shp, e.g. UTF-8  (plain text)
  ### crop ROI from the world map
  ROI = raster::crop(WORLD, raster::extent(lon_min, lon_max, lat_min, lat_max))
  ### crop the data to include only the ROI
  excluded = MERGED_ORDER[!((MERGED_ORDER$LATITUDE >= lat_min) & (MERGED_ORDER$LATITUDE <= lat_max) & (MERGED_ORDER$LONGITUDE >= lon_min) & (MERGED_ORDER$LONGITUDE <= lon_max)), ]
  MERGED_ORDER = droplevels(MERGED_ORDER[(MERGED_ORDER$LATITUDE >= lat_min) & (MERGED_ORDER$LATITUDE <= lat_max) & (MERGED_ORDER$LONGITUDE >= lon_min) & (MERGED_ORDER$LONGITUDE <= lon_max), ])
  ### visualize result of cropping
  # png("World_map_region_of_interest_and_complement.png", width=2000, height=700)
  svg("World_map_region_of_interest_and_complement.svg", width=15, height=5)
  par(mfrow=c(1,2))
  plot(WORLD, main=paste0("ROI: ", lat_min, " to ", lat_max, "N and ", lon_min, " to ", lon_max, "E"))
  points(x=MERGED_ORDER$LONGITUDE, y=MERGED_ORDER$LATITUDE, pch=20, col="blue")
  rect(lon_min, lat_min, lon_max, lat_max, lty=2, lwd=2, border="gray")
  legend("bottomright", legend=paste0("Number of accessions = ", nrow(MERGED_ORDER)))
  plot(WORLD, main="EXCLUDED")
  points(x=excluded$LONGITUDE, y=excluded$LATITUDE, pch=20, col="red")
  rect(lon_min, lat_min, lon_max, lat_max, lty=2, lwd=2, border="gray")
  legend("bottomright", legend=paste0("Number of accessions = ", nrow(excluded)))
  dev.off()
  ### define the kinship and distance matrices
  MAT_kinship = MAT_kinship[MERGED_ORDER$SORTER, MERGED_ORDER$SORTER]
  MAT_distances = geodist(x=data.frame(LAT=MERGED_ORDER$LATITUDE, LON=MERGED_ORDER$LONGITUDE))
    ### MISC: trying to account for boundaries defined by bodies of water and mountains via inverse square law thing-o
    # d_transformed = d_orig + (d_to_nearest_flat_ground^2 / d_orig)
    # # where:
    # #   * d_transformed = penalized distance
    # #   * d_orig = original purely coordinates-based geodesic distance
    # #   * d_to_nearest_flat_ground = distance from the midpoint of the line connecting the two points of interest to the nearest flat ground (non-water and non-mountain)
  MAT_combined = scale(cbind(MAT_kinship, MAT_distances), center=TRUE, scale=TRUE) ### centered, scaled, and ...
  MAT_combined = (MAT_combined - min(MAT_combined)) / (max(MAT_combined) - min(MAT_combined)) ### restricted between 0 and 1
  # par(mfrow=c(1,2))
  # hist(MAT_kinship)
  # hist(MAT_distances)
  ################################################################################
  ### KMEANS CLUSTERING AND PLOTTING FOR EACH OF THE THREE "DISTANCE" MATRICES ###
  ################################################################################
  func_kclustering = function(center, mat, name="NONE"){
    k = tryCatch(kmeans(mat, centers=round(center)),
                error=function(e){NA})
    if (prod(is.na(k)) == 0){
      saveRDS(k, file=paste0("kclustering-k", center, "-", name, ".rds"))
    }
    return(0)
  }
  DISTANCES_MATRICES = list(1.00-MAT_kinship, MAT_distances, MAT_combined)
  DISTANCES_MATRICES_NAMES = c("1_Minus_Kinship", "Geographical_Distances", "Combined_Kinship_Geographic_Distances")
  for (i in 1:length(DISTANCES_MATRICES)){
    # i = 1
    mat = DISTANCES_MATRICES[[i]]
    ### parallel k-means clustering
    nCores = 12
    registerDoParallel(nCores)
    foreach(x=1:nrow(mat)) %dopar% func_kclustering(center=x, mat=mat, name=DISTANCES_MATRICES_NAMES[i])
    ### merge output
    kclustering_rds_fnames = system(paste0("ls | grep kclustering | grep ", DISTANCES_MATRICES_NAMES[i]), intern=TRUE)
    CENTER = c()
    WITHINSS = c()
    BETWEENSS = c()
    SILHOUETTE = c()
    pb = txtProgressBar(min=0, max=length(kclustering_rds_fnames), style=3); counter = 1
    for (f in kclustering_rds_fnames){
      # f = kclustering_rds_fnames[1]
      k = readRDS(f)
      sil = silhouette(k$cluster, dist=mat)
      CENTER = c(CENTER, as.numeric(unlist(strsplit(gsub(paste0("-", DISTANCES_MATRICES_NAMES[i], ".rds"), "", f), "-k"))[2]))
      WITHINSS = c(WITHINSS, mean(k$withinss))
      BETWEENSS = c(BETWEENSS, mean(k$betweenss))
      SILHOUETTE = tryCatch(c(SILHOUETTE, summary(sil)$avg.width), error=function(e){c(SILHOUETTE, NA)})
      setTxtProgressBar(pb, counter); counter = counter + 1
    }
    close(pb)
    DF_KMEANS = data.frame(CENTER, WITHINSS, BETWEENSS, SILHOUETTE)
    DF_KMEANS = DF_KMEANS[order(DF_KMEANS$CENTER, decreasing=FALSE), ]
    DF_KMEANS$WB_RATIO = DF_KMEANS$WITHINSS / (DF_KMEANS$WITHINSS + DF_KMEANS$BETWEENSS)
    saveRDS(DF_KMEANS, file=paste0("DF_KMEANS-", DISTANCES_MATRICES_NAMES[i], ".rds"))
    # DF_KMEANS = readRDS(paste0("DF_KMEANS-", DISTANCES_MATRICES_NAMES[i], ".rds"))

    ### plot the decrease in the mean within cluster sum of squares scaled by the total mean sum of squares
    COLORS = paste0(RColorBrewer::brewer.pal(3,"Set1"), "80")
    # png(paste0("Clustering-", DISTANCES_MATRICES_NAMES[i], ".png"), width=1000, height=700)
    svg(paste0("Clustering-", DISTANCES_MATRICES_NAMES[i], ".svg"), width=10, height=7)
    par(mar=c(5,5,5,5))
    plot(x=DF_KMEANS$CENTER, y=DF_KMEANS$WB_RATIO, type="p", pch=20,
        col=COLORS[1], xlab="Number of Clusters", ylab="log10(Within SS / (Within SS + Between SS))", main=DISTANCES_MATRICES_NAMES[i])
    ### model the curve as y = a + (b/x^c) and plot
    mod_kmeans = function(par, df=data.frame(x=DF_KMEANS$CENTER, y=DF_KMEANS$WB_RATIO), predict=FALSE){
      a = par[1]
      b = par[2]
      c = par[3]
      x = df$x
      y = df$y
      pred = a + (b/(x^c))
      loss = sum((y - pred)^2)
      if (predict==TRUE){
        out = pred
      } else {
        out = loss
      }
      return(out)
    }
    par = optim(fn=mod_kmeans, par=c(1,1,1), df=data.frame(x=DF_KMEANS$CENTER, y=DF_KMEANS$WB_RATIO), method="Nelder-Mead")$par
    pred = mod_kmeans(par=par, df=data.frame(x=DF_KMEANS$CENTER, y=DF_KMEANS$WITHINSS), predict=TRUE)
    lines(x=DF_KMEANS$CENTER, y=pred, lty=2, lwd=1, col="black")
    ### compute and plot the first derivative of this curve
    a = par[1]
    b = par[2]
    c = par[3]
    newx = seq(min(DF_KMEANS$CENTER), max(DF_KMEANS$CENTER), length=10000)
    dy = -b*c*(newx^(-c-1))
    # d2y = -b*c*(-c-1)*(newx^(-c-2))
    # d3y = -b*c*(-c-1)*(-c-2)*(newx^(-c-3))
    par(new=TRUE)
    plot(x=newx, y=dy, type="l", lty=1, lwd=2, col=COLORS[2], xaxt="n", yaxt="n", xlab="", ylab="")
    axis(side=4)
    mtext(side=4, text="First Derivative", line=2)
    ### define the selected number of clusters as the number of clusters correstponding to the mean first derivative value
    ### let us think of it as setting the zero derivative at this mean(dy/dx) point!
    # CUT_OFF = mean(DF_KMEANS$WB_RATIO)
    CUT_OFF = mean(dy)
    # idx = DF_KMEANS$CENTER[DF_KMEANS$WB_RATIO >= CUT_OFF]
    idx = c(1:length(newx))[dy <= CUT_OFF]
    k_mean_dy = round(newx[idx[length(idx)]])
    ### plot this selected number of clusters
    abline(v=k_mean_dy, lty=2, lwd=2, col=COLORS[2])
    text(x=k_mean_dy, y=(abs(max(dy))-abs(min(dy)))/2, labels=paste0("Number of\nClusters\nSelected = ", k_mean_dy), pos=4, offset=1)
    ### cluster!
    k = tryCatch(readRDS(paste0("kclustering-k", k_mean_dy, "-", DISTANCES_MATRICES_NAMES[i], ".rds")),
                error=function(e){readRDS(paste0("kclustering_output/kclustering-k", k_mean_dy, "-", DISTANCES_MATRICES_NAMES[i], ".rds"))})
    table(k$cluster)
    legend("right", legend=c("Within Clusters Sum of Squares / Total Sum of Squares", "First Derivative (dy/dx)", "Selected number of clusters corresponding to the mean dy/dx"), pch=c(20, NA, NA), lty=c(2, 1, 2), col=c(COLORS[1], COLORS[2], COLORS[2]))
    dev.off()
    ### mapping into geographical locations
    ### add the clustering ID to the filtered, merged and sorted dataframe based on k2029 bim-bam files
    GROUPING = data.frame(CLUSTER=k$cluster, ECOTYPE=MERGED_ORDER$ECOTYPE, LAT=MERGED_ORDER$LATITUDE, LON=MERGED_ORDER$LONGITUDE)
    ### convert into a SpatialPointsDataFrame object
    GROUPING = sp::SpatialPointsDataFrame(coords=cbind(GROUPING$LON, GROUPING$LAT), data=data.frame(CLUSTER=GROUPING$CLUSTER, ECOTYPE=GROUPING$ECOTYPE), proj4string=CRS("+init=epsg:28992"))
    ### define the bounding box as the bounding box defined in the ROI
    GROUPING@bbox = ROI@bbox
    ### plot
    # png("Region_of_interest_map_and_clustering.png", width=1500, height=1000)
    svg(paste0("Region_of_interest_map_and_clustering-", DISTANCES_MATRICES_NAMES[i], ".svg"), width=10, height=7)
    plot(ROI, main=paste0("Region of Interest\n", lat_min, "°N to ", lat_max, "°N and ", lon_min, "°E to ", lon_max, "°E"))
    if (length(unique(GROUPING$CLUSTER)) > 26){
      COL = rainbow(length(unique(GROUPING$CLUSTER)))
    } else {
      COL = pals::alphabet2(length(unique(GROUPING$CLUSTER)))
    }
    points(GROUPING, col=COL[GROUPING$CLUSTER], pch=19)
    legend("bottomright", legend=paste0("Number of Clusters = ", length(unique(GROUPING$CLUSTER))))
    dev.off()
    ### save clustering based on geodesic distances
    if (DISTANCES_MATRICES_NAMES[i] == "Geographical_Distances"){
      saveRDS(k, file="Geodesic_based_clustering.rds")
      OUT = merge(as.data.frame(GROUPING), MERGED_ORDER, by="ECOTYPE")
      OUT = data.frame(CLUSTER=OUT$CLUSTER, ECOTYPE=OUT$ECOTYPE, SORTER=OUT$SORTER, NAME=OUT$NAME, COUNTRY=OUT$COUNTRY, LATITUDE=OUT$LATITUDE, LONGITUDE=OUT$LONGITUDE)
      write.table(OUT, file="Geodesic_based_clustering.csv", sep=",", row.names=FALSE, quote=FALSE)
    }
  }
' > k_means_clustering_ROI.R
Rscript k_means_clustering_ROI.R 25 72 -22 90
mkdir kclustering_output
mv kclustering_output-k* kclustering_output/
### OUTPUT:
### (1) Geodesic_based_clustering.csv (CLUSTER,ECOTYPE,SORTER,NAME,COUNTRY,LATITUDE,LONGITUDE)
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
echo -e "THIN-OUT AND GROUP THE BIM-BAM FILES THEN CONVERT INTO MATRICES"
echo -e "AND SIMULATE QTL AND PHENOTYPES"
echo -e "AND FINALLY PERFORM GPAS"
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
cd ${DIR}
echo -e "##################"
echo -e "Thin-out bim-bam"
echo -e "##################"
tail -n+2 Geodesic_based_clustering.csv | cut -d, -f2 > ROI_filter_k2029.txt ### include only the 1,030 populations
cut -f2 k2029_MAF0.01_SNPS.bim | sed '1~2d' | sed '1~2d' > ROI_filter_snps.txt ### remove every 2nd and 3rd SNPs
${SRC_DIR}/plink2 --bed k2029_MAF0.01_SNPS.bed \
                  --bim k2029_MAF0.01_SNPS.bim \
                  --fam k2029_MAF0.01_SNPS.fam \
                  --keep-fam ROI_filter_k2029.txt \
                  --extract ROI_filter_snps.txt \
                  --snps-only \
                  --maf 0.001 \
                  --threads 12 \
                  --make-bed \
                  --out ROI_ALL
### OUTPUTS:
### (1) ROI_ALL.bed
### (2) ROI_ALL.bim
### (3) ROI_ALL.fam
echo -e "##################"
echo -e "Convert into vcf"
echo -e "##################"
NCLUSTERS=$(tail -n+2 Geodesic_based_clustering.csv | cut -d, -f1 | sort -g | uniq | tail -n1)
for i in $(seq 1 $NCLUSTERS)
do
  echo $i
  if [ $i -lt 10 ]
  then
    cluster_number=0${i}
  else
    cluster_number=${i}
  fi
  grep "^${i}," Geodesic_based_clustering.csv | cut -d, -f2 > filter.temp
  ${SRC_DIR}/plink2 --bed ROI_ALL.bed \
                    --bim ROI_ALL.bim \
                    --fam ROI_ALL.fam \
                    --keep-fam filter.temp \
                    --threads 12 \
                    --recode vcf \
                    --out ROI_CLUSTER_${cluster_number}
done
rm filter.temp
rm *.log
### OUTPUTS:
### (1-14) ROI_CLUSTER_${i}.vcf (for i in 01 02 03 ... 14)
echo -e "##################"
echo -e "Convert into csv"
echo -e "##################"
echo -e '### CONVERT VCF INTO nxl MATRIX (csv format)
  ### NOTE: assumes all loci are biallelic
  ### load libraries
  using GeneticVariation
  using DelimitedFiles
  using Statistics
  using LinearAlgebra
  using DataFrames
  using ProgressMeter
  ### inputs
  fname = ARGS[1] ### vcf file name
  n = parse(Int, ARGS[2]) ### number of individuals
  l = parse(Int, ARGS[3]) ### number of loci
  MAF = parse(Float64, ARGS[4]) ### minimum allele frequency ### ONLY USED IF PC1_AND_K == true
  out_fname = string(split(fname, ".vcf")[1], ".csv") ### filename of the output genotype matrix in csv format
  if isfile(out_fname)
    rm(out_fname)
  end
  ### output array
  OUT = convert(Array{Int64,2}, zeros(n, l))
  ### parse vcf into an nxl array
  ### open vcf stream
  vcf = GeneticVariation.VCF.Reader(open(fname, "r"))
  # RECORD = [VCF.Record("20\t14370\trs6054257\tG\tA\t29\tPASS\tNS=3;DP=14;AF=0.5;DB;H2\tGT:GQ:DP:HQ\t0|0:48:1:51,51\t1|0:48:8:51,51")]
  ### open sync stream
  io = open(out_fname, "a")
  ### iterate across loci
  pb = Progress(l, 1, "Converting vcf to Julia Array...", 30); counter = [1]
  for i in vcf
    # RECORD[1] = i
    # i = RECORD[1]
    chr = GeneticVariation.VCF.chrom(i)
    pos = GeneticVariation.VCF.pos(i)
    ref = GeneticVariation.VCF.ref(i)
    alt = GeneticVariation.VCF.alt(i)[1]
    gen = vcat(GeneticVariation.VCF.genotype(i)...)
    OUT[:, counter[1]] = reshape(parse.(Int, vcat(split.(gen, "/")...)), 2, n)[1,:]
    ProgressMeter.update!(pb, counter[1]); counter[1] = counter[1] + 1
  end
  close(io)
  close(vcf)
  ### write out
  DelimitedFiles.writedlm(out_fname, OUT, ",")
' > vcf2csv.jl
l=$(cat ROI_ALL.bim | wc -l)
MAF=${MAF}
echo -e '#!/bin/bash
n=$(echo $(head -n11 $1 | tail -n1 | awk "{print NF}") - 9 | bc)
julia vcf2csv.jl $1 $n $2 $3
' > vcf2csv.sh
chmod +x vcf2csv.sh
time parallel ./vcf2csv.sh {} $l $MAF ::: $(ls -S ROI_CLUSTER_*.vcf)
### OUTPUTS:
### (1-14) ROI_CLUSTER_${i}.csv (nxl; for i in 01 02 03 ... 14)

echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
echo -e "ITERATE ACROSS:"
echo -e "   - 5, 10 & 100 QTL"
echo -e "   - 1 to 10 replications"
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
### Execution on HPC via slurm:
for VAR_nQTL in 100 10 5
do
for VAR_REP in $(seq 1 5)
do
echo -e '#!/bin/bash' > slurmer_${VAR_REP}_${VAR_nQTL}.sh
echo -e "# Partition for the job:
#SBATCH --partition=snowy
# Multithreaded (SMP) job: must run on one node and the cloud partition
#SBATCH --nodes=1
# The name of the job:
#SBATCH --job-name=\"Athalian_GPASim\"
# The project ID which this job should run under:
#SBATCH --account=\"punim0543\"
# Maximum number of tasks/CPU cores used by the job:
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
# The amount of memory in megabytes per process in the job:
#SBATCH --mem=120GB
# The maximum running time of the job in days-hours:mins:sec
#SBATCH --time=3-0:0:00
# Run the job from the directory where it was launched (default)
# The modules to load:
module load Julia/1.0.0-spartan_gcc-6.2.0
module load R/3.5.2-GCC-6.2.0
module load Python/2.7.13-GCC-6.2.0-bare
module load GSL/2.5-intel-2018.u4
module load parallel/20181222-spartan_gcc-6.2.0.lua
module load datamash
# Execute
REP=$VAR_REP
nQTL=$VAR_nQTL
DIR=/data/cephfs/punim0543/jparil/arabidopsis_GPAS_ABC
GEN_PRED_SRC_DIR=/data/cephfs/punim0543/jparil/Softwares/genomic_prediction/src
SRC_DIR=/data/cephfs/punim0543/jparil/Softwares/genomic_prediction/misc
cd $DIR
\${SRC_DIR}/Athaliana_GPAS_ABC_1_simulate_GPAS.sh \$nQTL \$REP \$DIR \$GEN_PRED_SRC_DIR
### INPUTS
### (1) nQTL               ### number of QTL to simulate
### (2) REP                ### replication ID number
### (3) DIR                ### directory where the k2029 data are located (i.e. root folder of the Athaliana GPAS ABC validations/simulations)
### (4) GEN_PRED_SRC_DIR   ### directory where the genomic_prediction.git source codes are located
### OUTPUTS:
### (1) CROSS_VALIDATION_OUTPUT_ROI_Within_Indiseq_INPUT.csv
### (2) CROSS_VALIDATION_OUTPUT_ROI_Across_Indiseq_INPUT.csv
### (3) CROSS_VALIDATION_OUTPUT_ROI_Within_Poolseq_INPUT.csv
### (4) CROSS_VALIDATION_OUTPUT_ROI_Across_Poolseq_INPUT.csv
" >> slurmer_${VAR_REP}_${VAR_nQTL}.sh
chmod +x slurmer_${VAR_REP}_${VAR_nQTL}.sh
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
echo -e slurmer_${VAR_REP}_${VAR_nQTL}.sh
echo -e "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
sbatch slurmer_${VAR_REP}_${VAR_nQTL}.sh
done
done
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
