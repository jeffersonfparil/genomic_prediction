### INPUT
args = commandArgs(trailingOnly=TRUE)
fname = args[1]

### TEST
# fname="/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE_GENETICS-combine_SNP_and_transcript_data/EXP00_GWAlpha_2pops_4her/alphas/IF_pheno-FIXED_GWAlpha_Alphas.csv"
# fname="/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE_GENETICS-combine_SNP_and_transcript_data/EXP00_GWAlpha_2pops_4her/alphas/IG_pheno-FIXED_GWAlpha_Alphas.csv"
# fname="/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE_GENETICS-combine_SNP_and_transcript_data/EXP00_GWAlpha_2pops_4her/alphas/IS_pheno-FIXED_GWAlpha_Alphas.csv"
# fname="/home/student.unimelb.edu.au/jparil/Documents/QUANTITATIVE_GENETICS-combine_SNP_and_transcript_data/EXP00_GWAlpha_2pops_4her/alphas/IT_pheno-FIXED_GWAlpha_Alphas.csv"

### LOAD DATA
dat = read.csv(fname)

### SORT BY SACFFOLD NAME
dat$SCAFF = as.numeric(gsub("\\|.*", "", sub("scaffold_", "", dat$CHROM)))
dat = dat[with(dat, order(SCAFF, POS)), ]
PLOT_ME = aggregate(LOD ~ SCAFF + POS, FUN=max, data=dat) ### restrict to one LOD entry per locus
PLOT_ME = PLOT_ME[with(PLOT_ME, order(SCAFF, POS)), ]
PLOT_ME$LOCUS = 1:nrow(PLOT_ME)


### PLOT
png(paste0(fname, ".png"), width=1700, height=600)
colors = c("#4eb3d3", "#a8ddb5")
plot(x=c(0, nrow(PLOT_ME)), y=c(0, max(PLOT_ME$LOD)), type="n", xlab="LOCI", ylab="-log10(pvalue)")
pb = txtProgressBar(min=0, max=length(unique(PLOT_ME$SCAFF)), style=3, width=30)
for (i in 1:length(unique(PLOT_ME$SCAFF))) {
  # i = 1
  # print(i)
  scaff = unique(PLOT_ME$SCAFF)[i]
  subdat = droplevels(subset(PLOT_ME, SCAFF==scaff))
  col = colors[(i%%2)+1]
  points(x=subdat$LOCUS, y=subdat$LOD, pch=19, col=col)
  setTxtProgressBar(pb, i)
}
close(pb)
dev.off()
