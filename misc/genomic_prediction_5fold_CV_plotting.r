library(agricolae)
dat = read.csv("CROSS_VALIDATION_5_FOLD_OUTPUT.csv")
dat = dat[dat$RMSD < 100, ]
# dat = subset(dat, MODEL != "FIXED_RR")
METRIC_NAMES = c("MEAN_DEVIANCE", "CORR", "SLOPE", "R2", "RMSD")
METRIC_LABELS = c("Mean Deviance", expression("Correlation (" ~ rho ~ ")"), "Regression Slope", expression("Coefficient of Determination (" ~ R^2 ~ ")"), "Root Mean Square Deviation (RMSD)")
for (trait in levels(dat$TRAIT)){
	# trait = "SULFOMETURON_RESISTANCE"
	sub = subset(dat, TRAIT==trait)
	sub = droplevels(sub)
	for (i in 1:length(METRIC_NAMES)){
		# i = 2
		metric = METRIC_NAMES[i]
		metric_label = METRIC_LABELS[i]
		mod = eval(parse(text=paste0("lm(", metric, " ~ MODEL, data=sub)")))
		HSD_TEST = HSD.test(mod, trt="MODEL")
		HSD = data.frame(MODEL=rownames(HSD_TEST$groups), MEAN=HSD_TEST$groups[,1], GROUP=HSD_TEST$groups[,2])
		SE = eval(parse(text=paste0("aggregate(", metric, " ~ MODEL, data=sub, FUN=function(x) sd(x)/sqrt(length(x)))")))
		colnames(SE) = c("MODEL", "SE")
		DF = merge(HSD, SE, by="MODEL")
		# if (metric == "RMSD"){
		# 	DF$MEAN = log10(DF$MEAN)
		# 	DF$SE = log10(DF$SE)
		# 	metric_label = expression(log[10] ~ "(RMSD)")
		# }
		# jpeg(paste0("BARPLOT_", trait, "_", metric, ".jpeg"), quality=100, width=1000, height=850)
		svg(paste0("BARPLOT_", trait, "_", metric, ".svg"), width=7, height=5)
		par(mar=c(4, 7, 0.5, 0.5))
		colours = RColorBrewer:::brewer.pal(9, "Pastel1")
		xlim=c(min(c(0.0, DF$MEAN - DF$SE)), max(DF$MEAN + DF$SE))
		p = barplot(DF$MEAN, col=colours, horiz=TRUE, las=1, xlim=xlim, xlab=metric_label, border=NA)
		# axis(side=2, at=p, labels=DF$MODEL, tick=FALSE, pos=0.1, srt=30)
		axis(side=2, at=p, labels=FALSE, tick=FALSE)
		text(y=p, par("usr")[1], labels=DF$MODEL, srt=0, pos=2, xpd=TRUE)
		text(x=DF$MEAN, y=p, cex=1, DF$GROUP, font=2, col=rgb(0.1, 0.1, 0.1, alpha=0.9))
		text(x=(max(DF$MEAN)-min(c(0, min(DF$MEAN))))/2, y=p, cex=1, round(DF$MEAN, 2), font=2, col=rgb(0.1, 0.1, 0.1, alpha=0.9))
		arrows(y0=p, x0=(DF$MEAN-DF$SE), x1=(DF$MEAN+DF$SE), angle=90,code=3,length=0.1, col=rgb(0.1, 0.1, 0.1, alpha=0.5))
		dev.off()
	}
}
