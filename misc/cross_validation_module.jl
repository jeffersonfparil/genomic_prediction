##################################################
###						                       ###
### Genomic Prediction Models Cross-Validation ###
###						                       ###
################################################## same module name and filename

module cross_validation_module

########################################
###									 ###
### load packages and custom modules ###
###									 ###
########################################
using Distributions
using Statistics
using LinearAlgebra
using Optim
using DataFrames
using CSV
using DelimitedFiles
using GLM
using ProgressMeter
using Plots; Plots.pyplot()
JULIA_SCRIPT_HOME = @__DIR__
# # test:
# JULIA_SCRIPT_HOME = "/data/Lolium/Softwares/genomic_prediction/src"
include(string(JULIA_SCRIPT_HOME, "/GWAS_module.jl"))
include(string(JULIA_SCRIPT_HOME, "/LS_module.jl"))
include(string(JULIA_SCRIPT_HOME, "/RR_module.jl"))
include(string(JULIA_SCRIPT_HOME, "/LASSO_module.jl"))
include(string(JULIA_SCRIPT_HOME, "/GLMNET_module.jl"))
include(string(JULIA_SCRIPT_HOME, "/sync_parsing_module.jl"))
include(string(JULIA_SCRIPT_HOME, "/filter_sync_by_MAF_module.jl"))
include(string(JULIA_SCRIPT_HOME, "/GWAlpha_LS_module.jl"))
include(string(JULIA_SCRIPT_HOME, "/GWAlpha_module.jl"))
include(string(JULIA_SCRIPT_HOME, "/RR_POOLSEQ_module.jl"))
# include(string(JULIA_SCRIPT_HOME, "/LASSO_POOLSEQ_module.jl"))
# include(string(JULIA_SCRIPT_HOME, "/GLMNET_POOLSEQ_module.jl"))
# include(string(JULIA_SCRIPT_HOME, "/EUCLIDIST_POOLSEQ_module.jl"))


############################################################################################
############################################################################################
############################################################################################
############################################################################################
############################################################################################
############################
###						 ###
### function definitions ###
###						 ###
############################

### merge populations to have common loci
### PEPPER ME WITH COMMENTS!!!!!!!!20190630
function merge_two_geno_func(GENO1, GENO2, file_type)
	geno1 = DataFrames.DataFrame(GENO1)
		DataFrames.rename!(geno1, :x1 => :CHROM, :x2 => :POS, :x3 => :ALLELE)
		geno1.CHROM = convert(Array{String}, string.(geno1[:,1]))
		geno1.POS = convert(Array{Int64}, geno1[:,2])
		geno1.ALLELE = convert(Array{String}, geno1[:,3])
	geno2 = DataFrames.DataFrame(GENO2)
		DataFrames.rename!(geno2, :x1 => :CHROM, :x2 => :POS, :x3 => :ALLELE)
		geno2.CHROM = convert(Array{String}, string.(geno2[:,1]))
		geno2.POS = convert(Array{Int64}, geno2[:,2])
		geno2.ALLELE = convert(Array{String}, geno2[:,3])
	id_pop1 = DataFrames.DataFrame(CHROM=geno1.CHROM, POS=geno1.POS, ALLELE=geno1.ALLELE)
	id_pop2 = DataFrames.DataFrame(CHROM=geno2.CHROM, POS=geno2.POS, ALLELE=geno2.ALLELE)
	id_merge = DataFrames.join(id_pop1, id_pop2, on=[:CHROM, :POS, :ALLELE])
	if file_type=="csv"
		X1_raw = DataFrames.join(geno1, id_merge, on=[:CHROM, :POS, :ALLELE])
		X2_raw = DataFrames.join(geno2, id_merge, on=[:CHROM, :POS, :ALLELE])
		X1 = convert(Array{Float64}, X1_raw[:,4:end])'
		X2 = convert(Array{Float64}, X2_raw[:,4:end])'
		return(id_merge, X1, X2)
	elseif file_type=="sync"
		return(id_merge)
	end
end

### genomic prediction iterative models
function predict_ITERATIVE_MODELS_func(X_test, loci_info, beta_trained, y_trained, X_trained)
	# ######################
	# #test:
	# X_test = X2
	# loci_info = LOCI
	# beta_trained = EFF1
	# y_trained = y1
	# X_trained = X1
	# ######################
	### data specs
	n_test = size(X_test)[1]
	n_trained = size(X_trained)[1]
	m_test = size(X_test)[2]
	m_trained = size(X_trained)[2]
	n_loci_with_effects = length(loci_info)
	### prepare predict sum of products model
	y_trained_pred = X_trained[:, loci_info] * beta_trained
	X_regress = convert(Array{Float64}, hcat(ones(n_trained), y_trained_pred)) #converting to a proper float type for the inv() function
	beta_regressor = inv(X_regress' * X_regress) * (X_regress' * y_trained)
	### predict
	y_pred_immature = X_test[:, loci_info] * beta_trained
	Y_PRED = hcat(ones(n_test), y_pred_immature) * beta_regressor
	return(Y_PRED)
end

### genomic prediction non-iterative models
function predict_NON_ITERATIVE_MODELS_func(X_test, loci_info, intercept, beta_trained)
	# # ######################
	# # #test:
	# X_test = X_test
	# loci_info = LOCI
	# intercept = INTERCEPT
	# beta_trained = EFF
	# # ######################
	### data specs
	n = size(X_test)[1]
	m = length(loci_info)
	### predict
	Y_PRED = hcat(ones(n), X_test[:, loci_info]) * vcat(intercept, beta_trained)
	return(Y_PRED)
end

### assess genomic prediction accuracy
function asses_model_accuracy_func(y_true, y_pred, PLOT=false, PLOT_ID="")
	# mean deviation of the predcited from the true phenotypic values
	MEAN_DEVIANCE = mean(abs.(y_true .- y_pred)) #in percentage unit since the units we're using is in percentage eh!
	VAR_DEVIANCE = var(abs.(y_true .- y_pred)) #in percentage unit since the units we're using is in percentage eh!
	# Pearson' product moment correlation
	CORR = cor(y_true, y_pred)
	# modeling the true phenotypic values as a function of the predicted values
	data = DataFrames.DataFrame(y_true=convert(Array{Float64}, y_true), y_pred=convert(Array{Float64}, y_pred))
	# model = GLM.fit(GLM.LinearModel, GLM.@formula(y_true ~ 0 + y_pred), data) # fixed intercept at the origin because we assume a slope of one for perfect genomic prediction accuracy
	model = GLM.fit(GLM.LinearModel, GLM.@formula(y_true ~ y_pred), data)
	if length(coef(model)[:,1]) == 1
		local INTERCEPT = 0.0
	else
		local INTERCEPT = coef(model)[1,1]
	end
	SLOPE = coef(model)[end,1]
	R2 = var(GLM.predict(model)) / var(y_true) # not sure if this is correct
	if PLOT==true
		Plots.plot([0,100], [0,100], seriestype=:line, color=:gray, xlab="Predicted", ylab="Observed") #1:1 line: perfection
		Plots.plot!(y_pred, y_true, seriestype=:scatter)
		newx = convert(Array{Float64}, collect(0:1:100))
		newy = INTERCEPT .+ (SLOPE .* newx)
		Plots.plot!([0,newx], [0,newy], seriestype=:line)
		Plots.savefig(string("Genomic_precition_accuracy_scatterplot_", PLOT_ID, ".png"))
	end
	RMSD = sqrt( (sum(y_pred .- y_true)^2)/length(y_true) ) #root mean suare deviation or RMSE (E for error)
	# output: we want percent deviance to be zerol; correlation to be 1; intercept to be zero; and slope to be 1
	out = DataFrames.DataFrame(MEAN_DEVIANCE=MEAN_DEVIANCE, VAR_DEVIANCE=VAR_DEVIANCE, CORRELATION=CORR, INTERCEPT=INTERCEPT, SLOPE=SLOPE, R2=R2, RMSD=RMSD)
	return(out)
end

#####################
###				  ###
### main function ###
###				  ###
#####################
### types of cross validations:
# level 1: (1.1) one population vs (1.2) multiple populations
# level 2: (2.1) individual geno vs (2.2) pools
# level 3
function cross_validation(ARGS)
	#############
	### input ###
	#############
	println("Loading input files")
	### csv file containing the population ID in column 1, the data resolutions (i.e. individual or pool) in column 2, the genotype data filenames in column 3, and the phenotype data filenames in the 4th and last column
	FILE_LIST_SPEC = CSV.read(ARGS[1], datarow=1)
	### csv file containing the QTL identities if present (for simulated data only or for those data we know exactly the genetic architecture of the trait)
	QTL_spec_fname = ARGS[2]
	### expected significant LD for QTL detection in kb
	LD_kb = parse(Float64, ARGS[3])
	###########
	# test:
	# FILE_LIST_SPEC = CSV.read("julia_cross_validation_input_file_list.csv", datarow=1)
	# QTL_spec_fname = "QUANTI_g140_p1_QTL_SPEC.csv"
	# LD_kb = 1
	###########
	println("Parsing input files")
	DataFrames.rename!(FILE_LIST_SPEC, :Column1=>:POP_ID, :Column2=>:RESOLUTION, :Column3=>:POOLSIZE, :Column4=>:GENO_FNAME, :Column5=>:PHENO_FNAME)

	### ouput arrays
	println("Preparing output arrays")
	MODEL = []
	POP_TRAIN = []
	POP_TEST = []
	PREDICTORS = []
	NON_ZERO_PREDICTORS = []
	MEAN_DEVIANCE = []
	VAR_DEVIANCE = []
	CORRELATION = []
	INTERCEPT = []
	SLOPE = []
	R2 = []
	RMSD = []
	### for simulated datasets
	println("Testing if QTLs are known (i.e. simulated dataset)")
	if isempty(QTL_spec_fname) != true
		qtl_spec = sort(CSV.read(QTL_spec_fname), (:chr, :pos))
		### getting the cummulative allele effect contribution
		nqtl = length(unique(string.(qtl_spec.chr, qtl_spec.pos)))
		nalleles = convert(Int, nrow(qtl_spec) / nqtl)
		allele_frac_eff = qtl_spec.eff ./ sum(qtl_spec.eff)
		qtl_frac_eff = [sum(allele_frac_eff[(((i-1)*nalleles)+1):(i*nalleles)]) for i in 1:nqtl]
		### QTL_SPEC dataframe
		QTL_SPEC = hcat(qtl_spec[qtl_spec.allele .== "A", 1:2], qtl_frac_eff)
		DataFrames.rename!(QTL_SPEC, :x1 => :qtl_frac_eff)
		QTL_SPEC.chr = [string(x) for x in QTL_SPEC.chr]
		QTL_DETECTED = []
		QTL_FRAC_EFF = []
	end
	##################################
	### WITHIN POPULATION TRAINING ###
	################################## NPOP^2 cross-validations
	println("Within population training")
	for pop_train in unique(FILE_LIST_SPEC.POP_ID)
		for pop_test in unique(FILE_LIST_SPEC.POP_ID)
			##########
			# test:
			# pop_train = "QUANTI_g140_p1"
			# pop_test = "QUANTI_g140_p3"
			##########
			RESOLUTION = intersect(FILE_LIST_SPEC[FILE_LIST_SPEC.POP_ID .== pop_train, :].RESOLUTION, FILE_LIST_SPEC[FILE_LIST_SPEC.POP_ID .== pop_test, :].RESOLUTION)
			for res in RESOLUTION
				##########
				#test:
				# res=RESOLUTION[1]
				##########
				geno_train_fname = FILE_LIST_SPEC.GENO_FNAME[(FILE_LIST_SPEC.POP_ID .== pop_train) .& (FILE_LIST_SPEC.RESOLUTION .== res)][1]
				pheno_train_fname = FILE_LIST_SPEC.PHENO_FNAME[(FILE_LIST_SPEC.POP_ID .== pop_train) .& (FILE_LIST_SPEC.RESOLUTION .== res)][1]
				geno_test_fname = FILE_LIST_SPEC.GENO_FNAME[(FILE_LIST_SPEC.POP_ID .== pop_test) .& (FILE_LIST_SPEC.RESOLUTION .== res)][1]
				pheno_test_fname = FILE_LIST_SPEC.PHENO_FNAME[(FILE_LIST_SPEC.POP_ID .== pop_test) .& (FILE_LIST_SPEC.RESOLUTION .== res)][1]
				###########################################
				###										###
				### INDIVIDUAL GENOTYPE DATA AVAILABLE? ###
				###										###
				###########################################
				if res == "individual"
					println("Individual genotype data")
					# load 0,1,2 genotype matrices of pop1 and pop2
					GENO_train = DelimitedFiles.readdlm(geno_train_fname, ',')
					GENO_test = DelimitedFiles.readdlm(geno_test_fname, ',')
					### merge the two genotype data so we start off with common SNPs and alleles
					id_merge, X_train, X_test = merge_two_geno_func(GENO_train, GENO_test, "csv")
					pheno_train = CSV.read(pheno_train_fname, delim=",")
					pheno_test = CSV.read(pheno_test_fname, delim=",")
					### preparte the phenotype data
					y_train = convert(Array{Float64}, pheno_train.y)
					y_test = convert(Array{Float64}, pheno_test.y)
					MODELS = ["GWAS", "LS", "RR", "LASSO", "GLMNET"]
					for model in MODELS
						println(string("Using the ", model, " model"))
						if model =="GWAS"
							############
							### GWAS ###
							############
							### perform GWAS to extract the alelele effects per locus
							LOCI, EFF, PVAL, LOD = GWAS_module.GWAS(X_train, y_train, 1.0/(2*length(y_train)))
							### predict using the GWAS-derive allele effects and regression of the observed with the preidcited_raw
							y_pred = predict_ITERATIVE_MODELS_func(X_test, LOCI, EFF, y_train, X_train)
						elseif model == "LS"
							##########
							### LS ###
							##########
							### perform Least-Squares Regression to extract the alelele effects per locus
							LOCI, INT, EFF, PVAL, LOD = LS_module.LS(X_train, y_train, 1.0/(2*length(y_train)))
							### predict using the GWAS-derive allele effects and regression of the observed with the preidcited_raw
							y_pred = predict_NON_ITERATIVE_MODELS_func(X_test, LOCI, INT, EFF)
						elseif model == "RR"
							##########
							### RR ###
							##########
							### perform Ridge Regression to extract the alelele effects per locus
							LOCI, INT, EFF, PVAL, LOD = RR_module.RR(X_train, y_train, 1.0/(2*length(y_train)))
							### predict using the GWAS-derive allele effects and regression of the observed with the preidcited_raw
							y_pred = predict_NON_ITERATIVE_MODELS_func(X_test, LOCI, INT, EFF)
						elseif model == "LASSO"
							#############
							### LASSO ###
							#############
							### perform LASSO Regression to extract the alelele effects per locus
							LOCI, INT, EFF, PVAL, LOD = LASSO_module.LASSO(X_train, y_train, 1.0/(2*length(y_train)))
							### predict using the GWAS-derive allele effects and regression of the observed with the preidcited_raw
							y_pred = predict_NON_ITERATIVE_MODELS_func(X_test, LOCI, INT, EFF)
						elseif model == "GLMNET"
							##############
							### GLMNET ###
							##############
							### perform GLMNET Regression to extract the alelele effects per locus
							LOCI, INT, EFF, PVAL, LOD = GLMNET_module.GLMNET(X_train, y_train, 1.0/(2*length(y_train)))
							### predict using the GWAS-derive allele effects and regression of the observed with the preidcited_raw
							y_pred = predict_NON_ITERATIVE_MODELS_func(X_test, LOCI, INT, EFF)
						end
						### assess acuracy of the genomic prediction model
						accuracy = asses_model_accuracy_func(y_test, y_pred, false)
						push!(MODEL, model) #push strings into arrays rather than append
						push!(POP_TRAIN, pop_train)
						push!(POP_TEST, pop_test)
						# push!(PREDICTORS, sum(abs.(EFF) .> 1.0e-8)) # the number of ~non-zero predictors
						push!(PREDICTORS, length(EFF))
						push!(NON_ZERO_PREDICTORS, sum(EFF .!= 0.0))
						push!(MEAN_DEVIANCE, accuracy.MEAN_DEVIANCE[1])
						push!(VAR_DEVIANCE, accuracy.VAR_DEVIANCE[1])
						push!(CORRELATION, accuracy.CORRELATION[1])
						push!(INTERCEPT, accuracy.INTERCEPT[1])
						push!(SLOPE, accuracy.SLOPE[1])
						push!(R2, accuracy.R2[1])
						push!(RMSD, accuracy.RMSD[1])
						### for simulated datasets
						if isempty(QTL_spec_fname) != true
							BONFERRONI_THRESHOLD = -log10(0.05/length(LOCI))
							PUTATIVE_QTL = id_merge[LOCI[LOD .>= BONFERRONI_THRESHOLD], :]
							# PUTATIVE_QTL = hcat(id_merge[LOCI[LOD .>= BONFERRONI_THRESHOLD], :], LOD[LOD .>= BONFERRONI_THRESHOLD]); DataFrames.rename!(PUTATIVE_QTL, :x1 => :LOD)
							QTL_DETECTED_CHROM = []
							QTL_DETECTED_POS = []
							for q_true in 1:size(QTL_SPEC)[1]
								for q_putative in 1:size(PUTATIVE_QTL)[1]
									if (QTL_SPEC.chr[q_true] == PUTATIVE_QTL.CHROM[q_putative]) & (abs(QTL_SPEC.pos[q_true] - PUTATIVE_QTL.POS[q_putative]) <= (LD_kb * 1000))
										push!(QTL_DETECTED_CHROM, PUTATIVE_QTL.CHROM[q_putative])
										push!(QTL_DETECTED_POS, PUTATIVE_QTL.POS[q_putative])
									end
								end
							end
							push!(QTL_DETECTED, length(unique(string.(QTL_DETECTED_CHROM, "_", QTL_DETECTED_POS))) / size(QTL_SPEC)[1])
						end
						### cleanup
						LOCI=nothing; INT=nothing; EFF=nothing; PVAL=nothing; LOD=nothing
						# ### plot oserved vs predicted phenotypes
						# Plots.plot(y_test, y_pred, seriestype=:scatter)
					end # end loop across individual genotype models
				#####################################
				###								  ###
				### POOL GENOTYPE DATA AVAILABLE? ###
				###								  ###
				#####################################
				##########
				#test:
				# res=RESOLUTION[2]
				# geno_train_fname = FILE_LIST_SPEC.GENO_FNAME[(FILE_LIST_SPEC.POP_ID .== pop_train) .& (FILE_LIST_SPEC.RESOLUTION .== res)][1]
				# pheno_train_fname = FILE_LIST_SPEC.PHENO_FNAME[(FILE_LIST_SPEC.POP_ID .== pop_train) .& (FILE_LIST_SPEC.RESOLUTION .== res)][1]
				# geno_test_fname = FILE_LIST_SPEC.GENO_FNAME[(FILE_LIST_SPEC.POP_ID .== pop_test) .& (FILE_LIST_SPEC.RESOLUTION .== res)][1]
				# pheno_test_fname = FILE_LIST_SPEC.PHENO_FNAME[(FILE_LIST_SPEC.POP_ID .== pop_test) .& (FILE_LIST_SPEC.RESOLUTION .== res)][1]
				##########
				# elseif split(res, '-')[1] == "pool"
			elseif res == "pool" #no pool-size indication
					println("Pool-seq dataset")
					### parse the sync genotype and phenotype files
					GENO_train = DelimitedFiles.readdlm(geno_train_fname, '\t')
					GENO_test = DelimitedFiles.readdlm(geno_test_fname, '\t')
					# sync_parsing_module.sync_parse([geno_train_fname])
					# 	X_train = DelimitedFiles.readdlm(string(split(geno_train_fname, ".")[end-1], "_ALLELEFREQ.csv"), ',')
					# sync_parsing_module.sync_parse([geno_test_fname])
					# 	X_test = DelimitedFiles.readdlm(string(split(geno_test_fname, ".")[end-1], "_ALLELEFREQ.csv"), ',')
					y_train = DelimitedFiles.readdlm(string(split(pheno_train_fname, '.')[end-1], ".csv"))[:,1] #csv pool means
					y_test = DelimitedFiles.readdlm(string(split(pheno_test_fname, '.')[end-1], ".csv"))[:,1] #csv pool means
					npools = length(y_train)
					# pool_size = parse(Int, split(res, '-')[2])
					pool_size = FILE_LIST_SPEC.POOLSIZE[FILE_LIST_SPEC.GENO_FNAME .== geno_train_fname]
					MAF = 1.0/(2 * sum(pool_size .* npools))
					### merge the two genotype data so we start off with common SNPs and alleles
					MERGED_geno_train_fname = "train_loci_subset.sync"
					MERGED_geno_test_fname = "test_loci_subset.sync"
					id_merge = merge_two_geno_func(GENO_train, GENO_test, "sync")
					CSV.write("ID_MERGE.csv", id_merge, delim="\t")
					sh_script_train = string("grep -f ID_MERGE.csv ", geno_train_fname, " > ", MERGED_geno_train_fname)
					run(`sh -c $sh_script_train`)
					sh_script_test = string("grep -f ID_MERGE.csv ", geno_test_fname, " > ", MERGED_geno_test_fname)
					run(`sh -c $sh_script_test`)
					run(`rm ID_MERGE.csv`)
					rm(string(split(MERGED_geno_train_fname, ".")[end-1], "_ALLELEFREQ.csv"), force=true)
					sync_parsing_module.sync_parse([MERGED_geno_train_fname])
						X_train = DelimitedFiles.readdlm(string(split(MERGED_geno_train_fname, ".")[end-1], "_ALLELEFREQ.csv"), ',')[:, 4:end]'
					rm(string(split(MERGED_geno_test_fname, ".")[end-1], "_ALLELEFREQ.csv"), force=true)
					sync_parsing_module.sync_parse([MERGED_geno_test_fname])
						X_test = DelimitedFiles.readdlm(string(split(MERGED_geno_test_fname, ".")[end-1], "_ALLELEFREQ.csv"), ',')[:, 4:end]'
					### find filtering index by MAF
					LOCI = collect(1:size(X_train)[2])[ filter_sync_by_MAF_module.filter_sync_by_MAF([MERGED_geno_train_fname, string(MAF)]) ]
					# MODELS = ["GWAlpha_LS", "GWAlpha_ML", "GWAlpha_RR", "GWAlpha_LASSO", "GWAlpha_GLMNET"]
					# MODELS = ["GWAlpha_LS", "GWAlpha_ML", "GWAlpha_RR"]
					MODELS = ["GWAlpha_ML", "GWAlpha_RR"]
					for model in MODELS
						println(string("Using the ", model, " model"))
						if model == "GWAlpha_LS"
							##################
							### GWAlpha_LS ###
							##################
							INPUT_ARRAY = [MERGED_geno_train_fname, pheno_train_fname, string(MAF)]
							GWAlpha_LS_module.GWAlpha_LS(INPUT_ARRAY) #input all loci where MAF filtering sets loci failing the MAF threshold to alpha=0
							GWAlpha_OUT = CSV.read(string(split(pheno_train_fname, '.')[1], "-GWAlpha_LS_Alphas.csv"))
							EFF = GWAlpha_OUT.ALPHA
							LOD = GWAlpha_OUT.LOD
							y_pred = predict_ITERATIVE_MODELS_func(X_test, LOCI, EFF, y_train, X_train)
						elseif model == "GWAlpha_ML"
							##################
							### GWAlpha_ML ###
							##################
							INPUT_ARRAY = [MERGED_geno_train_fname, pheno_train_fname, string(MAF)]
							GWAlpha_module.GWAlpha(INPUT_ARRAY) #input all loci where MAF filtering sets loci failing the MAF treshold to alpha=0
							GWAlpha_OUT = CSV.read(string(split(pheno_train_fname, '.')[1], "-GWAlpha_ML_Alphas.csv"))
							EFF = GWAlpha_OUT.ALPHA
							LOD = GWAlpha_OUT.LOD
							y_pred = predict_ITERATIVE_MODELS_func(X_test, LOCI, EFF, y_train, X_train)
						elseif model == "GWAlpha_RR"
							##################
							### GWAlpha_RR ###
							##################
							INPUT_ARRAY = [MERGED_geno_train_fname, string(split(pheno_train_fname, '.')[end-1], ".csv"), string(MAF)]
							RR_POOLSEQ_module.RR_POOLSEQ(INPUT_ARRAY) #input all loci where MAF filtering sets loci failing the MAF treshold to alpha=0
							GWAlpha_OUT = CSV.read(string(split(pheno_train_fname, '.')[1], "-GWAlpha_RR_Alphas.csv"))
							INT = GWAlpha_OUT.BETA[1]
							EFF = GWAlpha_OUT.BETA[2:end]
							LOD = GWAlpha_OUT.LOD[2:end]
							y_pred = predict_NON_ITERATIVE_MODELS_func(X_test, LOCI, INT, EFF)
						# elseif model == "GWAlpha_LASSO"
						# elseif model == "GWAlpha_GLMNET"
						# elseif model == "GWAlpha_EUCLIDIST"
						end
						### assess acuracy of the genomic prediction model
						accuracy = asses_model_accuracy_func(y_test, y_pred, false)
						push!(MODEL, model) #push strings into arrays rather than append
						push!(POP_TRAIN, pop_train)
						push!(POP_TEST, pop_test)
						# push!(PREDICTORS, sum(abs.(EFF) .> 1.0e-8)) # the number of ~non-zero predictors
						push!(PREDICTORS, length(EFF))
						push!(NON_ZERO_PREDICTORS, sum(EFF .!= 0.0))
						push!(MEAN_DEVIANCE, accuracy.MEAN_DEVIANCE[1])
						push!(VAR_DEVIANCE, accuracy.VAR_DEVIANCE[1])
						push!(CORRELATION, accuracy.CORRELATION[1])
						push!(INTERCEPT, accuracy.INTERCEPT[1])
						push!(SLOPE, accuracy.SLOPE[1])
						push!(R2, accuracy.R2[1])
						push!(RMSD, accuracy.RMSD[1])
						### for simulated datasets
						if isempty(QTL_spec_fname) != true
							BONFERRONI_THRESHOLD = -log10(0.05/length(LOCI))
							# PUTATIVE_QTL = repeat(id_merge, inner=6)[LOCI[LOD .>= BONFERRONI_THRESHOLD], :] #id_merge repeat by the number of alleles in the sync file which is 6
							PUTATIVE_QTL = repeat(id_merge, inner=6)[LOCI[LOD .>= BONFERRONI_THRESHOLD], :]
							QTL_DETECTED_CHROM = []
							QTL_DETECTED_POS = []
							QTL_DETECTED_FREF = [] #QTL fraction effect
							for q_true in 1:size(QTL_SPEC)[1]
								for q_putative in 1:size(PUTATIVE_QTL)[1]
									if (QTL_SPEC.chr[q_true] == PUTATIVE_QTL.CHROM[q_putative]) & ((QTL_SPEC.pos[q_true] - PUTATIVE_QTL.POS[q_putative]) <= LD_kb)
										# println(q_true)
										# println(q_putative)
										# push!(QTL_DETECTED_CHROM, PUTATIVE_QTL.CHROM[q_putative])
										# push!(QTL_DETECTED_POS, PUTATIVE_QTL.POS[q_putative])
										push!(QTL_DETECTED_CHROM, QTL_SPEC.chr[q_true]) 		# all QTL within the LD block
										push!(QTL_DETECTED_POS, QTL_SPEC.pos[q_true])			# which means a single QTL detected may
										push!(QTL_DETECTED_FREF, QTL_SPEC.qtl_frac_eff[q_true])	# actually contain multple causal loci in LD
									end
								end
							end
							nqtl_detected = length(unique(string.(QTL_DETECTED_CHROM, "_", QTL_DETECTED_POS)))
							push!(QTL_DETECTED, nqtl_detected / size(QTL_SPEC)[1])
							if nqtl_detected > 0
								push!(QTL_FRAC_EFF, sum(QTL_DETECTED_FREF) / nqtl_detected)
							else
								push!(QTL_FRAC_EFF, 0.0)
							end
						end
						### cleanup
						y_pred=nothing; INT=nothing; EFF=nothing; LOD=nothing
						# ### plot oserved vs predicted phenotypes
						# Plots.plot(y_test, y_pred, seriestype=:scatter)
					end
				else
					println("Stfu!")
				end
			end
		end
	end
	##########################################################
	### MERGE ALL POPULATIONS INTO ONE TRAINING POPULATION ###
	##########################################################
	println("Merge all populations into a single training population")
	### (1) Individual genotyping: subsample individuals per population
	### (2) Pool-seq: subsample individuals then create n pools per population ### NOT IMPLEMENTED!!!!
	### (3) Pool-seq: subsample individuals then create 1 pool per population

#############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	#########################################################
	### (1) IND-seq: subsample individuals per population ###
	#########################################################
	NLIBRARY = 500 #maximum number of Individual-seq libraries we can fit into a single sequencing run
	idx_fnames_individual_geno_data = [match(Regex("individual"), x)!=nothing for x in FILE_LIST_SPEC.RESOLUTION]
	fnames_individual_geno_data = FILE_LIST_SPEC.GENO_FNAME[idx_fnames_individual_geno_data]
	fnames_individual_pheno_data = FILE_LIST_SPEC.PHENO_FNAME[idx_fnames_individual_geno_data]
	nPop = length(fnames_individual_geno_data)
	sample_size = [convert(Int64, round(NLIBRARY / nPop))]

	loci_info = DelimitedFiles.readdlm(fnames_individual_geno_data[1], ',')[:,1:3]
	m = size(loci_info, 1)
	nAlleles = length(unique(loci_info[:,end]))
	nLoci = convert(Int64, m / nAlleles)
	IND_X_train = Array{Int64}(undef, sample_size[1]*nPop, m)
	IND_X_test = Array{Int64}(undef, sample_size[1]*nPop, m)
	IND_y_train = Array{Float64}(undef, sample_size[1]*nPop)
	IND_y_test = Array{Float64}(undef, sample_size[1]*nPop)
	pop_label = repeat(collect(1:nPop), inner=sample_size[1])
	for i in 1:nPop
		_start_ = ((i-1)*sample_size[1]) + 1
		_end_ = i*sample_size[1]
		X = DelimitedFiles.readdlm(fnames_individual_geno_data[i], ',')[:,4:end]' #load the genotype data nInd x mPredictors
		y = DelimitedFiles.readdlm(fnames_individual_pheno_data[i], ','; skipstart=1)[:,3] #load the transformed phenotype data ([0,1])
		n = size(X, 1)
		randomizer = sortperm(rand(Int, n))
		IND_X_train[_start_:_end_,:] = X[randomizer[1:sample_size[1]],:]
		IND_X_test[_start_:_end_,:] = X[randomizer[(end-(sample_size[1]-1)):end],:]
		IND_y_train[_start_:_end_,:] = y[randomizer[1:sample_size[1]]]
		IND_y_test[_start_:_end_,:] = y[randomizer[(end-(sample_size[1]-1)):end]]
	end

	#########################################################################################################
	### (2) POOL-seq: Pooling each population and sorting by increasing phenotypic value into a sync file ###
	#########################################################################################################
	y_df = DataFrames.DataFrame(POP=repeat(collect(1:nPop), inner=sample_size[1]), y_train=IND_y_train, y_test=IND_y_test)
	y_means = aggregate(groupby(y_df, :POP), mean)
	idx_sort_train = sortperm(y_means.y_train_mean) # sort from smallest to the larget phenotype value
	idx_sort_test = sortperm(y_means.y_test_mean) # sort from smallest to the larget phenotype value
	SYNC_X_train = Array{String}(undef, nLoci, nPop)
	SYNC_X_test = Array{String}(undef, nLoci, nPop)
	### training
	for i in idx_sort_train
		idx = y_df.POP .== i
		X_train = IND_X_train[idx, :]
		sums_train = Statistics.sum(X_train, dims=1) #generate a vector of sums across alleles and loci
		sums_train = reshape(sums_train, (nAlleles, nLoci))' #reshape into a matrix of m=nLoci and n=5 alleles: A-T-C-G-DEL
		sums_train = hcat(sums_train, sums_train[:,end]); sums_train[:,5] .= 0 #add the N allele such that we have A-T-C-G-N-DEL and set 0 to the whole N column
	    ### concatenate allele counts per locus
		sync_column_train = Array{String}(undef, nLoci)
	    for i in 1:length(sync_column_train)
			sync_column_train[i] = join(string.(sums_train[i,:]), ":")
	    end
		SYNC_X_train[:,i] = sync_column_train
	end
	### validation
	for i in idx_sort_test
		idx = y_df.POP .== i
		X_test = IND_X_test[idx, :]
		sums_test = Statistics.sum(X_test, dims=1) #generate a vector of sums across alleles and loci
		sums_test = reshape(sums_test, (nAlleles, nLoci))' #reshape into a matrix of m=nLoci and n=5 alleles: A-T-C-G-DEL
		sums_test = hcat(sums_test, sums_test[:,end]); sums_test[:,5] .= 0 #add the N allele such that we have A-T-C-G-N-DEL and set 0 to the whole N column
		### concatenate allele counts per locus
		sync_column_test = Array{String}(undef, nLoci)
		for i in 1:length(sync_column_test)
			sync_column_test[i] = join(string.(sums_test[i,:]), ":")
		end
		SYNC_X_test[:,i] = sync_column_test
	end

	###### save sync file, phenotype files as *.csv and *.py
	LOCI_INFO = hcat(reshape(loci_info[:,1], (nAlleles, nLoci))'[:,1],
		 			 reshape(loci_info[:,2], (nAlleles, nLoci))'[:,1])
	LOCI_INFO_DF = DataFrames.DataFrame(CHROM=repeat(LOCI_INFO[:,1], inner=6),
										POS=repeat(LOCI_INFO[:,2], inner=6),
										ALLELE=repeat(["A", "T", "C", "G", "N", "DEL"], outer=size(LOCI_INFO, 1)))
	## TRAINING POP
	DelimitedFiles.writedlm(
		string(split(fnames_individual_geno_data[1], "_p")[1], "_POPULATION_POOLS_TRAIN.sync"),
		hcat(LOCI_INFO,
			 repeat(["N"], inner=nLoci),
			 SYNC_X_train),
		'\t')
	Pheno_name = string("Pheno_name='pheno'", ";")
	sig = string("sig=", sqrt(var(IND_y_train)), ";")
	MIN = string("MIN=", minimum(IND_y_train), ";")
	MAX = string("MAX=", maximum(IND_y_train), ";")
	perc = string("perc=[", join(cumsum(repeat([1.0/nPop], nPop-1)), ","), "];")
	PERCENTILES = append!([0.0], Statistics.quantile(IND_y_train, [1.0/nPop] .* collect(1:nPop)))
	q = string("q=[", join(PERCENTILES[2:end], ","), "];")
	write_me_out = [Pheno_name, sig, MIN, MAX, perc, q]
	DelimitedFiles.writedlm(string(split(fnames_individual_geno_data[1], "_p")[1], "_POPULATION_POOLS_TRAIN.py"), write_me_out, '\t')
	DelimitedFiles.writedlm(string(split(fnames_individual_geno_data[1], "_p")[1], "_POPULATION_POOLS_TRAIN.csv"), y_means.y_train_mean[idx_sort_train], '\t')
	## VALIDATION POP
	DelimitedFiles.writedlm(
		string(split(fnames_individual_geno_data[1], "_p")[1], "_POPULATION_POOLS_TEST.sync"),
		hcat(LOCI_INFO,
			 repeat(["N"], inner=nLoci),
			 SYNC_X_test),
		'\t')
	Pheno_name = string("Pheno_name='pheno'", ";")
	sig = string("sig=", sqrt(var(IND_y_test)), ";")
	MIN = string("MIN=", minimum(IND_y_test), ";")
	MAX = string("MAX=", maximum(IND_y_test), ";")
	perc = string("perc=[", join(cumsum(repeat([1.0/nPop], nPop-1)), ","), "];")
	PERCENTILES = append!([0.0], Statistics.quantile(IND_y_test, [1.0/nPop] .* collect(1:nPop)))
	q = string("q=[", join(PERCENTILES[2:end], ","), "];")
	write_me_out = [Pheno_name, sig, MIN, MAX, perc, q]
	DelimitedFiles.writedlm(string(split(fnames_individual_geno_data[1], "_p")[1], "_POPULATION_POOLS_TEST.py"), write_me_out, '\t')
	DelimitedFiles.writedlm(string(split(fnames_individual_geno_data[1], "_p")[1], "_POPULATION_POOLS_TEST.csv"), y_means.y_test_mean[idx_sort_test], '\t')
#############@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#########################
###					  ###
### VALIDATION TESTS  ###
### WITH SUBSAMPLINGS ###
###					  ###
#########################

	####################
	### IND-seq data ###
	####################
	println("================================================")
	println("Subsampled individual genotype data")
	X_train = IND_X_train
	y_train = IND_y_train
	POP_LIST_BASENAMES = unique(FILE_LIST_SPEC.POP_ID)
	for i in 1:nPop
		idx = y_df.POP .== i
		X_test = IND_X_test[idx, :]
		y_test = IND_y_test[idx]
		MODELS = ["GWAS", "LS", "RR", "LASSO", "GLMNET"]
		for model in MODELS
			println(string("Using the ", model, " model"))
			if model =="GWAS"
				############
				### GWAS ###
				############
				### perform GWAS to extract the alelele effects per locus
				LOCI, EFF, PVAL, LOD = GWAS_module.GWAS(X_train, y_train, 1.0/(2*length(y_train)))
				### predict using the GWAS-derive allele effects and regression of the observed with the preidcited_raw
				y_pred = predict_ITERATIVE_MODELS_func(X_test, LOCI, EFF, y_train, X_train)
			elseif model == "LS"
				##########
				### LS ###
				##########
				### perform Least-Squares Regression to extract the alelele effects per locus
				LOCI, INT, EFF, PVAL, LOD = LS_module.LS(X_train, y_train, 1.0/(2*length(y_train)))
				### predict using the GWAS-derive allele effects and regression of the observed with the preidcited_raw
				y_pred = predict_NON_ITERATIVE_MODELS_func(X_test, LOCI, INT, EFF)
			elseif model == "RR"
				##########
				### RR ###
				##########
				### perform Ridge Regression to extract the alelele effects per locus
				LOCI, INT, EFF, PVAL, LOD = RR_module.RR(X_train, y_train, 1.0/(2*length(y_train)))
				### predict using the GWAS-derive allele effects and regression of the observed with the preidcited_raw
				y_pred = predict_NON_ITERATIVE_MODELS_func(X_test, LOCI, INT, EFF)
			elseif model == "LASSO"
				#############
				### LASSO ###
				#############
				### perform LASSO Regression to extract the alelele effects per locus
				LOCI, INT, EFF, PVAL, LOD = LASSO_module.LASSO(X_train, y_train, 1.0/(2*length(y_train)))
				### predict using the GWAS-derive allele effects and regression of the observed with the preidcited_raw
				y_pred = predict_NON_ITERATIVE_MODELS_func(X_test, LOCI, INT, EFF)
			elseif model == "GLMNET"
				##############
				### GLMNET ###
				##############
				### perform GLMNET Regression to extract the alelele effects per locus
				LOCI, INT, EFF, PVAL, LOD = GLMNET_module.GLMNET(X_train, y_train, 1.0/(2*length(y_train)))
				### predict using the GWAS-derive allele effects and regression of the observed with the preidcited_raw
				y_pred = predict_NON_ITERATIVE_MODELS_func(X_test, LOCI, INT, EFF)
			end
			### assess acuracy of the genomic prediction model
			accuracy = asses_model_accuracy_func(y_test, y_pred, false)
			push!(MODEL, model) #push strings into arrays rather than append
			push!(POP_TRAIN, "Across_all_popns")
			push!(POP_TEST, POP_LIST_BASENAMES[i])
			# push!(PREDICTORS, sum(abs.(EFF) .> 1.0e-8)) # the number of ~non-zero predictors
			push!(PREDICTORS, length(EFF))
			push!(NON_ZERO_PREDICTORS, sum(EFF .!= 0.0))
			push!(MEAN_DEVIANCE, accuracy.MEAN_DEVIANCE[1])
			push!(VAR_DEVIANCE, accuracy.VAR_DEVIANCE[1])
			push!(CORRELATION, accuracy.CORRELATION[1])
			push!(INTERCEPT, accuracy.INTERCEPT[1])
			push!(SLOPE, accuracy.SLOPE[1])
			push!(R2, accuracy.R2[1])
			push!(RMSD, accuracy.RMSD[1])
			### for simulated datasets
			if isempty(QTL_spec_fname) != true
				BONFERRONI_THRESHOLD = -log10(0.05/length(LOCI))
				PUTATIVE_QTL = LOCI_INFO_DF[LOCI[LOD .>= BONFERRONI_THRESHOLD], :]
				# PUTATIVE_QTL = repeat(id_merge, inner=6)[LOCI[LOD .>= BONFERRONI_THRESHOLD], :] #id_merge repeat by the number of alleles in the sync file which is 6
				# PUTATIVE_QTL = repeat(id_merge, inner=6)[LOCI[LOD .>= BONFERRONI_THRESHOLD], :]
				QTL_DETECTED_CHROM = []
				QTL_DETECTED_POS = []
				QTL_DETECTED_FREF = [] #QTL fraction effect
				if size(PUTATIVE_QTL, 1) > 0
					for q_true in 1:size(QTL_SPEC)[1]
						for q_putative in 1:size(PUTATIVE_QTL)[1]
							if (QTL_SPEC.chr[q_true] == PUTATIVE_QTL[q_putative, 1]) & ((QTL_SPEC.pos[q_true] - PUTATIVE_QTL[q_putative, 2]) <= LD_kb)
								# println(q_true)
								# println(q_putative)
								# push!(QTL_DETECTED_CHROM, PUTATIVE_QTL.CHROM[q_putative])
								# push!(QTL_DETECTED_POS, PUTATIVE_QTL.POS[q_putative])
								push!(QTL_DETECTED_CHROM, QTL_SPEC.chr[q_true]) 		# all QTL within the LD block
								push!(QTL_DETECTED_POS, QTL_SPEC.pos[q_true])			# which means a single QTL detected may
								push!(QTL_DETECTED_FREF, QTL_SPEC.qtl_frac_eff[q_true])	# actually contain multple causal loci in LD
							end
						end
					end
				end
				nqtl_detected = length(unique(string.(QTL_DETECTED_CHROM, "_", QTL_DETECTED_POS)))
				push!(QTL_DETECTED, nqtl_detected / size(QTL_SPEC)[1])
				if nqtl_detected > 0
					push!(QTL_FRAC_EFF, sum(QTL_DETECTED_FREF) / nqtl_detected)
				else
					push!(QTL_FRAC_EFF, 0.0)
				end
			end
			### cleanup
			LOCI=nothing; INT=nothing; EFF=nothing; PVAL=nothing; LOD=nothing
			# ### plot oserved vs predicted phenotypes
			# Plots.plot(y_test, y_pred, seriestype=:scatter)
		end # end loop across individual genotype models
	end

	#####################
	### POOL-seq data ###
	#####################
	println("================================================")
	println("Subsampled Pool-seq genotype data")
	FNAME_SYNC_TRAIN = string(split(fnames_individual_geno_data[1], "_p")[1], "_POPULATION_POOLS_TRAIN.sync")
	FNAME_SYNC_TEST = string(split(fnames_individual_geno_data[1], "_p")[1], "_POPULATION_POOLS_TEST.sync")
	FNAME_PHEN_TRAIN = string(split(fnames_individual_geno_data[1], "_p")[1], "_POPULATION_POOLS_TRAIN.py")
	FNAME_PHEN_TEST = string(split(fnames_individual_geno_data[1], "_p")[1], "_POPULATION_POOLS_TEST.py")
	MAF = 1.0/(2 * sum(sample_size[1] .* nPop))
	### convert sync files into csv allele freq files
	rm(string(split(FNAME_SYNC_TRAIN, ".")[end-1], "_ALLELEFREQ.csv"), force=true)
	sync_parsing_module.sync_parse([FNAME_SYNC_TRAIN])
		X_train = DelimitedFiles.readdlm(string(split(FNAME_SYNC_TRAIN, ".")[end-1], "_ALLELEFREQ.csv"), ',')[:, 4:end]'
		y_train = DelimitedFiles.readdlm(string(split(FNAME_PHEN_TRAIN, '.')[end-1], ".csv"))
	rm(string(split(FNAME_SYNC_TEST, ".")[end-1], "_ALLELEFREQ.csv"), force=true)
	sync_parsing_module.sync_parse([FNAME_SYNC_TEST])
		X_test = DelimitedFiles.readdlm(string(split(FNAME_SYNC_TEST, ".")[end-1], "_ALLELEFREQ.csv"), ',')[:, 4:end]'
		y_test = DelimitedFiles.readdlm(string(split(FNAME_PHEN_TEST, '.')[end-1], ".csv"))
	### find filtering index by MAF
	LOCI = collect(1:size(X_train, 2))[ filter_sync_by_MAF_module.filter_sync_by_MAF([FNAME_SYNC_TRAIN, string(MAF)]) ]
	# MODELS = ["GWAlpha_LS", "GWAlpha_ML", "GWAlpha_RR", "GWAlpha_LASSO", "GWAlpha_GLMNET"]
	# MODELS = ["GWAlpha_LS", "GWAlpha_ML", "GWAlpha_RR"]
	MODELS = ["GWAlpha_ML", "GWAlpha_RR"]
	for model in MODELS
		println(string("Using the ", model, " model"))
		if model == "GWAlpha_LS"
			##################
			### GWAlpha_LS ###
			##################
			INPUT_ARRAY = [FNAME_SYNC_TRAIN, FNAME_PHEN_TRAIN, string(MAF)]
			GWAlpha_LS_module.GWAlpha_LS(INPUT_ARRAY) #input all loci where MAF filtering sets loci failing the MAF threshold to alpha=0
			GWAlpha_OUT = CSV.read(string(split(FNAME_PHEN_TRAIN, '.')[1], "-GWAlpha_LS_Alphas.csv"))
			EFF = GWAlpha_OUT.ALPHA
			LOD = GWAlpha_OUT.LOD
			y_pred = predict_ITERATIVE_MODELS_func(X_test, LOCI, EFF, y_train, X_train)
		elseif model == "GWAlpha_ML"
			##################
			### GWAlpha_ML ###
			##################
			INPUT_ARRAY = [FNAME_SYNC_TRAIN, FNAME_PHEN_TRAIN, string(MAF)]
			GWAlpha_module.GWAlpha(INPUT_ARRAY) #input all loci where MAF filtering sets loci failing the MAF treshold to alpha=0
			GWAlpha_OUT = CSV.read(string(split(FNAME_PHEN_TRAIN, '.')[1], "-GWAlpha_ML_Alphas.csv"))
			EFF = GWAlpha_OUT.ALPHA
			LOD = GWAlpha_OUT.LOD
			y_pred = predict_ITERATIVE_MODELS_func(X_test, LOCI, EFF, y_train, X_train)
		elseif model == "GWAlpha_RR"
			##################
			### GWAlpha_RR ###
			##################
			INPUT_ARRAY = [FNAME_SYNC_TRAIN, string(split(FNAME_PHEN_TRAIN, '.')[end-1], ".csv"), string(MAF)]
			RR_POOLSEQ_module.RR_POOLSEQ(INPUT_ARRAY) #input all loci where MAF filtering sets loci failing the MAF treshold to alpha=0
			GWAlpha_OUT = CSV.read(string(split(FNAME_PHEN_TRAIN, '.')[1], "-GWAlpha_RR_Alphas.csv"))
			INT = GWAlpha_OUT.BETA[1]
			EFF = GWAlpha_OUT.BETA[2:end]
			LOD = GWAlpha_OUT.LOD[2:end]
			y_pred = predict_NON_ITERATIVE_MODELS_func(X_test, LOCI, INT, EFF)
		# elseif model == "GWAlpha_LASSO"
		# elseif model == "GWAlpha_GLMNET"
		# elseif model == "GWAlpha_EUCLIDIST"
		end
		### assess acuracy of the genomic prediction model
		accuracy = asses_model_accuracy_func(y_train[:,1], y_pred[:,1], false)
		push!(MODEL, model) #push strings into arrays rather than append
		push!(POP_TRAIN, "Across_all_popns_pools")
		push!(POP_TEST, "Across_all_popns_pools")
		# push!(PREDICTORS, sum(abs.(EFF) .> 1.0e-8)) # the number of ~non-zero predictors
		push!(PREDICTORS, length(EFF))
		push!(NON_ZERO_PREDICTORS, sum(EFF .!= 0.0))
		push!(MEAN_DEVIANCE, accuracy.MEAN_DEVIANCE[1])
		push!(VAR_DEVIANCE, accuracy.VAR_DEVIANCE[1])
		push!(CORRELATION, accuracy.CORRELATION[1])
		push!(INTERCEPT, accuracy.INTERCEPT[1])
		push!(SLOPE, accuracy.SLOPE[1])
		push!(R2, accuracy.R2[1])
		push!(RMSD, accuracy.RMSD[1])
		### for simulated datasets
		if isempty(QTL_spec_fname) != true
			BONFERRONI_THRESHOLD = -log10(0.05/length(LOCI))
			PUTATIVE_QTL = LOCI_INFO_DF[LOCI[LOD .>= BONFERRONI_THRESHOLD], :]
			# PUTATIVE_QTL = repeat(id_merge, inner=6)[LOCI[LOD .>= BONFERRONI_THRESHOLD], :] #id_merge repeat by the number of alleles in the sync file which is 6
			# PUTATIVE_QTL = repeat(id_merge, inner=6)[LOCI[LOD .>= BONFERRONI_THRESHOLD], :]
			QTL_DETECTED_CHROM = []
			QTL_DETECTED_POS = []
			QTL_DETECTED_FREF = [] #QTL fraction effect
			if size(PUTATIVE_QTL, 1) > 0
				for q_true in 1:size(QTL_SPEC)[1]
					for q_putative in 1:size(PUTATIVE_QTL)[1]
						if (QTL_SPEC.chr[q_true] == PUTATIVE_QTL[q_putative, 1]) & ((QTL_SPEC.pos[q_true] - PUTATIVE_QTL[q_putative, 2]) <= LD_kb)
							# println(q_true)
							# println(q_putative)
							# push!(QTL_DETECTED_CHROM, PUTATIVE_QTL.CHROM[q_putative])
							# push!(QTL_DETECTED_POS, PUTATIVE_QTL.POS[q_putative])
							push!(QTL_DETECTED_CHROM, QTL_SPEC.chr[q_true]) 		# all QTL within the LD block
							push!(QTL_DETECTED_POS, QTL_SPEC.pos[q_true])			# which means a single QTL detected may
							push!(QTL_DETECTED_FREF, QTL_SPEC.qtl_frac_eff[q_true])	# actually contain multple causal loci in LD
						end
					end
				end
			end
			nqtl_detected = length(unique(string.(QTL_DETECTED_CHROM, "_", QTL_DETECTED_POS)))
			push!(QTL_DETECTED, nqtl_detected / size(QTL_SPEC)[1])
			if nqtl_detected > 0
				push!(QTL_FRAC_EFF, sum(QTL_DETECTED_FREF) / nqtl_detected)
			else
				push!(QTL_FRAC_EFF, 0.0)
			end
		end
		### cleanup
		y_pred=nothing; INT=nothing; EFF=nothing; LOD=nothing
		# ### plot oserved vs predicted phenotypes
		# Plots.plot(y_test, y_pred, seriestype=:scatter)
	end

	####################
	### MERGE OUTPUT ###
	####################
	if isempty(QTL_spec_fname) != true
		OUT = DataFrames.DataFrame(MODEL=MODEL, POP_TRAIN=POP_TRAIN, POP_TEST=POP_TEST, PREDICTORS=PREDICTORS, MEAN_DEVIANCE=MEAN_DEVIANCE, VAR_DEVIANCE=VAR_DEVIANCE, CORRELATION=CORRELATION, INTERCEPT=INTERCEPT, SLOPE=SLOPE, R2=R2, RMSD=RMSD, QTL_DETECTED=QTL_DETECTED)
	else
		OUT = DataFrames.DataFrame(MODEL=MODEL, POP_TRAIN=POP_TRAIN, POP_TEST=POP_TEST, PREDICTORS=PREDICTORS, MEAN_DEVIANCE=MEAN_DEVIANCE, VAR_DEVIANCE=VAR_DEVIANCE, CORRELATION=CORRELATION, INTERCEPT=INTERCEPT, SLOPE=SLOPE, R2=R2, RMSD=RMSD)
	end
	return(OUT)
end
### test:
file_list_spec_fname = "julia_cross_validation_input_file_list.csv"
QTL_spec_fname = "QUANTI_g140_QTL_SPEC.csv"
LD_kb = 1000
INPUT = [file_list_spec_fname, QTL_spec_fname, LD_kb]
test = cross_validation(INPUT)
by(test, :MODEL, :CORRELATION => mean, :RMSD => mean, :QTL_DETECTED => mean)
end #end of cross_validation_module
