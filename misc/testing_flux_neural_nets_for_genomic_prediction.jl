using Flux
### number of input data points
n = 2
m = 5
### model definition
# predict(x) = W*x .+ b
### input data
x = rand(m)
### actual weights and biases
W_true = rand(n, m)
b_true = rand(n)
### expected output data
y = (W_true * x) .+ b_true
### prior parameter estimates
W = rand(n, m) ### weights (paramters to estimate)
b = rand(n) ### biases (also parameters to estimate)
### the cost function
# function loss(x, y)
#   ŷ = predict(x)
#   sum((y .- ŷ).^2)
# end ### equivalent to:
function Loss(x, y)
  ŷ = W*x .+ b
  sum((y .- ŷ).^2)
end
### pre-training prediction
Loss(x, y)
### training
# for i in 1:1000
for i in 1:10
    ### define the gradient function with the input function defining the gradient as the loss function and...
    ### the input parameters defined by the output of the function params(W,b) which just lists the weights and biases
    gs = gradient(() -> Loss(x, y), params(W, b)) ### jus says the output of params(W,b) is the input ()-> for loss(x,y) and equivalent to:
    # gs = gradient(params(W,b)) do
    #         Loss(x,y)
    #     end
    ### calculate the gradient of the loss function across the weights and biases...
    ### in other words - the instantaneous changes in loss at the points defined by the current weights and biases (the derivatives!)
    dLoss_dW = gs[W]
    dLoss_db = gs[b]
    ### update the weights and biases by...
    ### reducing the current weights and biases by 0.1 of the gradients of the weights and biases...
    ### reduction because we are minimizing the loss
    ### should converge into a minimum at d(loss)/d(params) == 0 --> where the slope is zero..
    ### in other words - the derivatives dLoss_dW and dLoss_db should approach zero!
    W .-= 0.1 .* dLoss_dW ### we are actually increasing the value of the parameter proportional (0.1 learning rate) to the slope which is sloping downwards...
    b .-= 0.1 .* dLoss_db ### so subtraction actually cancels the negative sign of the slope redering the whole thing as an addition!!!!
end
### assessment
using Statistics
hcat(W_true*x .+ b_true, W*x .+ b)
RMSD = sqrt(mean(((W_true*x .+ b_true) .- (W*x .+ b)).^2))

###############################################################
###############################################################
###############################################################
### That was a single layer linea relationships
### Now let's try two layers linear relationships
###############################################################
###############################################################
###############################################################
using Flux

nInputs = 5
nOutputs = 2
nNodes_hiddenLayer1 = 3

input =                                 x       = rand(nInputs)
input_to_hiddenLayer1_connections =     W1_true = rand(nNodes_hiddenLayer1, nInputs)
input_to_hiddenLayer1_biases =          b1_true = rand(nNodes_hiddenLayer1)
hiddenLayer1_to_output_connections =    W2_true = rand(nOutputs, nNodes_hiddenLayer1)
hiddenLayer1_to_output_biases =         b2_true = rand(nOutputs)

function model_input2hid1(input)
    (W1_true * input) + input_to_hiddenLayer1_biases
end
function model_hid12output(input_to_hiddenLayer1_output)
    (W2_true * input_to_hiddenLayer1_output) + hiddenLayer1_to_output_biases
end
function MODEL_2_LAYERS(input)
    model_hid12output(
        σ.(
            model_input2hid1(
                input
            )
        )
    )
end

### or simply define a variable defining a function with the input as the input vector corresponding to the
MODEL_2_LAYERS =  Chain(
                    Dense(nInputs, nNodes_hiddenLayer1, σ),
                    Dense(nNodes_hiddenLayer1, nOutputs),
                    softmax
                )
### test
MODEL_2_LAYERS(input)

### VERY SIMPLY WITH HIGH-LEVEL FUNCTIONS!
using Flux
### define the number of input, hidden layer and output nodes
nInputs = 5
nOutputs = 2
nNodes_hiddenLayer1 = 3
### define the multi-layer model
model = Flux.Chain(
            Flux.Dense(nInputs, nNodes_hiddenLayer1, σ),
            Flux.Dense(nNodes_hiddenLayer1, nOutputs)
        )
### define the cost function we want to minimize
loss_function(input, output) = Flux.mse(model(input), output)
### define the trainable list of parameters
params_list = Flux.params(model)
### define input and output data to be used in the definition of the gradient function immediately below
input = rand(nInputs)
output = rand(nOutputs)
### define the gradient of the cost function as a function of the parameters
grad_function = Flux.gradient(() -> loss_function(input, output), params_list)
### define the optimizer we want to use with the learning rate of 1%
optimizer = Flux.Descent(0.1)
### train
Flux.@epochs 10 Flux.train!(loss_function, params_list, [(input, output)], optimizer)


### Test on naively simulated Pool-seq data
using Statistics, Distributions, UnicodePlots, Flux
### simulate
n = 60
# m = Int64(1e5)
m = 1000
X = Array{Float64, 2}(undef, n, m)
X_ref = abs.(rand(n, Int64(m/2))) #biallelic
X_alt = 1 .- X_ref
X[:, 1:2:m] = X_ref
X[:, 2:2:m] = X_alt
nQTL = 10
posQTL = sort!(sample(collect(1:m), nQTL))
b = zeros(m)
b[posQTL] = rand(Distributions.Chisq(2), nQTL)
err = rand(Distributions.Normal(0, 1), n)
y = (X*b) .+ err
# y = hcat((X*b) .+ err, (X*b) .+ err)
UnicodePlots.histogram(y)

### build the neural network model
nInputs = size(X, 2)
nOutputs = size(y, 2)
nHiddenLayers = 3
nNodes_l1 = Int(round(m/nHiddenLayers))
nNodes_l2 = Int(round(nNodes_l1/nHiddenLayers))
nNodes_l3 = Int(round(nNodes_l2/nHiddenLayers))
# nNodes_l4 = Int(round(nNodes_l3/nHiddenLayers))
# nNodes_l5 = Int(round(nNodes_l4/nHiddenLayers))
model = Flux.Chain(
                    Flux.Dense(nInputs, nNodes_l1, σ),
                    Flux.Dense(nNodes_l1, nNodes_l2, σ),
                    Flux.Dense(nNodes_l2, nNodes_l3, σ),
                    # Flux.Dense(nNodes_l3, nNodes_l4, σ),
                    # Flux.Dense(nNodes_l4, nNodes_l5, σ),
                    # Flux.Dense(nNodes_l5, nOutputs)
                    Flux.Dense(nNodes_l3, nOutputs)
        )
### define the cost function as a simple mean square error
cost_function(input, output) = Flux.mse(model(input), output)
### define the trainable paramaters list
params_list = Flux.params(model)
### define the optimizer
optimizer = Flux.Descent(0.1)
### divide into training and validation sets 9:1
nFold=6
idx_rand = sample(collect(1:n), n, replace=false)
idx_train = idx_rand[1:Int64(round((1/nFold)*(nFold-1)*n))]
idx_valid = idx_rand[Int64(round((1/nFold)*(nFold-1)*n)+1):n]
X_train = X[idx_train, :]; y_train = y[idx_train]
X_valid = X[idx_valid, :]; y_valid = y[idx_valid]
### train
### define the training data iterator
data_iterator = Flux.Iterators.repeated((X_train', y_train'), 10)
@time Flux.@epochs 1000 Flux.train!(cost_function, params_list, data_iterator, optimizer)
### validate
UnicodePlots.scatterplot(y_valid, model(X_valid')[1,:])
Statistics.cor(y_valid, model(X_valid')[1,:])
cost_function(X_valid', y_valid')
