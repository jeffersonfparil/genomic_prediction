### Plotting the change in phenotype value through time

### Hard-coded datasets: (temporary for now 2011906014)
setwd("/data/Lolium/Softwares/quantinemo_linux/lolium_2019-06-13_07-28-07")
min_max_phen = read.table("../QTL_min_max_GEBV.spec", header=T, sep="\t")

phen_fnames = system("ls *.phe", intern=TRUE)
for (i in phen_fnames) {
  #test:
  # i = phen_fnames[1]
  print(i)
  time_raw = strsplit(i, "_")[[1]]
  time = strsplit(time_raw[length(time_raw)], "[.]")[[1]][1]
  dat = read.table(file=i, header=FALSE, skip=2)
  dat[,1] = rep(time, times=nrow(dat))
  colnames(dat) = c("GEN_TIME", "PHEN")
  dat = as.data.frame(dat)
  if (exists("PHEN")==FALSE){
    PHEN = dat
  } else {
    PHEN = rbind(PHEN, dat)
  }
}

PHEN$PHEN = (PHEN$PHEN - min_max_phen$MIN_GEBV) / (min_max_phen$MAX_GEBV - min_max_phen$MIN_GEBV)

# plot(density(PHEN$PHEN), type="n")
# for (i in levels(PHEN$GEN_TIME)) {
#   sub = subset(PHEN, GEN_TIME==i)
#   lines(density(sub$PHEN), lty=2, col=rgb(0.9, 0.2, 0.1, alpha=0.2))
# }
phen_means = aggregate(PHEN ~ GEN_TIME, data=PHEN, FUN=mean)
phen_means$GEN_TIME = 1:nrow(phen_means)
plot(phen_means, pch=20, col=rgb(0.5, 0.5, 0.5, alpha=0.6))
mod = lm(PHEN ~ poly(GEN_TIME, 20), data=phen_means)
newx = seq(1, nrow(phen_means), by=0.01)
newy = predict(mod, newdata=data.frame(GEN_TIME=newx))
lines(x=newx, y=newy, lty=2, lwd=2, col=rgb(0.9, 0.2, 0.1, alpha=0.8))
