#!/bin/bash

### input
QUANTINEMO_DIR=${1}         # location of the quantinemo executable
GEN_PRED_SRC_DIR=${2}       # location of the genomic_prediction/src git repo clone locally
POPOOL2_DIR=${3}            # popoolation2 scripts location
OUTDIR=${4}                 # existing output directory to dump all the output files and folder
OUTPREFIX=${5}              # prefix for the quantiNemo2 initiation (*.ini) file and the output folder
nIndividuals=${6}           # number of individuals to simulate
nPools=${7}                 # number of equally sized pools to group individuals into
nLoci=${8}                  # number of loci to simulate (neutral + QTL)
nQTL=${9}                   # number of QTL among the all the loci simulated
nAlleles=${10}              # number of alleles per loci, e.g. 5 for A,T,C,G, and DEL
nGen=${11}                  # number of generations to simulate
nPop=${12}                  # number of populations/subpopulations to simulate
migration=${13}             # migration rate across the populations (surrently using the 1D stepping stone model see line 117)

### SAMPLE EXECUTION:
# time \
# ./quantinemo2_geno_pheno_sim.sh /data/Lolium/Softwares/quantinemo_linux \
# /data/Lolium/Softwares/genomic_prediction/src \
# /data/Lolium/Softwares/popoolation2_1201 \
# /data/Lolium/Quantitative_Genetics/genomic_prediction_simulations/QUANTINEMO2_SIM \
# lolium \
# 100 \
# 5 \
# 2000 \
# 10 \
# 5 \
# 140 \
# 7 \
# 0.001

# ### SAMPLE INPUTS (in ssh_uni and ssh_lolium)
# QUANTINEMO_DIR=/data/Lolium/Softwares/quantinemo_linux
# GEN_PRED_SRC_DIR=/data/Lolium/Softwares/genomic_prediction/src
# POPOOL2_DIR=/data/Lolium/Softwares/popoolation2_1201
# OUTDIR=/data/Lolium/Quantitative_Genetics/genomic_prediction_simulations/QUANTINEMO2_SIM
# OUTPREFIX=lolium
# nIndividuals=100
# nPools=5
# nLoci=2000
# nQTL=10
# nAlleles=5
# nGen=140
# nPop=7
# migration=0.001 #1.00x10^(-3) migration rate

### prepare quantiNemo2, genomic_prediction.git, popoolation2, and julia1 libraries
###### download quantiNemo2 if not yet installed
if [ $(ls ${QUANTINEMO_DIR} | wc -l) -eq 0 ]
then
  echo "########################################"
  echo "Downloading QuantiNemo2"
  echo "########################################"
  cd ${QUANTINEMO_DIR%/*}
  wget https://www2.unil.ch/popgen/softwares/quantinemo/files/quantinemo_linux.zip
  # unzip quantinemo_linux.zip -d ${QUANTINEMO_DIR} #the directory is already called quantinemo_linux
  unzip quantinemo_linux.zip
  rm quantinemo_linux.zip
  echo "########################################"
fi
###### clone the genomic_prediction git repo not yet installed
if [ $(ls ${GEN_PRED_SRC_DIR} | wc -l) -eq 0 ]
then
  echo "########################################"
  echo "Cloning genomic_prediction.git repository"
  echo "########################################"
  cd ${GEN_PRED_SRC_DIR%/*/*} #outside of genomic_prediction/src
  git clone https://gitlab.com/jeffersonfparil/genomic_prediction.git
  echo "########################################"
fi
###### download popoolation2 if not yet installed
if [ $(ls ${POPOOL2_DIR} | wc -l) -eq 0 ]
then
  echo "########################################"
  echo "Downloading Popoolation2"
  echo "########################################"
  cd ${POPOOL2_DIR%/*}
  wget https://sourceforge.net/projects/popoolation2/files/popoolation2_1201.zip
  unzip popoolation2_1201.zip -d ${POPOOL2_DIR}
  rm popoolation2_1201.zip
  echo "########################################"
fi
###### installing julia libraries
echo -e "using Pkg" > install_julia_pkgs.jl
echo -e "Pkg.add([" >> install_julia_pkgs.jl
echo -e "'CSV'," >> install_julia_pkgs.jl
echo -e "'DataFrames'," >> install_julia_pkgs.jl
echo -e "'DelimitedFiles'," >> install_julia_pkgs.jl
echo -e "'LinearAlgebra'," >> install_julia_pkgs.jl
echo -e "'Optim'," >> install_julia_pkgs.jl
echo -e "'Plots'," >> install_julia_pkgs.jl
echo -e "'ProgressMeter'," >> install_julia_pkgs.jl
echo -e "'Statistics'," >> install_julia_pkgs.jl
echo -e "'StatsBase'," >> install_julia_pkgs.jl
echo -e "'Lasso'," >> install_julia_pkgs.jl
echo -e "'GLMNet'" >> install_julia_pkgs.jl
echo -e "])" >> install_julia_pkgs.jl
sed -i s/\'/\"/g install_julia_pkgs.jl
julia install_julia_pkgs.jl
rm install_julia_pkgs.jl

### setwd
cd $OUTDIR

### Setup mating system
echo -e "mating_system 0" > ${OUTPREFIX}.ini                    #hermaphrodite random mating

### Demography
echo -e "patch_number $nPop" >> ${OUTPREFIX}.ini                # Number of populations or subpopulations to simulate
echo -e "generations $nGen" >> ${OUTPREFIX}.ini                 #Lolium has been introduced to Australia around 1880
echo -e "patch_capacity $nIndividuals" >> ${OUTPREFIX}.ini      #carrying capacity set to a billion
echo -e "regulation_model_offspring 1" >> ${OUTPREFIX}.ini      #regulation of population size to carrying capacity (patch_capacity) via random culling for offsprings
echo -e "regulation_model_adults 1" >> ${OUTPREFIX}.ini         #regulation of population size to carrying capacity (patch_capacity) via random culling for adults
echo -e "mating_nb_offspring_model 3" >> ${OUTPREFIX}.ini       #simple fecundity by rouding the number of offsrpings) with fecundity rate of...
echo -e "mean_fecundity 1" >> ${OUTPREFIX}.ini                  #... 1 which means a constant population size which is just an approximation though I don't know how bad of an approximation it is
echo -e "dispersal_rate $migration" >> ${OUTPREFIX}.ini         #no migration for our single population right nrow
echo -e "dispersal_model 2" >> ${OUTPREFIX}.ini                 #1-dimensional stepping-stone model
echo -e "dispersal_border_model 2" >> ${OUTPREFIX}.ini          #migrants from the border gets lost for the 1D stepping-stone model

### Genotype configuration
# nLoci=40000 #total number of loci: neutral and QTL
# # nMarkers=300
# nQTL=10
# nAlleles=5
# echo -e "ntrl_loci $nMarkers" >> ${OUTPREFIX}.ini              #number of neutral loci
# echo -e "quanti_loci $nQTL" >> ${OUTPREFIX}.ini                #number of QTL
echo -e "quanti_loci $nLoci" >> ${OUTPREFIX}.ini                 #total number of loci: neutral and QTL
# echo -e "ntrl_all $nAlleles" >> ${OUTPREFIX}.ini               #number of alleles per locus; and since we're simulating SNPs we want 5: A,T,C,G and DEL excluding N
echo -e "quanti_all $nAlleles" >> ${OUTPREFIX}.ini               #number of alleles per locus; and since we're simulating SNPs we want 5: A,T,C,G and DEL excluding N
# echo -e "ntrl_allelic_file " >> ${OUTPREFIX}.ini               #neutral allelic file set as default
    ###### Build the quanti_allelic_file
    echo -e "# Quantitative Alleles Specifications File" > QTL.spec
    echo -e "[FILE_INFO]{" >> QTL.spec
    echo -e "  col_locus 1" >> QTL.spec
    echo -e "  col_allele 2" >> QTL.spec
    echo -e "  col_allelic_value 3" >> QTL.spec
    echo -e "  col_mut_freq 4" >> QTL.spec
    echo -e "  col_ini_freq 5" >> QTL.spec
    echo -e "}" >> QTL.spec
    echo -e "#locus\tallele\tvalue\tmut_freq\tini_freq" >> QTL.spec
    ###### Empirical allele count distribution: script found in "quantinemo2_geno_sim_00_QTL_build_testing.r" commented-out
    echo -e "ALLELE_COUNT_1,2.47495389965245e-08" > Allele_counts_dist.spec
    echo -e "ALLELE_COUNT_2,0.673727405623913" >> Allele_counts_dist.spec
    echo -e "ALLELE_COUNT_3,0.25736361168522" >> Allele_counts_dist.spec
    echo -e "ALLELE_COUNT_4,0.0660927963591863" >> Allele_counts_dist.spec
    echo -e "ALLELE_COUNT_5,0.00281616158214214" >> Allele_counts_dist.spec
    echo -e "ALLELE_COUNT_6,0" >> Allele_counts_dist.spec
    ###### R script to simulate the loci specifications
    ### inputs
    echo -e "nLoci = $nLoci" > QTL_build.r
    echo -e "nQTL = $nQTL" >> QTL_build.r
    echo -e "nAlleles = $nAlleles" >> QTL_build.r
    echo -e "allele_dist = read.csv('Allele_counts_dist.spec', header=FALSE)" >> QTL_build.r
    ### prepare ouput columns i.e. locus ID, allele ID, and ad allele effects
    echo -e "col_locus = rep(1:nLoci, each=nAlleles)" >> QTL_build.r
    echo -e "col_allele = rep(1:nAlleles, times=nLoci)" >> QTL_build.r
    ### setting mutation rate at zero
    echo -e "col_mut_freq = rep(c(0), times=nLoci*nAlleles)" >> QTL_build.r
    ### setting initial allle frequencies across all loci - simplistically as 1/nAlleles
    ## creating an artificial list of sparsity vectors to simulate allele count distribution for a speedy algorithm??! 20190628
    echo -e "n_possible_sparsity_designs = 100" >> QTL_build.r
    echo -e "sparsity_list = c()" >> QTL_build.r
    echo -e "for (i in 2:nAlleles) { #do not simulate monomorphic sites" >> QTL_build.r
    echo -e "  for (j in 1:round(allele_dist[i,2]*n_possible_sparsity_designs)) {" >> QTL_build.r
    echo -e "    sparse_vec = c(rep(0, time=nAlleles-i), rep(1, times=i))" >> QTL_build.r
    echo -e "    sparse_vec = sparse_vec[order(runif(nAlleles))]" >> QTL_build.r
    echo -e "    sparsity_list = c(sparsity_list, sparse_vec)" >> QTL_build.r
    echo -e "  }" >> QTL_build.r
    echo -e "}" >> QTL_build.r
    echo -e "sparsity_df = matrix(sparsity_list, ncol=nAlleles, byrow=TRUE)" >> QTL_build.r
    echo -e "ALLELE_COUNTS_MAT = sparsity_df[sample(1:nrow(sparsity_df), size=nLoci, replace=TRUE), ]" >> QTL_build.r
    echo -e "ALLELE_COUNTS_VEC = matrix(t(ALLELE_COUNTS_MAT), nrow=1)" >> QTL_build.r
    echo -e "col_ini_freq = rep(1/rowSums(ALLELE_COUNTS_MAT), each=nAlleles) * ALLELE_COUNTS_VEC[1,]" >> QTL_build.r
    ### select the QTL wherein each QTL is composed of nAlleles alleles with effects sampled from a chi-square distribution
    echo -e "col_allelic_value = rep(c(0), times=nLoci*nAlleles)" >> QTL_build.r
    echo -e "idxQTL = sample(1:nLoci, nQTL, replace=FALSE) * nAlleles" >> QTL_build.r
    echo -e "idxQTL = rep(idxQTL, each=nAlleles)" >> QTL_build.r
    echo -e "idxQTL = idxQTL - rep((nAlleles-1):0, times=nQTL)" >> QTL_build.r
    echo -e "idxQTL_nonZero_iniFreq = col_ini_freq[idxQTL] != 0" >> QTL_build.r
    echo -e "nQTL_nonZero_iniFreq = sum(idxQTL_nonZero_iniFreq)" >> QTL_build.r
    echo -e "col_allelic_value[idxQTL[idxQTL_nonZero_iniFreq]] = round(rchisq(n=nQTL_nonZero_iniFreq, df=1),2)" >> QTL_build.r
    ### merge the columns into the output dataframe and write the outputs
    echo -e "out = data.frame(col_locus, col_allele, col_allelic_value, col_mut_freq, col_ini_freq)" >> QTL_build.r
    echo -e "write.table(out, file='QTL.spec', col.names=FALSE, row.names=FALSE, append=TRUE, sep='\t')" >> QTL_build.r
    ### MISC: find minimum and maximum possible additive genetic values
    echo -e "sub = out[(out\$col_allelic_value != 0), ]" >> QTL_build.r
    echo -e "MIN_GEBV = 2*sum(aggregate(sub\$col_allelic_value ~ sub\$col_locus, FUN=min)[,2])" >> QTL_build.r
    echo -e "MAX_GEBV = 2*sum(aggregate(sub\$col_allelic_value ~ sub\$col_locus, FUN=max)[,2])" >> QTL_build.r
    echo -e "write.table(data.frame(MIN_GEBV, MAX_GEBV), file='QTL_min_max_GEBV.spec', row.names=FALSE, sep='\t')" >> QTL_build.r
    Rscript QTL_build.r
    rm QTL_build.r
    ######################################################################################
    ###### alternative:
    # # In R for building different allelic effects and initial allele frequencies
    # # by sampling from different distributions and modelling genetic architectures:
    # args = commandArgs(trailing=TRUE)
    # nQTL = args[1] #number of QTL to simulate
    ######################################################################################
echo -e "quanti_allelic_file QTL.spec" >> ${OUTPREFIX}.ini      #neutral allelic file set as default
# echo -e "ntrl_ini_allele_model 0" >> ${OUTPREFIX}.ini           #genotypes are set to be maximally polymorph while set to 1 for monomorph or fixed alleles per genotype
echo -e "quanti_ini_allele_model 0" >> ${OUTPREFIX}.ini         #genotypes are set to be maximally polymorph while set to 1 for monomorph or fixed alleles per genotype
# echo -e "ntrl_mutation_rate 0" >> ${OUTPREFIX}.ini              #mutation rate (set to no mutation for simplicity now)
echo -e "quanti_mutation_rate 0" >> ${OUTPREFIX}.ini            #mutation rate (set to no mutation for simplicity now)
# echo -e "ntrl_mutation_model 0" >> ${OUTPREFIX}.ini             #random mutation model (see pages 56-60)
echo -e "quanti_mutation_model 0" >> ${OUTPREFIX}.ini           #random mutation model (see pages 56-60)
# echo -e "ntrl_nb_trait 1" >> ${OUTPREFIX}.ini                   #number of traits
echo -e "quanti_nb_trait 1" >> ${OUTPREFIX}.ini                 #number of traits
    ###### Setting the genetic map
    ######### neutral loci
    # echo -e "nChrom = 7" > genome_ntrl_pos.r
    # echo -e "nNtrl = $nMarkers" >> genome_ntrl_pos.r
    # echo -e "chrom_len_cM = c(97.7, 151.5, 63.3, 119.2, 89.1, 115.2, 113.7)" >> genome_ntrl_pos.r #Chromosome length in cM #from Pfeifer2013
    # echo -e "nAllelesPerChrom = round(nNtrl / nChrom)" >> genome_ntrl_pos.r
    # echo -e "counter_chrom=1" >> genome_ntrl_pos.r
    # echo -e "for (i in chrom_len_cM){" >> genome_ntrl_pos.r
    # echo -e "  if (i != chrom_len_cM[nChrom]){" >> genome_ntrl_pos.r
    # echo -e "    out = matrix(seq(from=1, to=i, length.out=nAllelesPerChrom), nrow=1)" >> genome_ntrl_pos.r
    # # echo -e "  } else {" >> genome_ntrl_pos.r
    # echo -e "    out = matrix(seq(from=1, to=i, length.out=(nAllelesPerChrom + (nNtrl - (nChrom*nAllelesPerChrom)))), nrow=1)" >> genome_ntrl_pos.r
    # echo -e "  }" >> genome_ntrl_pos.r
    # echo -e "  write.table(out, file=paste0('genome_loci_', counter_chrom, '.temp'), col.names=FALSE, row.names=FALSE, sep=' ')" >> genome_ntrl_pos.r
    # echo -e "  counter_chrom = counter_chrom + 1" >> genome_ntrl_pos.r
    # echo -e "}" >> genome_ntrl_pos.r
    # Rscript genome_ntrl_pos.r
    # echo -e "ntrl_genome { {1: $(cat genome_loci_1.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{2: $(cat genome_loci_2.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{3: $(cat genome_loci_3.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{4: $(cat genome_loci_4.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{5: $(cat genome_loci_5.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{6: $(cat genome_loci_6.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{7: $(cat genome_loci_7.temp)} }" >> ${OUTPREFIX}.ini
    # rm genome_ntrl_pos.r *.temp
    ######### QTL
    # echo -e "nChrom = 7" > genome_quanti_pos.r
    # echo -e "nQTL = $nQTL" >> genome_quanti_pos.r
    # echo -e "chrom_len_cM = c(97.7, 151.5, 63.3, 119.2, 89.1, 115.2, 113.7)" >> genome_quanti_pos.r #Chromosome length in cM #from Pfeifer2013
    # echo -e "CHROM = sample(1:length(chrom_len_cM), size=nQTL, replace=TRUE)" >> genome_quanti_pos.r
    # echo -e "CHROM_LEN = chrom_len_cM[CHROM]" >> gen"../QTL.spec"ome_quanti_pos.r
    # echo -e "CHROM_POS = sapply(CHROM_LEN, FUN=function(x)sample(seq(1, x, by=0.01), size=1))" >> genome_quanti_pos.r
    # echo -e "while (length(CHROM_POS) < length(CHROM_LEN)){" >> genome_quanti_pos.r
    # echo -e "  CHROM_POS = sapply(CHROM_LEN, FUN=function(x)sample(seq(1, x, by=0.01), size=1))" >> genome_quanti_pos.r
    # echo -e "}" >> genome_quanti_pos.r
    # echo -e "df = data.frame(QTL=1:nQTL, CHROM, CHROM_POS)" >> genome_quanti_pos.r
    # echo -e "CHROM_LIST = unique(CHROM)" >> genome_quanti_pos.r
    # echo -e "CHROM_LIST = CHROM_LIST[order(CHROM_LIST)]" >> genome_quanti_pos.r
    # echo -e "for (i in CHROM_LIST) {" >> genome_quanti_pos.r
    # echo -e "  sub = subset(df, CHROM==i)" >> genome_quanti_pos.r
    # echo -e "  out = matrix(c(sub\$CHROM_POS, sub\$QTL), nrow=2, byrow=TRUE)" >> genome_quanti_pos.r
    # echo -e "  out = out[,order(out[1,])]" >> genome_quanti_pos.r
    # echo -e "  write.table(out, file=paste0('genome_QTL_', i, '.temp'), col.names=FALSE, row.names=FALSE, sep=' ')" >> genome_quanti_pos.r
    # echo -e "}" >> genome_quanti_pos.r
    # Rscript genome_quanti_pos.r
    # echo -e "quanti_genome { {1: $(head -n1 genome_QTL_1.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{2: $(head -n1 genome_QTL_2.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{3: $(head -n1 genome_QTL_3.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{4: $(head -n1 genome_QTL_4.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{5: $(head -n1 genome_QTL_5.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{6: $(head -n1 genome_QTL_6.temp)}" >> ${OUTPREFIX}.ini
    # echo -e "\t\t{7: $(head -n1 genome_QTL_7.temp)} }" >> ${OUTPREFIX}.ini
    # # echo -e "quanti_locus_index { {1: $(tail -n1 genome_QTL_1.temp)}" >> ${OUTPREFIX}.ini
    # # echo -e "\t\t{2: $(tail -n1 genome_QTL_2.temp)}" >> ${OUTPREFIX}.ini
    # # echo -e "\t\t{3: $(tail -n1 genome_QTL_3.temp)}" >> ${OUTPREFIX}.ini
    # # echo -e "\t\t{4: $(tail -n1 genome_QTL_4.temp)}" >> ${OUTPREFIX}.ini
    # # echo -e "\t\t{5: $(tail -n1 genome_QTL_5.temp)}" >> ${OUTPREFIX}.ini
    # # echo -e "\t\t{6: $(tail -n1 genome_QTL_6.temp)}" >> ${OUTPREFIX}.ini
    # # echo -e "\t\t{7: $(tail -n1 genome_QTL_7.temp)} }" >> ${OUTPREFIX}.ini
    # rm genome_quanti_pos.r *.temp
    ######### merged loci: neutral + QTL
    echo -e "nChrom = 7" > genome_pos.r
    echo -e "nLoci = $nLoci" >> genome_pos.r
    echo -e "chrom_len_cM = c(97.7, 151.5, 63.3, 119.2, 89.1, 115.2, 113.7)" >> genome_pos.r #Chromosome length in cM #from Pfeifer2013
    echo -e "nAllelesPerChrom = round(nLoci / nChrom)" >> genome_pos.r
    echo -e "counter_chrom=1" >> genome_pos.r
    echo -e "for (i in chrom_len_cM){" >> genome_pos.r
    echo -e "  if (i != chrom_len_cM[nChrom]){" >> genome_pos.r
    # echo -e "    out = matrix(seq(from=1, to=i, length.out=nAllelesPerChrom), nrow=1)" >> genome_pos.r #equidistant markers across the genome
    echo -e "    out = matrix(sort(sample(seq(from=0.00001, to=i, by=0.00001), size=nAllelesPerChrom)), nrow=1)" >> genome_pos.r #random smpling
    echo -e "  } else {" >> genome_pos.r
    # echo -e "    out = matrix(seq(from=1, to=i, length.out=(nAllelesPerChrom + (nLoci - (nChrom*nAllelesPerChrom)))), nrow=1)" >> genome_pos.r #equidistant markers across the genome
    echo -e "    out = matrix(sort(sample(seq(from=0.00001, to=i, by=0.00001), size=(nAllelesPerChrom + (nLoci - (nChrom*nAllelesPerChrom))))), nrow=1)" >> genome_pos.r #random sampling
    echo -e "  }" >> genome_pos.r
    echo -e "  write.table(out, file=paste0('genome_loci_', counter_chrom, '.temp'), col.names=FALSE, row.names=FALSE, sep=' ')" >> genome_pos.r
    echo -e "  counter_chrom = counter_chrom + 1" >> genome_pos.r
    echo -e "}" >> genome_pos.r
    Rscript genome_pos.r
    echo -e "quanti_genome { {1: $(cat genome_loci_1.temp)}" >> ${OUTPREFIX}.ini
    echo -e "\t\t{2: $(cat genome_loci_2.temp)}" >> ${OUTPREFIX}.ini
    echo -e "\t\t{3: $(cat genome_loci_3.temp)}" >> ${OUTPREFIX}.ini
    echo -e "\t\t{4: $(cat genome_loci_4.temp)}" >> ${OUTPREFIX}.ini
    echo -e "\t\t{5: $(cat genome_loci_5.temp)}" >> ${OUTPREFIX}.ini
    echo -e "\t\t{6: $(cat genome_loci_6.temp)}" >> ${OUTPREFIX}.ini
    echo -e "\t\t{7: $(cat genome_loci_7.temp)} }" >> ${OUTPREFIX}.ini
    rm genome_pos.r *.temp
    echo -e "quanti_locus_index {$(seq 1 $nLoci)}" >> ${OUTPREFIX}.ini
### Phenotype settings
echo -e "quanti_heritability 0.50" >> ${OUTPREFIX}.ini          #narrow-sense heritability set to 50%
echo -e "quanti_selection_model 2" >> ${OUTPREFIX}.ini          #directional selection
### Summary Statistics Output
echo -e "stat_log_time $nGen" >> ${OUTPREFIX}.ini               #output summary statistics for the last generation only
echo -e "stat {q.adlt.fst" >> ${OUTPREFIX}.ini
# echo -e "stat {q.adlt.ho" >> ${OUTPREFIX}.ini
# echo -e "q.adlt.fstat" >> ${OUTPREFIX}.ini
echo -e "q.adlt.fst.wc_pair" >> ${OUTPREFIX}.ini
echo -e "q.adlt.R2}" >> ${OUTPREFIX}.ini
### Save genotype and phenotype data
# echo -e "ntrl_save_genotype 1" >> ${OUTPREFIX}.ini            #output in FSTAT format: main genotype block after the loci ID: n x l --> number of individuals x number of loci: [1:5][1:5] --> genotype format, e.g. 25 for a heterozygote containing the 2nd allele and the 5th allele
# echo -e "ntrl_genot_filename NEUTRAL" >> ${OUTPREFIX}.ini
echo -e "quanti_genot_logtime $nGen" >> ${OUTPREFIX}.ini        #write genotype output for the final generation only
echo -e "quanti_save_genotype 1" >> ${OUTPREFIX}.ini            #output in FSTAT format: main genotype block after the loci ID: n x l --> number of individuals x number of loci: [1:5][1:5] --> genotype format, e.g. 25 for a heterozygote containing the 2nd allele and the 5th allele
echo -e "quanti_genot_filename QUANTI" >> ${OUTPREFIX}.ini
# echo -e "ntrl_save_phenotype 1" >> ${OUTPREFIX}.ini
# echo -e "ntrl_phenot_filename NEUTRAL" >> ${OUTPREFIX}.ini
echo -e "quanti_phenot_logtime $nGen" >> ${OUTPREFIX}.ini        #write phenotype output for the final generation only
echo -e "quanti_save_phenotype 1" >> ${OUTPREFIX}.ini
echo -e "quanti_phenot_filename QUANTI" >> ${OUTPREFIX}.ini
### write out chromosome lengths in bp (@Ansari2016) and cM (@Pfeifer2013)
echo -e "Chrom\tMbp\tcM" > Lperenne_genome.spec
echo -e "1\t470.30\t97.7" >> Lperenne_genome.spec
echo -e "2\t429.90\t151.5" >> Lperenne_genome.spec
echo -e "3\t406.56\t63.3" >> Lperenne_genome.spec
echo -e "4\t369.03\t119.2" >> Lperenne_genome.spec
echo -e "5\t339.41\t89.1" >> Lperenne_genome.spec
echo -e "6\t322.62\t115.2" >> Lperenne_genome.spec
echo -e "7\t284.07\t113.7" >> Lperenne_genome.spec

#################
### EXECUTION ###
#################
${QUANTINEMO_DIR}/quantinemo ${OUTPREFIX}.ini

####################################
### PARSE AND ANALYZE THE OUTPUT ###
####################################

#################################################################
#(1) parse quantinemo2 outputs ONLY FOR THE LAST GENERATION!!!
echo "#########################################################################################"
echo "PARSING QUANTINEMO2 OUTPUT, POOLING AND BUILDING GENOMIC PREDCTION INPUT FILES"
MOST_RECENT_OUTPUT_DIR=$(echo ${OUTDIR}/$(ls ${OUTDIR} | grep lolium_ | tail -n1))
cd ${MOST_RECENT_OUTPUT_DIR}
### separate genotype data of each patch (sub-population) in bash
NHEADERS=$(echo $nLoci + 1 | bc)
NHEADERS_PLUS_ONE=$(echo $NHEADERS + 1 | bc)
head -n  ${NHEADERS} QUANTI_g${nGen}.dat > DAT_HEADER.temp
tail -n +${NHEADERS_PLUS_ONE} QUANTI_g${nGen}.dat > DAT_GENOTYPES.temp
touch SUBPOP_SIZES.txt
for subpop in $(seq 1 $nPop)
do
  grep ^${subpop} DAT_GENOTYPES.temp > SUBPOP_GREP.temp
  cat DAT_HEADER.temp SUBPOP_GREP.temp > QUANTI_g${nGen}_p${subpop}.dat
  cut -d' ' -f1 SUBPOP_GREP.temp | wc -l >> SUBPOP_SIZES.txt
done
### separate phenotype data of each patch (sub-population) in bash
### NOTE: bug in *.phe file - no grouping based on patches in the first column!
NHEADERS=2
NHEADERS_PLUS_ONE=$(echo $NHEADERS + 1 | bc)
head -n  ${NHEADERS} QUANTI_g${nGen}.phe > PHE_HEADER.temp
tail -n +${NHEADERS_PLUS_ONE} QUANTI_g${nGen}.phe > PHE_PHENOTYPES.temp
Rscript -e "args=commandArgs(trailing=TRUE); PHE_PHENOTYPES_fname=args[1]; nPop_fname=args[2]; dat=read.table(PHE_PHENOTYPES_fname, header=FALSE); popSizes=read.table(nPop_fname, header=FALSE)[,1]; dat[,1]=rep(1:length(popSizes), times=popSizes); write.table(dat, file=PHE_PHENOTYPES_fname, sep='\t', row.names=FALSE, col.names=FALSE)" PHE_PHENOTYPES.temp SUBPOP_SIZES.txt
for subpop in $(seq 1 $nPop)
do
  grep ^${subpop} PHE_PHENOTYPES.temp > SUBPOP_GREP.temp
  cat PHE_HEADER.temp SUBPOP_GREP.temp > QUANTI_g${nGen}_p${subpop}.phe
done
### cleanup
rm *.temp
rm QUANTI_g${nGen}.dat QUANTI_g${nGen}.phe
cd $OUTDIR
### julia parsing script for each subpopulation
for SUBPOP in $(seq 1 $nPop)
do
  julia ${GEN_PRED_SRC_DIR}/quantinemo2_geno_sim_01a_OUTPUTPARSE.jl  ${MOST_RECENT_OUTPUT_DIR}/QUANTI_g${nGen}_p${SUBPOP}.dat \
                                            ${OUTPREFIX}.ini \
                                            Lperenne_genome.spec \
                                            quanti \
                                            QTL.spec \
                                            QTL_min_max_GEBV.spec \
                                            ${nPools}
        ##############
        ### inputs ###
        ##############
        ### (1) dat_fname = quantinemo2 genotype FSTAT file outpu
        ### (2) ini_fname = quantinemo2 initiation file
        ### (3) genome_spec_fname = genome specificications: chromosome length in cM and bp
        ### (4) quanti_vs_ntrl = Did we simulate quantitative or just neutral loci? ("quanti" or "ntrl")
        ### (5) QTL_spec_fname = loci specifications: locus number, alleles, effect or value, mutation freq and initial freq
        ### (6) QTL_min_max_GEBV = minimum and maximum possible genotypic value specification based on the QTL effects
        ### (7) Number of equally sized pools to group individual genotypes into
        ###############
        ### outputs ###
        ###############
        ### (1) Genotype matrix (*_GENO_.csv) (nind x nloci*5) (NO HEADER)
        ### (2) Loci information (*_LOCI_SPEC.csv) (nloci x 3) (HEADER: chromosome, position in cM, position in bp)
        ### (3) Phenotype data (*_PHENO.csv) (nind x 3) (HEADER: individual ID, untransformed phenotypic value, transformed phenotyp value to range from 0-1 based on possible extremes based on allele effects)
        ### (4) Pooling percentiles (*_POOLS_PERCENTILES.csv) (npools+1 x 1) (NO HEADER)
        ### (5) Mean phenotypic values per pool (*_POOLS_PHENO.csv) (npools x 1) (NO HEADER)
        ### (6) Allele frequency matrix (*_POOLS_GENO.csv) (npools x nloci*5) (NO HEADER)
        ### (7) Synchronized pileup file (*_POOLS_GENO.sync) (nloci x npools+3) (NO HEADER)
        ### (8) Pooled phenotype file in tandem with the sync file (*_POOLS_PHENO.py) (6 x 1) (NO HEADER: pheno_name, sig, min, max, perc, & q)
        ### (9) QTL information (*_QTL_SPEC.csv) (nQTL*5 x 4) (HEADER: chromosome,position in bp, allele, effect)
        ### (10) Pileup file format combining individual genotype into a single population of simulated sequencing reads (*_POPULATION.pileup) (nloci x 6) (NO HEADER)
done
### generate synchronized pileup file where each pool is the whole subpopulation --> to simulate Pool-seq per population
ls ${MOST_RECENT_OUTPUT_DIR}/QUANTI_g${nGen}_p*_GENO.csv | grep -v POOL > fname_list.txt
julia ${GEN_PRED_SRC_DIR}/quantinemo2_geno_sim_01b_simPool-seq.jl fname_list.txt $nLoci $nAlleles
        ################
        #### inputs ####
        ################
        #### fname_list_txt = text file listing the filenames of the genotype data output of quantinemo2_geno_sim_01a_OUTPUTPARSE.jl
        #### nLoci = number of loci in the genotype files
        #### nAlleles = number of alleles per loci in the genotype files
        #################
        #### outputs ####
        #################
        #### (1) synchronized pileup file (*_ALLPOP_GENO.sync) (nLoci x nPop+3) (NO HEADER)
echo "#########################################################################################"

#################################################################
#(2) summary statistics
echo "#########################################################################################"
echo "CALCULATING SUMMARY STATISTICS"
#########(2.0) navigate to the output folder and count the number of columns preceeding the R2 estimates
cd ${MOST_RECENT_OUTPUT_DIR}
NCOL_FST_PAIRWISE=$(echo "v1 = $nPop - 1; denom = v1 * $nPop; out = denom / 2; out + 3" | bc) # ( (n(n-1)) / 2 ) + 3 ### the upper triangular plus the replicate, generation and total Fst columns
#########(2.1) individual genotype data
###### LD (extracting from the quantiNemo2 stats output)
cut -f${NCOL_FST_PAIRWISE}- simulation_stats.txt | head -n1 | sed 's/\t/\n/g' > LD_labels.temp
cut -f${NCOL_FST_PAIRWISE}- simulation_stats.txt | tail -n1 | sed 's/\t/\n/g' > LD_data.temp
julia ${GEN_PRED_SRC_DIR}/quantinemo2_geno_sim_02_LDEXTRACT.jl LD_data.temp QUANTI_g${nGen}_p1_LOCI_SPEC.csv
        ##############
        ### inputs ###
        ##############
        ### (1) LD_data.temp = simulation stats R^2 data (and labels) from the quantiNemo2 output
        ### (2) QUANTI_g140_LOCI_SPEC.csv: loci specifications output of quantinemo2_geno_sim_01_OUTPUTPARSE.jl
        ###############
        ### outputs ###
        ###############
        # (1) LD_data_parsed.csv = LD parsed into a matrix (HEADER: CHR, DIST, R2)
        # (2) LD_heatmap.png = LD (R^2)) heatmap
        # (3) LD decay plot i.e. LD_decay.svg
        # (4) LD_decay_R2_20p.csv = distance in kb when R^2 drops to 20% (HEADER: Disance_kb_r2_20)
###### F-satistics
cut -f 1-${NCOL_FST_PAIRWISE} simulation_stats.txt | sed 's/ \+//g' | sed 's/\t/,/g' > F_stats.csv #capture the first 6 columns then remove the sapces and then convert the tabs into commas for a scv file
### output (HEADER: replicate, generation, observeed heterozygosity, Fst (for multiple populations), Fis (intrapopulation inbreeding coefficient), and Fit (inbreeding coefficient across all populations))
#########(2.2) pool data
###### calculate Fst by non-overlapping window
###### popoolation2 does not calculate the Fst across all pools (only pairwise) so
###### I've implemented Weir & Cockerham's (1984) Fst for multiple populations, loci and alleles in Julia1.0
NSAMPLES=$(echo $nIndividuals / $nPools | bc)
echo $NSAMPLES > NSAMPLES.temp
for i in $(seq 2 $nPools)
do
  echo $NSAMPLES >> NSAMPLES.temp
done
sed -i ':a;N;$!ba;s/\n/,/g' NSAMPLES.temp
WINDOWSIZE=1000000 # 1 Mb
STEPSIZE=$WINDOWSIZE
### calculate pool-Fst for each subpopulation
for i in $(seq 1 $nPop)
do
  julia ${GEN_PRED_SRC_DIR}/quantinemo2_geno_sim_03_POOLS_GENO_Fst.jl \
                                            QUANTI_g${nGen}_p${i}_POOLS_GENO.sync \
                                            ${WINDOWSIZE} \
                                            $(cat NSAMPLES.temp)
        ##############
        ### inputs ###
        ##############
        ### (1) synchronized pileup file (nloci x npools+3) (NO HEADER)
        ### (2) size of the window to compute the Fst on (non-overlapping windows; stepsize not implemented at the moment)
        ### (3) size of each pool delimited by comma, e.g. 10,20,50,30
        ###############
        ### outputs ###
        ###############
        ### (1) Fst per chromosom per window (*__window_{window_size}bp_Fst_data.csv) (HEADER: chr,window,Fst)
        ### (2) Fst summary statistics (mean, min, max & var) across chromosomes and windows (*__window_{window_size}bp_Fst_mean.csv) (HEADER: mean_Fst)
done

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
##############################################################################################################################
##############
#####################
#### F R O M    T H I S    P O I N T: we can insert population combinatorial groupings; then perform the steps below for each population grouping (20190814)
######################
##############
##############################################################################################################################
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#########(2.3) bulk population data
echo "Calculating summary statistics on bulk population data."
### Calculate Fst across subpopulations
if [ $nPop > 1 ]
then
  sed ':a;N;$!ba;s/\n/,/g' SUBPOP_SIZES.txt > NSAMPLES.temp
  WINDOWSIZE=1000000 # 1 Mb
  STEPSIZE=$WINDOWSIZE
  julia ${GEN_PRED_SRC_DIR}/quantinemo2_geno_sim_03_POOLS_GENO_Fst.jl \
                                            QUANTI_g${nGen}_ALLPOP_GENO.sync \
                                            ${WINDOWSIZE} \
                                            $(cat NSAMPLES.temp)
else
  echo -e "NULL - 1 POPULATION ONLY" > QUANTI_g${nGen}_ALLPOP_GENO_window_{WINDOWSIZE}bp_Fst_data.csv
  echo -e "NA" > QUANTI_g${nGen}_ALLPOP_GENO_window_{WINDOWSIZE}bp_Fst_sumstats.csv
fi
        ##############
        ### inputs ###
        ##############
        ### (1) synchronized pileup file (*_ALLPOP_GENO.sync) (nLoci x nPop+3) (NO HEADER)
        ### (2) size of the window to compute the Fst on (non-overlapping windows; stepsize not implemented at the moment)
        ### (3) size of each pool delimited by comma, e.g. 10,20,50,30
        ###############
        ### outputs ###
        ###############
        ### (1) Fst per chromosom per window (*_window_{window_size}bp_Fst_data.csv) (HEADER: chr,window,Fst)
        ### (2) Fst summary statistics (mean, min, max & var) across chromosomes and windows (*__window_{window_size}bp_Fst_mean.csv) (HEADER: mean_Fst)
### Calculate summary statistics per chromosome with npstat per subpopulation
for i in $(seq 1 $nPop)
do
  for chrom in 1 2 3 4 5 6 7
  do
      echo "###############################"
      echo "CHROM = $chrom"
      grep ^${chrom} QUANTI_g${nGen}_p${i}_POPULATION.pileup > QUANTI_g${nGen}_p${i}_POPULATION_CHR${chrom}.pileup
      window_size=$(echo $(grep $(echo "^$chrom") ${OUTDIR}/Lperenne_genome.spec | awk '{print $2}') "* 1000000" | bc )
      n=$(echo 2 \* $(head -n$i SUBPOP_SIZES.txt | tail -n1) | bc) #subpoulation size
      /data/Lolium/Softwares/npstat/npstat -n $n \
      -l $window_size \
      -nolowfreq 1 \
      -mincov $n \
      -maxcov $n \
      QUANTI_g${nGen}_p${i}_POPULATION_CHR${chrom}.pileup
      Rscript -e "args=commandArgs(trailing=TRUE); scaffold_name=args[1]; filtered_stats_fname=args[2]; stats=read.table(filtered_stats_fname, header=TRUE); stats = stats[stats\$length > 0, ]; scaff_col=rep(scaffold_name, times=nrow(stats)); out=data.frame(SCAFF=scaff_col, stats); write.table(out, file=filtered_stats_fname, sep='\t', row.names=FALSE, col.names=FALSE)" ${chrom} QUANTI_g${nGen}_p${i}_POPULATION_CHR${chrom}.pileup.stats
      echo "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#"
  done
  echo -e "chrom\twindow\tcoverage\toutgroup_coverage\tdepth\tS\tWatterson_estimator\tTajimas_Pi\tTajimas_D\tFay_Wu_H_unorm\tFay_Wu_H_norm\tS_var\tV_theta\toutgroup_divergence\tnonsynonimous_polymorph\tsynonymous_polymorph\tnonsynonimous_divergence\tsynonymous_divergence\talpha" > QUANTI_g${nGen}_p${i}_POPULATION_NPSTAT.stat
  cat *.pileup.stats >> QUANTI_g${nGen}_p${i}_POPULATION_NPSTAT.stat
  sed -i 's/"//g' QUANTI_g${nGen}_p${i}_POPULATION_NPSTAT.stat
  rm *_POPULATION_CHR?.pileup *.pileup.stats
done
### merging subpopulations into a single NPSTAT file: QUANTI_g${nGen}_ALLPOP_PERCHROM_NPSTAT.csv
ls QUANTI_g${nGen}_p*_POPULATION_NPSTAT.stat > fnames_npstat.temp
Rscript -e "fnames = read.table('fnames_npstat.temp', header=FALSE); for (i in 1:nrow(fnames)) {f = as.character(fnames[i, 1]); pop = strsplit(f, '_')[[1]][3]; npstat = read.table(f, header=TRUE); npstat = cbind(pop=rep(pop, times=nrow(npstat)), npstat); if (exists('OUT') == FALSE) { OUT = npstat } else { OUT = rbind(OUT, npstat) } }; f_split = strsplit(f, '_')[[1]]; out_fname = paste0( paste(f_split[1], f_split[2], 'ALLPOP', 'PERCHROM', 'NPSTAT', sep='_'), '.csv' ); write.table(OUT, file=out_fname, sep=',', row.names=FALSE); MEANS = t(colMeans(OUT[, 4:ncol(OUT)])); means_fname = paste0( paste(f_split[1], f_split[2], 'ALLPOP', 'MEANS', 'NPSTAT', sep='_'), '.csv' ); write.table(MEANS, file=means_fname, sep=',', row.names=FALSE)"
rm *.temp *NPSTAT.stat
# ### summarizing the summary statistics across crhomosomes and windows in R: ### NO NEED SINCE THEY'RE ALREADY PER CHROMOSOME 20190628
echo "#########################################################################################"

###########################################
### POPULATION-LEVEL SUMMARY STATISTICS ###
###########################################
# QUANTI_g140_ALLPOP_GENO_window_1000000bp_Fst_data.csv
# QUANTI_g140_ALLPOP_GENO_window_1000000bp_Fst_sumstats.csv
# QUANTI_g140_ALLPOP_PERCHROM_NPSTAT.csv
# QUANTI_g140_ALLPOP_MEANS_NPSTAT.csv

#################################################################
### testing cross_validation_module.jl
#################################################################
cd ${MOST_RECENT_OUTPUT_DIR}
touch julia_cross_validation_input_file_list.csv
for fname in $(ls QUANTI_g140_p*_GENO.csv | grep -v POOL)
do
  ID=$(echo ${fname%_GENO.csv*})
  N=$(awk -F, '{print NF}' <(head -n1 $fname))
  echo $ID
  echo -e "${ID},individual,,${fname},${ID}_PHENO.csv" >> julia_cross_validation_input_file_list.csv
  # echo -e "${ID},pool-$(echo $N / $nPools | bc),${ID}_POOLS_GENO.sync,${ID}_POOLS_PHENO.py" >> julia_cross_validation_input_file_list.csv
  echo -e "${ID},pool,$(echo $N / $nPools | bc),${ID}_POOLS_GENO.sync,${ID}_POOLS_PHENO.py" >> julia_cross_validation_input_file_list.csv #with pool size column
done

# julia ${GEN_PRED_SRC_DIR}/cross_validation_module.jl julia_cross_validation_input_file_list.csv
