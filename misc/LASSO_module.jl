#############
###       ###
### LASSO ###
###       ###
############# same module name and filename

module LASSO_module

#####################
###				  ###
### load packages ###
###				  ###
#####################
using MultivariateStats
using LinearAlgebra
using Distributions
using Optim
using ProgressMeter
using Statistics
using StatsBase
# using Lasso
using GLMNet #when Lasso fails just setting alpha = 1.0


############################
###						 ###
### function definitions ###
###						 ###
############################

### MY OWN SUCKY LASSO IMPLEMENTATION
### coordinate descent update function of the locus-wise LASSO minimization
function coordinate_descent_update_func(y, X, lambda, b) # no closed form for solving the LASSO coefficients (unlike RR)
	# ########
	# # test:
	# X = hcat(ones(size(X_train)[1]), X_train)
	# y = y_train
	# lambda = 2
	# b = zeros(size(X)[2])
	# ########
	b_out = copy(b)
	l = size(X)[2] #or length(b)
	loci_list = collect(1:l)
	for j in 1:l
		idx_not_j = loci_list[loci_list .!= j]
		# p_j = X[:, j]' * (y .- (X[:, idx_not_j] * b_out[idx_not_j]))
		# z_j = X[:, j]' * X[:, j]
		p_j = sum( X[:, j] .* (y .- (X[:, idx_not_j] * b_out[idx_not_j])) ) # summing and element-wise operations faster than them matrix multiplication
		z_j = sum( X[:, j] .* X[:, j] )
		if p_j  < -lambda
			b_out[j] = (1/z_j) * (p_j + lambda)
		elseif p_j > lambda
			b_out[j] = (1/z_j) * (p_j - lambda)
		else
			b_out[j] = 0
		end
	end
	return(b_out)
end

### coordinate descent
function LASSO_coordinate_descent_func(y, X, lambda, niter=10000, epsilon=0.0001)
	b = zeros(size(X,2))
	b_delta = [0.0]
	for i in 1:niter
		b_new = coordinate_descent_update_func(y, X, lambda, b)
		# b_delta[1] = abs(mean(b .- b_new)) # mean difference
		b_delta[1] = abs(maximum(b .- b_new)) # maximum difference
		# println(i)
		# println(b_delta)
		b[:] = b_new
		if b_delta[1] < epsilon
			# convergence message
			println("Minimum mean difference achieved at ", round(b_delta[1], digits=7), " after ", i, " iterations!")
			break
		end
	end
	# non-convergence message
	if b_delta[1] >= epsilon
		println("Convergence using epsilon=", epsilon, " was not achieved. Terminated after ", niter, " iterations!")
	end
	return(b, b_delta[1])
end

b, b_delta = LASSO_coordinate_descent_func(y, X, 0.0, 1000000, 0.00000001)[1]
using UnicodePlots
UnicodePlots.scatterplot()

# ### Lambda optimisation function (minimise)
# function lambda_optim_func(par, y, X)
# 	lambda = par[1]
# 	b, b_delta = LASSO_coordinate_descent_func(y, X, lambda, 100, 0.0001)
# 	dy = sum((y - (X * b)).^2)/length(y)
# 	return(dy)
# end
# optim_out = Optim.optimize(par -> lambda_optim_func(par, y, X), [0.0], [1.0], [0.0])
# sum(LASSO_coordinate_descent_func(y, X, optim_out.minimizer[1])[1])

#####################
###				  ###
### main function ###
###				  ###
#####################

function LASSO(X_raw, y, MAF; COVARIATE=nothing)
	# # ### test
	# X_raw = X_train
	# y = y_train
	# MAF = 0.01
	# COVARIATE=PC_train[:,1:nPC]
	# ###########
	n = size(X_raw)[1]
	l = size(X_raw)[2]
	### filter loci by MAF
	LOC = collect(1:l)[ ((mean(X_raw, dims=1) ./ 2) .> MAF)[:] .& ((mean(X_raw, dims=1) ./ 2) .< (1.0-MAF))[:] ]
	X_filtered = X_raw[:, LOC]
	### build the intercept and covariate matrix to be concatenated (hcat) to the allele count vector
	if COVARIATE == nothing
		INTERCEPT_AND_COVARIATE = ones(n)
	else
		INTERCEPT_AND_COVARIATE = hcat(ones(n), COVARIATE)
	end
	### concatenate the intercept or/and covariates with the genotype datas
	X = hcat(INTERCEPT_AND_COVARIATE, X_filtered)
	### fit!
	LASSO_paths = GLMNet.glmnetcv(X, y, alpha=1.0)
	beta = LASSO_paths.path.betas[:, argmin(LASSO_paths.meanloss)]
	# INTERCEPT = beta[1]
	# EFF = beta[2:end]
	INTCOVAR_EFF = beta[1:size(INTERCEPT_AND_COVARIATE, 2)]
	EFF = beta[(size(INTERCEPT_AND_COVARIATE, 2)+1):end]
	PVAL = ones(length(LOC))
		idx_less_than_mean = EFF .< mean(EFF)
		idx_more_than_mean = EFF .> mean(EFF)
		PVAL[idx_less_than_mean] = 2 .* ( Distributions.cdf.(Distributions.Normal(mean(EFF), std(EFF)), EFF[idx_less_than_mean]) )
		PVAL[idx_more_than_mean] = 2 .* ( 1 .- Distributions.cdf.(Distributions.Normal(mean(EFF), std(EFF)), EFF[idx_more_than_mean]) )
	LOD = -log10.(PVAL)
	return(LOC, INTCOVAR_EFF, EFF, PVAL, LOD)
end

end #end of LASSO module
