using GWAlpha
using DelimitedFiles
using RCall
using Random

# ARGS = ["CLETH"]
HERBI = ARGS[1]

sync_fname = string("MERGED_", HERBI, "_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync")
geno_fname = string("MERGED_", HERBI, "_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv")
phen_fname = string("MERGED_", HERBI, "_pheno.csv")
cova_fname = string("MERGED_", HERBI, "_MAPQ20_BASQ20_MAF0.001_DEPTH50_COVARIATE_FST.csv")

SYNC = DelimitedFiles.readdlm(sync_fname, '\t'); GWAlpha.sync_processing_module.sync_parse(sync_fname)
GENO = DelimitedFiles.readdlm(geno_fname, ',')
PHEN = DelimitedFiles.readdlm(phen_fname, ',')
COVA = DelimitedFiles.readdlm(cova_fname, ',')
n = size(PHEN,1)
p = size(GENO,1)

REP = []
FOLD = []
MODEL = []
CORR = []
R2 = []
MAD = []
RMSE = []
k = 5 ### k-fold cross-validation
r = 20 ### r replications
VALID_size = Int(floor(n/k))
TRAIN_size = n - VALID_size
for r in 1:r
  idx_shuffled = Random.randperm(n)
  @time for i in 1:k
      ### divide into n-1 training datapoints and 1 validation datapoint
      _init_ = ((i-1)*VALID_size) + 1
      _slut_ = i*VALID_size
      VALID_idx = idx_shuffled[_init_:_slut_]
      TRAIN_idx = idx_shuffled[[sum(x .== VALID_idx) == 0 for x in idx_shuffled]]
      TRAIN_SYNC = hcat(SYNC[:, 1:3], SYNC[:, TRAIN_idx .+ 3])
      TRAIN_GENO = hcat(GENO[:, 1:3], GENO[:, TRAIN_idx .+ 3])
      TRAIN_PHEN = PHEN[TRAIN_idx, :]
      TRAIN_COVA = COVA[TRAIN_idx, TRAIN_idx]
      VALID_SYNC = hcat(SYNC[:, 1:3], SYNC[:, VALID_idx .+ 3])
      VALID_GENO = hcat(GENO[:, 1:3], GENO[:, VALID_idx .+ 3])
      VALID_PHEN = reshape(PHEN[VALID_idx, :], VALID_size, 2)
      ### filenames
      TRAIN_SYNC_fname = string("MERGED_", HERBI, "_TRAIN_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync")
      TRAIN_GENO_fname = string("MERGED_", HERBI, "_TRAIN_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv")
      TRAIN_PHEN_fname = string("MERGED_", HERBI, "_TRAIN_pheno.csv")
      TRAIN_COVA_fname = string("MERGED_", HERBI, "_TRAIN_MAPQ20_BASQ20_MAF0.001_DEPTH50_COVARIATE_FST.csv")
      VALID_SYNC_fname = string("MERGED_", HERBI, "_VALID_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync")
      VALID_GENO_fname = string("MERGED_", HERBI, "_VALID_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv")
      VALID_PHEN_fname = string("MERGED_", HERBI, "_VALID_pheno.csv")
      ### write-out
      DelimitedFiles.writedlm(TRAIN_SYNC_fname, TRAIN_SYNC)
      DelimitedFiles.writedlm(TRAIN_GENO_fname, TRAIN_GENO, ',')
      DelimitedFiles.writedlm(TRAIN_PHEN_fname, TRAIN_PHEN, ',')
      DelimitedFiles.writedlm(TRAIN_COVA_fname, TRAIN_COVA, ',')
      DelimitedFiles.writedlm(VALID_SYNC_fname, VALID_SYNC)
      DelimitedFiles.writedlm(VALID_GENO_fname, VALID_GENO, ',')
      DelimitedFiles.writedlm(VALID_PHEN_fname, VALID_PHEN, ',')
      ### MIXED-REML-WEIRCOCK
      @time GWAlpha.PoolGPAS(filename_sync=TRAIN_SYNC_fname, filename_phen=TRAIN_PHEN_fname, maf=0.001, depth=50, model="MIXED", filename_random_covariate=TRAIN_COVA_fname, varcomp_est="REML")
      ### RIDGE
      @time GWAlpha.PoolGPAS(filename_sync=TRAIN_SYNC_fname, filename_phen=TRAIN_PHEN_fname, maf=0.001, depth=50, model="GLMNET", glmnet_alpha=0.00)
      ### LASSO
      @time GWAlpha.PoolGPAS(filename_sync=TRAIN_SYNC_fname, filename_phen=TRAIN_PHEN_fname, maf=0.001, depth=50, model="GLMNET", glmnet_alpha=1.00)
      ### validation
      BETA_MIXED_fname = string("MERGED_", HERBI, "_TRAIN_MAPQ20_BASQ20_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv")
      BETA_RIDGE_fname = string("MERGED_", HERBI, "_TRAIN_MAPQ20_BASQ20_MAF0.001_DEPTH50-GLMNET_ALPHA0.0-OUTPUT.csv")
      BETA_LASSO_fname = string("MERGED_", HERBI, "_TRAIN_MAPQ20_BASQ20_MAF0.001_DEPTH50-GLMNET_ALPHA1.0-OUTPUT.csv")
      for f in [BETA_MIXED_fname, BETA_RIDGE_fname, BETA_LASSO_fname]
          model = split(f, "-")[2]
          BETA = DelimitedFiles.readdlm(f, ',')
          chrom=BETA[2:end,1]; pos=BETA[2:end,2]; allele=BETA[2:end,3]; effect=BETA[2:end,5]; intercept=BETA[2,5]; y_true=VALID_PHEN[:,2]
          @rput chrom; @rput pos; @rput allele; @rput effect; @rput intercept; @rput VALID_GENO; @rput y_true;
          R"BETA = data.frame(chrom=unlist(as.character(chrom)), pos=unlist(as.numeric(pos)), allele=unlist(as.character(allele)), effect=unlist(as.numeric(effect)))
            VALID_GENO = as.data.frame(VALID_GENO)
            colnames(VALID_GENO) = c('chrom', 'pos', 'allele')
            VALID_GENO$chrom = as.character(VALID_GENO$chrom)
            VALID_GENO$pos = as.numeric(VALID_GENO$pos)
            VALID_GENO$allele = as.character(VALID_GENO$allele)
            MERGED = as.data.frame(merge(BETA, VALID_GENO, by=c('chrom', 'pos', 'allele')))
            y_pred = intercept + (matrix(unlist(MERGED[,5:ncol(MERGED)]), byrow=TRUE, nrow=length(y_true)) %*% MERGED[,4])
            corr = cor(y_true, y_pred)
            r2 = summary(lm(y_true~y_pred))$r.sq
            mad = mean(abs(y_true-y_pred))
            rmse = sqrt(mean((y_true-y_pred)^2))
          "
          @rget corr
          @rget r2
          @rget mad
          @rget rmse
          ### outputs
          push!(REP, r)
          push!(FOLD, i)
          push!(MODEL, model)
          push!(CORR, corr)
          push!(R2, r2)
          push!(MAD, mad)
          push!(RMSE, rmse)
      end
      ### clean-up
      rm(TRAIN_SYNC_fname)
      rm(TRAIN_GENO_fname)
      rm(TRAIN_PHEN_fname)
      rm(TRAIN_COVA_fname)
      rm(VALID_SYNC_fname)
      rm(VALID_GENO_fname)
      rm(VALID_PHEN_fname)
      rm(BETA_MIXED_fname); rm(string("MERGED_", HERBI, "_TRAIN_MAPQ20_BASQ20_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-RANEF-OUTPUT.csv"))
      rm(BETA_RIDGE_fname)
      rm(BETA_LASSO_fname)
  end ### cross-validations
end ### replications

### OUTPUT
using DataFrames
using CSV
OUT = DataFrames.DataFrame(REP=REP, FOLD=FOLD, MODEL=MODEL, CORR=CORR, R2=R2, MAD=MAD, RMSE=RMSE)
OUT_fname = string("MERGED_", HERBI, "_", k, "-FOLDCV.csv")
CSV.write(OUT_fname, OUT)
