#############################
###		   			      ###
### HDF5 SNP data parsing ###
###		   			      ###
#############################

# #######################
# ### 1001_SNP_MATRIX ###
# #######################
# ### Downloading Arabidopsis SNP datasets for testing GWAS vs GWAlpha-ML vs GWAlpha-MCMC vs RR-Poolseq
# ### VM unimelb home directory
# cd /data/Lolium/Quantitative_Genetics/At_empirical/SNP_variants
# GMI-MPI v3.1
# wget http://1001genomes.org/data/GMI-MPI/releases/v3.1/SNP_matrix_imputed_hdf5/1001_SNP_MATRIX.tar.gz
# wget http://1001genomes.org/data/GMI-MPI/releases/v3.1/SNP_matrix_imputed_hdf5/hdf5_demo.py
# tar -xvzf 1001_SNP_MATRIX.tar.gz
# rm 1001_SNP_MATRIX.tar.gz
# ###################
# ### panzea_SNPs ###
# ###################
# ### register to cyverse: https://user.cyverse.org/register
# ### confirm account
# ### download iCommands: wget ftp://ftp.renci.org/pub/irods/releases/4.1.10/ubuntu14/irods-icommands-4.1.10-ubuntu14-x86_64.deb
# ### install iCommands: sudo dpkg -i irods-icommands-4.1.10-ubuntu14-x86_64.deb; sudo apt install -f
# ### login to cyVerse and browse: http://datacommons.cyverse.org/browse/iplant/home/shared/panzea
# ### setup iCommands: $ iinit
# ###						data.cyverse.org
# ###						1247
# ###						jparil
# ###						iplant
# ###						qwertyuiop0987654321
# ### dowload the GBS genotype data v2.7: iget /iplant/home/shared/panzea/genotypes/GBS/v27/ZeaGBSv27_publicSamples_imputedV5_AGPv4-181023.h5

#######################################################################################################################################################
### Fix the original 1001 genomes SNPs file with "hdf5_SNP_fix_for_julia.py"																		###
### This is because julia's HDF5 library cannot read the binary SNPs data from the original hdf5 file: "1001_SNP_MATRIX/imputed_snps_binary.hdf5"	###
### i.e. ./hdf5_SNP_fix_for_julia.py 																												###
#######################################################################################################################################################

module hdf5_SNP_parsing_module

### load libraries
using HDF5
using StatsKit
# using FreqTables
using ProgressMeter
using JLD2

### parsing for the 1001 genomes HDF5 file
### INPUT: 1001 genomes HDF5 SNP filename
### OUTPUT: extracted accession names, chrom, pos and genotype data
### SAMPLE: extract_SNP_info("imputed_snps_binary.hdf5")
function extract_SNP_info(fixed_hdf5_file)
	X_hdf5 = HDF5.h5open(fixed_hdf5_file, "r")
	names(X_hdf5)
	acc = HDF5.read(X_hdf5["accessions"])
	chrom = HDF5.read(X_hdf5["chromosomes"])
	pos = HDF5.read(X_hdf5["positions"])
	X = (HDF5.read(X_hdf5["snps"]))
	close(X_hdf5)
	return([acc, chrom, pos, X])
end

### parsing for the panzea HDF5 file
### INPUT: panzea hdf5 filename
### OUTPUT: julia jld2 file with same filename but with .jld2 extension
### SAMPLE: extract_SNP_info_PANZEA("ZeaGBSv27_publicSamples_imputedV5_AGPv4-181023.h5")
function extract_SNP_info_PANZEA(panzea_hdf5_file)
	#tests:
	# panzea_hdf5_file = "ZeaGBSv27_publicSamples_imputedV5_AGPv4-181023.h5"
	f = HDF5.h5open(panzea_hdf5_file, "r")
	names(f)
	X_temp = HDF5.read(f["Genotypes"])

	### taxa
	taxa = fill(convert(String, "abc"), length(keys(X_temp)))
	i = [1] # I bloody hate julia's scoping, characters and strings!!!
	for k in keys(X_temp)
		# println(k)
		# println(String(join(k)))
		tmp = String(join(k))
		println(tmp)
		# append!(taxa, tmp)
		taxa[i[1]] = tmp
		i[1] = i[1] + 1
	end

	### chrom,  pos and reference alleles
	chrom = HDF5.read(f["Positions"])["ChromosomeIndices"]
	pos = HDF5.read(f["Positions"])["Positions"]
	ref = HDF5.read(f["Positions"])["ReferenceAlleles"]

	### X
	n = length(taxa)
	l = length(X_temp[taxa[1]]["calls"])
	X = zeros(UInt8, (n, l))
	# X = Array{Union{Missing, UInt8}}(missing, n, l)

	progress_bar = ProgressMeter.Progress(n, dt=1, barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow) #progress bar
	for i in 1:n
		if (taxa[i] != "AlleleStates") && (taxa[i] != "_Descriptors")
			x0 = X_temp[taxa[i]]["calls"]
			x1 = fill(convert(UInt8, 255), length(x0)) ### because geting error with NN: -1
			x1[x0 .>  -1] = convert(Array{UInt8}, x0[x0 .> -1])
			# sequence every signed 8-bits or every 16th number of base 8: ACGT+-ZN
			# A_: 0x00-0x0f: AA AC AG AT A+ A- AZ AN
			# C_: 0x10-0x1f: CA CC CG CT C+ C- CZ CN
			# G_: 0x20-0x2f: GA GC GG GT G+ G- GZ GN
			# T_: 0x30-0x3f: TA TC TG TT T+ T- TZ TN
			# +_: 0x40-0x4f: +A +C +G +T ++ +- +Z +N
			# -_: 0x50-0x5f: -A -C -G -T -+ -- -Z -N
			# Z_: 0xe0-0xef: ZA ZC ZG ZT Z+ Z- ZZ ZN
			# N_: 0xf0-0xff: NA NC NG NT N+ N- NZ NN
			X[i, :] = x1
		end
		# println(i)
		ProgressMeter.update!(progress_bar, i)
	end
	close(f)

	# summarize into 0, 1, 2 matrix assuming biallelic loci everywhere
	### BUT BUT BUT!!! WHAT TO DO WITH Ns AND Zs AND MULTIPLE ALLELES?!?!?!
	### NEW PLAN: use ref::: count number of times ref is and set 0xff as missing and of course all the while accounting for heterozygotes

	function bit8_to_refcount(xij, refj)
		# missing
		if xij == 0xff
			out = 0
		# non-missing
		else
			# first allele
			if floor(xij / 0x10) == refj
				out = 1
			else
				out = 0
			end
			# second allele
			if (xij % 0x10) == refj
				out = out + 1
			end
		end
		return(out)
	end

	progress_bar = ProgressMeter.Progress(n*l, dt=1, barglyphs=BarGlyphs("[=> ]"), barlen=50, color=:yellow); counter=[1]
	for i in 1:n
		for j in 1:l
			xij = X[i,j]
			X[i,j] = bit8_to_refcount(xij, ref[j])
			ProgressMeter.update!(progress_bar, counter[1]); counter[1] = counter[1] + 1
		end
	end
	@save join([split(panzea_hdf5_file, ".")[1], ".jld2"]) taxa chrom pos ref X
	return([taxa, chrom, pos, ref, X])
end

end # end of hdf5_SNP_parsing_module

### sample execution:
# julia
# include("hdf5_SNP_parsing_module.jl")
# taxa, chrom, pos, ref, X = hdf5_SNP_parsing_module.extract_SNP_info_PANZEA("ZeaGBSv27_publicSamples_imputedV5_AGPv4-181023.h5")
