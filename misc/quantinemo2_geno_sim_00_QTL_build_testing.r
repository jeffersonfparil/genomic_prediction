### inputs
nLoci = 2000
nQTL = 10
nAlleles = 5
allele_dist = read.csv('Allele_counts_dist.spec', header=FALSE)
### prepare ID columns i.e. locus ID, and allele ID
col_locus = rep(1:nLoci, each=nAlleles)
col_allele = rep(1:nAlleles, times=nLoci)
### setting mutation rate at zero
col_mut_freq = rep(c(0), times=nLoci*nAlleles)
### setting initial allle frequencies across all loci - simplistically as 1/nAlleles
## creating an artificial list of sparsity vectors to simulate allele count distribution for a speedy algorithm??! 20190628
n_possible_sparsity_designs = 100
sparsity_list = c()
for (i in 2:nAlleles) { #do not simulate monomorphic sites
  for (j in 1:round(allele_dist[i,2]*n_possible_sparsity_designs)) {
    sparse_vec = c(rep(0, time=nAlleles-i), rep(1, times=i))
    sparse_vec = sparse_vec[order(runif(nAlleles))]
    sparsity_list = c(sparsity_list, sparse_vec)
  }
}
sparsity_df = matrix(sparsity_list, ncol=nAlleles, byrow=TRUE)
ALLELE_COUNTS_MAT = sparsity_df[sample(1:nrow(sparsity_df), size=nLoci, replace=TRUE), ]
ALLELE_COUNTS_VEC = matrix(t(ALLELE_COUNTS_MAT), nrow=1)
col_ini_freq = rep(1/rowSums(ALLELE_COUNTS_MAT), each=nAlleles) * ALLELE_COUNTS_VEC[1,]
### select the QTL wherein each QTL is composed of nAlleles alleles with effects sampled from a chi-square distribution
col_allelic_value = rep(c(0), times=nLoci*nAlleles)
idxQTL = sample(1:nLoci, nQTL, replace=FALSE) * nAlleles
idxQTL = rep(idxQTL, each=nAlleles)
idxQTL = idxQTL - rep((nAlleles-1):0, times=nQTL)
idxQTL_nonZero_iniFreq = col_ini_freq[idxQTL] != 0
nQTL_nonZero_iniFreq = sum(idxQTL_nonZero_iniFreq)
col_allelic_value[idxQTL[idxQTL_nonZero_iniFreq]] = round(rchisq(n=nQTL_nonZero_iniFreq, df=1),2)
### merge the columns into the output dataframe and write the outputs
out = data.frame(col_locus, col_allele, col_allelic_value, col_mut_freq, col_ini_freq)
write.table(out, file='QTL.spec', col.names=FALSE, row.names=FALSE, append=TRUE, sep='\t')
### MISC: find minimum and maximum possible additive genetic values
sub = out[(out$col_allelic_value != 0), ]
MIN_GEBV = 2*sum(aggregate(sub$col_allelic_value ~ sub$col_locus, FUN=min)[,2])
MAX_GEBV = 2*sum(aggregate(sub$col_allelic_value ~ sub$col_locus, FUN=max)[,2])
write.table(data.frame(MIN_GEBV, MAX_GEBV), file='QTL_min_max_GEBV.spec', row.names=FALSE, sep='\t')


# ### getting empirical distribution of allele counts from empirical data i.e. *_ALLELEFREQ.csv data
# ### set working dierctory
# setwd("/data/Lolium/Quantitative_Genetics/Genomic_prediction_2018Lolium_50s/")
# ### list all *_ALLELEFREQ.csv files parsed from the sync files
# flist = list.files(pattern="_ALLELEFREQ.csv")
# ### prepare columns of allele counts frequencies (6 possible SNP alleles: A,T,C,G,N, and DEL)
# ALLELE_COUNT_DIST = matrix(0, nrow=length(flist), ncol=6)
# colnames(ALLELE_COUNT_DIST) = c("ALLELE_COUNT_1", "ALLELE_COUNT_2", "ALLELE_COUNT_3", "ALLELE_COUNT_4", "ALLELE_COUNT_5", "ALLELE_COUNT_6")
# ### iterate across the parsed sync files
# for (i in 1:length(flist)) {
#   f = flist[i]
#   # ### test
#   # f="IF_ALLELEFREQ.csv"
#   ### extract loci names as scaffold name + pos
#   print("+++++++++++++++++++++++++++++++++++++")
#   print(f)
#   ### counting the number of unique loci
#   # system(paste0("f=", f, "; cut -d, -f1-2 $f > ${f%.*}_LOCI.temp; sed -i 's/,/_/g' ${f%.*}_LOCI.temp; sed -i 's/|/-/g' ${f%.*}_LOCI.temp"))
#   # dat = read.delim(paste0(strsplit(f, "[.]")[[1]][1], "_LOCI.temp"), header=FALSE)
#   # TABLE = table(dat$V1)
#   # for (j in 1:6) {
#   #   freq = sum(TABLE == j)/length(TABLE)
#   #   ALLELE_COUNT_DIST[i, j] = freq
#   # }
#   ### alternative method counting the uique loci filtered by MAF=1/2n=1/1000=0.001
#   MAF = 0.001
#   dat = read.csv(f, header=FALSE)
#   colnames(dat) = c("SCAF", "POS", "ALLELE", "POOL1_FREQ", "POOL2_FREQ", "POOL3_FREQ", "POOL4_FREQ", "POOL1_FREQ")[1:ncol(dat)]
#   idx_MAF_filter = tryCatch({
#     (rowMeans(dat[, 4:ncol(dat)]) >= MAF) & ((1 - rowMeans(dat[, 4:ncol(dat)])) < (1-MAF))
#   }, error = function(e){
#     (dat[,4] >= MAF) & ((1-dat[,4]) < (1-MAF))
#   })
#   dat_filtered = dat[idx_MAF_filter, ]
#   UNIQ_ID = paste(dat_filtered$SCAF, dat_filtered$POS, sep="_")
#   TABLE = table(unlist(table(UNIQ_ID)))
#   ALLELE_COUNT_DIST[i, as.numeric(names(TABLE))] = TABLE
# }
# ALLELE_COUNTS_PER_LOCUS_FREQUENCY = as.matrix(colMeans(ALLELE_COUNT_DIST / rowSums(ALLELE_COUNT_DIST), na.rm=TRUE))
# write.table(ALLELE_COUNTS_PER_LOCUS_FREQUENCY, "Allele_counts_distribution.csv", sep=",", col.names=FALSE)
