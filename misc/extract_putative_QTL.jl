using DataFrames
using DelimitedFiles
using CSV
using Statistics
using Distributions
# using UnicodePlots

DIR = ARGS[1]
cd(DIR)
LOCI_FNAME = ARGS[2]
correlation_threshold = parse(Float64, ARGS[3])
BETA_FNAME = ARGS[4]
STATS_FNAME = ARGS[5]
PLOT=parse(Bool, ARGS[6])
# DIR = "/data/Lolium/Quantitative_Genetics/GPWAS_60_SEAUS"
# cd(DIR)
# LOCI_FNAME = "PC1_X_COVARIATE/LOCI_info.csv"
# correlation_threshold = parse(Float64, "0.95")
# BETA_FNAME = "BETAS-PC1_X_COVARIATE-CLETHODIM_RESISTANCE-FIXED_LS.csv"
# STATS_FNAME = "STATS-PC1_X_COVARIATE-CLETHODIM_RESISTANCE-FIXED_LS.csv"
# PLOT = false

println("Loading alelelic effects across cross-validation iterations...")
BETA = DelimitedFiles.readdlm(BETA_FNAME, ',')
println("Loading statistics across cross-validation iterations...")
STATS = CSV.read(STATS_FNAME)
println("Loading loci identities...")
LOCI = try
    CSV.read(LOCI_FNAME)
catch
    CSV.read(string(DIR, "/", LOCI_FNAME))
end
println("Extracting putative QTL...")
########################################################
### FILTERING BY MODEL AND/OR STATS VALUE THRESHOLDS ###
########################################################
# idx = (STATS.MODEL .== model) .& (STATS.CORR .>= correlation_threshold)
idx = (STATS.CORR .>= correlation_threshold)
idx[ismissing.(idx)] .= false
idx = convert(Array{Bool, 1}, idx)
##########################
### BETA SUMAMRY STATS ###
##########################
MEAN_BETA = mean(BETA[idx, (size(BETA,2)-size(LOCI,1)+1):end], dims=1)[1,:] ### include only the genomic loci (i.e. exclude the intercept and covariate/s)
VAR_BETA = var(BETA[idx, (size(BETA,2)-size(LOCI,1)+1):end], dims=1)[1,:]   ### include only the genomic loci (i.e. exclude the intercept and covariate/s)
Z_BETA = MEAN_BETA ./ VAR_BETA
LOD_BETA = zeros(length(MEAN_BETA))
normal_dist = Distributions.Normal(mean(MEAN_BETA), std(MEAN_BETA))
idx_totheleftofmean = MEAN_BETA .<= mean(MEAN_BETA)
idx_totherightofmean = MEAN_BETA .>= mean(MEAN_BETA)
LOD_BETA[idx_totheleftofmean] .= -1 .* log.(10, Distributions.cdf.(normal_dist, MEAN_BETA[idx_totheleftofmean]))
LOD_BETA[idx_totherightofmean] .= -1 .* log.(10, Distributions.ccdf.(normal_dist, MEAN_BETA[idx_totherightofmean]))
###############
### MAIN DF ###
###############
DF = DataFrames.DataFrame(CHROM=LOCI.CHROM, POS=LOCI.POS, ALLELE=LOCI.ALLELE, MEAN=MEAN_BETA, VAR=VAR_BETA, LOD=LOD_BETA)
#########################
### LOCI FOR BLASTING ###
#########################
# lod_threshold = -log(10, 1.0*10^(-maximum(LOD_BETA)/2)/(size(BETA,1)*size(BETA,2)))
lod_threshold = -log(10, (10^(-6))/size(BETA,2))
OUT = DF[DF.LOD .>= lod_threshold, :]
########################
### OUTPUT AND PLOTS ###
#######################
fname_out = string("PUTATIVE_QTL-", BETA_FNAME)
CSV.write(fname_out, OUT)
# UnicodePlots.scatterplot(MEAN_BETA)
# UnicodePlots.histogram(MEAN_BETA)
# UnicodePlots.scatterplot(VAR_BETA)
# UnicodePlots.scatterplot(Z_BETA)
# UnicodePlots.scatterplot(LOD_BETA)
# UnicodePlots.scatterplot(abs.(MEAN_BETA), LOD_BETA) ### THE abs.(BETAS) ARE POSITIVELY RELATED TO THE Z and LOD VALUES!!!!
if PLOT == true
    using Plots; Plots.pyplot()
    p = Plots.plot(LOD_BETA, seriestype=:scatter, label="LOD Scores")
    Plots.plot!(p, [1,length(LOD_BETA)], [lod_threshold, lod_threshold], line=(:line, :dash, 0.5, 2, :red), label="LOD Threshold")
    Plots.savefig(string("MANHATTAN_", join(split(fname_out, '.')[1:(end-1)], '.'), ".svg"))
end
