HERBI = ARGS[1]
# HERBI="GLYPHOSATE" ### RUN ME NEXT: 20200507-12:27:40
using Distributed
Distributed.addprocs(length(Sys.cpu_info())-1)
@everywhere using GWAlpha
using DelimitedFiles
using DataFrames
using CSV
using RCall
SYNC = DelimitedFiles.readdlm("MERGED_ALL_MAPQ20_BASQ20_MAF0.001_DEPTH50.sync", '\t')
PHEN, header = DelimitedFiles.readdlm("MERGED_ALL.phe", ',', header=true)

idx_train = collect(1:size(PHEN,1))[PHEN[:,7] .== HERBI]
idx_valid_no_leak = collect(1:size(PHEN,1))[ (PHEN[:,7] .!= HERBI) .& [sum(PHEN[idx_train, 4] .== x)==0 for x in PHEN[:,4]] ]
### prep GWAlpha inputs
fname_train_sync = string(HERBI, "_TRAIN.sync")
fname_train_phen = string(HERBI, "_TRAINp.csv")
fname_valid_sync = string(HERBI, "_VALID.sync")
fname_valid_phen = string(HERBI, "_VALIDp.csv")
DelimitedFiles.writedlm(fname_train_sync, SYNC[:, vcat([1,2,3], [idx_train .+ 3]...)], '\t')
DelimitedFiles.writedlm(fname_train_phen, PHEN[idx_train, [6,9]], ',')
DelimitedFiles.writedlm(fname_valid_sync, SYNC[:, vcat([1,2,3], [idx_valid_no_leak .+ 3]...)], '\t')
DelimitedFiles.writedlm(fname_valid_phen, PHEN[idx_valid_no_leak, [6,9]], ',')
### model building (MIXED-REML-FST-WEIRCOCK)
GWAlpha.PoolGPAS(filename_sync=fname_train_sync, filename_phen=fname_train_phen, model="MIXED", random_covariate="FST", varcomp_est="REML")
### auto-cross validating as a test
fname_train_beta = string(HERBI, "_TRAIN_MAF0.001_DEPTH1-MIXEDREML_FST-OUTPUT.csv")
fname_train_geno = string(HERBI, "_TRAIN_MAF0.001_DEPTH1_ALLELEFREQ.csv")
BETA = CSV.read(fname_train_beta)
GENO_train = CSV.read(fname_train_geno, header=false)
@rput BETA; @rput GENO_train;
R"colnames(GENO_train) = c('CHROM', 'POS', 'ALLELE', paste0('SAMPLE_', 1:(ncol(GENO_train)-3)))
MERGED = droplevels(merge(BETA, GENO_train, by=c('CHROM', 'POS', 'ALLELE')))
beta = MERGED$ALPHA
X = MERGED[, 8:ncol(MERGED)]
y_pred = BETA$ALPHA[BETA$CHROM=='Intercept'] + (t(as.matrix(X)) %*% beta)
"
@rget y_pred
y_pred = y_pred[:,1]
y_true = DelimitedFiles.readdlm(fname_train_phen, ',')[:,2]
using UnicodePlots
UnicodePlots.scatterplot(y_true, y_pred)
### proper prediction
fname_train_beta = string(HERBI, "_TRAIN_MAF0.001_DEPTH1-MIXEDREML_FST-OUTPUT.csv")
fname_valid_geno = string(HERBI, "_VALID_ALLELEFREQ.csv")
BETA = CSV.read(fname_train_beta)
GWAlpha.sync_processing_module.sync_parse(fname_valid_sync)
GENO_valid = CSV.read(fname_valid_geno, header=false)
@rput BETA; @rput GENO_valid;
R"colnames(GENO_valid) = c('CHROM', 'POS', 'ALLELE', paste0('SAMPLE_', 1:(ncol(GENO_valid)-3)))
MERGED = droplevels(merge(BETA, GENO_valid, by=c('CHROM', 'POS', 'ALLELE')))
beta = MERGED$ALPHA
X = MERGED[, 8:ncol(MERGED)]
y_pred = BETA$ALPHA[BETA$CHROM=='Intercept'] + (t(as.matrix(X)) %*% beta)
"
@rget y_pred
### ouput
fname_output = string(HERBI, "_PREDICTIONS.csv")
YEAR = PHEN[idx_valid_no_leak, 1]
SAMPLING = PHEN[idx_valid_no_leak, 2]
POP_ID = PHEN[idx_valid_no_leak, 3]
POPULATION = PHEN[idx_valid_no_leak, 4]
POOL = PHEN[idx_valid_no_leak, 5]
POOL_SIZE = PHEN[idx_valid_no_leak, 6]
PREDICTED_HERBICIDE = repeat([HERBI], length(y_pred))
PREDICTED_SURVIVAL_RATE = y_pred[:,1]
OUTPUT = DataFrames.DataFrame(YEAR=YEAR, SAMPLING=SAMPLING, POP_ID=POP_ID, POPULATION=POPULATION, POOL=POOL, POOL_SIZE=POOL_SIZE, PREDICTED_HERBICIDE=PREDICTED_HERBICIDE, PREDICTED_SURVIVAL_RATE=PREDICTED_SURVIVAL_RATE)
CSV.write(fname_output, OUTPUT)
