##########################################################################
###																	   ###
### Compare GWAS-GBLUP, GWAlpa-GBLUP, PLS, LASSO, RR, Basyesian models ###
###																	   ###
##########################################################################

#####################
###				  ###
### load packages ###
###				  ###
#####################
using RData
using Distributions
using DataFrames
using Plots; pyplot()
using ProgressMeter

####################
###				 ###
### Load modules ###
###				 ###
####################
include("src/simulate_phenotypes_module.jl")
include("src/GWAS_module.jl")
include("src/GWAlpha_module.jl")
include("src/RR_POOLSEQ_module.jl")

#####################
###				  ###
### main function ###
###				  ###
#####################
function parametric_models_module(ARGS)
	### capture the input filename_sync
	if length(ARGS)!=2
		println("Please specify the sync allele counts and phenotype files :-)")
		exit()
	end
	filename_X = ARGS[1] #genotype data

	#########################################################################
	#########################################################################
	#########################################################################
	######## TEST ########
	#########################################################################
	#########################################################################
	#########################################################################
	# using RData
	# using Distributions
	# using DataFrames
	# using Plots; pyplot()
	# using ProgressMeter
	# using DelimitedFiles
	# using ColorBrewer
	
	# include("src/GWAS_module.jl")

	# filename_X = "res/At_134Salk_SNP.rds"
	# n_QTL = 10
	# MAF = 0.03
	# heritability = 0.50

	# #### TEST GWAS on individual plant data
	# ## load individual line genotype data
	# X = RData.load(filename_X)
	# n = size(X)[1]
	# l = size(X)[2]
	# filename_y = "res/At_phenotype_FT10.rds"
	# ## load FT10 phenotype data
	# y = RData.load(filename_y)
	# y = y.Phenotype
	# LOC, EFF, PVAL, LOD = GWAS_module.GWAS(X, y, MAF)

	# ## extact loci data from the sync file
	# sync = DelimitedFiles.readdlm("res/GWAlpha_X.sync", '\t')
	# # OUT = DataFrames.DataFrame(CHROM=sync[:,1], POS=sync[:,2], ALLELE=ALLELE_ID, FREQ=ALLELE_FREQ, ALPHA=ALPHA_OUT, PVALUES=P_VALUES, LOD=LOD)
	# OUT = DataFrames.DataFrame(CHROM=sync[LOC,1], POS=sync[LOC,2], EFFECT=EFF, PVAL=PVAL, LOD=LOD)
	# CSV.write(string("GWAS-", filename[length(filename)-1], "-OUT.csv"), OUT)
	# ## manhattan plot
	# LOD_plot = Plots.plot([1, length(LOD)], [0, maximum(LOD)], 
	# 	seriestype=:scatter, 
	# 	marker=(:circle, 5, 0.5, :White, Plots.stroke(0, :white)), 
	# 	xlabel="SNP ID", 
	# 	ylabel="LOD", 
	# 	legend=false, 
	# 	size=(1200, 400))
	# contigs = unique(OUT.CHROM)
	# colours = ColorBrewer.palette("Pastel1", length(contigs))
	# colours_lab = ColorBrewer.palette("Set1", length(contigs))
	# i_loc = [0, 0] # counter and locus position
	# for contig in contigs
	# 	subset_LOD = OUT[OUT.CHROM .== contig, :LOD]
	# 	x0 = (i_loc[2]+1)
	# 	x1 = (i_loc[2]+length(subset_LOD)) 
	# 	Plots.plot!(LOD_plot, x0:x1, subset_LOD, 
	# 				seriestype=:scatter, 
	# 				marker=(:circle, 5, 0.5, colours[i_loc[1]+1], Plots.stroke(0, :white)), 
	# 				xlabel="SNP ID", 
	# 				ylabel="LOD", 
	# 				legend=false, 
	# 				size=(1700, 500))
	# 	Plots.annotate!([(x0+((x1-x0)/2), 0, text(contig, 10, colours_lab[i_loc[1]+1], :center))])
	# 	i_loc[1] = i_loc[1]+1
	# 	i_loc[2] = i_loc[2]+length(subset_LOD)
	# end
	# bonferroni_threshold = -log10(0.05 / length(LOD))
	# Plots.plot!(LOD_plot, [1,length(LOD)], [bonferroni_threshold, bonferroni_threshold], line=(:line, :dash, 0.5, 2, :red), label="Bonferroni threshold")
	# Plots.savefig(string("GWAS-OUT-MANHATTAN.png"))
	#########################################################################
	#########################################################################
	#########################################################################
	######################
	#########################################################################
	#########################################################################
	#########################################################################

	# TEST
	filename_X = "res/At_134Salk_SNP.rds"
	n_QTL = 10
	MAF = 0.03
	heritability = 0.50

	# load individual line genotype data
	X = RData.load(filename_X)
	n = size(X)[1]
	l = size(X)[2]
	
	#simiulate y
	QTL_sim = simulate_phenotypes_module.simulate_phenotype(X, n_QTL, MAF, heritability)
	QTL = DataFrames.DataFrame(POS=QTL_sim[1], FREQ=QTL_sim[2], EFFECT=QTL_sim[3])
	sort!(QTL, :POS)
	y = QTL_sim[4]
	y = convert(Array{Float64, 1}, y)


	#####################
	###				  ###
	### Balanced Data ###
	###				  ###
	##################### No missing data points for all n individuals

	############
	### GWAS ###
	############
	LOD_GWAS = GWAS_module.GWAS(X, y, MAF)
	QTL[:LOD_GWAS] = LOD_GWAS[QTL.POS]

	bonferroni_threshold = -log10(0.05 / l)
	performance = sum(QTL.LOD_GWAS .> bonferroni_threshold)*100 / n_QTL

	# plot_GWAS = Plots.plot([1:l, QTL.POS], [GWAS_LOD, QTL.LOD_GWAS], seriestype=:scatter, xlabel="SNP ID", ylabel="LOD", label=["" string("QTL - ", performance, "% identified")])
	plot_GWAS = Plots.plot(1:l, LOD_GWAS, seriestype=:scatter, marker=(:circle, 5, 0.5, :Gray, Plots.stroke(0, :white)), xlabel="SNP ID", ylabel="LOD", label="LOD across all loci", size=(1500, 700))
	Plots.plot!(plot_GWAS, QTL.POS, QTL.LOD_GWAS, seriestype=:scatter, marker=(:circle, 8, 0.5, :red, Plots.stroke(1, :white)), legend=true, label=string("QTL - ", performance, "% identified"))
	Plots.plot!(plot_GWAS, [1,l], [bonferroni_threshold, bonferroni_threshold], line=(:line, :dash, 0.5, 2, :red), label="Bonferroni threshold")

	##################
	### RR-Poolseq ###
	##################
	


	#######################
	###					###
	### Unbalanced Data ###
	###					###
	####################### Missing data points (low to high sparsity)
	### Subset
	### (1) Partial least-squares --> cull-out loci with at least 1 missing data point
	### Imputation algorithms
	### (1) Hot deck: random sampling from same data class
	### (2) Cold deck: random sampling from a different data class
	### (3) Mean substitution: mean of all non-missing data (across of within classes)
	### (4) Regression: predict missing data from a regression model built from the non-missing data

	### output
	return 0
end