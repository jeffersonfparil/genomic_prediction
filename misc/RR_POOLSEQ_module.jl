################################
###							 ###
### RR-BLUP on Pool-Seq Data ###
###							 ###
################################ same module name and filename

module RR_POOLSEQ_module

#####################
###				  ###
### load packages ###
###				  ###
#####################
using DelimitedFiles
using Distributions
using LinearAlgebra
using Optim
using DataFrames
using CSV
using Plots; Plots.pyplot()
using ColorBrewer
JULIA_SCRIPT_HOME = @__DIR__
include(string(JULIA_SCRIPT_HOME, "/filter_sync_by_MAF_module.jl"))

############################
###						 ###
### function definitions ###
###						 ###
############################

### Tikhonov regularisation or linear reularisation using the L2-norm (squared Eucledan/distance norm) or ridge-regression
function RR_BLUE_func(y, X, lambda)
	n = size(X)[1]
	l = size(X)[2]
	if n < l
		b = X' * inv((X * X') + (LinearAlgebra.I * lambda)) * y
	else
		b = inv((X' * X) + (LinearAlgebra.I * lambda)) * (X' * y)
	end
	return(b)
end

### Lambda optimisation function (minimise)
function lambda_optim_func(lambda, y, X)
	b = RR_BLUE_func(y, X, lambda[1])
	dy = sum((y - (X * b)).^2)/length(y)
	return(dy)
end

#####################
###				  ###
### main function ###
###				  ###
##################### #inputs are the genotype sync filename and the phenotyp py filename
function RR_POOLSEQ(ARGS)
	#######################################
	### capture the input filename_sync ###
	#######################################
	if length(ARGS)!=3
		println("Please specify the sync allele counts file, the phenotype file and the minimum allele frequency threshold :-)")
		exit()
	end
	filename_sync = ARGS[1] #sync file of allele counts data per pool
	filename_phen = ARGS[2] #phenotype file with .py extension
	MAF = parse(Float64, ARGS[3])

	#####################################
	### test hardcoded input filenames:
	# filename_sync = "res/GWAlpha_X.sync"; filename_phen = "res/GWAlpha_y.csv"; MAF = 0.001
	# filename_sync="QUANTI_g140_POOLS_GENO.sync"; filename_phen="QUANTI_g140_POOLS_PHENO.csv"; MAF=0.005
	#####################################

	###########################################################################
	### parse the sync file into allele frequencies, filter by MAF, and load ###
	############################################################################
	ls = []
	dir_sync_file = join(split(filename_sync, "/")[1:(end-1)], "/")
	if dir_sync_file == ""
		append!(ls, readdir()[:])
	else
		append!(ls, readdir(dir_sync_file)[:])
	end

	### test if the *_ALLELEFREQ.csv file or the parsed sync file into allele frequency file is not present
	if sum(string(split(split(filename_sync, "/")[end], ".")[1], "_ALLELEFREQ.csv") .== ls) == 0
		JULIA_SCRIPT_HOME = @__DIR__
		# JULIA_SCRIPT_HOME = "/data/Lolium/Softwares/genomic_prediction/src"
		include(string(JULIA_SCRIPT_HOME, "/sync_parsing_module.jl"))
		# sync_parsing_module.sync_parse([filename_sync, MAF]) #output will be string(split(filename_sync, ".")[1], "_ALLELEFREQ.csv")
		sync_parsing_module.sync_parse([filename_sync]) #output will be string(split(filename_sync, ".")[1], "_ALLELEFREQ.csv")
		#FORMAT: CHROM,POS,REF,ALLELE,ALLELE_FREQ_POOL1,ALLELE_FREQ_POOL2,...ALLELE_FREQ_POOLN
	end
	geno = DelimitedFiles.readdlm(string(split(filename_sync, ".")[end-1], "_ALLELEFREQ.csv"), ',')
	# X = convert(Array{Float64}, geno[:, 4:end]') #npools x nSNP_alleles
	# NPOOLS = size(X)[1]
	# NPREDS = size(X)[2]
	X_raw = convert(Array{Float64}, geno[:, 4:end]') #npools x nSNP_alleles
	n = size(X_raw)[1]
	l = size(X_raw)[2]
	LOCI = collect(1:l)[ filter_sync_by_MAF_module.filter_sync_by_MAF([filename_sync, string(MAF)]) ]
	X_filtered = X_raw[:, LOCI]
	X = hcat( ones(n), X_filtered ) #add the intercept
	# NPOOLS = size(X)[1]
	# # NPREDS = size(X)[2]
	# NPREDS = size(X)[2] - 1 # less the intercept

	###################################################
	### load phenotype file (GWAlpha.py-cogenompatible) ###
	###################################################
	phen = DelimitedFiles.readdlm(filename_phen, ',') #we need a new y file format with explicit pool phenotype mean without normality assumption of the distribution
	# y_raw = phen[:,end]
	y = phen[:,end]
	# mu_y = mean(y_raw)
	# sd_y = std(y_raw)
	# y = (y_raw .- mu_y) ./ sd_y
	# y = y_raw * 100 # just making it percent resistance

	####################################################
	### Optimse the lambda for the L2 regularisation ###
	####################################################
	lower_limit=[-1.0e-10]; upper_limit=[1.0e10]
	lambda = Optim.optimize(par->lambda_optim_func(par, y, X), lower_limit, upper_limit, [1.0])

	########################
	### Ridge regression ###
	########################
	b = RR_BLUE_func(y, X, lambda.minimizer[1])

	#############
	## OUTPUT ###
	#############
	CHROM = vcat(["Intercept"], geno[:,1][LOCI])
	POS = vcat(NaN, geno[:,2][LOCI])
	ALLELE_ID = vcat(["N"], geno[:,3][LOCI])
	ALLELE_FREQ = vcat(NaN, maximum(X_raw[:, LOCI], dims=1)[1,:])
	INTERCEPT = b[1]
	EFF = b[2:end]

	function pval_N(x, mu, sd)
		if x < mu
			2*(Distributions.cdf(Distributions.Normal(mu, sd), x))
		else
			2*(1 - Distributions.cdf(Distributions.Normal(mu, sd), x))
		end
	end
	P_VALUES = [pval_N(x, mean(b), std(b)) for x in EFF]
	# P_VALUES = [if Distributions.cdf.(Distributions.Normal(alpha_mean, alpha_sd), alpha) > 0.5; 2*(1-Distributions.cdf.(Distributions.Normal(alpha_mean, alpha_sd), alpha)); else 2*(Distributions.cdf.(Distributions.Normal(alpha_mean, alpha_sd), alpha)); end for alpha in ALPHA_OUT]
	LOD = -Distributions.log.(10, P_VALUES)

	### OUTPUT DATAFRAME
	filename = split(filename_phen, ['/', '.'])
	OUT = DataFrames.DataFrame(CHROM=CHROM, POS=POS, ALLELE=ALLELE_ID, FREQ=ALLELE_FREQ, BETA=vcat([INTERCEPT], EFF), PVALUES=vcat([NaN], P_VALUES), LOD=vcat([NaN], LOD))
	alphas_fname = string(filename[length(filename)-1], "-GWAlpha_RR_Alphas.csv")
	CSV.write(alphas_fname, OUT)
	### Manhattan plot
	LOD_plot = Plots.plot([1, length(LOD)], [0, maximum(LOD)],
		seriestype=:scatter,
		marker=(:circle, 5, 0.5, :White, Plots.stroke(0, :white)),
		xlabel="SNP ID",
		ylabel="LOD",
		legend=false,
		size=(1200, 400));
	contigs = unique(OUT.CHROM)
	colours = ColorBrewer.palette("Pastel1", length(contigs))
	colours_lab = ColorBrewer.palette("Set1", length(contigs))
	i_loc = [0, 0] # counter and locus position
	for contig in contigs
		subset_LOD = OUT[OUT.CHROM .== contig, :LOD]
		x0 = (i_loc[2]+1)
		x1 = (i_loc[2]+length(subset_LOD))
		Plots.plot!(LOD_plot, x0:x1, subset_LOD,
					seriestype=:scatter,
					marker=(:circle, 5, 0.5, colours[i_loc[1]+1], Plots.stroke(0, :white)),
					xlabel="SNP ID",
					ylabel="LOD",
					legend=false,
					size=(1700, 500));
		Plots.annotate!([(x0+((x1-x0)/2), 0, text(contig, 10, colours_lab[i_loc[1]+1], :center))]);
		i_loc[1] = i_loc[1]+1
		i_loc[2] = i_loc[2]+length(subset_LOD)
	end
	bonferroni_threshold = -log10(0.05 / length(LOD))
	Plots.plot!(LOD_plot, [1,length(LOD)], [bonferroni_threshold, bonferroni_threshold], line=(:line, :dash, 0.5, 2, :red), label="Bonferroni threshold");
	plot_fname = string(filename[length(filename)-1], "-GWAlpha_RR_Manhattan.png")
	Plots.savefig(plot_fname)


	println("Everything went well. Please check the output files:")
	println(alphas_fname)
	println(plot_fname)
	println("===============================================================")
	return(0)
end

end #end of RR_POOLSEQ_module

### sample execution within julia
# include("/data/Lolium/Softwares/genomic_prediction/src/RR_POOLSEQ_module.jl")
# INPUT = ["/data/Lolium/Quantitative_Genetics/Genomic_prediction_2018Lolium_50s/UF.sync",
# 		 "/data/Lolium/Quantitative_Genetics/Genomic_prediction_2018Lolium_50s/UF_y.csv",
# 		  0.001]
#
# RR_POOLSEQ(INPUT)
