for VAR_nQTL in 5 10 100                                  ### NUMBER OF QTL: 5 10 100
do
  for VAR_migration in 0.001 0.0001                       ### MIGRATION_RATES: 0.01 0.001 0.0001
  do
    for VAR_foreground_selection in 0.25                  ### FOREGROUND SELECTION SLOPES: 0.00 0.25
    do
      for VAR_background_selection in 0.00 0.25 -0.25     ### BACKGROUND SELECTION SLOPES: 0.00 0.25 -0.25
      do
        for VAR_gradient in 0 1 2                         ### NON-ZERO QTL ALLELE GRADIENTS: 0 1 2
        do
          for VAR_rep in 1 2 3 4 5                        ### REPLICATION NUMBER: 1 2 3 4 5
          do
            echo LOLSIM_${VAR_rep}rep_${VAR_nQTL}qtl_${VAR_migration}mr_${VAR_foreground_selection}fgs_${VAR_background_selection}bgs_${VAR_gradient}grad
            cd LOLSIM_${VAR_rep}rep_${VAR_nQTL}qtl_${VAR_migration}mr_${VAR_foreground_selection}fgs_${VAR_background_selection}bgs_${VAR_gradient}grad
            sed -i 's/FALSE_POSITIVE/FALSE_DISCOVERY/g' CROSS_VALIDATION_OUTPUT_MERGED.csv
            cd ..
            # mkdir LOLSIM_${VAR_rep}rep_${VAR_nQTL}qtl_${VAR_migration}mr_${VAR_foreground_selection}fgs_${VAR_background_selection}bgs_${VAR_gradient}grad
          done
        done
      done
    done
  done
done

parallel ./scp_GPASim06_parallel.sh {} ::: $(ls | grep LOLSIM_)
