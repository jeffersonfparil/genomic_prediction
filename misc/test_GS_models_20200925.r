setwd("/data/Lolium/Quantitative_Genetics")

### OLS
test_OLS = function(X, y, idx_train, idx_valid, PLOT=FALSE){
    # idx_train = sample(c(TRUE, FALSE), size=length(y), replace=TRUE)
    # idx_valid = !idx_train
    y_train = y[idx_train]
    y_valid = y[idx_valid]
    X_train = X[idx_train, ]
    X_valid = X[idx_valid, ]
    beta = t(X_train) %*% solve(X_train%*%t(X_train)) %*% y_train
    y_pred = X_valid %*% beta
    if (PLOT==TRUE){
        plot(x=y_valid, y=y_pred); grid()
        correlation = cor(y_valid,y_pred)
        RMSE = sqrt(mean((y_valid-y_pred)^2))
        legend("topleft", legend=c(paste0("cor=", round(correlation,2)),
                                paste0("RMSE=", round(RMSE,2))
                                )
            )
    }
    return(list(y_valid=y_valid, y_pred=y_pred[,1]))
}

### BLR
test_BLR = function(X, y, idx_train, idx_valid, PLOT=FALSE){
    library(BLR)
    # idx_train = sample(c(TRUE, FALSE), size=length(y), replace=TRUE)
    # idx_valid = !idx_train
    # y_train = y[idx_train]
    y_valid = y[idx_valid]
    # X_train = X[idx_train, ]
    # X_valid = X[idx_valid, ]

    yNA = y
    yNA[idx_valid] = NA

    fm = 
    BLR(y=yNA,
        XF=X,
        XR=NULL,
        XL=NULL,
        GF=NULL,
        prior=list(varE=list(df=3,S=0.25),
                varU=list(df=3,S=0.63),
                lambda=list(shape=0.52,rate=1e-4,type='random',value=30)),
        nIter=1e3,
        burnIn=1e2,
        saveAt="example_BLR_"
        )
    system("rm example_BLR_*") ### clean-up
        # nIter,
        # burnIn,
        # thin,
        # thin2,
        # saveAt,
        # minAbsBeta,
        # weights)
    # MSE.tst<-mean((fm$yHat[whichNa]-y[whichNa])^2)
    # MSE.tstMSE.trn<-mean((fm$yHat[-whichNa]-y[-whichNa])^2)
    # MSE.trnCOR.tst<-cor(fm$yHat[whichNa],y[whichNa])
    # COR.tstCOR.trn<-cor(fm$yHat[-whichNa],y[-whichNa])
    
    ### how do we trasform these prediction back to the original phenotype range????
    # y_pred = fm$yHat[idx_valid] + fm$mu
    y_pred = (fm$yHat[idx_valid] * sd(yNA, na.rm=TRUE)) + mean(yNA, na.rm=TRUE)
    
    if (PLOT==TRUE){
        plot(x=y_valid, y=y_pred); grid()
        correlation = cor(y_valid,y_pred)
        RMSE = sqrt(mean((y_valid-y_pred)^2))
        legend("topleft", legend=c(paste0("cor=", round(correlation,2)),
                                paste0("RMSE=", round(RMSE,2))
                                )
            )
    }
    return(list(y_valid=y_valid, y_pred=y_pred))
}

### RF
test_RF = function(X, y, idx_train, idx_valid, PLOT=FALSE){
    library(randomForest)
    # idx_train = sample(c(TRUE, FALSE), size=length(y), replace=TRUE)
    # idx_valid = !idx_train
    y_train = y[idx_train]
    y_valid = y[idx_valid]
    X_train = X[idx_train, ]
    X_valid = X[idx_valid, ]
    rf = randomForest(x=X_train, y=y_train, ntree=1e3)

    y_pred = predict(rf, X_valid)
    if (PLOT==TRUE){
        plot(x=y_valid, y=y_pred); grid()
        correlation = cor(y_valid,y_pred)
        RMSE = sqrt(mean((y_valid-y_pred)^2))
        legend("topleft", legend=c(paste0("cor=", round(correlation,2)),
                                paste0("RMSE=", round(RMSE,2))
                                )
            )
    }
    return(list(y_valid=y_valid, y_pred=y_pred))
}

### PCR (principal component regression)
test_PCR = function(X, y, idx_train, idx_valid, PLOT=FALSE){
    # idx_train = sample(c(TRUE, FALSE), size=length(y), replace=TRUE)
    # idx_valid = !idx_train
    y_train = y[idx_train]
    y_valid = y[idx_valid]
    X_train = X[idx_train, ]
    X_valid = X[idx_valid, ]
    SVD = svd(X_train)
    beta = SVD$v %*% solve(diag(SVD$d)) %*% t(SVD$u) %*% y_train
    y_pred = X_valid %*% beta
    if (PLOT==TRUE){
        plot(x=y_valid, y=y_pred); grid()
        correlation = cor(y_valid,y_pred)
        RMSE = sqrt(mean((y_valid-y_pred)^2))
        legend("topleft", legend=c(paste0("cor=", round(correlation,2)),
                                paste0("RMSE=", round(RMSE,2))
                                )
            )
    }
    return(list(y_valid=y_valid, y_pred=y_pred[,1]))
}

### distance matrix OLS regression
test_DReg = function(X, y, idx_train, idx_valid, PLOT=FALSE){
    ### build the distrance matrix including all training and validation sets
    DIST = dist(X)
    D = matrix(NA, nrow=nrow(X), ncol=nrow(X))
    D[lower.tri(D)] = as.numeric(DIST)
    D = t(D)
    D[lower.tri(D)] = as.numeric(DIST)
    diag(D) = 0
    ### cross-validate
    # idx_train = sample(c(TRUE, FALSE), size=length(y), replace=TRUE)
    # idx_valid = !idx_train
    y_train = y[idx_train]
    y_valid = y[idx_valid]
    D_train = D[idx_train, ]
    D_valid = D[idx_valid, ]
    beta = t(D_train) %*% solve(D_train%*%t(D_train)) %*% y_train
    y_pred = D_valid %*% beta
    if (PLOT==TRUE){
        plot(x=y_valid, y=y_pred); grid()
        correlation = cor(y_valid,y_pred)
        RMSE = sqrt(mean((y_valid-y_pred)^2))
        legend("topleft", legend=c(paste0("cor=", round(correlation,2)),
                                paste0("RMSE=", round(RMSE,2))
                                )
            )
    }
    return(list(y_valid=y_valid, y_pred=y_pred[,1]))
}

### multivariate model using the colorimetrics (sub-phenotypes) and
### using the predictors to predict the survival rate per population
test_MV2t = function(X, y, idx_train, idx_valid, BAMLIST, RAWP, RAWC, PLOT=FALSE){
    ### remove pools per population data because the input colorimetric data does not include the mean values per pool
    if (herbi != "CLETH"){
        RAWP = droplevels(RAWP[RAWP$SAMPLING=="ACROSS", ])
        idx_across = !grepl("Pool", BAMLIST$V1) & !grepl("03_BAM/IF", BAMLIST$V1) &
                                            !grepl("03_BAM/IG", BAMLIST$V1) &
                                            !grepl("03_BAM/IS", BAMLIST$V1) &
                                            !grepl("03_BAM/IT", BAMLIST$V1) &
                                            !grepl("03_BAM/UF", BAMLIST$V1) &
                                            !grepl("03_BAM/UG", BAMLIST$V1) &
                                            !grepl("03_BAM/US", BAMLIST$V1) &
                                            !grepl("03_BAM/UT", BAMLIST$V1)
        # X = X[idx_across, ]
        # y = y[idx_across]
    } else {
        idx_across = rep(TRUE, length(y))
    }
    ### filter by herbi and extract population data iteratively so it's properly sorted
    for (i in 1:nrow(RAWP)){
        if (exists("COLO")==FALSE){
            COLO = RAWC[as.character(RAWC$POP) == as.character(RAWP$POPULATION[i]), !(colnames(RAWC) %in% c("RESISTANCE", "RR_PRED_RESISTANCE"))] ### exclude the resistance BLUEs, and predicted survival
        } else {
            COLO = rbind(COLO, RAWC[as.character(RAWC$POP) == as.character(RAWP$POPULATION[i]), !(colnames(RAWC) %in% c("RESISTANCE", "RR_PRED_RESISTANCE"))]) ### exclude the resistance BLUEs, and predicted survival
        }
    }
    COLO$SURVIVAL_RATE = RAWP$SURVIVAL_RATE
    ### Note: COLO$RESISTANCE are BLUES from lmer(RESISTANCE ~ 0 + (1|BATCH) + HERBI_X_POP, data=DF_SURVIVAL)
    ### But here we will be using COLO$SURVIVAL_RATE to regress the colorimetrics on.
    C = as.matrix(COLO[, !(colnames(COLO) %in% c("HERBI", "POP", "SURVIVAL_RATE"))])
    rm(COLO)

    ### need to remove som columns in C to avoid singularities
    # ### doing this by hand...
    # corr_threshold = 0.50
    # corr_C = cor(C)
    # IDX = which((abs(corr_C)>=corr_threshold) & upper.tri(corr_C), arr.ind=TRUE)
    # idx_col = c(1:ncol(C))[!(c(1:ncol(C)) %in% IDX[,2])]
    # C_new = C[, idx_col]

    ### or just select changes from day 0 to day 14
    C = C[, grepl("DELTA_D14_D0", colnames(C))]
    C = scale(C, scale=TRUE, center=TRUE)

    # idx_train = sample(c(TRUE, FALSE), size=length(y), replace=TRUE)
    # idx_valid = !idx_train
    y_train = y[(idx_across & idx_train)]
    y_valid = y[(idx_across & idx_valid)]
    X_train = X[(idx_across & idx_train), ]
    X_valid = X[(idx_across & idx_valid), ]
    C_train = C[(idx_train[idx_across]), ]
    C_valid = C[(idx_valid[idx_across]), ]

    ### train
    # mod_X2C = lm(C_train ~ X_train)
    # BETA_X2C = coefficients(mod_X2C)
    BETA_X2C = t(X_train) %*% solve(X_train %*% t(X_train)) %*% C_train
    # mod_C2y = lm(y_train ~ C_train)
    # beta_C2y = coefficients(mod_C2y)
    # beta_C2y = t(C_train) %*% solve(C_train %*% t(C_train)) %*% y_train
    beta_C2y = solve(t(C_train) %*% C_train) %*% t(C_train) %*% y_train

    ### validate
    C_pred = X_valid %*% BETA_X2C

    y_pred = C_pred %*% beta_C2y

    ### plot
    if (PLOT==TRUE){
        par(mfrow=c(1,2))
        plot(C_valid, C_pred, pch=20, col=rgb(0.5,0.5,0.5,0.5)); grid()
        plot(x=y_valid, y=y_pred); grid()
        correlation = cor(y_valid,y_pred)
        RMSE = sqrt(mean((y_valid-y_pred)^2))
        legend("topleft", legend=c(paste0("cor=", round(correlation,2)),
                                paste0("RMSE=", round(RMSE,2))
                                )
            )
    }
    return(list(y_valid=y_valid, y_pred=y_pred[,1]))
}

for (seed in c(123, 456, 111, 333, 420)){
    # seed=123
    set.seed(seed)
    for (herbi in c("CLETH", "GLYPH", "SULFO", "TERBU")){
        # herbi = "CLETH"
        print(herbi)
        fname_geno = paste0("05_SYNC/", herbi, "_MERGED_ALL_MAF0.001_DEPTH50_ALLELEFREQ.csv")
        fname_phen = paste0("01_PHENOTYPES/", herbi, "_MERGED_ALL_pheno.csv")
        fname_bamlist = paste0("04_MPILEUP/", herbi, "_MERGED_ALL_bam.list")
        fname_raw_phen = paste0("01_PHENOTYPES/", herbi, "_MERGED_ALL.pheno")
        fname_raw_colo = "01_PHENOTYPES/COLORIMETRIC/BLUES_AND_COLORIMETRIC_PRED_RESISTANCES.csv"

        GENO = read.csv(fname_geno, header=FALSE)
        PHEN = read.csv(fname_phen, header=FALSE)
        BAMLIST = read.table(fname_bamlist, header=FALSE)
        RAWP = read.csv(fname_raw_phen)
        RAWC = read.csv(fname_raw_colo)
        RAWC = droplevels(RAWC[grepl(herbi, RAWC$HERBI), ])
        X = t(GENO[,4:ncol(GENO)])
        y = PHEN[,2]

        ### sample
        idx_train = sample(c(TRUE, FALSE), size=length(y), replace=TRUE)
        idx_valid = !idx_train

        ### train and predict using different models
        # MODELS = c("OLS", "BLR", "RF", "PCR", "DReg", "MV2t")
        MODELS = c("OLS", "BLR", "RF")
        model = c()
        y_valid = c()
        y_pred = c()
        for (model_name in MODELS){
            # model_name = MODELS[1]
            print(model_name)
            if (model_name!="MV2t"){
                output = eval(parse(text=paste0("test_", model_name, "(X, y, idx_train, idx_valid)")))
            } else {
                output = eval(parse(text=paste0("test_", model_name, "(X, y, idx_train, idx_valid, BAMLIST, RAWP, RAWC)")))
            }
            model = c(model, rep(model_name, length(output$y_valid)))
            y_valid = c(y_valid, output$y_valid)
            y_pred = c(y_pred, output$y_pred)
        }

        ### merge output and plot
        OUTPUT = data.frame(model, y_valid, y_pred)
        COLORS = hcl.colors(n=length(MODELS), palette="viridis")
        if (herbi=="CLETH"){
            herbicide_fullname = "Clethodim"
        } else if(herbi=="GLYPH") {
            herbicide_fullname = "Glyphosate"
        } else if(herbi=="SULFO") {
            herbicide_fullname = "Sulfometuron"
        } else if(herbi=="TERBU") {
            herbicide_fullname = "Terbuthylazine"
        } 

        svg(paste0("seed_", seed, "_test_", herbi, ".svg"), width=10, height=9)
        plot(x=OUTPUT$y_valid, y=OUTPUT$y_pred, type="n",
            xlab=paste0("Observed ", herbicide_fullname, " Resistance"),
            ylab=paste0("Predicted ", herbicide_fullname, " Resistance"))
        grid()
        R2_list = c()
        cor_list = c()
        RMSE_list = c()
        for (i in 1:length(MODELS)){
            # i = 1
            model_name = MODELS[i]
            subout = OUTPUT[OUTPUT$model==model_name, ]
            x=subout$y_valid
            y=subout$y_pred
            mod = lm(y ~ x)
            points(x, y, pch=i, col=COLORS[i])
            abline(mod, lty=2, , col=COLORS[i])
            R2_list = c(R2_list, summary(mod)$adj.r.sq)
            cor_list = c(cor_list, cor(x, y))
            RMSE_list = c(RMSE_list, sqrt(mean((x-y)^2)))
        }
        legend("topleft", legend=paste0(MODELS, " (R2adj=", round(R2_list*100), 
                                                "%; corr=", round(cor_list*100), 
                                                "%; RMSE=", round(RMSE_list, 4), ")"),
               col=COLORS, pch=1:length(MODELS), lty=2, lwd=2)
        dev.off()
    }
}
